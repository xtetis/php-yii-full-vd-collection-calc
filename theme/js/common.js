var prev_sequence_status = 0;
var prev_sequence_status_is_equal_total = 0;
var part_of_calculation = '/sudold/';


function fn__set_progress(total, current) {
    $('.progress')
        .show();
    $('.progress .progress-bar')
        .html(current + '/' + total);
    $('.progress .progress-bar')
        .attr('aria-valuenow', current);
    $('.progress .progress-bar')
        .attr('aria-valuemax', total);
    //aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">25%</div>
    var progress_percents = Math.floor((current / total) * 100);
    $('.progress .progress-bar')
        .attr('style', 'width: ' + progress_percents + '%');
}


function fn__is_json_correct(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

$(function() {



    $('[data-toggle="tooltip"]')
        .tooltip();


    $("#contract")
        .mask("Z999999999999");
    $(".contract")
        .mask("Z999999999999");
    /**
     * Определяем первую часть урла для того, чтобы правильно делать AJAX запросы
     */
    if (window.location.pathname.indexOf("/bank") == 0) {
        part_of_calculation = "/bankold/";
    } else {
        part_of_calculation = "/sudold/";
    }






    //===============================================================================
    var page_id = $('main.container');
    //alert(page_id)
    if (page_id.length) {
        //exit;
        var id_attr = page_id.attr('id');
        if (id_attr == 'page__sud_calc') {
            var _loan_key = $('#loan_key')
                .val();
            var _contract = $('#contract')
                .val();
            var _name = $('#name')
                .val();
            var _nopeni = $('#nopeni')
                .val();
            var _first_payment_schedule = $('#first_payment_schedule')
                .val();
            //alert(_nopeni);


            // Создаем SEQUENCE
            $.post("/ajax/createsequence", {
                    loan_key: _loan_key
                })
                .done(function(obj_sequence) {
                    // Создали SEQUENCE
                    console.log(obj_sequence);
                    //alert(obj_sequence.result_str);
                    //alert(obj_sequence.result);

                    // Показываем лоадер
                    $('#loading_container')
                        .show();



                    // Таймер для проверки прогрессбара
                    //============================================================================
                    var try_count = 0;
                    progress_timer = setInterval(function() {
                        $.post("/ajax/getsequencestatus", {
                                sequence: obj_sequence.result_str
                            })
                            .done(function(obj_getsequencestatus) {
                                try_count = try_count + 1;

                                fn__set_progress(obj_sequence.result, obj_getsequencestatus.result);

                                console.log('prev_sequence_status = ' + prev_sequence_status);
                                console.log('current_sequence_status = ' + obj_getsequencestatus.result);
                                console.log('total = ' + obj_sequence.result);
                                console.log('try_count = ' + try_count);

                                if (
                                    (Number(obj_sequence.result) == Number(obj_getsequencestatus.result)) &&
                                    (prev_sequence_status == Number(obj_sequence.result))
                                ) {
                                    /**
                                     * Делаем еще 3 попытки и 
                                     * выводим сообщение о том, что рассчет вероятнее всего завис 
                                     * и необходимо вручную перейти на страницу списка расчетов
                                     */
                                    prev_sequence_status_is_equal_total = prev_sequence_status_is_equal_total + 1;
                                    console.log('prev_sequence_status_is_equal_total = ' + prev_sequence_status_is_equal_total);
                                    console.log('%5 = ' + (prev_sequence_status_is_equal_total % 5));

                                    if ((prev_sequence_status_is_equal_total % 5) == 4) {
                                        if (confirm("Вероятнее всего расчет окончен, но сервер не отвечает. Перейти на страницу списка расчетов ?")) {
                                            window.location.href = part_of_calculation + "history/" + _contract;
                                            exit;
                                        }
                                    }
                                }

                                prev_sequence_status = Number(obj_getsequencestatus.result);
                            });
                    }, 2000);
                    //============================================================================



                    // Запускаем расчет
                    $.ajax({
                            type: 'POST',
                            url: "/ajax/calc",
                            data: {
                                loan_key: _loan_key,
                                name: _name,
                                nopeni: _nopeni,
                                sequence: obj_sequence.result_str,
                                first_payment_schedule: _first_payment_schedule
                            },
                            timeout: 3600000 // 1 час
                        })
                        .done(function(obj) {
                            console.log(obj);

                            if (obj.result > 0) {
                                // Переходим на страницу списка завершенных рассчетов
                                window.location.href = part_of_calculation + "history/" + _contract + '/' + obj.result;
                                exit;
                            } else {
                                alert('Ошибку: ' + obj.result_str);
                            }
                        })
                        .always(function() {
                            // Удаляем SEQUENCE
                            $.post("/ajax/dropsequence", {
                                sequence: obj_sequence.result_str
                            });

                            // Скрываем лоадер
                            $('#loading_container')
                                .hide();

                            clearTimeout(progress_timer);
                        });


                });


        }
    }

    //===============================================================================









    /**
     * Нажатие кнопки "Расчет"
     */
    //============================================================================
    $("#btn__submit_calc")
        .click(function() {
            $('#modal__calc_confirm')
                .modal('show');
        });
    //============================================================================





    /**
     * Нажатие кнопки "История расчетов"
     */
    //============================================================================
    $("#btn__submit_history")
        .click(function() {
            $('#modal__calc_history')
                .modal('show');
        });
    //============================================================================









    /**
     * Нажатие кнопки "Расчет" в модальном окне
     */
    //============================================================================
    $("#btn__submit_calc_modal")
        .click(function() {
            $('#modal_input_calc_name__error')
                .html('');
            $('#modal_input_contract__error')
                .html('');

            var modal_input_calc_name = $('#modal_input_calc_name')
                .val();
            var modal_input_contract = $('#modal_input_contract')
                .val();

            modal_input_calc_name = modal_input_calc_name.trim();
            if (modal_input_calc_name.length < 3) {
                $('#modal_input_calc_name__error')
                    .html('Обязательно укажите наименоваание расчета');
                return false;
            }

            modal_input_contract = modal_input_contract.trim();
            if (modal_input_contract.length < 3) {
                $('#modal_input_contract__error')
                    .html('Обязательно укажите номер контракта');
                return false;
            }


            var checkbox = $("#modal_input_nopeni:checked");
            modal_input_nopeni = 0;
            if (checkbox.length) {
                modal_input_nopeni = 1;
            }

            var checkbox = $("#modal_input_first_payment_schedule:checked");
            var modal_input_first_payment_schedule = 0;
            if (checkbox.length) {
                modal_input_first_payment_schedule = 1;
            }





            $('#loading_container')
                .show();
            $.post("/ajax/validatecontract", {
                    loan_contract: modal_input_contract
                })
                .done(function(obj) {
                    console.log(obj);
                    if (obj.result > 0) {
                        // Переходим на страницу списка завершенных рассчетов
                        $.redirect(part_of_calculation + "calc", {
                            name: modal_input_calc_name,
                            contract: modal_input_contract,
                            nopeni: modal_input_nopeni,
                            first_payment_schedule: modal_input_first_payment_schedule
                        }, "POST");
                        exit;
                    } else {
                        $('#modal_input_contract__error')
                            .html(obj.result_str);
                        return false;
                    }
                })
                .always(function() {
                    $('#loading_container')
                        .hide();
                });



            return false;

        });
    //============================================================================











    /**
     * Нажатие кнопки "Просмотреть историю" в модальном окне
     */
    //============================================================================
    $("#btn__submit_calc_history")
        .click(function() {
            $('#modal_input_history_contract__error')
                .html('');

            var modal_input_history_contract = $('#modal_input_history_contract')
                .val();

            modal_input_history_contract = modal_input_history_contract.trim();
            if (modal_input_history_contract.length < 3) {
                $('#modal_input_history_contract__error')
                    .html('Обязательно укажите номер контракта');
                return false;
            }


            $.post("/ajax/validatecontract", {
                    loan_contract: modal_input_history_contract
                })
                .done(function(obj) {
                    console.log(obj);
                    if (obj.result > 0) {
                        // Переходим на страницу списка завершенных рассчетов
                        window.location = part_of_calculation + "history/" + modal_input_history_contract;
                        return false;
                    } else {
                        $('#modal_input_history_contract__error')
                            .html(obj.result_str);
                        return false;
                    }
                });



            return false;

        });
    //============================================================================



});