var prev_sequence_status = 0;
var prev_sequence_status_is_equal_total = 0;
var part_of_calculation = '/sud/';



function fn__is_json_correct(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

$(function() {



    $('[data-toggle="tooltip"]')
        .tooltip();


    $("#contract")
        .mask("Z999999999999");
    $(".contract")
        .mask("Z999999999999");









    /**
     * Нажатие кнопки "Расчет"
     */
    //============================================================================
    $("#btn__submit_calc")
        .click(function() {
            $('#modal__calc_confirm')
                .modal('show');
        });
    //============================================================================





    /**
     * Нажатие кнопки "История расчетов"
     */
    //============================================================================
    $("#btn__submit_history")
        .click(function() {
            $('#modal__calc_history')
                .modal('show');
        });
    //============================================================================









    /**
     * Нажатие кнопки "Расчет" в модальном окне
     */
    //============================================================================
    $("#btn__submit_calc_modal_sud")
        .click(function() {
            $('.sudform_error')
                .html('');

            var _name = $('#modal_input_name')
                .val();
            var _contract = $('#modal_input_contract')
                .val();

            var _involve_restruct = ($("#modal_input_involve_restruct:checked")
                .length) ? 1 : 0;

            var _bankcard = Number($("#modal_input_bankcard")
                .val());





            $.post("/ajax/calcsud", {
                    name: _name,
                    contract: _contract,
                    involve_restruct: _involve_restruct,
                    bankcard: _bankcard
                })
                .done(function(obj) {
                    console.log(obj);
                    if (obj.result) {
                        window.location.href = obj.return_url;
                        return false;
                    } else {
                        console.log(obj.errors);
                        $.each(obj.errors, function(k, v) {
                            $('#error__' + k)
                                .html(v[0]);
                        });
                    }
                }, 'json');



            return false;

        });
    //============================================================================











    /**
     * Нажатие кнопки "Просмотреть историю" в модальном окне
     */
    //============================================================================
    $("#btn__submit_calc_history")
        .click(function() {
            $('.sudform_error')
                .html('');

            var _contract = $('#modal_input_history_contract')
                .val();


            $.post("/ajax/validatecontract", {
                    loan_contract: _contract
                })
                .done(function(obj) {
                    console.log(obj);
                    if (obj.result > 0) {
                        // Переходим на страницу списка завершенных рассчетов
                        window.location = "/bankcard/history/" + _contract;
                    } else {
                        $('#modal_input_history_contract__error')
                            .html(obj.result_str);
                    }
                });



            return false;

        });
    //============================================================================





    /**
     * Нажатие кнопки "Отчет" в просмотре PDF
     */
    //============================================================================
    $(".btn_pdf_print")
        .click(function() {
            var _idx = $(this)
                .attr('idx');
            var _pdfgen_url = $('#PDFGEN_URL')
                .val();

            var _return_as_html = 0;
            if ($(this).hasClass( "btn_pdf_print_as_html" ))
            {
                _return_as_html = 1;
            }

            $.post("/ajax/get_card_pdf_json", {
                    calc_version_key: _idx,
                    return_as_html: _return_as_html
                })
                .done(function(obj) {
                    console.log(obj);
                    if (obj.result > 0) {
                        //$('#json_to_print').val(obj.result_str);

                        $.redirect("/ajax/pgfgencard/" + _idx+'?return_as_html='+_return_as_html, {},
                            "POST",
                            '_blank');
                        /*
                        $.redirect(_pdfgen_url + "json/", {
                                json: obj.result_str
                            },
                            "POST",
                            '_blank');
                        */
                    } else {
                        fn__show_message(obj.result_str);
                    }
                }).fail(function(xhr, textStatus, errorThrown) {
                    alert('error');
                    alert(errorThrown.toString());
                });;



            return false;

        });
    //============================================================================




    /**
     * Нажатие кнопки "Пояснительная записка" в просмотре PDF
     */
    //============================================================================
    $(".btn_pdf_print_zap")
        .click(function() {
            var _idx = $(this)
                .attr('idx');
            var _pdfgen_url = $('#PDFGEN_URL')
                .val();

            $.post("/ajax/get_pdf_json_zap", {
                    calc_version_key: _idx
                })
                .done(function(obj) {
                    console.log(obj);
                    if (obj.result > 0) {
                        $.redirect(_pdfgen_url + "json/", {
                                json: obj.result_str
                            },
                            "POST",
                            '_blank');
                    } else {
                        showMessage(obj.result_str);
                    }
                });



            return false;

        });
    //============================================================================




    // Показываем и скрываем прогресс бар
    $(document)
        .ajaxStop(function() {
            console.log('ajaxStop');
            $('#progress_container')
                .hide();

        })
        .ajaxSend(function(event, jqxhr, settings) {
            console.log('ajaxSend');
            var arrayNoSpinner = [
                '/ajax/noauth/verify_actual_session',
                '/ajax/add_event',
                '/ajax/noauth/errorlog'
            ];
            if (arrayNoSpinner.indexOf(settings.url) == -1) {
                $('#progress_container')
                    .show();
            }
        })
        .ajaxComplete(function() {
            console.log('ajaxComplete');
            $('#progress_container')
                .hide();
        })
        .ajaxError(function() {
            console.log('ajaxError');
            $('#progress_container')
                .hide();
            fn__show_message('Некорректный ответ');
        });


});