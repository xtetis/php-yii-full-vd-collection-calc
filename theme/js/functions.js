function fn__show_message(message)
{
    $('#container_top_show_message')
        .html(message);
    $("#container_top_show_message")
        .fadeIn('slow');
    setTimeout(function ()
    {
        $("#container_top_show_message")
            .fadeOut('slow');
    }, 4000);
}
