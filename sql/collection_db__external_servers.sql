-- SQL Manager for PostgreSQL 5.9.3.51215
-- ---------------------------------------
-- Хост         : asuztest-local
-- База данных  : collection_db
-- Версия       : PostgreSQL 9.3.14 on x86_64-unknown-linux-gnu, compiled by gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-4), 64-bit



DROP USER MAPPING IF EXISTS FOR palexander SERVER asuz01;
DROP USER MAPPING IF EXISTS FOR collection_db_owner SERVER asuz01;
DROP SERVER IF EXISTS asuz01;
--
-- Definition for foreign server asuz01 (OID = 471078) : 
--
CREATE SERVER asuz01
  FOREIGN DATA WRAPPER postgres_fdw
  OPTIONS (
    hostaddr '10.255.254.252',
    dbname 'asuz');
--
-- Definition for user mapping collection_db_owner (OID = 471079) : 
--
CREATE USER MAPPING FOR collection_db_owner
  SERVER asuz01
  OPTIONS (
    "user" 'asuz_fdw',
    password 'z5UzTD83yt');
--
-- Definition for user mapping palexander (OID = 471314) : 
--
CREATE USER MAPPING FOR palexander
  SERVER asuz01
  OPTIONS (
    "user" 'asuz_fdw',
    password 'z5UzTD83yt');
