-- SQL Manager for PostgreSQL 5.9.3.51215
-- ---------------------------------------
-- Хост         : asuztest-local
-- База данных  : collection_db
-- Версия       : PostgreSQL 9.3.14 on x86_64-unknown-linux-gnu, compiled by gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-4), 64-bit



CREATE SCHEMA dwh AUTHORIZATION bi_readwrite;
CREATE SCHEMA asuz_db_local AUTHORIZATION collection_db_owner;
CREATE SCHEMA buh AUTHORIZATION collection_db_owner;
CREATE SCHEMA facts AUTHORIZATION collection_db_owner;
CREATE SCHEMA loan_calc_history AUTHORIZATION collection_db_owner;
CREATE SCHEMA logging AUTHORIZATION collection_db_owner;
CREATE SCHEMA repair_data AUTHORIZATION collection_db_owner;
CREATE SCHEMA asuz_db AUTHORIZATION collection_db_owner;
--
-- Definition for language plpgsql (OID = 12674) : 
--
CREATE TRUSTED PROCEDURAL LANGUAGE plpgsql
   HANDLER "plpgsql_call_handler"
   VALIDATOR "pg_catalog"."plpgsql_validator";
--
-- Definition for function user_key (OID = 466189) : 
--
SET search_path = public, pg_catalog;
SET check_function_bodies = false;
CREATE FUNCTION public.user_key (
)
RETURNS integer
AS 
$body$
DECLARE
  _result integer;
BEGIN
  /*SELECT u.user_key
  FROM t_users u
  WHERE u.user_name = SESSION_USER
  INTO _result;*/
  _result=1;
  RETURN _result;
END;
$body$
LANGUAGE plpgsql
IMMUTABLE SECURITY DEFINER;
--
-- Definition for function get_yesterday_date (OID = 466310) : 
--
CREATE FUNCTION public.get_yesterday_date (
)
RETURNS date
AS 
$body$
DECLARE
	v__yesterday DATE;
BEGIN
	v__yesterday = NOW() - interval '1 day';
  	RETURN v__yesterday;
END;
$body$
LANGUAGE plpgsql
STABLE;
--
-- Definition for function loans_tr_after_f (OID = 466311) : 
--
CREATE FUNCTION public.loans_tr_after_f (
)
RETURNS trigger
AS 
$body$
DECLARE

BEGIN

  RETURN NULL;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function loans_tr_bef_f (OID = 466312) : 
--
CREATE FUNCTION public.loans_tr_bef_f (
)
RETURNS trigger
AS 
$body$
DECLARE
  _application_key integer;
  _people_key integer;
BEGIN
  IF (TG_OP = 'INSERT') THEN
    new.date_work = CURRENT_TIMESTAMP;
  END IF;

  if (TG_OP = 'INSERT') or (TG_OP = 'UPDATE') then
    if COALESCE(NEW.test_revenue_pension_certificate,false) OR
       COALESCE(NEW.test_fixed_income_pension_certificate,false)
    then
      NEW.test_revenue_pension_certificate = true;
      NEW.test_fixed_income_pension_certificate = true;
    end if;

  end if;

  if (TG_OP = 'INSERT') or (TG_OP = 'UPDATE') then
    --конвертация в новый формат хранения
    IF (TG_OP = 'INSERT') THEN
      INSERT INTO asuz.application(application_key, requested_amount, requested_term, loan_key)
      VALUES(DEFAULT, null, null, NEW.loan_key)
      RETURNING application_key
      INTO _application_key;
      INSERT INTO asuz.people2application(p2a_key, application_key, people_key, type_depending_key,  borrower_key, loan_key)
      VALUES(DEFAULT, _application_key, (asuz.people_value(NEW.borrower_key,1)).people_key, 1, NEW.borrower_key, NEW.loan_key);
      _people_key = (asuz.people_value(NEW.borrower_key,2)).people_key;
      IF _people_key IS NOT NULL THEN
        INSERT INTO asuz.people2application(p2a_key, application_key, people_key, type_depending_key,  borrower_key, loan_key)
        VALUES(DEFAULT, _application_key, _people_key, 2, NEW.borrower_key, NEW.loan_key);
      END IF;
      _people_key = (asuz.people_value(NEW.borrower_key,3)).people_key;
      IF _people_key IS NOT NULL THEN
        INSERT INTO asuz.people2application(p2a_key, application_key, people_key, type_depending_key,  borrower_key, loan_key)
        VALUES(DEFAULT, _application_key, _people_key, 3, NEW.borrower_key, NEW.loan_key);
      END IF;
      _people_key = (asuz.people_value(NEW.borrower_key,4)).people_key;
      IF _people_key IS NOT NULL THEN
        INSERT INTO asuz.people2application(p2a_key, application_key, people_key, type_depending_key,  borrower_key, loan_key)
        VALUES(DEFAULT, _application_key, _people_key, 4, NEW.borrower_key, NEW.loan_key);
      END IF;

    ELSE

    END IF;
    PERFORM l.* FROM loans l
    WHERE l.loan_key = NEW.loan_key AND COALESCE(l.contract,'')='';
    IF COALESCE(NEW.agreed,false) = true AND FOUND  then
      --RAISE EXCEPTION '%,%',NEW.agreed,NEW.contract;
      SELECT 'Z'||b.pass_series||b.pass_number||lpad((select (COUNT(*)+1)::varchar from loans l
                                                 where l.borrower_key = b.borrower_key
                                                  and COALESCE(l.principial_contract,0)=0 AND l.issued = true)
                                                  ,2,'0')
      FROM t_borrowers b
      WHERE b.borrower_key = NEW.borrower_key
      INTO    NEW.contract;
      --get_next_series('dog',null)::varchar;
      --PERFORM * FROM fix_number_in_series('dog'::varchar,NEW.contract);

    END IF;

    IF COALESCE(NEW.agreed,false) = false  then
      --заполнение "осмотр бизнеса"
      SELECT ib.state_business FROM borrowers_state_business ib
      JOIN t_borrowers b ON ib.borrower_key=b.borrower_key
      WHERE ib.borrower_key=NEW.borrower_key
        AND ib.input_date >=CURRENT_DATE - interval '7 day' AND b.ie_business_state=true
      ORDER BY  ib.input_date DESC
      LIMIT 1
      INTO NEW.test_state_business;
      NEW.test_state_business = COALESCE(NEW.test_state_business,0);
    end if;

    return new;
  elseif (TG_OP = 'DELETE') then
    IF old.issued = true THEN
      RAISE EXCEPTION 'Нельзя удалять записи со статусом "Выдан"';
    END IF;
    return old;
  end if;
  return null;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function repayment_schedules_tr_after_f (OID = 466313) : 
--
CREATE FUNCTION public.repayment_schedules_tr_after_f (
)
RETURNS trigger
AS 
$body$
DECLARE

BEGIN

  RETURN NULL;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_separate_subdivision (OID = 466314) : 
--
CREATE FUNCTION public.get_separate_subdivision (
  _subdivision_key integer,
  _date date,
  out saparate_subdivision_key integer,
  out separate_subdivision character varying,
  out subdivision_key integer,
  out date_b date
)
RETURNS record
AS 
$body$
DECLARE
	v__saparate_subdivision_key INTEGER;
BEGIN

    SELECT t.saparate_subdivision_key
    FROM  public.separate_subdivision t
    WHERE
    	t.subdivision_key = _subdivision_key
 		AND
        COALESCE(t.date_b,'2012.01.01') <= _date
    ORDER BY t.date_b DESC NULLS LAST
    LIMIT 1
    INTO v__saparate_subdivision_key;

	SELECT * FROM public.separate_subdivision t
    WHERE t.saparate_subdivision_key = v__saparate_subdivision_key
    INTO saparate_subdivision_key, separate_subdivision, subdivision_key, date_b;
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function __calc_all_bv_facts_loans_period (OID = 466343) : 
--
SET search_path = buh, pg_catalog;
CREATE FUNCTION buh.__calc_all_bv_facts_loans_period (
  _date_start date,
  _date_end date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
DECLARE
  	v__loans_rec RECORD;
    v__rec_dates RECORD;
    v__rec__stat_tables RECORD;
BEGIN
	v__rec__stat_tables = buh.set_stat_tables_count_b();
    IF v__rec__stat_tables.result = -1 THEN
        result = -1;
        result_str=v__rec__stat_tables.result_str;
    	RETURN;
    END IF;
    
	-- Перебираем циклом все даты
	-- -------------------------------------------------------------------------
	FOR v__rec_dates IN
		select generate_series(_date_start, _date_end, interval '1' day) 
    LOOP
	    SELECT * FROM buh._calc_all_bv_facts_loans_day(v__rec_dates.generate_series::DATE)
        INTO result, result_str;
    END LOOP;

    result = 1;
    result_str='ok';
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function _calc_all_bv_facts_loans_day (OID = 466344) : 
--
CREATE FUNCTION buh._calc_all_bv_facts_loans_day (
  _date date = public.get_yesterday_date(),
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
DECLARE
  	v__loans_rec RECORD;
    v__rec__stat_tables RECORD;
    _yesterday date;
BEGIN

	v__rec__stat_tables = buh.set_stat_tables_count_b();
    IF v__rec__stat_tables.result = -1 THEN
        result = -1;
        result_str=v__rec__stat_tables.result_str;
    	RETURN;
    END IF;

	_yesterday=_date-'1 day'::interval;
    result = -1;
	result_str = 'Start';
  	FOR v__loans_rec IN

        SELECT DISTINCT *
        FROM
        (
            SELECT DISTINCT 
                t.loan_key
            FROM 
                buh.balance_value t
            WHERE 
                t.transact_date=_yesterday
                AND 
                t.opened
                
            UNION

            SELECT DISTINCT 
            	f.loan_key
            FROM
                facts.facts f
            WHERE 
                f.fact_date = _date
                AND
                f.fact_name_key = 1 -- Выдача займа
            ) as tf
        --limit 100
    LOOP
    	SELECT * FROM buh.calc_loan_day(v__loans_rec.loan_key,_date)
        INTO result, result_str;
        
    END LOOP;
    
--    result = 1;
--    result_str = 'OK';
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function _report_1_OO (OID = 466345) : 
--
CREATE FUNCTION buh."_report_1_OO" (
  _report_month date,
  _rep_mode integer = 1,
  out separate_subdivision_name character varying,
  out separate_subdivision character varying,
  out traffic character varying,
  out total_balance_b numeric,
  out loan_balance_b numeric,
  out loan_balance_b_overdue numeric,
  out loan_balance_b_due numeric,
  out interest_balance_b numeric,
  out fine_balance_b numeric,
  out revenue_0_first numeric,
  out revenue_0_second numeric,
  out revenue_1_first numeric,
  out revenue_1_second numeric,
  out revenue_for_delay_0_first numeric,
  out revenue_for_delay_0_second numeric,
  out discount_0_first numeric,
  out discount_0_second numeric,
  out discount_1_first numeric,
  out discount_1_second numeric,
  out discount_0_first_for_delay numeric,
  out discount_0_second_for_delay numeric,
  out fine_calc numeric,
  out debt_relief_death_loan numeric,
  out debt_relief_death_interest numeric,
  out debt_relief_death_interest_for_delay numeric,
  out debt_relief_death_fine numeric,
  out debt_relief_cession_loan numeric,
  out debt_relief_cession_interest numeric,
  out debt_relief_cession_interest_for_delay numeric,
  out debt_relief_cession_fine numeric,
  out debt_relief_collector_loan numeric,
  out debt_relief_collector_interest numeric,
  out debt_relief_collector_interest_for_delay numeric,
  out debt_relief_collector_fine numeric,
  out debt_relief_exclusive_cases_loan numeric,
  out debt_relief_exclusive_cases_interest numeric,
  out debt_relief_exclusive_cases_interest_for_delay numeric,
  out debt_relief_exclusive_cases_fine numeric,
  out insurance_calc_revenue numeric,
  out payment_loan_current numeric,
  out payment_loan_avance numeric,
  out payment_interest_current numeric,
  out payment_interest_avance numeric,
  out payment_interest_for_delay_current numeric,
  out payment_fine numeric,
  out payment_advance_payment numeric,
  out payment_paid_more numeric,
  out payment_unfound numeric,
  out total_balance_e numeric,
  out loan_balance_e numeric,
  out loan_balance_e_overdue numeric,
  out loan_balance_e_due numeric,
  out interest_balance_e numeric,
  out fine_balance_e numeric
)
RETURNS SETOF record
AS 
$body$
DECLARE
  _rep1 RECORD;
  _report_month_end DATE;
  
  --interest_for_delay_balance_b NUMERIC;
  --interest_for_delay_balance_e NUMERIC;
  
  --payment_loan numeric;
  --payment_interest numeric;
BEGIN
  _report_month_end = _report_month + INTERVAL '1 month' - INTERVAL '1 day';
  FOR _rep1 IN
  --SELECT COALESCE(CASE WHEN _rep_mode = 2  THEN loan_cessionary(r.loan_key,_report_month_end) ELSE NULL END, separate_subdivision_name((setting_value(s.subdivision_key,'separate_subdivision', _report_month_end)).value),s.subdivision_name) AS _separate_subdivision_name,
  --  COALESCE(CASE WHEN _rep_mode = 2  THEN loan_cessionary(r.loan_key,_report_month_end) ELSE NULL END,(setting_value(s.subdivision_key,'separate_subdivision',_report_month_end)).value,s.subdivision_name) AS _separate_subdivision,
  SELECT COALESCE(/*separate_subdivision_name((setting_value(s.subdivision_key,'separate_subdivision', _report_month_end)).value),s.subdivision_name*/ 'tmp') AS _separate_subdivision_name,
    COALESCE((public.get_separate_subdivision(r.subdivision_key, _report_month_end)).separate_subdivision,/*s.subdivision_name*/'tmp') AS _separate_subdivision,  
    
   
    r.traffic,
    SUM(r.revenue_0_first) AS _revenue_0_first,  SUM(r.revenue_0_second) AS _revenue_0_second,
    SUM(r.revenue_1_first) AS _revenue_1_first,  SUM(r.revenue_1_second) AS _revenue_1_second,
    SUM(r.discount_0_first) AS _discount_0_first,  SUM(r.discount_0_second) AS _discount_0_second,
    SUM(r.discount_1_first) AS _discount_1_first,  SUM(r.discount_1_second) AS _discount_1_second,
    SUM(r.fine_calc) AS _fine_calc,
    SUM(r.debt_relief_death_loan) AS _debt_relief_death_loan, SUM(r.debt_relief_death_interest) AS _debt_relief_death_interest,
    SUM(r.debt_relief_death_fine) AS _debt_relief_death_fine,
    SUM(r.debt_relief_cession_loan) AS _debt_relief_cession_loan, SUM(r.debt_relief_cession_interest) AS _debt_relief_cession_interest,
    SUM(r.debt_relief_cession_fine) AS _debt_relief_cession_fine,
    SUM(r.debt_relief_collector_loan) AS _debt_relief_collector_loan, SUM(r.debt_relief_collector_interest) AS _debt_relief_collector_interest,
    SUM(r.debt_relief_collector_fine) AS _debt_relief_collector_fine,
    SUM(r.debt_relief_exclusive_cases_loan) AS _debt_relief_exclusive_cases_loan, SUM(r.debt_relief_exclusive_cases_interest) AS _debt_relief_exclusive_cases_interest,
    SUM(r.debt_relief_exclusive_cases_fine) AS _debt_relief_exclusive_cases_fine,


    --31.05.2017
    SUM(r.debt_relief_death_interest_for_delay) AS _debt_relief_death_interest_for_delay,
    SUM(r.debt_relief_cession_interest_for_delay) AS _debt_relief_cession_interest_for_delay,
    SUM(r.debt_relief_collector_interest_for_delay) AS _debt_relief_collector_interest_for_delay,
    --SUM(r.debt_relief_discount_interest_for_delay) AS _debt_relief_death_interest_for_delay,
    --06.06.2017
    SUM(r.debt_relief_exclusive_cases_interest_for_delay) AS _debt_relief_exclusive_cases_interest_for_delay,


    SUM(r.insurance_calc_revenue) AS  _insurance_calc_revenue,
    --SUM(r.payment_loan) AS _payment_loan,
    --SUM(r.payment_interest) AS _payment_interest,
    SUM(r.payment_fine) AS _payment_fine,

    SUM(r.interest_balance_b) AS _interest_balance_b,
    SUM(r.interest_balance_e) AS _interest_balance_e,
    SUM(r.loan_balance_b) AS _loan_balance_b,
    SUM(r.loan_balance_e) AS _loan_balance_e,
    SUM(r.fine_balance_b) AS _fine_balance_b,
    SUM(r.fine_balance_e) AS _fine_balance_e,
    SUM(r.total_balance_b) AS _total_balance_b,
    SUM(r.total_balance_e) AS _total_balance_e,
      --17.05.2017
    SUM(r.loan_balance_b_overdue) AS _loan_balance_b_overdue,
    SUM(r.loan_balance_b_due)     AS _loan_balance_b_due,
    SUM(r.loan_balance_e_overdue) AS _loan_balance_e_overdue,
    SUM(r.loan_balance_e_due)     AS _loan_balance_e_due,
    SUM(r.payment_loan_current)   AS _payment_loan_current,
    SUM(r.payment_loan_avance)    AS _payment_loan_avance,

    --25.05.2017
    --SUM(r.interest_for_delay_balance_b)       AS _interest_for_delay_balance_b,
    --SUM(r.interest_for_delay_balance_e)       AS _interest_for_delay_balance_e,
    SUM(r.revenue_for_delay_0_first)          AS _revenue_for_delay_0_first,
    SUM(r.revenue_for_delay_0_second)         AS _revenue_for_delay_0_second,
    SUM(r.payment_interest_current)           AS _payment_interest_current,
    SUM(r.payment_interest_avance)            AS _payment_interest_avance,
    SUM(r.payment_interest_for_delay_current) AS _payment_interest_for_delay_current,

    --06.06.2017
    SUM(r.payment_advance_payment) AS _payment_advance_payment,
    SUM(r.payment_paid_more) AS _payment_paid_more,
    SUM(r.payment_unfound) AS _payment_unfound,

    SUM(r.discount_0_first_for_delay)  AS _discount_0_first_for_delay,
    SUM(r.discount_0_second_for_delay) AS _discount_0_second_for_delay



  FROM buh.fn__get_buh_report (_report_month_end) AS r
  GROUP BY 1,2,3
  LOOP
    separate_subdivision_name = _rep1._separate_subdivision_name;
    separate_subdivision = _rep1._separate_subdivision;
    traffic = _rep1.traffic;
    revenue_0_first = _rep1._revenue_0_first;
    revenue_0_second = _rep1._revenue_0_second;
    revenue_1_first = _rep1._revenue_1_first;
    revenue_1_second = _rep1._revenue_1_second;
    discount_0_first = _rep1._discount_0_first;
    discount_0_second = _rep1._discount_0_second;
    discount_1_first = _rep1._discount_1_first;
    discount_1_second = _rep1._discount_1_second;
    discount_0_first_for_delay = _rep1._discount_0_first_for_delay;
    discount_0_second_for_delay = _rep1._discount_0_second_for_delay;
    fine_calc = _rep1._fine_calc;
    debt_relief_death_loan = _rep1._debt_relief_death_loan;
    debt_relief_death_interest = _rep1._debt_relief_death_interest;
    debt_relief_death_fine = _rep1._debt_relief_death_fine;
    debt_relief_cession_loan = _rep1._debt_relief_cession_loan;
    debt_relief_cession_interest = _rep1._debt_relief_cession_interest;
    debt_relief_cession_fine = _rep1._debt_relief_cession_fine;
    debt_relief_collector_loan = _rep1._debt_relief_collector_loan;
    debt_relief_collector_interest = _rep1._debt_relief_collector_interest;
    debt_relief_collector_fine = _rep1._debt_relief_collector_fine;
    debt_relief_exclusive_cases_loan = _rep1._debt_relief_exclusive_cases_loan;
    debt_relief_exclusive_cases_interest = _rep1._debt_relief_exclusive_cases_interest;
    debt_relief_exclusive_cases_fine = _rep1._debt_relief_exclusive_cases_fine;

    --31.05.2017
	debt_relief_death_interest_for_delay = _rep1._debt_relief_death_interest_for_delay;
	debt_relief_cession_interest_for_delay = _rep1._debt_relief_cession_interest_for_delay;
	debt_relief_collector_interest_for_delay = _rep1._debt_relief_collector_interest_for_delay;
    --debt_relief_discount_interest_for_delay = _rep1._debt_relief_discount_interest_for_delay;
    --06.06.2017
    debt_relief_exclusive_cases_interest_for_delay = _rep1._debt_relief_exclusive_cases_interest_for_delay;


    insurance_calc_revenue = _rep1._insurance_calc_revenue;
    --payment_loan = _rep1._payment_loan;
    --payment_interest  = _rep1._payment_interest;
    payment_fine = _rep1._payment_fine;

    interest_balance_b = _rep1._interest_balance_b;
    interest_balance_e = _rep1._interest_balance_e;
    loan_balance_b = _rep1._loan_balance_b;
    loan_balance_e = _rep1._loan_balance_e;
    fine_balance_b = _rep1._fine_balance_b;
    fine_balance_e = _rep1._fine_balance_e;
    total_balance_b = _rep1._total_balance_b;
    total_balance_e = _rep1._total_balance_e;
    --17.05.2017
    loan_balance_b_overdue = _rep1._loan_balance_b_overdue;
    loan_balance_b_due = _rep1._loan_balance_b_due;
    loan_balance_e_overdue = _rep1._loan_balance_e_overdue;
    loan_balance_e_due = _rep1._loan_balance_e_due;
    payment_loan_current = _rep1._payment_loan_current;
    payment_loan_avance = _rep1._payment_loan_avance;
    --25.05.2017
    --interest_for_delay_balance_b       = _rep1._interest_for_delay_balance_b;
    --interest_for_delay_balance_e       = _rep1._interest_for_delay_balance_e;
    revenue_for_delay_0_first          = _rep1._revenue_for_delay_0_first;
    revenue_for_delay_0_second         = _rep1._revenue_for_delay_0_second;
    payment_interest_current           = _rep1._payment_interest_current;
    payment_interest_avance            = _rep1._payment_interest_avance;
    payment_interest_for_delay_current = _rep1._payment_interest_for_delay_current;

    --06.06.2017
    payment_advance_payment = _rep1._payment_advance_payment;
    payment_paid_more = _rep1._payment_paid_more;
    payment_unfound = _rep1._payment_unfound;
--raise notice '%',_rep1;

    RETURN NEXT;
  END LOOP;

END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function _tmp__balance_value__calc (OID = 466347) : 
--
CREATE FUNCTION buh._tmp__balance_value__calc (
)
RETURNS integer
AS 
$body$
DECLARE
	v__rec RECORD;
    v__summ1 NUMERIC;
    v__summ2 NUMERIC;
    v__summ3 NUMERIC;
BEGIN


    TRUNCATE TABLE buh._tmp__balance_value;

    INSERT INTO 
      buh._tmp__balance_value
    (
      balance_value_key,
      loan_key,
      od_loan_b,
      od_loan_e,
      od_overpayment_planned_b,
      od_overpayment_planned_e,
      od_overpayment_for_delay_b,
      od_overpayment_for_delay_e,
      od_fine_b,
      od_fine_e,
      transact_date,
      create_date,
      n_loan_b,
      n_loan_e,
      n_overpayment_planned_b,
      n_overpayment_planned_e,
      loan_b,
      loan_e,
      client_balance_b,
      client_balance_e,
      active,
      opened,
      n_overpayment_conditionally_b,
      n_overpayment_conditionally_e,
      f1,
      f2,
      f3
    ) 



    SELECT 
        t.* ,
        0,
        0,
        0
    FROM (

        SELECT 
        b.*
        FROM
            buh.balance_value b
        WHERE 
            b.loan_key IN
                (
                    SELECT 
                        l.loan_key
                    FROM 
                        public.loans l
                    JOIN 
                        public.repayment_schedules r ON
                        r.loan_key = l.loan_key
                    WHERE 
                        r.payment_date = '18.09.2017'
                )
            AND
            b.transact_date = '18.09.2017'

    ) as t ;
    
    
    
    FOR v__rec IN
    	SELECT * FROM buh._tmp__balance_value
    LOOP
    

        SELECT COALESCE(SUM(ltr.summ),0) FROM buh.loan_transact ltr
        WHERE ltr.loan_key = v__rec.loan_key AND ltr.transact_date =  v__rec.transact_date
        AND ltr.account_transaction_key = 10
        INTO v__summ1;

        SELECT COALESCE(SUM(ltr.summ),0) FROM buh.loan_transact ltr
        WHERE ltr.loan_key = v__rec.loan_key AND ltr.transact_date =  v__rec.transact_date
        AND ltr.account_transaction_key = 11
        INTO v__summ2;

        SELECT COALESCE(SUM(ltr.summ),0) FROM buh.loan_transact ltr
        WHERE ltr.loan_key = v__rec.loan_key AND ltr.transact_date =  v__rec.transact_date
        AND ltr.account_transaction_key = 12
        INTO v__summ3;
        
        
        UPDATE 
          buh."_tmp__balance_value"  
        SET 
          f1 = v__summ1,
          f2 = v__summ2,
          f3 = v__summ3
         
        WHERE 
          balance_value_key = v__rec.balance_value_key
        ;

    END LOOP;

	RETURN 1;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function add_balance_value_end (OID = 466348) : 
--
CREATE FUNCTION buh.add_balance_value_end (
  _loan_key integer,
  _od_loan_e numeric,
  _od_overpayment_planned_e numeric,
  _od_overpayment_for_delay_e numeric,
  _od_fine_e numeric,
  _n_loan_e numeric,
  _n_overpayment_planned_e numeric,
  _loan_e numeric,
  _client_balance_e numeric,
  _transact_date date,
  _active integer = 1
)
RETURNS integer
AS 
$body$
DECLARE
	v__balance_value_key INTEGER;
BEGIN
INSERT INTO 
  buh.balance_value
(
  loan_key,
  od_loan_b,
  od_loan_e,
  od_overpayment_planned_b,
  od_overpayment_planned_e,
  od_overpayment_for_delay_b,
  od_overpayment_for_delay_e,
  od_fine_b,
  od_fine_e,
  transact_date,
  n_loan_b,
  n_loan_e,
  n_overpayment_planned_b,
  n_overpayment_planned_e,
  loan_b,
  loan_e,
  client_balance_b,
  client_balance_e,
  active
) 
VALUES (
  _loan_key,
  0,
  _od_loan_e,
  0,
  _od_overpayment_planned_e,
  0,
  _od_overpayment_for_delay_e,
  0,
  _od_fine_e,
  _transact_date,
  0,
  _n_loan_e,
  0,
  _n_overpayment_planned_e,
  0,
  _loan_e,
  0,
  _client_balance_e,
  _active
)
RETURNING
	balance_value_key
INTO
	v__balance_value_key;
    
    RETURN v__balance_value_key;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function add_transact_calc_log (OID = 466349) : 
--
CREATE FUNCTION buh.add_transact_calc_log (
  _loan_key integer,
  _date date,
  _procedure character varying,
  _message character varying
)
RETURNS integer
AS 
$body$
/*
<history_list>
<item><date_m>10.10.2017</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__transact_calc_log_key INTEGER;
BEGIN
    INSERT INTO 
      buh.transact_calc_log
    (
      loan_key,
      "date",
      procedure,
      message
    ) 
    VALUES (
      _loan_key,
      _date,
      _procedure,
      _message
    )
    RETURNING
        transact_calc_log_key
    INTO
        v__transact_calc_log_key;
        
    RETURN v__transact_calc_log_key;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_all_loans_day (OID = 466350) : 
--
CREATE FUNCTION buh.calc_all_loans_day (
  _date date = public.get_yesterday_date(),
  out result integer,
  out result_str character varying
)
RETURNS SETOF record
AS 
$body$
DECLARE
  	v__loans_rec RECORD;
BEGIN
  	FOR v__loans_rec IN
    	SELECT 
        	t.loan_key 
        FROM 
        	public.loans t
        WHERE coalesce(t.issued, FALSE) -- выданный
        	AND coalesce(t.closed_date::date,'01.01.2200'::date)>=_date
        LIMIT 100    
    LOOP
    	SELECT * FROM buh.calc_loan_day(v__loans_rec.loan_key,_date)
        INTO result, result_str;
        
        RETURN NEXT;
    END LOOP;
    
    RETURN;
END;
$body$
LANGUAGE plpgsql;
--
-- Definition for function calc_loan__0 (OID = 466351) : 
--
CREATE FUNCTION buh.calc_loan__0 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>10.10.2017</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__date_one_day_before DATE;
	v__count_balance_value INTEGER;
    
    v__bv__od_loan_b NUMERIC;
    v__bv__od_overpayment_planned_b NUMERIC;
    v__bv__od_overpayment_for_delay_b NUMERIC;
    v__bv__od_fine_b NUMERIC;
    v__bv__n_loan_b NUMERIC;
    v__bv__n_overpayment_planned_b NUMERIC;
    v__bv__loan_b NUMERIC;
    v__bv__client_balance_b NUMERIC;
    v__bv__n_overpayment_conditionally_b NUMERIC;
    v__bv__all_loan_percents_b NUMERIC;
    
    v__message VARCHAR;
    
    v__rec_balance_value RECORD;
    
    v__loan_creation_date DATE;
    v__count_loans INTEGER;
BEGIN
/*
calc_0 - Переносим балансовую величину с предыдуще годня

*/


	-- Получаем дату на один день ранее предыдущей
	-- -------------------------------------------------------------------------
    SELECT t.creation_date 
    FROM asuz_db.loans t
    WHERE t.loan_key = _loan_key
	INTO v__loan_creation_date;
	-- -------------------------------------------------------------------------
	
    
    
	-- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_loans = ROW_COUNT;
    -- -------------------------------------------------------------------------
    
    
    
    -- Если не найдены данные  = Ошибка
	-- -------------------------------------------------------------------------
    IF v__count_loans != 1 THEN
        v__message = 'Нарушена структура данных loans для loan_key='||COALESCE(_loan_key::VARCHAR,'')||'  calc_loan__0';
        PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__0',v__message);
        result = -1;
        result_str = v__message;
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------



	-- Если дата выдачи равна дате\параметру - выходим из функции,
    -- потому как балансовых величин еще не было
	-- -------------------------------------------------------------------------
	IF v__loan_creation_date = _date THEN
		result_str = 'OK';
		result = 1;
		RETURN;
    END IF;
	-- -------------------------------------------------------------------------



	-- Получаем дату на один день ранее предыдущей
	-- -------------------------------------------------------------------------
	v__date_one_day_before = _date - interval '1 day';
	-- -------------------------------------------------------------------------



	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,v__date_one_day_before) THEN
        v__message = 'Нарушена структура данных balance_value для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(v__date_one_day_before::VARCHAR,'')||' calc_loan__0';
        PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__0',v__message);
        result = -1;
        result_str = v__message;
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------

	


    -- Получаем тукущую запись в balance_value, она же максимальная 
    -- активная для текущего займа
    -- -------------------------------------------------------------------------
    SELECT *
    FROM buh.balance_value bv
    WHERE 
    	bv.loan_key = _loan_key AND 
        bv.active = 1 AND
        bv.transact_date = v__date_one_day_before
    INTO 
    	v__rec_balance_value;
    -- -------------------------------------------------------------------------





	-- Устанавливаем балансовые величины. которые будут перенесены на начало
    -- -------------------------------------------------------------------------
    v__bv__od_loan_b = v__rec_balance_value.od_loan_e;
    v__bv__od_overpayment_planned_b = v__rec_balance_value.od_overpayment_planned_e;
    v__bv__od_overpayment_for_delay_b = v__rec_balance_value.od_overpayment_for_delay_e;
    v__bv__od_fine_b = v__rec_balance_value.od_fine_e;
    v__bv__n_loan_b = v__rec_balance_value.n_loan_e;
    v__bv__n_overpayment_planned_b = v__rec_balance_value.n_overpayment_planned_e;
    v__bv__loan_b = v__rec_balance_value.loan_e;
    v__bv__client_balance_b = v__rec_balance_value.client_balance_e;
    v__bv__n_overpayment_conditionally_b = v__rec_balance_value.n_overpayment_conditionally_e;
    v__bv__all_loan_percents_b = v__rec_balance_value.all_loan_percents_e;
    -- -------------------------------------------------------------------------




	-- Вставляем балансовую величину на текущий день
	-- -------------------------------------------------------------------------
    IF v__rec_balance_value.opened = TRUE THEN
        INSERT INTO 
          buh.balance_value
        (
          loan_key,
          od_loan_b,
          od_loan_e,
          od_overpayment_planned_b,
          od_overpayment_planned_e,
          od_overpayment_for_delay_b,
          od_overpayment_for_delay_e,
          od_fine_b,
          od_fine_e,
          transact_date,
          n_loan_b,
          n_loan_e,
          n_overpayment_planned_b,
          n_overpayment_planned_e,
          loan_b,
          loan_e,
          client_balance_b,
          client_balance_e,
          active,
          n_overpayment_conditionally_b,
          n_overpayment_conditionally_e,
          all_loan_percents_b,
          all_loan_percents_e
        ) 
        VALUES (

          _loan_key,
          v__bv__od_loan_b,
          0,
          v__bv__od_overpayment_planned_b,
          0,
          v__bv__od_overpayment_for_delay_b,
          0,
          v__bv__od_fine_b,
          0,
          _date,
          v__bv__n_loan_b,
          0,
          v__bv__n_overpayment_planned_b,
          0,
          v__bv__loan_b,
          0,
          v__bv__client_balance_b,
          0,
          1,
          v__bv__n_overpayment_conditionally_b,
          0,
          v__bv__all_loan_percents_b,
          0
        );
    END IF;
	-- -------------------------------------------------------------------------
    
	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__1 (OID = 466353) : 
--
CREATE FUNCTION buh.calc_loan__1 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__count INTEGER;
  	v__count_fact INTEGER;
    v__count_loan_transact INTEGER;
    v__rec RECORD;
    v__rec_fact RECORD;
    v__message VARCHAR;
    v__rec_subdivision RECORD;
    v__subdivision_key INTEGER;
    v__separate_subdivision VARCHAR;
    v__non_cash INTEGER;
    v__account_transaction_key INTEGER;
BEGIN
/*
calc_1 - на входе лоан кей, дата старт, тата энд

Найти все записи с выдачей займа
fact_nam_key = 1 (для указанного лоана)

Есть ли о них записи в loan_transact нет,
то инсертим запись

На сумму, которая в факте зафиксирована.
Проверяю наличие записи в balance_value

Поа ничего не будет. По нулям. Баланс на начало и на конец

На каждую дату считать.
Первая запись и на начало дня  и на конец дня.

Одна запись.

Если для одного лоана больше чем 1 факта выдачи - то ругаемся. В лог. Сделать лог.
И далее проверять логе. если есть ошибка то ничего не делать 
*/

    -- Получаем запись офакте выдачи указанного займа за данный период
	-- -------------------------------------------------------------------------
    SELECT * 
    FROM facts.facts t
    WHERE 
        t.fact_name_key = 1 AND 
        t.loan_key = _loan_key AND 
        t.fact_date = _date
    INTO 
        v__rec_fact;
	-- -------------------------------------------------------------------------



	-- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_fact = ROW_COUNT;
    -- -------------------------------------------------------------------------
    
    
    
    -- Если фактов выдачи займа более 1 = Ошибка
	-- -------------------------------------------------------------------------
    IF v__count_fact > 1 THEN
        v__message = 'Фактов выдачи для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' найдено '||COALESCE(v__count_fact::VARCHAR,'')||' calc_loan__1';
        PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__1',v__message);
        result = -1;
        result_str = v__message;
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------
    


    -- Если фактов выдачи займа равно 1  - тогда начинаем считать
	-- -------------------------------------------------------------------------
    IF v__count_fact = 1 THEN
        

    	-- Получаем код подразделения для текущей даты
        -- ---------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- ---------------------------------------------------------------------
        
        
        
		-- Получаем какой тип выдачи займа (нал\безнал)
        -- ---------------------------------------------------------------------
		SELECT l.non_cash
        FROM asuz_db.loans l
        WHERE l.loan_key = _loan_key
        INTO v__non_cash;
        v__non_cash = COALESCE(v__non_cash::INTEGER,0);
        -- ---------------------------------------------------------------------



		-- Получаем код транзакции
        -- ---------------------------------------------------------------------
    	v__account_transaction_key = 42; --Безнал
    	IF v__non_cash = 0 THEN
            v__account_transaction_key = 41; --Нал
        END IF;
        -- ---------------------------------------------------------------------


        
        -- Получаем количество транзакций "Выдача займа" для указанного займа 
        -- на указанную дату
        -- ---------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
        	t.loan_key = _loan_key AND 
            t.fact_key = v__rec_fact.fact_key AND 
            t.account_transaction_key = v__account_transaction_key AND
            t.active = 1 AND
            t.transact_date = _date
        INTO v__count_loan_transact;
        -- ---------------------------------------------------------------------
    
        
        -- Если искомая транзакция уже найдена = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Уже найдена транзакция для fact_key='||COALESCE(v__rec_fact.fact_key::VARCHAR,'')||' calc_loan__1';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__1',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------


    	-- Вставляем запись в loan_transact
        -- ---------------------------------------------------------------------
        INSERT INTO 
          buh.loan_transact
        (
          loan_key,
          summ,
          fact_key,
          account_transaction_key,
          transact_date,
          separate_subdivision_key
        ) 
        VALUES (
          _loan_key,
          v__rec_fact.value,
          v__rec_fact.fact_key,
          v__account_transaction_key,
          _date,
          v__separate_subdivision
        )
        RETURNING
        	loan_transact_key
        INTO
        	result;
        -- ---------------------------------------------------------------------


		-- Вставляем изначальную балансовую величину
        -- ---------------------------------------------------------------------
        INSERT INTO 
          buh.balance_value
        (
          loan_key,
          od_loan_b,
          od_loan_e,
          od_overpayment_planned_b,
          od_overpayment_planned_e,
          od_overpayment_for_delay_b,
          od_overpayment_for_delay_e,
          od_fine_b,
          od_fine_e,
          transact_date,
          n_loan_b,
          n_loan_e,
          n_overpayment_planned_b,
          n_overpayment_planned_e,
          loan_b,
          loan_e,
          client_balance_b,
          client_balance_e,
          n_overpayment_conditionally_b,
          n_overpayment_conditionally_e,
          all_loan_percents_b,
          all_loan_percents_e
        ) 
        VALUES (
          _loan_key,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          _date,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0
        );
        -- ---------------------------------------------------------------------

    END IF;
	-- -------------------------------------------------------------------------




    
    

	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__1_1 (OID = 466355) : 
--
CREATE FUNCTION buh.calc_loan__1_1 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
	Расчет доначисленных % и пени
    
	Алгоритм следующий
		1.			Получаем процентную ставку (базовую или ануитетную) v__koeff
		2.			Получаем базу для начисления процентов и пени (это Просроченное тело займа на начало дня) 
					v__overpayment_base = buh.get_overpayment_base(_loan_key,_date);
		3.			Если займ выдан до 01.07.2014 - тогда проценты за просроченный основной долг не начисляем
		4.			Расчитываем "Доначисленные проценты за просрочку на тело займа" по формуле
					v__od_overpayment = (v__overpayment_base * (v__koeff/100))
		5.			Получаем предел суммы общей переплаты
			5.1		Предел получем так (хранимка buh.get_max_overpayment_summ(_loan_key,_date)):
					Начисление плановых процентов + Доначисление процентов за просрочку + Начисление пени за просрочку НЕ БОЛЕЕ , 
						-- чем 300% от ТЕЛА ЗАЙМА (общего при выдаче), если займ выдан после '01.01.2017' 
						-- чем 400% от ТЕЛА ЗАЙМА (общего при выдаче), если займ выдан до '01.01.2017' 
					Если займ выдан после 28.01.2019 - тогда предел общей переплаты составляет 
		  	 			-- 250% от ТЕЛА ЗАЙМА (общего при выдаче)
		6.			Если Расчитанная сумма просроченной переплаты более той ограничительной суммы, что получена в пункте №5
					 тогда "Доначисленные проценты за просрочку на тело займа" на расчетный день равны нолю.
		7.			Получаем ограничение "Предел просроченной переплаты"
			7.1		Предел получем так (хранимка buh.get_max_overpayment_for_delay_summ(_loan_key,_date)):
						-- сумма процентов за просрочку НЕ БОЛЕЕ , чем 200% от ТЕЛА ЗАЙМА (общего при выдаче)
						-- Если займ выдан раньше 01.01.2017, то предел равен 1000000
                        -- Если займ выдан позже 27.01.2019, то предел равен 1000000
		8.			Если Расчитанная сумма просроченной переплаты более той ограничительной суммы, что получена в пункте №7 
					 тогда "Доначисленные проценты за просрочку на тело займа" на расчетный день равны нолю.        
		9.			Создаем транзакцию "Доначисленные проценты за просрочку на тело займа"
		10.			Расчитываем сумму "Начисление пени за просрочку" по формуле
					 v__od_fine = (v__overpayment_base * (0.2/365))
		11.			Получаем предел суммы общей переплаты (с учетом) транзакции "Доначисленные проценты за просрочку на тело займа"
			11.1	Предел получем так (хранимка buh.get_max_overpayment_summ(_loan_key,_date)):
					Начисление плановых процентов + Доначисление процентов за просрочку + Начисление пени за просрочку НЕ БОЛЕЕ , 
						-- чем 300% от ТЕЛА ЗАЙМА (общего при выдаче), если займ выдан до '01.01.2017' 
						-- чем 400% от ТЕЛА ЗАЙМА (общего при выдаче), если займ выдан после '01.01.2017' 
					Если займ выдан после 28.01.2019 - тогда предел общей переплаты составляет 
						-- 250% от ТЕЛА ЗАЙМА (общего при выдаче)
		12.		  Если займ выдан позже чем '28.01.2019' - тогда ограничиваем сумму пени функцией 
                    buh.get_max_overpayment_summ, если нет - тогда пени никак не ограничиваются
                    Ограничения введены согласно указаниям в задаче https://redmine.vivadengi.ru/issues/456444#note-20
                    Если расчитанная сумма "Начисление пени за просрочку" (пункт № 10 v__od_fine) более той ограничительной суммы, 
					что получена в пункте №11 тогда "Начисление пени за просрочку" на расчетный день равны нолю.     
                    
		13.			Если займ До 01.07.2014 тогда "Начисление пени за просрочку" считаем так:
					 2% на просроченный основной долг в день до 60 дня по каждому платежу
		14.			Создаем транзакцию "Начисление пени за просрочку"ё
</description>
<history_list>
	<item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
    <item><date_m>21.02.2019</date_m><task_n>473770</task_n><author>Тихоненко В.В.</author></item>
    <item><date_m>11.03.2019</date_m><task_n>477118</task_n><author>Тихоненко В.В.</author></item>
    <item><date_m>12.03.2019</date_m><task_n>477118</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__rec_balance_value RECORD;
    v__count_balance_value INTEGER;
    v__message VARCHAR;
    v__creation_date DATE;
    v__rate NUMERIC;
    v__koeff NUMERIC;
    v__od_overpayment NUMERIC;
    v__od_fine NUMERIC;
    v__separate_subdivision VARCHAR;
    v__account_transaction_key INTEGER;
    v__overpayment_base NUMERIC;
    v__max_overpayment_summ NUMERIC;
    
    v__permissible_loan_key INTEGER;
    v__month INTEGER;
    v__loan_term INTEGER;
    
    v__rec_loan RECORD; -- Содержит запись о текущем займе из таблицы loans
    v__max_payment_date DATE; -- Максимальная дата оплаты до текущей даты
    v__max_payment_date_plus_60 DATE; -- Максимальная дата оплаты до текущей даты ПЛЮС 60 дней
    v__bv__od_loan_e NUMERIC; -- Балансовая величина просроченного тела займа на конец 
    v__day_peni NUMERIC; -- Начисленные пени в текущий день
    v__first_payment_date DATE; -- Дата первого платежа согласно действующему графику платежей
    v__rec__first_payment_schedule RECORD; -- Признак того, что необходимо при расчете брать первоначальный график платежей, из договора
    v__overpayment_base__payment_date NUMERIC; -- Балансовая величина просроченного тела займа на дату платежа по графику 
    v__rec RECORD; -- Запись для перебора дат по графику платежей
BEGIN




	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,_date) THEN
        result = -1;
        result_str = 'Ошибка проверки балансовой величины';
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------



    -- -------------------------------------------------------------------------
    SELECT *
    FROM buh.balance_value bv
    WHERE 
    	bv.loan_key = _loan_key AND 
        bv.active = 1 AND
        bv.transact_date = _date
    INTO 
    	v__rec_balance_value;
    -- -------------------------------------------------------------------------





	-- Получаем данные о текущем займе
	-- -------------------------------------------------------------------------
    SELECT *  FROM asuz_db.loans l
    WHERE l.loan_key = _loan_key
    INTO v__rec_loan;
	-- -------------------------------------------------------------------------
    
    
    
    -- Если у клиента просроченное тело займа на начало (58.03.02)
    -- -------------------------------------------------------------------------
    IF COALESCE(v__rec_balance_value.od_loan_b::INTEGER,0) != 0 THEN   
    
    	-- Получаем код подразделения для текущей даты
        -- ---------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- ---------------------------------------------------------------------
        

        
        -- Получаем коэффициент, на который умножать
        -- ---------------------------------------------------------------------
        v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key, _date, 1);

        --для определения версии алгоритма начисления
        SELECT l.creation_date, l.loan_term
        FROM asuz_db.loans l
        WHERE l.loan_key = _loan_key
        INTO v__creation_date, v__loan_term;
        -- ---------------------------------------------------------------------
        
        

        
		-- Получаем базовую ставку в зависимоит от даты создания займа
		v__koeff = buh.get_rate_by_creation_date(_loan_key);
        v__overpayment_base = buh.get_overpayment_base(_loan_key,_date);



     
        -- Просроченное тела займа > 0 = Ошибка
        -- ---------------------------------------------------------------------
        IF v__koeff IS NULL THEN
            v__message = 'Коэффициент равен Null calc_loan__1_1';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__1_1',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------
        


		-- Если займ До 01.07.2014 тогда считаем как 
		-- Срочные проценты за просроченный основной долг
        -- Не рассчитываются
        -- ---------------------------------------------------------------------
        IF v__rec_loan.creation_date < '01.07.2014' THEN
        	v__koeff = 0; -- Не начисляется
        END IF;
        -- ---------------------------------------------------------------------

        
        
        
        -- Считаем просроченное тело займа
        -----------------------------------------------------------------------
          v__od_overpayment = trunc((v__overpayment_base * (v__koeff/100)),2); -- Обрезаем до копеек
        -----------------------------------------------------------------------


        --проверка предела просроченной переплаты
        v__max_overpayment_summ  = buh.get_max_overpayment_for_delay_summ(_loan_key,_date);
        IF v__max_overpayment_summ < v__od_overpayment THEN
        	v__od_overpayment = 0;
        ELSE
        --проверка предела общей переплаты
          v__max_overpayment_summ  = buh.get_max_overpayment_summ(_loan_key,_date);
          IF v__max_overpayment_summ < v__od_overpayment THEN
            v__od_overpayment = 0;
          END IF;            
        END IF;
        

        

        
        
        
        -- Создаем транзакцию "Доначисление процентов за просрочку на тело займа"
        -- ---------------------------------------------------------------------
        IF v__od_overpayment > 0 THEN
          v__account_transaction_key = 11; -- Доначисление процентов за просрочку на тело займа
          INSERT INTO 
            buh.loan_transact
          (
            loan_key,
            summ,
            fact_key,
            account_transaction_key,
            transact_date,
            separate_subdivision_key
          ) 
          VALUES (
            _loan_key,
            v__od_overpayment,
            0,
            v__account_transaction_key,
            _date,
            v__separate_subdivision
          );
        END IF;
        -- ---------------------------------------------------------------------

		
        -- Считаем пени
        v__od_fine = trunc((v__overpayment_base * (0.2/365)),2); -- Обрезаем до копеек

       


		-- Если займ выдан позже чем '28.01.2019' - тогда ограничиваем сумму пени функцией 
        -- buh.get_max_overpayment_summ, если нет - тогда пени никак не ограничиваются
        -- Ограничения введены согласно указаниям в задаче https://redmine.vivadengi.ru/issues/456444#note-20
        -- ==================================================================================
		IF v__rec_loan.creation_date > '28.01.2019' THEN
            v__max_overpayment_summ  = buh.get_max_overpayment_summ(_loan_key,_date);
            IF v__max_overpayment_summ < v__od_fine THEN
                v__od_fine = 0;
            END IF;
        END IF;
		-- Если займ До 01.07.2014 тогда считаем как 
		-- 2% на просроченный основной долг в день до 60 дня по каждому платежу
        -- -----------------------------------------------------------------------------------------------------------------------------------------
        IF v__rec_loan.creation_date < '01.07.2014' THEN
            v__od_fine = 0;
            IF buh.get_peni__60_days_date_enable(_loan_key, _date) THEN
            	-- 2% от просроченного основного долга
                v__bv__od_loan_e = buh.get_op_balance(_loan_key, _date,67,'od_loan_b');
            	v__day_peni = 0.02 * v__bv__od_loan_e;
                v__day_peni = trunc(v__day_peni,2); -- Обрезаем до копеек
                
                v__od_fine = v__day_peni;
            END IF;
        END IF;
        -- ==================================================================================

        
        
        
        
        



        IF v__od_fine > 0 THEN
          -- Создаем транзакцию по просроченным пени
          -- ---------------------------------------------------------------------
          v__account_transaction_key = 12; -- Начисление пени за просрочку
          INSERT INTO 
            buh.loan_transact
          (
            loan_key,
            summ,
            fact_key,
            account_transaction_key,
            transact_date,
            separate_subdivision_key
          ) 
          VALUES (
            _loan_key,
            v__od_fine,
            0,
            v__account_transaction_key,
            _date,
            v__separate_subdivision
          );
        END IF;
        -- ---------------------------------------------------------------------        
        
        
    END IF;
    -- -------------------------------------------------------------------------
    
    

	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__2 (OID = 466357) : 
--
CREATE FUNCTION buh.calc_loan__2 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
========================================================================================
            2. Учесть начисление плановых процентов.
            Найти временной интервал по графику обязательств где мы находимся на дату анализа
            Определяем кол-во дней в интервале и сумму плановых процентов в очередном обязательстве.
            Делим одно на второе и получаем сумму процентов плановых к начислению за день.
========================================================================================
</description>
<history_list>
  <item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2020</date_m><task_n>587292</task_n><author>Тихоненко В.В.</author></item>

</history_list>
*/
DECLARE
    v__count_loan_transact NUMERIC;
    v__message VARCHAR;
    v__overpayment_sum_for_date NUMERIC;
    v__loan_summ_for_date NUMERIC;    
    v__separate_subdivision VARCHAR;
	v__account_transaction_key INTEGER;
    v__loan_creation_date DATE;
	v__min_date DATE;
  	v__max_date DATE;
    v__permissible_loan_key INTEGER;
    v__account_key_76_03_01 INTEGER;
    v__op_balance_76_03_01 NUMERIC;
    
    v__rec_fact record;
    v__account_58_03_02 INTEGER;
    v__op_balance_58_03_02 NUMERIC;
    v__count_fact integer;
    /*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
    
    v__rec__first_payment_schedule RECORD; -- Признак того, что необходимо при расчете брать первоначальный график платежей, из договора
    */
BEGIN


/*

calc_2_1 Графики платежей					

Для указанного лоана найти даты.
Формирую по датам начисление процентов.

*/

	-- Получаем количество записей о начислении процентов на текущий день
	-- -------------------------------------------------------------------------
    SELECT COUNT(*)
    FROM buh.loan_transact t
    WHERE 
        t.loan_key = _loan_key AND
        t.active = 1 AND
        t.account_transaction_key = 10 AND --  Начисление плановых процентов
    	t.transact_date = _date
    INTO
    	v__count_loan_transact;
	-- -------------------------------------------------------------------------
    
    
    -- Если уже найдена активная искомая транзакция = Ошибка
    -- -------------------------------------------------------------------------
    IF v__count_loan_transact > 0 THEN
        v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' account_transaction_key = 10 calc_loan__2';
        PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__2',v__message);
        result = -1;
        result_str = v__message;
        RETURN;
    END IF;
    -- -------------------------------------------------------------------------
    



	-- Получаем количество записей  Формирование суммы к уплате тело ТЕХНИЧЕСКАЯ ТРАНЗАКЦИЯ (ТАКОЙ НЕ БЫЛО)
	-- -------------------------------------------------------------------------
    SELECT COUNT(*)
    FROM buh.loan_transact t
    WHERE 
    	t.transact_date = _date AND
        t.loan_key = _loan_key AND
        t.account_transaction_key = 53 AND --  Начисление плановых процентов
        t.active = 1
    INTO
    	v__count_loan_transact;
	-- -------------------------------------------------------------------------
    
    
    -- Если уже найдена активная искомая транзакция = Ошибка
    -- -------------------------------------------------------------------------
    IF v__count_loan_transact > 0 THEN
        v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' .account_transaction_key = 53 calc_loan__2';
        PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__2',v__message);
        result = -1;
        result_str = v__message;
        RETURN;
    END IF;
    -- -------------------------------------------------------------------------







    -- Получаем v__permissible_loan_key
    -- Получаем дату создания займа
    -- -------------------------------------------------------------------------
	/*
    
    Код верен в том случае, если данные из АСУЗа корректные. 
    Но если они битые, тогжа стоит применять
	buh.get_permissible_loan_for_date
    Согласно задаче #587292 

    */
    SELECT 
    --	t.permissible_loan_key,  
        t.creation_date
    FROM 
    	asuz_db.loans t
    WHERE 
    	t.loan_key = _loan_key
    INTO 
    --	v__permissible_loan_key, 
        v__loan_creation_date;
        
    v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key,_date);

        
    v__min_date = v__loan_creation_date;
    -- -------------------------------------------------------------------------
    


    

    -- Получаем максимальную дату по займу
    -- -------------------------------------------------------------------------
    /*
	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
    
    SELECT max(t.payment_date)
    FROM asuz_db.repayment_schedules t 
    INTO v__max_date;
    */
    SELECT max(COALESCE(ro.payment_date_ori,t.payment_date))
    FROM asuz_db.repayment_schedules t 
    LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
    	on ro.repayment_schedule_key=t.repayment_schedule_key
    WHERE
    	t.permissible_loan_key = v__permissible_loan_key
    INTO v__max_date;
    -- -------------------------------------------------------------------------
    
    /*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
    
    
	-- Получаем настройку - нужно ли брать первоначальны график платежей, 
    -- тот что при заклчении договора
	-- -------------------------------------------------------------------------
    v__rec__first_payment_schedule  = buh.get_calc_first_payment_schedule(_loan_key);
	-- -------------------------------------------------------------------------



	-- Рассчитать по первому графику платежей на момент выдачи
    -- -------------------------------------------------------------------------
	IF v__rec__first_payment_schedule.result = 1 THEN
      SELECT max(t.payment_date)
      FROM asuz_db.tmp_payment_schedules t 
      WHERE t.application_key = v__rec__first_payment_schedule.application_key
	  INTO v__max_date;
    END IF;
    -- -------------------------------------------------------------------------
*/




	-- Проверяем дату на вхождение в диапазон допустимых дат
    -- -------------------------------------------------------------------------    
    IF _date<v__min_date OR _date>v__max_date THEN
        v__message = 'Дата не входит в диапазон допустимых дат для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__2';
        PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__2',v__message);
        result = -1;
        result_str = v__message;
        RETURN;
    END IF;
    -- -------------------------------------------------------------------------
    

	-- Для первого дня не считаем проценты (выдача займа)
    -- -------------------------------------------------------------------------    
    IF _date = v__min_date THEN
        result = 1;
        result_str = 'OK';
        RETURN;
    END IF;
    -- -------------------------------------------------------------------------
    

    
	-- Получаем сумму платежа по графику на текущий день
	-- -------------------------------------------------------------------------  
    v__overpayment_sum_for_date = buh.get_overpayment_sum_for_date(_loan_key,_date);
    v__loan_summ_for_date = buh.get_loan_summ_for_date(_loan_key,_date);
  	-- -------------------------------------------------------------------------
    
    
    
    -- Получаем код подразделения для текущей даты
    -- -------------------------------------------------------------------------
    v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
    -- -------------------------------------------------------------------------
    
    
    
    
    
    
    
    
    -- Вславляем транзакцию с графиком платежей "Начисление плановых процентов"
  	-- -------------------------------------------------------------------------
    v__account_transaction_key = 10; -- Начисление плановых процентов
    INSERT INTO 
      buh.loan_transact
    (
      loan_key,
      summ,
      fact_key,
      account_transaction_key,
      transact_date,
      separate_subdivision_key
    ) 
    VALUES (
      _loan_key,
      v__overpayment_sum_for_date,
      0, -- Факт
      v__account_transaction_key,
      _date,
      v__separate_subdivision
    );
  	-- -------------------------------------------------------------------------
    
    
    
    
    -- Если текущая дата равна аннуитетной
  	-- -------------------------------------------------------------------------
    v__account_key_76_03_01 = 57;
    v__op_balance_76_03_01 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_01,'n_overpayment_planned_b');
	IF buh.is_date_in_repayment_shedules_annuitet(_loan_key,_date) THEN
        v__account_transaction_key = 70; -- ТЕХ Перевод в условно непросроченные проценты
        INSERT INTO 
          buh.loan_transact
        (
          loan_key,
          summ,
          fact_key,
          account_transaction_key,
          transact_date,
          separate_subdivision_key
        ) 
        VALUES (
          _loan_key,
          v__op_balance_76_03_01,
          0, -- Факт
          v__account_transaction_key,
          _date,
          v__separate_subdivision
        );
    END IF;
  	-- -------------------------------------------------------------------------
    
    ---------Тех. проводка перенос просроченного тела займа в непросроченное основное тело займа
    --- с 58.03.02 на 58.03
    --- только если есть факт =2 и адд_валуе=1
    
    SELECT * 
    FROM facts.facts t
    WHERE 
        t.fact_name_key = 2 AND 
        t.loan_key = _loan_key AND 
        t.fact_date::date = _date AND
        t.additional_value=1 --спец. факт для коррекции тела просроченного займа при реструктуре
    INTO 
        v__rec_fact;
    
    -- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_fact = ROW_COUNT;
	--raise notice 'v__count_fact=%',v__count_fact;
    -- Если фактов выдачи займа равно 1  - тогда начинаем считать
	-- -------------------------------------------------------------------------
    IF v__count_fact = 1 THEN
        
        -- ТЕХНИЧЕСКАЯ ТРАНЗАКЦИЯ (ТАКОЙ НЕ БЫЛО)
    	v__account_transaction_key = 78; 


        
        -- Получаем количество транзакций "Выдача займа" для указанного займа 
        -- на указанную дату
        -- ---------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
        	t.loan_key = _loan_key AND 
            t.fact_key = v__rec_fact.fact_key AND 
            t.account_transaction_key = v__account_transaction_key AND
            t.active = 1 AND
            t.transact_date = _date
        INTO v__count_loan_transact;
        -- ---------------------------------------------------------------------
    
        
        -- Если искомая транзакция уже найдена = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Уже найдена транзакция для fact_key='||COALESCE(v__rec_fact.fact_key::VARCHAR,'')||' calc_loan__2';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__2',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------

		v__account_58_03_02=67;--58.03.02
		--получаем оп.баланс по просроченному телу займа
        v__op_balance_58_03_02=buh.get_op_balance(_loan_key,_date, v__account_58_03_02,'od_loan_b');
		--raise notice 'v__op_balance_58_03_02=%',v__op_balance_58_03_02;
		if v__op_balance_58_03_02>0 then
            -- Вставляем запись в loan_transact
            -- ---------------------------------------------------------------------
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              v__op_balance_58_03_02,
              v__rec_fact.fact_key,
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            )
            RETURNING
                loan_transact_key
            INTO
                result;
        end if;
        -- ---------------------------------------------------------------------
    END IF;
	-- -------------------------------------------------------------------------


	-- Если дата совпадает с графиком платежа - тогда делаем транзакцию 
    -- по "Формирование суммы к уплате тело"
  	-- -------------------------------------------------------------------------    
    IF v__loan_summ_for_date != -1  AND v__loan_summ_for_date > 0 THEN
    	-- Формирование суммы к уплате тело
        -- ТЕХНИЧЕСКАЯ ТРАНЗАКЦИЯ (ТАКОЙ НЕ БЫЛО)
        v__account_transaction_key = 53; 
        INSERT INTO 
          buh.loan_transact
        (
          loan_key,
          summ,
          fact_key,
          account_transaction_key,
          transact_date,
          separate_subdivision_key
        ) 
        VALUES (
          _loan_key,
          v__loan_summ_for_date,
          0, -- Факт
          v__account_transaction_key,
          _date,
          v__separate_subdivision
        );
    END IF;
  	-- -------------------------------------------------------------------------
    
	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__3 (OID = 466359) : 
--
CREATE FUNCTION buh.calc_loan__3 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__count_facts INTEGER;
    v__rec_fact RECORD;
    v__separate_subdivision VARCHAR;
    v__non_cash INTEGER;
    v__account_transaction_key INTEGER;
    v__count_loan_transact INTEGER;
    v__message VARCHAR;
BEGIN

/*

calc_2_1 Учет поступлений

3. Учет поступлений
поиск факта, регистрация факта в виде проводки.

*/

    
    -- Получаем факт на текущую дату для указанного займа
	-- -------------------------------------------------------------------------
    FOR v__rec_fact IN
        SELECT *
        FROM 
            facts.facts t
        WHERE
            t.fact_name_key IN (4,5) AND -- Поступление средств по договору займа
            t.loan_key = _loan_key AND 
            t.fact_date = _date
    LOOP
 
        -- Получаем код подразделения для текущей даты
        -- ---------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- ---------------------------------------------------------------------

        v__account_transaction_key = 45; --Нал(additional_value = 0) 
    	IF COALESCE(v__rec_fact.additional_value,0) = 1 THEN
	    	v__account_transaction_key = 3; --Безнал (additional_value = 1)
        END IF;
        -- ---------------------------------------------------------------------
        
        
        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- ---------------------------------------------------------------------
        SELECT COUNT(*)
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND --  Начисление плановых процентов
            t.active = 1 AND
            t.transact_date = _date AND
            t.fact_key = v__rec_fact.fact_key
        INTO
            v__count_loan_transact;
        -- ---------------------------------------------------------------------
        
            
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__3';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__3',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------
        
        
        -- Вставляем запись о транзакции
        -- ---------------------------------------------------------------------
        INSERT INTO 
          buh.loan_transact
        (
          loan_key,
          summ,
          fact_key,
          account_transaction_key,
          transact_date,
          separate_subdivision_key
        ) 
        VALUES (
          _loan_key,
          v__rec_fact.value,
          v__rec_fact.fact_key,
          v__account_transaction_key,
          _date,
          v__separate_subdivision
        );
        -- ---------------------------------------------------------------------
        

    END LOOP;
    
    
    
    
    

	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__4_1 (OID = 466360) : 
--
CREATE FUNCTION buh.calc_loan__4_1 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__count_facts INTEGER;
    v__rec_fact RECORD;
    v__separate_subdivision VARCHAR;
    v__account_transaction_key INTEGER;
    v__account_transaction_key2 INTEGER;
    v__account_transaction_key3 INTEGER;
    v__count_loan_transact INTEGER;
    v__message VARCHAR;
    v__fact_name_key INTEGER;
    v__additional_value INTEGER;
    
    v__account_key_58_03_02 INTEGER;
    v__start_58_03_02 NUMERIC;
    v__debet_58_03_02 NUMERIC;
    v__credit_58_03_02 NUMERIC;
    v__summ_58_03_02 NUMERIC;
    
    
    
    v__account_key_58_03_01 INTEGER;
    v__start_58_03_01 NUMERIC;
    v__debet_58_03_01 NUMERIC;
    v__credit_58_03_01 NUMERIC;
    v__summ_58_03_01 NUMERIC;
    
    v__account_key_58_03 INTEGER;
    v__summ_58_03 NUMERIC;
    
    v__paym_summ NUMERIC;
    v__paym_summ2 NUMERIC;
BEGIN
/*
Коррекция основного долга
calc_4_1 - сделать заглушку

*/
	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,_date) THEN
        result = -1;
        result_str = 'Ошибка при проверке балансовой величины';
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------


	-- Устанавливаем тип факта
    -- -------------------------------------------------------------------------
	v__fact_name_key = 6; -- Поступление средств по договору займа
    -- -------------------------------------------------------------------------
    


    -- Получаем факт на текущую дату для указанного займа
    -- -------------------------------------------------------------------------
    SELECT *, -1*t.value AS fact_value
    FROM 
        facts.facts t
    WHERE
        t.fact_name_key IN (6) AND
        t.loan_key = _loan_key AND 
        t.fact_date = _date AND
--        t.additional_value != 4
        t.additional_value NOT IN (4,8)
    INTO
        v__rec_fact;	
    -- -------------------------------------------------------------------------
    
    
    
	-- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_facts = ROW_COUNT;
    -- -------------------------------------------------------------------------


	-- Устанавливаем код транзакции
    -- -------------------------------------------------------------------------
    v__additional_value = v__rec_fact.additional_value;
    
    v__account_transaction_key=CASE v__additional_value
        --  ТЕХ Корректировка  невыясненного платежа по телу (просроченное)
        WHEN 2 THEN 64
        -- ТЕХ Списание долга (тело) в случае смерти (просроченное)
        WHEN 3 THEN 65
        -- ТЕХ Цессия (тело) (просроченное)
        WHEN 5 THEN 66
        -- ТЕХ Прощение коллекторами (тело) (просроченное)
        WHEN 6 THEN 67
        --  ТЕХ Все  остальное (Списание в исключительных случаях  тело (неплатежеспособность.))  (просроченное)
        ELSE 68
    END;
    
    v__account_transaction_key2=CASE v__additional_value
        -- Корректировка невыясненного платежа по телу
        WHEN 2 THEN 23 
        -- Списание долга (тело) в случае смерти
        WHEN 3 THEN 26 
        -- Цессия (тело)
        WHEN 5 THEN 35 
        -- Прощение коллекторами (тело)
        WHEN 6 THEN 29 
        -- Все  остальное (Списание в исключительных случаях  тело (неплатежеспособность.))
        ELSE 32
    END;
	-- -------------------------------------------------------------------------





    
    -- Если найдены таковые факты, тогда продолжаем рассчет
    -- -------------------------------------------------------------------------
    IF v__count_facts > 0 THEN
    
    
        -- Получаем код подразделения для текущей даты
        -- ---------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- ---------------------------------------------------------------------
        
        






        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- ---------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND --  Начисление плановых процентов
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- ---------------------------------------------------------------------
        
            
        
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__4_2';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__4_2',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------
        
        



        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- ---------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key2 AND --  Начисление плановых процентов
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- ---------------------------------------------------------------------
        
            
        
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__4_2';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__4_2',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------
        
        




        -- Считаем операционный баланс для 58_03_02
        -- -------------------------------------------------------------------------
        v__account_key_58_03_02 = 67; -- Номер счета для 58_03_02
        v__summ_58_03_02 = buh.get_op_balance(_loan_key, _date,v__account_key_58_03_02,'od_loan_b');
        -- -------------------------------------------------------------------------




        -- Находим меньшую сумму из операционного баланса 58_03_02 и суммой факта
        -- -------------------------------------------------------------------------
        v__paym_summ = LEAST(v__summ_58_03_02,COALESCE(v__rec_fact.fact_value,0));
        -- -------------------------------------------------------------------------



        
        -- Вставляем запись о транзакции
        -- ---------------------------------------------------------------------
        INSERT INTO 
          buh.loan_transact
        (
          loan_key,
          summ,
          fact_key,
          account_transaction_key,
          transact_date,
          separate_subdivision_key
        ) 
        VALUES (
          _loan_key,
          v__paym_summ,
          v__rec_fact.fact_key,
          v__account_transaction_key,
          _date,
          v__separate_subdivision
        );
        -- ---------------------------------------------------------------------
        
        
        
        -- Считаем оставшуюся сумму
        -- ---------------------------------------------------------------------
        v__paym_summ2 = v__rec_fact.fact_value - v__paym_summ;
        -- ---------------------------------------------------------------------
        
        
        -- Если  осталось что-то с суммы факта
        -- ---------------------------------------------------------------------
		IF v__paym_summ2 > 0 THEN
            
            --  Считаем операционный баланс для 58_03_01
            -- -------------------------------------------------------------------------
            v__account_key_58_03_01 = 53; -- Номер счета для 58_03_01
	        v__summ_58_03_01 = buh.get_op_balance(_loan_key, _date,v__account_key_58_03_01,'n_loan_b');
            -- -------------------------------------------------------------------------



            -- Находим меньшую сумму из операционного баланса 58_03_01 и суммой факта
            -- -------------------------------------------------------------------------
            v__paym_summ2 = LEAST(v__summ_58_03_01,v__paym_summ2);
            -- -------------------------------------------------------------------------
            
            -- Тогда делаем транзакцию
            -- -------------------------------------------------------------------------
            IF v__paym_summ2 > 0 THEN
               INSERT INTO 
                  buh.loan_transact
                (
                  loan_key,
                  summ,
                  fact_key,
                  account_transaction_key,
                  transact_date,
                  separate_subdivision_key
                ) 
                VALUES (
                  _loan_key,
                  v__paym_summ2,
                  v__rec_fact.fact_key,
                  v__account_transaction_key2,
                  _date,
                  v__separate_subdivision
                );
            END IF;
            -- -------------------------------------------------------------------------

		END IF;
        -- ---------------------------------------------------------------------

		--в случае коррекции основного тела займа для additional_value = 3 - списываем основное тело, выданное клиенту
		if v__rec_fact.additional_value = 3 then
        	v__account_transaction_key3=82;
            -- Считаем оставшуюся сумму
            -- ---------------------------------------------------------------------
            v__paym_summ2 = v__rec_fact.fact_value - v__paym_summ - v__paym_summ2;
            -- ---------------------------------------------------------------------
            
            
            -- Если  осталось что-то с суммы факта
            -- ---------------------------------------------------------------------
            IF v__paym_summ2 > 0 THEN
                --  Считаем операционный баланс для 58_03
                -- -------------------------------------------------------------------------
                v__account_key_58_03 = 66; -- Номер счета для 58_03
                v__summ_58_03 = buh.get_op_balance(_loan_key, _date,v__account_key_58_03,'loan_b');
                -- -------------------------------------------------------------------------



                -- Находим меньшую сумму из операционного баланса 58_03 и суммой факта
                -- -------------------------------------------------------------------------
                v__paym_summ2 = LEAST(v__summ_58_03,v__paym_summ2);
                -- -------------------------------------------------------------------------
                  
                -- Тогда делаем транзакцию
                -- -------------------------------------------------------------------------
                IF v__paym_summ2 > 0 THEN
                   INSERT INTO 
                      buh.loan_transact
                    (
                      loan_key,
                      summ,
                      fact_key,
                      account_transaction_key,
                      transact_date,
                      separate_subdivision_key
                    ) 
                    VALUES (
                      _loan_key,
                      v__paym_summ2,
                      v__rec_fact.fact_key,
                      v__account_transaction_key3,
                      _date,
                      v__separate_subdivision
                    );
                END IF;
                -- -------------------------------------------------------------------------
            end if;
        end if;








        
    
    END IF;
    -- -------------------------------------------------------------------------
    

	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__4_2 (OID = 466362) : 
--
CREATE FUNCTION buh.calc_loan__4_2 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__count_facts INTEGER;
    v__rec_fact RECORD;
    v__separate_subdivision VARCHAR;
    v__account_transaction_key INTEGER;
    v__account_transaction_key2 INTEGER;
    v__account_transaction_key3 INTEGER;
    v__count_loan_transact INTEGER;
    v__message VARCHAR;
    v__fact_name_key INTEGER;
    v__additional_value INTEGER;
    
    v__account_key_76_03_02 INTEGER;
    v__start_76_03_02 NUMERIC;
    v__debet_76_03_02 NUMERIC;
    v__credit_76_03_02 NUMERIC;
    v__summ_76_03_02 NUMERIC;
    
    
    
    v__account_key_76_03_01 INTEGER;
    v__start_76_03_01 NUMERIC;
    v__debet_76_03_01 NUMERIC;
    v__credit_76_03_01 NUMERIC;
    v__summ_76_03_01 NUMERIC;
    
    
    
    v__account_key_76_03_04 INTEGER;
    v__start_76_03_04 NUMERIC;
    v__debet_76_03_04 NUMERIC;
    v__credit_76_03_04 NUMERIC;
    v__summ_76_03_04 NUMERIC;
    
    v__paym_summ NUMERIC;
    v__paym_summ2 NUMERIC;
BEGIN
/*
Коррекция процентов плановых
calc_4_2 - сделать заглушку

*/
	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,_date) THEN
        result = -1;
        result_str = 'Ошибка при проверке балансовой величины';
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------


	-- Устанавливаем тип факта
    -- -------------------------------------------------------------------------
	v__fact_name_key = 7;  --Регистрация факта создания коррекции(переплата)
    -- -------------------------------------------------------------------------
    


    

    -- Получаем факт на текущую дату для указанного займа
    -- -------------------------------------------------------------------------
    SELECT *, -1*t.value AS fact_value
    FROM 
        facts.facts t
    WHERE
        t.fact_name_key IN (7) AND
        t.loan_key = _loan_key AND 
        t.fact_date = _date AND
--        t.additional_value != 4
        t.additional_value NOT IN (4,8)
    INTO
        v__rec_fact;	
    -- -------------------------------------------------------------------------
    
    
    
	-- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_facts = ROW_COUNT;
    -- -------------------------------------------------------------------------


	-- Устанавливаем код транзакции
    -- -------------------------------------------------------------------------
    v__additional_value = v__rec_fact.additional_value;
    
    --транзакции для корректировки плановых просроченных процентов 76,03,02
    v__account_transaction_key=CASE v__additional_value
        -- Корректировка не выясненного платежа по процентам просроченным
        WHEN 2 THEN 58
        -- Списание долга (проценты) в случае смерти просроченные
        WHEN 3 THEN 59
        -- Цессия (проценты) просроченные
        WHEN 5 THEN 60
        -- Прощение коллекторами (проценты) просроченные
        WHEN 6 THEN 61
        -- Все  остальное (ТЕХ Списание в исключительных случаях проценты)
        ELSE 69
    END;
    --транзакции для корректировки плановых условно просроченных процентов 76,03,04
    v__account_transaction_key2=CASE v__additional_value
        -- Корректировка невыясненного платежа по процентам
        WHEN 2 THEN 71 --58
        -- Списание долга (проценты) в случае смерти
        WHEN 3 THEN 72 -- 59
        -- Цессия (проценты)
        WHEN 5 THEN 73 --60
        -- Прощение коллекторами (проценты)
        WHEN 6 THEN 74 -- 61
        -- Все  остальное
        ELSE 75
    END;
    --транзакции для корректировки плановых непросроченных процентов 76,03,01
    v__account_transaction_key3=CASE v__additional_value
        -- Корректировка невыясненного платежа по процентам
        WHEN 2 THEN 24 --58
        -- Списание долга (проценты) в случае смерти
        WHEN 3 THEN 52 -- 59
        -- Цессия (проценты)
        WHEN 5 THEN 36 --60
        -- Прощение коллекторами (проценты)
        WHEN 6 THEN 30 -- 61
        -- Все  остальное
        ELSE 33
    END;
	-- -------------------------------------------------------------------------





    
    -- Если найдены таковые факты, тогда продолжаем рассчет
    -- -------------------------------------------------------------------------
    IF v__count_facts > 0 THEN
    
    
        -- Получаем код подразделения для текущей даты
        -- ---------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- ---------------------------------------------------------------------
        
        






        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- ---------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND --  Начисление плановых процентов
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- ---------------------------------------------------------------------
        
            
        
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__4_2';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__4_2',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------
        
        



        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- ---------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key2 AND --  Начисление плановых процентов
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- ---------------------------------------------------------------------
        
            
        
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__4_2';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__4_2',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------
        
        




        -- Считаем операционный баланс для 76_03_02
        -- -------------------------------------------------------------------------
        v__account_key_76_03_02 = 63; -- Номер счета для 76_03_02
	    v__summ_76_03_02 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_02,'od_overpayment_planned_b');
        -- -------------------------------------------------------------------------




        -- Находим меньшую сумму из операционного баланса 76_03_02 и суммой факта
        -- КРОМЕ фактов в additional_value in (3,6,7) - тут принудительно списываем в отриц. сумму
        -- -------------------------------------------------------------------------
        if v__rec_fact.additional_value in (6,7) then
        	v__paym_summ=COALESCE(v__rec_fact.fact_value,0);
        else
        	v__paym_summ = LEAST(v__summ_76_03_02,COALESCE(v__rec_fact.fact_value,0));
        end if;
        -- -------------------------------------------------------------------------
        





        
        -- Вставляем запись о транзакции
        -- ---------------------------------------------------------------------
        IF v__paym_summ > 0 THEN
          INSERT INTO 
            buh.loan_transact
          (
            loan_key,
            summ,
            fact_key,
            account_transaction_key,
            transact_date,
            separate_subdivision_key
          ) 
          VALUES (
            _loan_key,
            v__paym_summ,
            v__rec_fact.fact_key,
            v__account_transaction_key,
            _date,
            v__separate_subdivision
          );
        END IF;
        -- ---------------------------------------------------------------------
        
        
        
        
      
        -- Считаем оставшуюся сумму
        -- ---------------------------------------------------------------------
        v__paym_summ2 = v__rec_fact.fact_value - v__paym_summ;
        -- ---------------------------------------------------------------------



        -- Если  осталось что-то с суммы факта
        -- ---------------------------------------------------------------------
		IF v__paym_summ2 > 0 THEN
            
            --  Считаем операционный баланс для 76_03_04
            -- -------------------------------------------------------------------------
            v__account_key_76_03_04 = 71; -- Номер счета для 76_03_04
		    v__summ_76_03_04 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_04,'n_overpayment_conditionally_b');
            -- -------------------------------------------------------------------------




            -- Находим меньшую сумму из операционного баланса 76_03_04 и суммой факта
            -- -------------------------------------------------------------------------
            v__paym_summ2 = LEAST(v__summ_76_03_04,v__paym_summ2);
            -- -------------------------------------------------------------------------
            
            -- Тогда делаем транзакцию
            -- -------------------------------------------------------------------------
            IF v__paym_summ2 > 0 THEN
               INSERT INTO 
                  buh.loan_transact
                (
                  loan_key,
                  summ,
                  fact_key,
                  account_transaction_key,
                  transact_date,
                  separate_subdivision_key
                ) 
                VALUES (
                  _loan_key,
                  v__paym_summ2,
                  v__rec_fact.fact_key,
                  v__account_transaction_key2,
                  _date,
                  v__separate_subdivision
                );
            END IF;
            -- -------------------------------------------------------------------------

		END IF;
        -- ---------------------------------------------------------------------


        
        
        
        
        
        
        
        -- Считаем оставшуюся сумму
        -- ---------------------------------------------------------------------
        v__paym_summ = v__rec_fact.fact_value - v__paym_summ - v__paym_summ2;
        -- ---------------------------------------------------------------------
        
        
        -- Если  осталось что-то с суммы факта
        -- ---------------------------------------------------------------------
		IF v__paym_summ > 0 THEN
            
            --  Считаем операционный баланс для 76_03_01
            -- -------------------------------------------------------------------------
            v__account_key_76_03_01 = 57; -- Номер счета для 76_03_01
		    v__summ_76_03_01 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_01,'n_overpayment_planned_b');
            -- -------------------------------------------------------------------------




            -- Находим меньшую сумму из операционного баланса 76_03_01 и суммой факта
            -- -------------------------------------------------------------------------
            v__paym_summ = LEAST(v__summ_76_03_01,v__paym_summ);
            -- -------------------------------------------------------------------------
            
            -- Тогда делаем транзакцию
            -- -------------------------------------------------------------------------
            IF v__paym_summ > 0 THEN
               INSERT INTO 
                  buh.loan_transact
                (
                  loan_key,
                  summ,
                  fact_key,
                  account_transaction_key,
                  transact_date,
                  separate_subdivision_key
                ) 
                VALUES (
                  _loan_key,
                  v__paym_summ,
                  v__rec_fact.fact_key,
                  v__account_transaction_key3,
                  _date,
                  v__separate_subdivision
                );
            END IF;
            -- -------------------------------------------------------------------------

		END IF;
        -- ---------------------------------------------------------------------











        
    
    END IF;
    -- -------------------------------------------------------------------------
    
    
    SELECT 
    	*
    FROM
    	buh.calc_loan__4_2_fact_18(_loan_key,_date)
    INTO
		result,
	    result_str;
        
    --принудительный перенос излишней суммы коррекции на счет клиента
	if v__rec_fact.additional_value in (3,6,7) then
    	--если оп.баланс по просроченным процентам отрицательный - то создаем проводку переноса суммы
        v__account_transaction_key=80;
        -- Операционный баланс для "Проср. % плановые на начало (76.03.02)"
        -- -------------------------------------------------------------------------
        v__account_key_76_03_02 = 63; -- Номер счета для 76_03_02
        v__summ_76_03_02 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_02,'od_overpayment_planned_b');
        -- -------------------------------------------------------------------------
		--если отриц. создаем проводку
        if v__summ_76_03_02 < 0 then
        	-- Вставляем запись о транзакции
            -- ---------------------------------------------------------------------
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              abs(v__summ_76_03_02),
              v__rec_fact.fact_key,
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            );
            -- ---------------------------------------------------------------------
        end if;
    end if;
    
        
	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__4_2_fact_18 (OID = 466364) : 
--
CREATE FUNCTION buh.calc_loan__4_2_fact_18 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
========================================================================================
           Учет коррекции по 18 факту "Выравнивание баланса плановых процентов с АСУЗ"
           используется при акционных займах
========================================================================================
</description>
<history_list>
  <item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2020</date_m><task_n>587292</task_n><author>Тихоненко В.В.</author></item>

</history_list>
*/
DECLARE
    v__count_facts INTEGER;
    v__rec_fact RECORD;
    v__separate_subdivision VARCHAR;
    v__account_transaction_key INTEGER;
    v__account_transaction_key2 INTEGER;
    v__account_transaction_key3 INTEGER;
    v__count_loan_transact INTEGER;
    v__message VARCHAR;
    v__fact_name_key INTEGER;
    v__additional_value INTEGER;
    
    v__account_key_76_03_02 INTEGER;
    v__start_76_03_02 NUMERIC;
    v__debet_76_03_02 NUMERIC;
    v__credit_76_03_02 NUMERIC;
    v__summ_76_03_02 NUMERIC;
    
    
    
    v__account_key_76_03_01 INTEGER;
    v__start_76_03_01 NUMERIC;
    v__debet_76_03_01 NUMERIC;
    v__credit_76_03_01 NUMERIC;
    v__summ_76_03_01 NUMERIC;
    
    
    
    v__account_key_76_03_04 INTEGER;
    v__start_76_03_04 NUMERIC;
    v__debet_76_03_04 NUMERIC;
    v__credit_76_03_04 NUMERIC;
    v__summ_76_03_04 NUMERIC;
    
    v__paym_summ NUMERIC;
    v__paym_summ2 NUMERIC;
    
    v__permissible_loan_key INTEGER;
    v__creation_date DATE;
    v__max_date_period DATE;
    v__min_date_period DATE;
    v__summ_max_date NUMERIC;
    v__tr_sum NUMERIC;
    
/*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
    v__rec__first_payment_schedule RECORD; -- Признак того, что необходимо при расчете брать первоначальный график платежей, из договора
*/
BEGIN
/*
Коррекция процентов плановых
calc_4_2 - сделать заглушку

*/
	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,_date) THEN
        result = -1;
        result_str = 'Ошибка при проверке балансовой величины';
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------


	-- Устанавливаем тип факта
    -- -------------------------------------------------------------------------
	v__fact_name_key = 18;  --Регистрация факта создания коррекции(переплата)
    -- -------------------------------------------------------------------------
    

/*
	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

	-- Получаем настройку - нужно ли брать первоначальны график платежей, 
    -- тот что при заклчении договора
	-- -------------------------------------------------------------------------
    v__rec__first_payment_schedule  = buh.get_calc_first_payment_schedule(_loan_key);
	-- -------------------------------------------------------------------------
*/
    

    -- Получаем факт на текущую дату для указанного займа
    -- -------------------------------------------------------------------------
    --FOR v__rec_fact IN
    SELECT --*, -1*t.value AS fact_value
       MAX(t.fact_key) AS fact_key,
       -1*SUM(t.value) AS fact_value
    FROM 
        facts.facts t
    WHERE
        t.fact_name_key IN (18) AND
        t.loan_key = _loan_key AND 
        t.fact_date = _date --AND
        --t.additional_value != 4
        
		AND 
        t.additional_value NOT IN (4,8)
        
    INTO
        v__rec_fact;	
    --LOOP
    
    -- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_facts = ROW_COUNT;
    
    IF v__rec_fact.fact_key  IS NULL THEN
        result = 0;
        result_str = 'OK';        
        RETURN;      
    END IF;
      -- Устанавливаем код транзакции
      -- -------------------------------------------------------------------------
      v__account_transaction_key=69;
      v__account_transaction_key2=75;
      v__account_transaction_key3=33;        
      -- ------------------------------------------------------------------------
      
      
      -- Получаем код подразделения для текущей даты
      -- ---------------------------------------------------------------------
      v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
      -- ---------------------------------------------------------------------
          
          





      -- Получаем v__permissible_loan_key
      -- Получаем дату создания займа
      -- -------------------------------------------------------------------------
      /*
      
      Код верен в том случае, если данные из АСУЗа корректные. 
      Но если они битые, тогжа стоит применять
      buh.get_permissible_loan_for_date
      Согласно задаче #587292 

      */
      SELECT 
      --	t.permissible_loan_key,  
          t.creation_date
      FROM 
          asuz_db.loans t
      WHERE 
          t.loan_key = _loan_key
      INTO 
      --	v__permissible_loan_key, 
          v__creation_date;
          
      v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key,_date);

      -- -------------------------------------------------------------------------


          	
      -- Получаем минимальную дату из тех, что больше указанной даты
      -- -------------------------------------------------------------------------
      SELECT MIN(COALESCE(ro.payment_date_ori,t.payment_date))
      FROM asuz_db.repayment_schedules t 
      LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
    		on ro.repayment_schedule_key=t.repayment_schedule_key
      WHERE t.permissible_loan_key = v__permissible_loan_key
      AND t.annuitet_date >= _date
      INTO v__max_date_period;
      
      /*
      Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
      
      SELECT MIN(t.annuitet_date)
      FROM asuz_db.repayment_schedules t 
      WHERE t.permissible_loan_key = v__permissible_loan_key
      AND t.annuitet_date >= _date
      INTO v__max_date_period;
      */
      --RAISE NOTICE 'v__max_date_period = %',v__max_date_period;
      -- -------------------------------------------------------------------------
      
      
    /*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
          
          
	-- Получаем минимальную дату из тех, что больше указанной даты
    -- если стоит настройка
    -- Рассчитать по первому графику платежей на момент выдачи
    -- -------------------------------------------------------------------------
	IF v__rec__first_payment_schedule.result = 1 THEN
      SELECT MIN(t.payment_date)
      FROM asuz_db.tmp_payment_schedules t 
      WHERE 
      	t.application_key = v__rec__first_payment_schedule.application_key
      	AND 
        t.payment_date >= _date
	  INTO v__max_date_period;
    END IF;
    -- -------------------------------------------------------------------------
	*/

      -- Получаем минимальную дату из тех, что больше указанной даты
      -- -------------------------------------------------------------------------
      SELECT MAX(COALESCE(ro.payment_date_ori,t.payment_date))
      FROM asuz_db.repayment_schedules t 
      LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
    		on ro.repayment_schedule_key=t.repayment_schedule_key
      WHERE t.permissible_loan_key = v__permissible_loan_key
      AND t.annuitet_date < _date
      INTO v__min_date_period;
      /*
      Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
      
      SELECT MAX(t.annuitet_date)
      FROM asuz_db.repayment_schedules t 
      WHERE t.permissible_loan_key = v__permissible_loan_key
      AND t.annuitet_date < _date
      INTO v__min_date_period;
      */
      v__min_date_period = COALESCE(v__min_date_period,v__creation_date);
      -- -------------------------------------------------------------------------
          
    
    /*  
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

	-- Получаем минимальную дату из тех, что больше указанной даты
    -- если стоит настройка
    -- Рассчитать по первому графику платежей на момент выдачи
    -- -------------------------------------------------------------------------
	IF v__rec__first_payment_schedule.result = 1 THEN
      SELECT MAX(t.payment_date)
      FROM asuz_db.tmp_payment_schedules t 
      WHERE 
      	t.application_key = v__rec__first_payment_schedule.application_key
      	AND 
        t.payment_date < _date
	  INTO v__min_date_period;
      v__min_date_period = COALESCE(v__min_date_period,v__creation_date);
    END IF;
    -- -------------------------------------------------------------------------
    */
    
    
          
	
      -- Получаем сумму процентов на дату v__max_date_period
      -- -------------------------------------------------------------------------
      SELECT t.overpayment_sum
      FROM asuz_db.repayment_schedules t 
      WHERE t.permissible_loan_key = v__permissible_loan_key
      AND t.annuitet_date = v__max_date_period
      INTO v__summ_max_date;
      -- -------------------------------------------------------------------------
     
      
    /*  
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

	-- Получаем сумму процентов на дату v__max_date_period
    -- если стоит настройка
    -- Рассчитать по первому графику платежей на момент выдачи
    -- -------------------------------------------------------------------------
	IF v__rec__first_payment_schedule.result = 1 THEN
      SELECT t.overpayment_sum
      FROM asuz_db.tmp_payment_schedules t 
      WHERE 
      	t.application_key = v__rec__first_payment_schedule.application_key
      	AND 
        t.payment_date = v__max_date_period
	  INTO v__summ_max_date;
    END IF;
    -- -------------------------------------------------------------------------
    */          
          
      
      
      -- Получаем сумму транзакция за период между графиком платежей
      SELECT
       sum(t.summ)
      FROM
          buh.loan_transact t
      WHERE t.loan_key = _loan_key AND
          t.transact_date > v__min_date_period
          AND
          t.transact_date <= v__max_date_period
          AND
          t.account_transaction_key IN (10)
          AND t.active = 1      
      INTO
          v__tr_sum;
              
      --если v__min_date_period < 01.09.2017 то надо добавить еще баланс на конец от 31,08,2017 по полю n_overpayment_planned_e
      if v__min_date_period < '01.09.2017' then
          select b.n_overpayment_planned_e
          from buh.balance_value b
          where b.loan_key=_loan_key
              and b.active=1
              and b.transact_date='31.08.2017'
          into v__paym_summ;
              
          v__tr_sum=coalesce(v__tr_sum,0)+coalesce(v__paym_summ,0);
      end if;
          
              
       --raise notice '%,%,%',v__min_date_period,v__max_date_period,v__tr_sum;


      -- Получаем количество активных транзакций на тукущую дату 
      -- для указанного займа по коду
      -- ---------------------------------------------------------------------
      SELECT COUNT(*) 
      FROM buh.loan_transact t
      WHERE 
          t.loan_key = _loan_key AND
          t.account_transaction_key = v__account_transaction_key AND --  Начисление плановых процентов
          t.active = 1 AND
          t.transact_date = _date
      INTO
          v__count_loan_transact;
      -- ---------------------------------------------------------------------
          
              
          
      -- Если уже найдена активная искомая транзакция = Ошибка
      -- ---------------------------------------------------------------------
      IF v__count_loan_transact > 0 THEN
          v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__4_2_fact_18';
          PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__4_2_fact_18',v__message);
          result = -1;
          result_str = v__message;
          RETURN;
      END IF;
      -- ---------------------------------------------------------------------
          
          


      -- -------------------------------------------------------------------------
      v__paym_summ = COALESCE(v__tr_sum,0) - (v__summ_max_date - COALESCE(v__rec_fact.fact_value,0));
      -- -------------------------------------------------------------------------
      /*
      сумма в асузе по графику платежей (ближайший плановый платеж, больший)
      вычитаем из сумму коррекции
      и вычитаем сумму факта
      И эта сумма можна записать в лоан транзакт
      */
          

--raise notice '%,%,%',v__paym_summ,v__account_transaction_key, v__tr_sum;
      IF v__paym_summ = 0 THEN
        result = 0;
        result_str = 'OK';        
        RETURN;
      END IF;
          
      IF v__paym_summ < 0 THEN
          --v__message = 'Обнаружена v__paym_summ<0 для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__4_2_fact_18';
          --PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__4_2_fact_18',v__message);
          --result = -1;
          --result_str = v__message;
          --RETURN;
      END IF;
          
          
      -- Считаем операционный баланс для 76_03_02
      -- -------------------------------------------------------------------------
      v__account_key_76_03_02 = 63; -- Номер счета для 76_03_02
      v__summ_76_03_02 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_02,'od_overpayment_planned_b');
      -- -------------------------------------------------------------------------
      --raise notice '%, %', v__summ_76_03_02, v__paym_summ;



      -- Находим меньшую сумму из операционного баланса 76_03_02 и суммой факта
      -- -------------------------------------------------------------------------
      v__paym_summ2 = LEAST(v__summ_76_03_02,v__paym_summ);
      -- -------------------------------------------------------------------------           
      if v__paym_summ2>0 then
          -- Вставляем запись о транзакции
          -- ---------------------------------------------------------------------
          INSERT INTO 
            buh.loan_transact
          (
            loan_key,
            summ,
            fact_key,
            account_transaction_key,
            transact_date,
            separate_subdivision_key
          ) 
          VALUES (
            _loan_key,
            v__paym_summ,
            v__rec_fact.fact_key,
            v__account_transaction_key,
            _date,
            v__separate_subdivision
          );
          -- ---------------------------------------------------------------------
      end if;
          
      v__paym_summ=v__paym_summ-v__paym_summ2;
          
      if v__paym_summ > 0 then
          --  Считаем операционный баланс для 76_03_04
          -- -------------------------------------------------------------------------
          v__account_key_76_03_04 = 71; -- Номер счета для 76_03_04
          v__summ_76_03_04 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_04,'n_overpayment_conditionally_b');
          -- -------------------------------------------------------------------------




          -- Находим меньшую сумму из операционного баланса 76_03_04 и суммой факта
          -- -------------------------------------------------------------------------
          v__paym_summ2 = LEAST(v__summ_76_03_04,v__paym_summ);
          -- -------------------------------------------------------------------------
              
          -- Тогда делаем транзакцию
          -- -------------------------------------------------------------------------
          IF v__paym_summ2 > 0 THEN
             INSERT INTO 
                buh.loan_transact
              (
                loan_key,
                summ,
                fact_key,
                account_transaction_key,
                transact_date,
                separate_subdivision_key
              ) 
              VALUES (
                _loan_key,
                v__paym_summ2,
                v__rec_fact.fact_key,
                v__account_transaction_key2,
                _date,
                v__separate_subdivision
              );
          END IF;
          -- -------------------------------------------------------------------------
          v__paym_summ=v__paym_summ-v__paym_summ2;
              
          --  Считаем операционный баланс для 76_03_01
          -- -------------------------------------------------------------------------
          v__account_key_76_03_01 = 57; -- Номер счета для 76_03_01
          v__summ_76_03_01 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_01,'n_overpayment_planned_b');
          -- -------------------------------------------------------------------------




          -- Находим меньшую сумму из операционного баланса 76_03_01 и суммой факта
          -- -------------------------------------------------------------------------
          v__paym_summ2 = LEAST(v__summ_76_03_01,v__paym_summ);
          -- -------------------------------------------------------------------------
              
          -- Тогда делаем транзакцию
          -- -------------------------------------------------------------------------
          IF v__paym_summ2 > 0 THEN
             INSERT INTO 
                buh.loan_transact
              (
                loan_key,
                summ,
                fact_key,
                account_transaction_key,
                transact_date,
                separate_subdivision_key
              ) 
              VALUES (
                _loan_key,
                v__paym_summ,
                v__rec_fact.fact_key,
                v__account_transaction_key3,
                _date,
                v__separate_subdivision
              );
          END IF;
          -- -------------------------------------------------------------------------
          
      
      END IF;
      -- -------------------------------------------------------------------------
    --END LOOP;  

	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__4_3 (OID = 466366) : 
--
CREATE FUNCTION buh.calc_loan__4_3 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
========================================================================================
           Коррекция доначисленные процентов     
========================================================================================
</description>
<history_list>
    <item><date_m>07.05.2017</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
    <item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__count_facts INTEGER;
    v__rec_fact RECORD;
    v__separate_subdivision VARCHAR;
    v__account_transaction_key INTEGER;
    v__count_loan_transact INTEGER;
    v__message VARCHAR;
    v__fact_name_key INTEGER;
    v__additional_value INTEGER;
    
    v__account_key_76_03_03 INTEGER;
    v__op_palance_76_03_03 NUMERIC;
    
    v__ob__od_overpayment_for_delay_b NUMERIC;
BEGIN
/*
Коррекция доначисленных процентов
calc_4_3 - сделать заглушку

*/

	-- Устанавливаем тип факта
    -- -------------------------------------------------------------------------
	v__fact_name_key = 8; -- Регистрация факта создания коррекции(проценты за просрочку)
    -- -------------------------------------------------------------------------
    


    

    -- Получаем факт на текущую дату для указанного займа
    -- -------------------------------------------------------------------------
    SELECT *, -1*t.value AS fact_value
    FROM 
        facts.facts t
    WHERE
        t.fact_name_key IN (v__fact_name_key) AND
        t.loan_key = _loan_key AND 
        t.fact_date = _date AND
        --t.additional_value != 4
        t.additional_value NOT IN (4,8)
    INTO
        v__rec_fact;	
    -- -------------------------------------------------------------------------
    
    
    
	-- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_facts = ROW_COUNT;
    -- -------------------------------------------------------------------------


	-- Устанавливаем код транзакции
    -- -------------------------------------------------------------------------
    v__additional_value = v__rec_fact.additional_value;
    v__account_transaction_key=CASE v__additional_value
        -- Корректировка доначисленных процентов за просрочку ввиду идентификации невыясненного платежа
        WHEN 2 THEN 51
        -- Списание долга (проценты за просрочку) в случае смерти
        WHEN 3 THEN 28
        -- Цессия (проценты за просроку)
        WHEN 5 THEN 37
        -- Прощение коллекторами (проценты за просрочку)
        WHEN 6 THEN 31
        -- Все  остальное
        ELSE 34
    END;
	-- -------------------------------------------------------------------------

    
    -- Если найдены таковые факты, тогда продолжаем рассчет
    -- -------------------------------------------------------------------------
    IF v__count_facts > 0 THEN
    
    
        -- Получаем код подразделения для текущей даты
        -- ---------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- ---------------------------------------------------------------------
        
        

        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- ---------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND --  Начисление плановых процентов
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- ---------------------------------------------------------------------
        
            
        
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__4_3';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__4_3',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------
        

		v__ob__od_overpayment_for_delay_b = buh.get_op_balance(_loan_key, _date,59,'od_overpayment_for_delay_b');
        
        IF v__ob__od_overpayment_for_delay_b > 0 THEN
        	v__ob__od_overpayment_for_delay_b = LEAST(v__ob__od_overpayment_for_delay_b,v__rec_fact.fact_value);
                
            -- Вставляем запись о транзакции
            -- ---------------------------------------------------------------------
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              v__ob__od_overpayment_for_delay_b,
              v__rec_fact.fact_key,
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            );
            -- ---------------------------------------------------------------------
            
        END IF;

        

        
    
    END IF;
    -- -------------------------------------------------------------------------
    
    --принудительный перенос излишней суммы коррекции на счет клиента
	if v__rec_fact.additional_value in (3,6,7) then
    	--если оп.баланс по доначисленным процентам отрицательный - то создаем проводку переноса суммы
        v__account_transaction_key=79;
        -- Операционный баланс для "Проср. % доначисленные на начало (76.03.03)"
        -- -------------------------------------------------------------------------
        v__account_key_76_03_03 = 59; -- Номер счета для 76_03_03
        v__op_palance_76_03_03 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_03,'od_overpayment_for_delay_b');
        -- -------------------------------------------------------------------------
		--если отриц. создаем проводку
        if v__op_palance_76_03_03 < 0 then
        	-- Вставляем запись о транзакции
            -- ---------------------------------------------------------------------
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              abs(v__op_palance_76_03_03),
              v__rec_fact.fact_key,
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            );
            -- ---------------------------------------------------------------------
        end if;
    end if;
    
	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__4_4 (OID = 466368) : 
--
CREATE FUNCTION buh.calc_loan__4_4 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>07.05.2017</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__count_facts INTEGER;
    v__rec_fact RECORD;
    v__separate_subdivision VARCHAR;
    v__account_transaction_key INTEGER;
    v__count_loan_transact INTEGER;
    v__message VARCHAR;
    v__fact_name_key INTEGER;
    
    v__account_key_76_05_02 INTEGER;
    v__op_palance_76_05_02 NUMERIC;
BEGIN
/*
Коррекция пени
calc_4_4 - сделать заглушку

*/

	-- Устанавливаем тип факта
    -- -------------------------------------------------------------------------
	v__fact_name_key = 9; --Регистрация факта создания коррекции(основной долг)
    -- -------------------------------------------------------------------------
    
    -- Устанавливаем код транзакции
    -- -------------------------------------------------------------------------
    v__account_transaction_key = 25; -- Корректировка пени, ввиду идентификации невыясненного платежа
    -- -------------------------------------------------------------------------

    
    
    -- Получаем факт на текущую дату для указанного займа
    -- -------------------------------------------------------------------------
    SELECT *, -1*t.value AS fact_value
    FROM 
        facts.facts t
    WHERE
        t.fact_name_key IN (v__fact_name_key) AND
        t.loan_key = _loan_key AND 
        t.fact_date = _date AND
        t.additional_value NOT IN (4,8)
    INTO
        v__rec_fact;	
    -- -------------------------------------------------------------------------
    
    
    
	-- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_facts = ROW_COUNT;
    -- -------------------------------------------------------------------------


    
    -- Если найдены таковые факты, тогда продолжаем рассчет
    -- -------------------------------------------------------------------------
    IF v__count_facts > 0 THEN
    
    
        -- Получаем код подразделения для текущей даты
        -- ---------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- ---------------------------------------------------------------------
        
        

        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- ---------------------------------------------------------------------
        SELECT COUNT(*)
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND --  Начисление плановых процентов
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- ---------------------------------------------------------------------
        
        
            
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__4_4';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__4_4',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------
        

        
        -- Вставляем запись о транзакции
        -- ---------------------------------------------------------------------
        INSERT INTO 
          buh.loan_transact
        (
          loan_key,
          summ,
          fact_key,
          account_transaction_key,
          transact_date,
          separate_subdivision_key
        ) 
        VALUES (
          _loan_key,
          v__rec_fact.fact_value,
          v__rec_fact.fact_key,
          v__account_transaction_key,
          _date,
          v__separate_subdivision
        );
        -- ---------------------------------------------------------------------
        
    
    END IF;
    -- -------------------------------------------------------------------------
    
    --принудительный перенос излишней суммы коррекции на счет клиента
	if v__rec_fact.additional_value in (3,6,7) then
    	--если оп.баланс по доначисленным процентам отрицательный - то создаем проводку переноса суммы
        v__account_transaction_key=81;
        -- Операционный баланс для "Проср. % пени на начало (76.05.02)"
        -- -------------------------------------------------------------------------
        v__account_key_76_05_02 = 77; -- Номер счета для 76_05_02
        v__op_palance_76_05_02 = buh.get_op_balance(_loan_key, _date,v__account_key_76_05_02,'od_fine_b');
        -- -------------------------------------------------------------------------
		--если отриц. создаем проводку
        if v__op_palance_76_05_02 < 0 then
        	-- Вставляем запись о транзакции
            -- ---------------------------------------------------------------------
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              abs(v__op_palance_76_05_02),
              v__rec_fact.fact_key,
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            );
            -- ---------------------------------------------------------------------
        end if;
    end if;

	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__5_1 (OID = 466370) : 
--
CREATE FUNCTION buh.calc_loan__5_1 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>07.05.2017</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__separate_subdivision VARCHAR;    
    v__account_transaction_key INTEGER;
    v__paym_summ NUMERIC;
    v__count_loan_transact  INTEGER;
    v__account_key_76_09  INTEGER;
    v__op_palance_76_09 NUMERIC;
    v__account_key_76_03_02 INTEGER;
    v__op_palance_76_03_02 NUMERIC;
    v__account_key_76_03_03 INTEGER;
    v__op_palance_76_03_03 NUMERIC;
    v__message VARCHAR;
BEGIN
/*
5.1. Погашение просроченные проценты (вначале доначисленные)

*/

	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,_date) THEN
        result = -1;
        result_str = 'Ошибка при проверке балансовой величины';
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------




    -- Устанавливаем код транзакции
    -- -------------------------------------------------------------------------
    v__account_transaction_key = 17; -- Погашение просроченные доначисленные проценты
    -- -------------------------------------------------------------------------



	-- Получаем подразделение
    -- -------------------------------------------------------------------------
    v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
    -- -------------------------------------------------------------------------


    -- Получаем количество активных транзакций на тукущую дату 
    -- для указанного займа по коду
    -- -------------------------------------------------------------------------
    SELECT COUNT(*) 
    FROM buh.loan_transact t
    WHERE 
        t.loan_key = _loan_key AND
        t.account_transaction_key = v__account_transaction_key AND
        t.active = 1 AND
        t.transact_date = _date
    INTO
        v__count_loan_transact;
    -- -------------------------------------------------------------------------
    
    
            
    -- Если уже найдена активная искомая транзакция = Ошибка
    -- -------------------------------------------------------------------------
    IF v__count_loan_transact > 0 THEN
        v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__5_1';
        PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__5_1',v__message);
        result = -1;
        result_str = v__message;
        RETURN;
    END IF;
    -- -------------------------------------------------------------------------
        
    
    





	-- Операционный баланс для "Баланс клиента на начало (76.09)"
    -- -------------------------------------------------------------------------
    v__account_key_76_09 = 56; -- Номер счета для 76_09
    v__op_palance_76_09 = buh.get_op_balance(_loan_key, _date,v__account_key_76_09,'client_balance_b');
    -- -------------------------------------------------------------------------


	-- Операционный баланс для "Проср. % доначисленные на начало (76.03.03)"
    -- -------------------------------------------------------------------------
    v__account_key_76_03_03 = 59; -- Начисленные прочие доходы по микрозаймам (в том числе по целевым микрозаймам), выданным физическим лицам
    v__op_palance_76_03_03 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_03,'od_overpayment_for_delay_b');
    -- -------------------------------------------------------------------------



	-- Абсолютные величины операционных балансов
    -- -------------------------------------------------------------------------
    v__op_palance_76_09 = abs(v__op_palance_76_09);
    v__op_palance_76_03_03 = abs(v__op_palance_76_03_03);
    -- -------------------------------------------------------------------------



	-- Находим меньшую из них
    -- -------------------------------------------------------------------------
	v__paym_summ = LEAST(v__op_palance_76_09,v__op_palance_76_03_03);
    -- -------------------------------------------------------------------------
    


            
    -- Вставляем запись о транзакции
    -- -------------------------------------------------------------------------                    
	IF v__paym_summ > 0 THEN
        INSERT INTO 
          buh.loan_transact
        (
          loan_key,
          summ,
          fact_key,
          account_transaction_key,
          transact_date,
          separate_subdivision_key
        ) 
        VALUES (
          _loan_key,
          v__paym_summ,
          0, -- Факт
          v__account_transaction_key,
          _date,
          v__separate_subdivision
        );
    END IF;
    -- -------------------------------------------------------------------------
    


	-- Операционный баланс для "Баланс клиента на начало (76.09)" после выплаты на доначисленные проыенты
    -- -------------------------------------------------------------------------
    v__account_key_76_09 = 56; -- Номер счета для 76_09
    v__op_palance_76_09 = buh.get_op_balance(_loan_key, _date,v__account_key_76_09,'client_balance_b');
    -- -------------------------------------------------------------------------
    


	-- Если операционный баланс положительный после оплаты доначисленных процентов
    -- -------------------------------------------------------------------------
	IF v__op_palance_76_09 < 0 THEN


        -- Устанавливаем код транзакции
        -- ---------------------------------------------------------------------
        v__account_transaction_key = 48; -- Погашение просроченного долга_ плановые проценты
        -- ---------------------------------------------------------------------



        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- ---------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- ---------------------------------------------------------------------
        
        
                
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__5_1';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__5_1',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------
        
    
        
        -- Операционный баланс для "Проср. % плановые на начало (76.03.02)"
        -- ---------------------------------------------------------------------
        v__account_key_76_03_02 = 63; -- ТЕХ Просроченные % плановые на начало
        v__op_palance_76_03_02 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_02,'od_overpayment_planned_b');
        -- ---------------------------------------------------------------------
            
            

        -- Абсолютные величины операционных балансов
        -- ---------------------------------------------------------------------
        v__op_palance_76_09 = abs(v__op_palance_76_09);
        v__op_palance_76_03_02 = abs(v__op_palance_76_03_02);
        -- ---------------------------------------------------------------------



        -- Находим меньшую из них
        -- ---------------------------------------------------------------------
        v__paym_summ = LEAST(v__op_palance_76_09,v__op_palance_76_03_02);
        -- ---------------------------------------------------------------------
        

                
        -- Вставляем запись о транзакции
        -- -------------------------------------------------------------------------                    
        IF v__paym_summ > 0 THEN
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              v__paym_summ,
              0, -- Факт
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            );
        END IF;
        -- -------------------------------------------------------------------------
        
    END IF;
    -- -------------------------------------------------------------------------	
    

	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__5_2 (OID = 466372) : 
--
CREATE FUNCTION buh.calc_loan__5_2 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>07.05.2017</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__separate_subdivision VARCHAR;    
    v__account_transaction_key INTEGER;
    v__paym_summ NUMERIC;
    v__count_loan_transact  INTEGER;
    v__account_key_76_09  INTEGER;
    v__op_palance_76_09 NUMERIC;
    v__account_key_58_03_02 INTEGER;
    v__op_palance_58_03_02 NUMERIC;
    v__message VARCHAR;
BEGIN
/*
5.2. Погашение просроченное тело
*/

	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,_date) THEN
        result = -1;
        result_str = 'Ошибка при проверке балансовой величины';
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------





    -- Устанавливаем код транзакции
    -- -------------------------------------------------------------------------
    v__account_transaction_key = 15; -- Погашение просроченного долга_ тело
    -- -------------------------------------------------------------------------



	-- Получаем подразделение
    -- -------------------------------------------------------------------------
    v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
    -- -------------------------------------------------------------------------


    -- Получаем количество активных транзакций на тукущую дату 
    -- для указанного займа по коду
    -- -------------------------------------------------------------------------
    SELECT COUNT(*) 
    FROM buh.loan_transact t
    WHERE 
        t.loan_key = _loan_key AND
        t.account_transaction_key = v__account_transaction_key AND
        t.active = 1 AND
        t.transact_date = _date
    INTO
        v__count_loan_transact;
    -- -------------------------------------------------------------------------
    
    
            
    -- Если уже найдена активная искомая транзакция = Ошибка
    -- -------------------------------------------------------------------------
    IF v__count_loan_transact > 0 THEN
        v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__5_2';
        PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__5_2',v__message);
        result = -1;
        result_str = v__message;
        RETURN;
    END IF;
    -- -------------------------------------------------------------------------



	-- Операционный баланс для "Баланс клиента на начало (76.09)"
    -- -------------------------------------------------------------------------
    v__account_key_76_09 = 56; -- Номер счета для 76_09
    v__op_palance_76_09 = buh.get_op_balance(_loan_key, _date,v__account_key_76_09,'client_balance_b');
    -- -------------------------------------------------------------------------



	-- Операционный баланс для "Проср. тело займа на начало (58.03.02)"
    -- -------------------------------------------------------------------------
    v__account_key_58_03_02 = 67; -- ТЕХ  Тело просроченное
    v__op_palance_58_03_02 = buh.get_op_balance(_loan_key, _date,v__account_key_58_03_02,'od_loan_b');
    -- -------------------------------------------------------------------------



	-- Абсолютные величины операционных балансов
    -- -------------------------------------------------------------------------
    v__op_palance_76_09 = abs(v__op_palance_76_09);
    v__op_palance_58_03_02 = abs(v__op_palance_58_03_02);
    -- -------------------------------------------------------------------------



	-- Находим меньшую из них
    -- -------------------------------------------------------------------------
	v__paym_summ = LEAST(v__op_palance_76_09,v__op_palance_58_03_02);
    -- -------------------------------------------------------------------------
    


            
    -- Вставляем запись о транзакции
    -- -------------------------------------------------------------------------                    
	IF v__paym_summ > 0 THEN
        INSERT INTO 
          buh.loan_transact
        (
          loan_key,
          summ,
          fact_key,
          account_transaction_key,
          transact_date,
          separate_subdivision_key
        ) 
        VALUES (
          _loan_key,
          v__paym_summ,
          0, -- Факт
          v__account_transaction_key,
          _date,
          v__separate_subdivision
        );
    END IF;
    -- -------------------------------------------------------------------------
    


	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__5_3 (OID = 466373) : 
--
CREATE FUNCTION buh.calc_loan__5_3 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__separate_subdivision VARCHAR;    
    v__account_transaction_key INTEGER;
    v__paym_summ NUMERIC;
    v__count_loan_transact  INTEGER;
    v__account_key_76_09  INTEGER;
    v__op_palance_76_09 NUMERIC;
    v__account_key_76_05_02 INTEGER;
    v__op_palance_76_05_02 NUMERIC;
    v__message VARCHAR;
BEGIN
/*
5.3. Погашение просроченные пени
*/


	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,_date) THEN
        result = -1;
        result_str = 'Ошибка при проверке балансовой величины';
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------





    -- Устанавливаем код транзакции
    -- -------------------------------------------------------------------------
    v__account_transaction_key = 19; -- Погашение пени
    -- -------------------------------------------------------------------------



	-- Получаем подразделение
    -- -------------------------------------------------------------------------
    v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
    -- -------------------------------------------------------------------------


    -- Получаем количество активных транзакций на тукущую дату 
    -- для указанного займа по коду
    -- -------------------------------------------------------------------------
    SELECT COUNT(*) 
    FROM buh.loan_transact t
    WHERE 
        t.loan_key = _loan_key AND
        t.account_transaction_key = v__account_transaction_key AND
        t.active = 1 AND
        t.transact_date = _date
    INTO
        v__count_loan_transact;
    -- -------------------------------------------------------------------------
    
    
            
    -- Если уже найдена активная искомая транзакция = Ошибка
    -- -------------------------------------------------------------------------
    IF v__count_loan_transact > 0 THEN
        v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__5_2';
        PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__5_2',v__message);
        result = -1;
        result_str = v__message;
        RETURN;
    END IF;
    -- -------------------------------------------------------------------------



	-- Операционный баланс для "Баланс клиента на начало (76.09)"
    -- -------------------------------------------------------------------------
    v__account_key_76_09 = 56; -- Номер счета для 76_09
    v__op_palance_76_09 = buh.get_op_balance(_loan_key, _date,v__account_key_76_09,'client_balance_b');
    -- -------------------------------------------------------------------------



	-- Операционный баланс для "Проср. % пени на начало (76.05.02)"
    -- -------------------------------------------------------------------------
    v__account_key_76_05_02 = 77; -- ТЕХ Просроченные % пени
    v__op_palance_76_05_02 = buh.get_op_balance(_loan_key, _date,v__account_key_76_05_02,'od_fine_b');
    -- -------------------------------------------------------------------------



	-- Абсолютные величины операционных балансов
    -- -------------------------------------------------------------------------
    v__op_palance_76_09 = abs(v__op_palance_76_09);
    v__op_palance_76_05_02 = abs(v__op_palance_76_05_02);
    -- -------------------------------------------------------------------------



	-- Находим меньшую из них
    -- -------------------------------------------------------------------------
	v__paym_summ = LEAST(v__op_palance_76_09,v__op_palance_76_05_02);
    -- -------------------------------------------------------------------------
    


            
    -- Вставляем запись о транзакции
    -- -------------------------------------------------------------------------                    
	IF v__paym_summ > 0 THEN
        INSERT INTO 
          buh.loan_transact
        (
          loan_key,
          summ,
          fact_key,
          account_transaction_key,
          transact_date,
          separate_subdivision_key
        ) 
        VALUES (
          _loan_key,
          v__paym_summ,
          0, -- Факт
          v__account_transaction_key,
          _date,
          v__separate_subdivision
        );
    END IF;
    -- -------------------------------------------------------------------------
    


	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__5_4 (OID = 466374) : 
--
CREATE FUNCTION buh.calc_loan__5_4 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
========================================================================================
           5.4. Погашение плановые проценты
           --5.4.-5.5. выполняется строго в дату исполнения очередных обязательств
========================================================================================
</description>
<history_list>
  <item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2020</date_m><task_n>587292</task_n><author>Тихоненко В.В.</author></item>

</history_list>
*/

DECLARE
    v__separate_subdivision VARCHAR;
    v__account_transaction_key INTEGER;
    v__count_loan_transact INTEGER;    
    v__message VARCHAR;
    v__paym_summ NUMERIC;
    v__count_repayment_schedules INTEGER;
    v__permissible_loan_key INTEGER;
    
    
    v__account_key_76_09 INTEGER;
    v__start_76_09 NUMERIC;
    v__debet_76_09 NUMERIC;
    v__credit_76_09 NUMERIC;
    v__summ_76_09 NUMERIC;
    
    
    v__account_key_76_03_04 INTEGER;
    v__start_76_03_04 NUMERIC;
    v__debet_76_03_04 NUMERIC;
    v__credit_76_03_04 NUMERIC;
    v__summ_76_03_04 NUMERIC;
    
    v__account_key_76_03_01 INTEGER;
    v__start_76_03_01 NUMERIC;
    v__debet_76_03_01 NUMERIC;
    v__credit_76_03_01 NUMERIC;
    v__summ_76_03_01 NUMERIC;
    
    v__is_closed_date boolean;
    
    v__summ NUMERIC;
    v__count INTEGER;
    
    /*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
    
    v__rec__first_payment_schedule RECORD; -- Признак того, что необходимо при расчете брать первоначальный график платежей, из договора
	*/
BEGIN
/*
5.4. Погашение плановые проценты
*/

	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,_date) THEN
        result = -1;
        result_str = 'Ошибка при проверке балансовой величины';
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------




	/*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
    
	-- Получаем настройку - нужно ли брать первоначальны график платежей, 
    -- тот что при заклчении договора
	-- -------------------------------------------------------------------------
    v__rec__first_payment_schedule  = buh.get_calc_first_payment_schedule(_loan_key);
	-- -------------------------------------------------------------------------
	*/


	-- Получаем v__permissible_loan_key
    -- -------------------------------------------------------------------------
    /*

    SELECT t.permissible_loan_key
    FROM asuz_db.loans t
    WHERE t.loan_key = _loan_key
    INTO v__permissible_loan_key;	
	
    Код верен в том случае, если данные из АСУЗа корректные. 
    Но если они битые, тогжа стоит применять
	buh.get_permissible_loan_for_date
    Согласно задаче #587292 


    */
    v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key,_date);
    -- -------------------------------------------------------------------------
    
    
    
    -- Проверяем, является ли текущая дата - датой очередного платежа
    -- -------------------------------------------------------------------------
    SELECT count(*)
    FROM asuz_db.repayment_schedules t 
	left join asuz_db.repayment_schedules_payment_date_ori ro on ro.repayment_schedule_key=t.repayment_schedule_key
    WHERE t.loan_key = _loan_key
    AND t.permissible_loan_key = v__permissible_loan_key
    AND COALESCE(ro.payment_date_ori, t.payment_date) = _date
    INTO v__count;
    /*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
        
    SELECT count(*)
    FROM asuz_db.repayment_schedules t 
    WHERE t.loan_key = _loan_key
    AND t.permissible_loan_key = v__permissible_loan_key
    AND t.payment_date = _date
    INTO v__count;
    */
    -- -------------------------------------------------------------------------
    
    
    
    
    /*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
    
	-- Проверяем, является ли текущая дата - датой очередного платежа
    -- если стоит настройка
    -- Рассчитать по первому графику платежей на момент выдачи
    -- -------------------------------------------------------------------------
	IF v__rec__first_payment_schedule.result = 1 THEN
      SELECT count(*)
      FROM asuz_db.tmp_payment_schedules t 
      WHERE 
      	t.application_key = v__rec__first_payment_schedule.application_key
      	AND 
        t.payment_date = _date
	  INTO v__count;
    END IF;
    -- -------------------------------------------------------------------------
	*/
    
    
    
    v__is_closed_date=buh.is_loan_close_date (_loan_key,_date);
    
    
    -- Если да (теущая дата = дата платежа) 
    -- -------------------------------------------------------------------------
    IF v__count > 0 OR v__is_closed_date THEN


        -- Устанавливаем код транзакции
        -- -------------------------------------------------------------------------
        v__account_transaction_key = 47; -- Погашение срочного долга_плановые проценты
        -- -------------------------------------------------------------------------



        -- Получаем подразделение
        -- -------------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- -------------------------------------------------------------------------


        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- -------------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- -------------------------------------------------------------------------
        
        
                
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- -------------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__5_4';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__5_4',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- -------------------------------------------------------------------------
            
        
        





        -- Считаем сумму по счету 76_09  (Операционный баланс)
        -- Начало  + дебем - кредит
        -- -------------------------------------------------------------------------
        v__account_key_76_09 = 56; -- Номер счета для 76_09
        v__summ_76_09 = buh.get_op_balance(_loan_key, _date,v__account_key_76_09,'client_balance_b');
        -- -------------------------------------------------------------------------








        -- Считаем для 76_03_04 (Операционный баланс)
        -- Начало  + дебет - кредит
        -- -------------------------------------------------------------------------
        v__account_key_76_03_04 = 71; -- Номер счета для 76_03_04
        v__summ_76_03_04 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_04,'n_overpayment_conditionally_b');
        -- -------------------------------------------------------------------------
        
        
        
        -- Если 76_09 - положительный  = регистритуем ошибку
        -- -------------------------------------------------------------------------        
        IF v__summ_76_09 > 0 THEN
            v__message = 'Счет 76_09  положительный для '||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__5_4';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__5_4',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- -------------------------------------------------------------------------



        -- Абсолютные величины просчитанных сумм
        -- -------------------------------------------------------------------------
        v__summ_76_03_04 = abs(v__summ_76_03_04);
        v__summ_76_09 = abs(v__summ_76_09);
        -- -------------------------------------------------------------------------

        -- Находим меньшую из них
        -- -------------------------------------------------------------------------
        v__paym_summ = LEAST(v__summ_76_03_04,v__summ_76_09);
        -- -------------------------------------------------------------------------
        
        

                
        -- Вставляем запись о транзакции
        -- -------------------------------------------------------------------------                    
        IF v__paym_summ > 0 THEN
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              v__paym_summ,
              0, -- Факт
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            );
        END IF;
        -- -------------------------------------------------------------------------
            
    END IF;
    -- -------------------------------------------------------------------------
    
    --ЕСЛИ НАСТУПИЛА дата закрытия займа
    --списываем непросроченные проценты
    IF v__is_closed_date then
    	-- Устанавливаем код транзакции
        -- -------------------------------------------------------------------------
        v__account_transaction_key = 76; -- Погашение срочного долга_плановые проценты при закрытии
        -- -------------------------------------------------------------------------



        -- Получаем подразделение
        -- -------------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- -------------------------------------------------------------------------


        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- -------------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- -------------------------------------------------------------------------
        
        
                
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- -------------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__5_4';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__5_4',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- -------------------------------------------------------------------------
            
        
        





        -- Считаем сумму по счету 76_09  (Операционный баланс)
        -- Начало  + дебем - кредит
        -- -------------------------------------------------------------------------
        v__account_key_76_09 = 56; -- Номер счета для 76_09
        v__summ_76_09 = buh.get_op_balance(_loan_key, _date,v__account_key_76_09,'client_balance_b');
        -- -------------------------------------------------------------------------








        -- Считаем для 76_03_01 (Операционный баланс)
        -- Начало  + дебет - кредит
        -- -------------------------------------------------------------------------
        v__account_key_76_03_01 = 57; -- Номер счета для 76_03_01
        v__summ_76_03_01 = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_01,'n_overpayment_planned_b');
        -- -------------------------------------------------------------------------
        
        
        
        -- Если 76_09 - положительный  = регистритуем ошибку
        -- -------------------------------------------------------------------------        
        IF v__summ_76_09 > 0 THEN
            v__message = 'Счет 76_09  положительный для '||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__5_4';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__5_4',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- -------------------------------------------------------------------------



        -- Абсолютные величины просчитанных сумм
        -- -------------------------------------------------------------------------
        v__summ_76_03_01 = abs(v__summ_76_03_01);
        v__summ_76_09 = abs(v__summ_76_09);
        -- -------------------------------------------------------------------------

        -- Находим меньшую из них
        -- -------------------------------------------------------------------------
        v__paym_summ = LEAST(v__summ_76_03_01,v__summ_76_09);
        -- -------------------------------------------------------------------------
        
        

                
        -- Вставляем запись о транзакции
        -- -------------------------------------------------------------------------                    
        IF v__paym_summ > 0 THEN
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              v__paym_summ,
              0, -- Факт
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            );
        END IF;
        -- -------------------------------------------------------------------------
    end if;
    

	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__5_5 (OID = 466376) : 
--
CREATE FUNCTION buh.calc_loan__5_5 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
========================================================================================
           --5.5. Погашение плановое тело
           выполняется строго в дату исполнения очередных обязательств
========================================================================================
</description>
<history_list>
  <item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2020</date_m><task_n>587292</task_n><author>Тихоненко В.В.</author></item>

</history_list>
*/
DECLARE
    v__separate_subdivision VARCHAR;
    v__account_transaction_key INTEGER;
    v__count_loan_transact INTEGER;    
    v__message VARCHAR;
    v__paym_summ NUMERIC;
    v__count_repayment_schedules INTEGER;
    
    
    v__account_key_76_09 INTEGER;
    v__start_76_09 NUMERIC;
    v__debet_76_09 NUMERIC;
    v__credit_76_09 NUMERIC;
    v__summ_76_09 NUMERIC;
    
    v__account_key_58_03_01 INTEGER;
    v__start_58_03_01 NUMERIC;
    v__debet_58_03_01 NUMERIC;
    v__credit_58_03_01 NUMERIC;
    v__summ_58_03_01 NUMERIC;
    
    v__account_key_58_03 INTEGER;
    v__start_58_03 NUMERIC;
    v__debet_58_03 NUMERIC;
    v__credit_58_03 NUMERIC;
    v__summ_58_03 NUMERIC;
    
    v__is_closed_date boolean;
    
    v__summ NUMERIC;
    v__permissible_loan_key INTEGER;
    v__count INTEGER;
    /*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
    
    v__rec__first_payment_schedule RECORD; -- Признак того, что необходимо при расчете брать первоначальный график платежей, из договора
	*/

BEGIN
/*
5.5. Погашение плановое тело
*/
	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,_date) THEN
        result = -1;
        result_str = 'Ошибка при проверке балансовой величины';
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------


	/*
	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

	-- Получаем настройку - нужно ли брать первоначальны график платежей, 
    -- тот что при заклчении договора
	-- -------------------------------------------------------------------------
    v__rec__first_payment_schedule  = buh.get_calc_first_payment_schedule(_loan_key);
	-- -------------------------------------------------------------------------
	*/




	-- Получаем v__permissible_loan_key
    -- -------------------------------------------------------------------------
    /*

    SELECT t.permissible_loan_key
    FROM asuz_db.loans t
    WHERE t.loan_key = _loan_key
    INTO v__permissible_loan_key;	
	
    Код верен в том случае, если данные из АСУЗа корректные. 
    Но если они битые, тогжа стоит применять
	buh.get_permissible_loan_for_date
    Согласно задаче #587292 


    */
    v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key,_date);
    -- -------------------------------------------------------------------------
    
    
    
    -- Проверяем, является ли текущая дата - датой очередного платежа
    -- -------------------------------------------------------------------------
    SELECT count(*)
    FROM asuz_db.repayment_schedules t 
	left join asuz_db.repayment_schedules_payment_date_ori ro on ro.repayment_schedule_key=t.repayment_schedule_key
    WHERE t.loan_key = _loan_key
    AND t.permissible_loan_key = v__permissible_loan_key
    AND COALESCE(ro.payment_date_ori, t.payment_date) = _date
    INTO v__count;
    
    /*
	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

    SELECT count(*)
    FROM asuz_db.repayment_schedules t 
    WHERE t.loan_key = _loan_key
    AND t.permissible_loan_key = v__permissible_loan_key
    AND t.payment_date = _date
    INTO v__count;
    */
    -- -------------------------------------------------------------------------
    
    
    /*
	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

	-- Проверяем, является ли текущая дата - датой очередного платежа
    -- если стоит настройка
    -- Рассчитать по первому графику платежей на момент выдачи
    -- -------------------------------------------------------------------------
	IF v__rec__first_payment_schedule.result = 1 THEN
      SELECT count(*)
      FROM asuz_db.tmp_payment_schedules t 
      WHERE 
      	t.application_key = v__rec__first_payment_schedule.application_key
      	AND 
        t.payment_date = _date
	  INTO v__count;
    END IF;
    -- -------------------------------------------------------------------------
    */
    
    v__is_closed_date=buh.is_loan_close_date (_loan_key,_date);
    
    -- Если да (теущая дата = дата платежа) ИЛИ дата закрытия займа
    -- -------------------------------------------------------------------------
    IF v__count > 0 OR v__is_closed_date THEN

        -- Устанавливаем код транзакции
        -- -------------------------------------------------------------------------
        v__account_transaction_key = 13; -- Погашение срочного долга_тело
        -- -------------------------------------------------------------------------



        -- Получаем подразделение
        -- -------------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- -------------------------------------------------------------------------


        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- -------------------------------------------------------------------------
        SELECT COUNT(*)
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- -------------------------------------------------------------------------
        
        
                
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- -------------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__5_5';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__5_5',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- -------------------------------------------------------------------------
            
        
        
        



        -- Считаем сумму по счету 76_09 (Операционный баланс)
        -- -------------------------------------------------------------------------
        v__account_key_76_09 = 56; -- Номер счета для 76_09
        v__summ_76_09 = buh.get_op_balance(_loan_key, _date,v__account_key_76_09,'client_balance_b');
        -- -------------------------------------------------------------------------








        -- Считаем для 58_03_01 (Операционный баланс)
        -- -------------------------------------------------------------------------
        v__account_key_58_03_01 = 53; -- Номер счета для 58_03_01
        v__summ_58_03_01 = buh.get_op_balance(_loan_key, _date,v__account_key_58_03_01,'n_loan_b');
        -- -------------------------------------------------------------------------


        -- Абсолютные величины просчитанных сумм
        -- -------------------------------------------------------------------------
        v__summ_58_03_01 = abs(v__summ_58_03_01);
        v__summ_76_09 = abs(v__summ_76_09);
        -- -------------------------------------------------------------------------

        -- Находим меньшую из них
        -- -------------------------------------------------------------------------
        v__paym_summ = LEAST(v__summ_58_03_01,v__summ_76_09);
        -- -------------------------------------------------------------------------
        
        
        
        
                
        -- Вставляем запись о транзакции
        -- ---------------------------------------------------------------------
        IF v__paym_summ > 0 THEN
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              v__paym_summ,
              0, -- Не знаю какой факт вставлять
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            );
        END IF;
        -- ---------------------------------------------------------------------
            
    END IF;
    -- -------------------------------------------------------------------------
    
    --ЕСЛИ НАСТУПИЛА дата закрытия займа
    --списываем непросроченное тело
    if v__is_closed_date then
    	-- Устанавливаем код транзакции
        -- -------------------------------------------------------------------------
        v__account_transaction_key = 77; -- Погашение срочного долга_тело при закрытии
        -- -------------------------------------------------------------------------



        -- Получаем подразделение
        -- -------------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- -------------------------------------------------------------------------


        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- -------------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- -------------------------------------------------------------------------
        
        
                
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- -------------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__5_5';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__5_5',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- -------------------------------------------------------------------------
            
        
        





        -- Считаем сумму по счету 76_09  (Операционный баланс)
        -- Начало  + дебем - кредит
        -- -------------------------------------------------------------------------
        v__account_key_76_09 = 56; -- Номер счета для 76_09
        v__summ_76_09 = buh.get_op_balance(_loan_key, _date,v__account_key_76_09,'client_balance_b');
        -- -------------------------------------------------------------------------








        -- Считаем для 76_03_01 (Операционный баланс)
        -- Начало  + дебет - кредит
        -- -------------------------------------------------------------------------
        v__account_key_58_03 = 66; -- Номер счета для 76_03_01
        v__summ_58_03 = buh.get_op_balance(_loan_key, _date,v__account_key_58_03,'loan_b');
        -- -------------------------------------------------------------------------
        
        
        
        -- Если 76_09 - положительный  = регистритуем ошибку
        -- -------------------------------------------------------------------------        
        IF v__summ_76_09 > 0 THEN
            v__message = 'Счет 76_09  положительный для '||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__5_5';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__5_5',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- -------------------------------------------------------------------------



        -- Абсолютные величины просчитанных сумм
        -- -------------------------------------------------------------------------
        v__summ_58_03 = abs(v__summ_58_03);
        v__summ_76_09 = abs(v__summ_76_09);
        -- -------------------------------------------------------------------------

        -- Находим меньшую из них
        -- -------------------------------------------------------------------------
        v__paym_summ = LEAST(v__summ_58_03,v__summ_76_09);
        -- -------------------------------------------------------------------------
        
        

                
        -- Вставляем запись о транзакции
        -- -------------------------------------------------------------------------                    
        IF v__paym_summ > 0 THEN
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              v__paym_summ,
              0, -- Факт
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            );
        END IF;
        -- -------------------------------------------------------------------------
    end if;    


	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__6 (OID = 466378) : 
--
CREATE FUNCTION buh.calc_loan__6 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
========================================================================================
	расчет суммы просроченного долга и плановых процентов
========================================================================================
</description>
<history_list>
	<item><date_m>07.05.2017</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
	<item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
  	<item><date_m>07.02.2020</date_m><task_n>587292</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__separate_subdivision VARCHAR;    
    v__account_transaction_key INTEGER;
    v__paym_summ NUMERIC;
    v__count_loan_transact  INTEGER;
    v__count INTEGER;
    v__account_key_58_03_01  INTEGER;
    v__op_palance_58_03_01 NUMERIC;
    v__account_key_76_03_01 INTEGER;
    v__op_palance_76_03_01 NUMERIC;
    v__permissible_loan_key INTEGER;
    v__account_key_76_03_04 INTEGER;
    v__message VARCHAR;
BEGIN

	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,_date) THEN
        result = -1;
        result_str = 'Ошибка при проверке балансовой величины';
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------






	-- Получаем v__permissible_loan_key
    -- -------------------------------------------------------------------------
    /*

    SELECT t.permissible_loan_key
    FROM asuz_db.loans t
    WHERE t.loan_key = _loan_key
    INTO v__permissible_loan_key;	
	
    Код верен в том случае, если данные из АСУЗа корректные. 
    Но если они битые, тогжа стоит применять
	buh.get_permissible_loan_for_date
    Согласно задаче #587292 


    */
    v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key,_date);
    -- -------------------------------------------------------------------------
    
    
    
    -- Проверяем, является ли текущая дата - датой очередного платежа
    -- -------------------------------------------------------------------------
    SELECT count(*)
    FROM asuz_db.repayment_schedules t 
    LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
    	on ro.repayment_schedule_key=t.repayment_schedule_key
    WHERE t.loan_key = _loan_key
    AND t.permissible_loan_key = v__permissible_loan_key
    AND COALESCE(ro.payment_date_ori, t.payment_date) = _date
    INTO v__count;
   
    /*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
    
    SELECT count(*)
    FROM asuz_db.repayment_schedules t 
    WHERE t.loan_key = _loan_key
    AND t.permissible_loan_key = v__permissible_loan_key
    AND t.payment_date = _date
    INTO v__count;
    */
    -- -------------------------------------------------------------------------
    
    
    
    
    -- Если да (теущая дата = дата платежа)
    -- -------------------------------------------------------------------------
    IF v__count > 0 THEN
    

        -- Устанавливаем код транзакции
        -- ---------------------------------------------------------------------
        v__account_transaction_key = 8; -- Перевод в просроченную задолженность по телу
        -- ---------------------------------------------------------------------
        
        

        -- Получаем подразделение
        -- ---------------------------------------------------------------------
        v__separate_subdivision = buh.get_separate_subdivision(_loan_key,_date);
        -- ---------------------------------------------------------------------



        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- ---------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- ---------------------------------------------------------------------
        
        
                
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__6';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__6',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------
        


        -- Операционный баланс для "Непросроченое тело займа на начало (58.03.01)"
        -- ---------------------------------------------------------------------
        v__account_key_58_03_01 = 53; -- Микрозаймы (в том числе целевые микрозаймы), выданные физическим лицам
        v__paym_summ = buh.get_op_balance(_loan_key, _date,v__account_key_58_03_01,'n_loan_b');
        -- ---------------------------------------------------------------------
        
        
                
        -- Вставляем запись о транзакции
        -- ---------------------------------------------------------------------
        IF v__paym_summ > 0 THEN
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              v__paym_summ,
              0, -- Факт
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            );
        END IF;
        -- ---------------------------------------------------------------------
        
        
        
        -- Устанавливаем код транзакции
        -- ---------------------------------------------------------------------
        v__account_transaction_key = 9; -- Перевод в просроченную задолженность по плановым процентам
        -- ---------------------------------------------------------------------
        
        

        -- Получаем количество активных транзакций на тукущую дату 
        -- для указанного займа по коду
        -- ---------------------------------------------------------------------
        SELECT COUNT(*) 
        FROM buh.loan_transact t
        WHERE 
            t.loan_key = _loan_key AND
            t.account_transaction_key = v__account_transaction_key AND
            t.active = 1 AND
            t.transact_date = _date
        INTO
            v__count_loan_transact;
        -- ---------------------------------------------------------------------
        
        
                
        -- Если уже найдена активная искомая транзакция = Ошибка
        -- ---------------------------------------------------------------------
        IF v__count_loan_transact > 0 THEN
            v__message = 'Обнаружена активная транзакция для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' calc_loan__6';
            PERFORM buh.add_transact_calc_log(_loan_key,_date,'calc_loan__6',v__message);
            result = -1;
            result_str = v__message;
            RETURN;
        END IF;
        -- ---------------------------------------------------------------------
        


        -- Операционный баланс для "Непросроченные % на начало (76.03.04)"
        -- ---------------------------------------------------------------------
        v__account_key_76_03_04 = 71; -- Расчеты по процентам по микрозаймам (в том числе целевым микрозаймам), выданным физическим лицам
        v__paym_summ = buh.get_op_balance(_loan_key, _date,v__account_key_76_03_04,'n_overpayment_conditionally_b');
        -- ---------------------------------------------------------------------
        
                
                
        -- Вставляем запись о транзакции
        -- ---------------------------------------------------------------------
        IF v__paym_summ > 0 THEN
            INSERT INTO 
              buh.loan_transact
            (
              loan_key,
              summ,
              fact_key,
              account_transaction_key,
              transact_date,
              separate_subdivision_key
            ) 
            VALUES (
              _loan_key,
              v__paym_summ,
              0, -- Факт
              v__account_transaction_key,
              _date,
              v__separate_subdivision
            );
        END IF;
        -- ---------------------------------------------------------------------
        
        
    END IF;
    -- -------------------------------------------------------------------------
    
    
	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan__7 (OID = 466380) : 
--
CREATE FUNCTION buh.calc_loan__7 (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>07.05.2017</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__rec_balance_value RECORD;
    v__rec_loan_transact RECORD;
    v__bv__od_loan_e NUMERIC;
    v__bv__od_overpayment_planned_e NUMERIC;
    v__od_overpayment_for_delay_e NUMERIC;
    v__bv__od_overpayment_for_delay_e NUMERIC;
    v__bv__od_fine_e NUMERIC;
    v__bv__n_loan_e NUMERIC;
    v__bv__n_overpayment_planned_e NUMERIC;
    v__bv__loan_e NUMERIC;
    v__bv__client_balance_e NUMERIC;
    v__bv__n_overpayment_conditionally_e NUMERIC;
    v__bv__all_loan_percents_e NUMERIC;
    v__tsumm NUMERIC;
    v__tsumm1 NUMERIC;
    v__tsumm2 NUMERIC;
    v__tsumm3 NUMERIC;
    v__tsumm4 NUMERIC;
    v__tsumm5 NUMERIC;
    v__tsumm6 NUMERIC;
    v__tsumm7 NUMERIC;
    v__tsumm8 NUMERIC;
    v__tsumm9 NUMERIC;
    v__tsumm10 NUMERIC;
    v_summ numeric;
    v__account_transaction_key INTEGER;
    v__count_balance_value INTEGER;
    v__message VARCHAR;
BEGIN
/*
7. расчет баланса на конец дня
*/
	-- Проверка наличия балансовой величины
	-- -------------------------------------------------------------------------
    IF NOT buh.is_correct_balance_value(_loan_key,_date) THEN
        result = -1;
        result_str = 'Ошибка при проверке балансовой величины';
        RETURN;
    END IF;
	-- -------------------------------------------------------------------------





    -- Получаем тукущую запись в balance_value, она же максимальная 
    -- активная для текущего займа
    -- -------------------------------------------------------------------------
    SELECT *
    FROM buh.balance_value bv
    WHERE 
    	bv.loan_key = _loan_key AND 
        bv.active = 1 AND
        bv.transact_date = _date AND
        bv.opened
    ORDER BY bv.balance_value_key ASC
    LIMIT 1
    INTO 
    	v__rec_balance_value;
    -- -------------------------------------------------------------------------
    
	
	-- Устанавливаем балансовые величины. которые будут перенесены на конец
    -- -------------------------------------------------------------------------
    
    v__bv__od_loan_e = COALESCE(v__rec_balance_value.od_loan_b,0);
    v__bv__od_overpayment_planned_e = COALESCE(v__rec_balance_value.od_overpayment_planned_b,0);
    v__bv__od_overpayment_for_delay_e = COALESCE(v__rec_balance_value.od_overpayment_for_delay_b,0);
    v__bv__od_fine_e = COALESCE(v__rec_balance_value.od_fine_b,0);
    v__bv__n_loan_e = COALESCE(v__rec_balance_value.n_loan_b,0);
    v__bv__n_overpayment_planned_e = COALESCE(v__rec_balance_value.n_overpayment_planned_b,0);
    v__bv__loan_e = COALESCE(v__rec_balance_value.loan_b,0);
    v__bv__client_balance_e = COALESCE(v__rec_balance_value.client_balance_b,0);
    v__bv__n_overpayment_conditionally_e = COALESCE(v__rec_balance_value.n_overpayment_conditionally_b,0);
    -- -------------------------------------------------------------------------

	-- 58.03.02 = 67
	v__bv__od_loan_e                  = buh.get_op_balance(_loan_key, _date,67,'od_loan_b');
    -- 76.03.02 = 63 (встречалось и 69, исправлено )
    v__bv__od_overpayment_planned_e   = buh.get_op_balance(_loan_key, _date,63,'od_overpayment_planned_b'); 
    -- 76.03.03 = 59
	v__bv__od_overpayment_for_delay_e = buh.get_op_balance(_loan_key, _date,59,'od_overpayment_for_delay_b');
    -- 76.05.02 = 77
	v__bv__od_fine_e                  = buh.get_op_balance(_loan_key, _date,77,'od_fine_b');
    -- 58.03.01 = 53
    v__bv__n_loan_e                   = buh.get_op_balance(_loan_key, _date,53,'n_loan_b');
    -- 76.03.01 = 57 (встречалось и 62, исправлено )
	v__bv__n_overpayment_planned_e    = buh.get_op_balance(_loan_key, _date,57,'n_overpayment_planned_b');
    -- 58.03 = 66
	v__bv__loan_e                     = buh.get_op_balance(_loan_key, _date,66,'loan_b');
    -- 76.09 = 56
    v__bv__client_balance_e           = buh.get_op_balance(_loan_key, _date,56,'client_balance_b');
	-- 76.03.04 = 71 - Условно непросроченные проценты
    v__bv__n_overpayment_conditionally_e = buh.get_op_balance(_loan_key, _date,71,'n_overpayment_conditionally_b');
    -- Cумма всех начисленных процентов по займу на конец
    v__bv__all_loan_percents_e = buh.get_op_balance_all_percents(_loan_key, _date,1);
    
    
    -- Если был факт закрытия займа на текущую дату, устанавливаем cоответствующий флаг.
    -- -------------------------------------------------------------------------
    IF buh.is_loan_close_date (_loan_key,_date) THEN
        UPDATE 
          buh.balance_value  
        SET 
          opened = FALSE
        WHERE 
          balance_value_key = v__rec_balance_value.balance_value_key;   
        /*--погашение с авансового счета
        --v__bv__n_overpayment_conditionally_e
        --v__bv__n_overpayment_planned_e
        --v__bv__loan_e
        if v__bv__client_balance_e<0 and v__bv__loan_e>0 then
        	v_summ=least(abs(v__bv__client_balance_e),v__bv__loan_e);
            if v_summ>0 then
            	--техническая проводка - погашение условно непросроченных процентов с авансового счета
                
            end if;
        end if;*/
    END IF;
    -- -------------------------------------------------------------------------

        
    
    -- Обновляем данные по балансовой величине
    -- -------------------------------------------------------------------------
    UPDATE 
      buh.balance_value  
    SET 
      od_loan_e = v__bv__od_loan_e,
      od_overpayment_planned_e = v__bv__od_overpayment_planned_e,
      od_overpayment_for_delay_e = v__bv__od_overpayment_for_delay_e,
      od_fine_e = v__bv__od_fine_e,
      n_loan_e = v__bv__n_loan_e,
      n_overpayment_planned_e = v__bv__n_overpayment_planned_e,
      loan_e = v__bv__loan_e,
      client_balance_e = v__bv__client_balance_e,
      n_overpayment_conditionally_e = v__bv__n_overpayment_conditionally_e,
      all_loan_percents_e = v__bv__all_loan_percents_e
    WHERE 
      balance_value_key = v__rec_balance_value.balance_value_key;
    -- -------------------------------------------------------------------------
    

    
    -- Если все балансовые величины равны 0 (кроме client_balance)
    -- Определяем займ как закрытый
    -- -------------------------------------------------------------------------
    IF 	v__bv__od_loan_e = 0 					AND
	    v__bv__od_overpayment_planned_e = 0 	AND
	    v__bv__od_overpayment_for_delay_e = 0 	AND
	    v__bv__od_fine_e = 0 					AND
	    v__bv__n_loan_e = 0 					AND
	    v__bv__n_overpayment_planned_e = 0 	AND
        v__bv__loan_e = 0 						
    THEN 
        UPDATE 
          buh.balance_value  
        SET 
          opened = FALSE
        WHERE 
          balance_value_key = v__rec_balance_value.balance_value_key;   
    END IF;
    -- -------------------------------------------------------------------------
    
    
    
    
    
    
    
	-- По идее нужно делать  балансовую вуличину на следующий день. 
    -- Но не уверен, что это нужно делать здесь

	result = 1;
	result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan_day (OID = 466382) : 
--
CREATE FUNCTION buh.calc_loan_day (
  _loan_key integer,
  _date date,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>10.10.2017</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
  	v__arr_functions VARCHAR[];
    v__call_function VARCHAR;
    v__sql VARCHAR;
    v__rec RECORD;
    v__rec_dates RECORD;
BEGIN

	-- Установка неактивыми данных на текущую дату.
    -- После тестирования - убрать.
	-- -------------------------------------------------------------------------
    PERFORM buh.clean_loan_day_activity(_loan_key,_date);
	-- -------------------------------------------------------------------------


	result = 1;
	result_str = 'OK';


	-- Определяем список функция для их вызова в цикле
	-- -------------------------------------------------------------------------
	v__arr_functions = ARRAY[
    	'calc_loan__0', --Переносим балансовую величину с предыдуще годня
        /*
        1. Учесть выдачу займа и создать первую балансовую запись.
        Берем все факты "выдача" за период 5 дней и для каждого факта проверяем наличие проводки "выдача займа".
        Создаем проводку для выдачи и запись в таблице баланс с нулевыми значениями.
        */
    	'calc_loan__1',
        
        /*        
        1.1. Расчет доначисленных % и пени
        Смотрим баланс 58.03.02 на начало опер. дня, и делаем расчет % и пени на конец опер. дня.
        */
        'calc_loan__1_1',      
        
        /*      
        2. Учесть начисление плановых процентов.
        Найти временной интервал по графику обязательств где мы находимся на дату анализа
        Определяем кол-во дней в интервале и сумму плановых процентов в очередном обязательстве.
        Делим одно на второе и получаем сумму процентов плановых к начислению за день.
        */
        'calc_loan__2',
        
        'calc_loan__3', --3. Учет поступлений (поиск факта, регистрация факта в виде проводки.)
						  --4. Учет коррекций
        'calc_loan__4_1', --4.1. Коррекция основного долга
        'calc_loan__4_2', --4.2. Коррекция процентов плановых     
        'calc_loan__4_3', --4.3. Коррекция доначисленные процентов     
        'calc_loan__4_4', --4.4. Коррекция пени
						  --5. Погашение обязательств (проверка баланса счета клиента 76.09)
        'calc_loan__5_1', --5.1. Погашение просроченные проценты (вначале доначисленные)
        'calc_loan__5_2', --5.2. Погашение просроченное тело
        'calc_loan__5_3', --5.3. Погашение просроченные пени
					      --5.1 - 5.3. выполняется ежедневно.
        'calc_loan__5_4', --5.4. Погашение плановые проценты
        'calc_loan__5_5', --5.5. Погашение плановое тело
        				  --5.4.-5.5. выполняется строго в дату исполнения очередных обязательств
        'calc_loan__6', --6. расчет суммы просроченного долга и плановых процентов
        'calc_loan__7' --7. расчет баланса на конец дня
    ];
	-- -------------------------------------------------------------------------
    

       
            
        
    -- Перебираем циклом функции в массиве
    -- ---------------------------------------------------------------------
    FOREACH v__call_function IN ARRAY v__arr_functions
    LOOP                
            
            
        -- ФОрмируем и вызываем функцию
        -- -----------------------------------------------------------------
        v__sql = 'SELECT * FROM buh.'||COALESCE(v__call_function,'')||'('||COALESCE(_loan_key::VARCHAR,'')||','''||COALESCE(_date::VARCHAR,'')||''')';
        EXECUTE v__sql 
        INTO result, result_str;
        -- -----------------------------------------------------------------
            
            
        -- Если функция отработала с ошибкой - выходим из рассчета
        -- -----------------------------------------------------------------
        IF result = -1 THEN 
            --RETURN; 
            CONTINUE;
        END IF;
        -- -----------------------------------------------------------------
            
    END LOOP;
    -- ---------------------------------------------------------------------



    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function clean_loan_day_activity (OID = 466384) : 
--
CREATE FUNCTION buh.clean_loan_day_activity (
  _loan_key integer,
  _date date
)
RETURNS integer
AS 
$body$
/*
<history_list>
<item><date_m>10.10.2017</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE

BEGIN

	-- Обнуляем активность всех транзакций для текущего займа в указанный период
	-- -------------------------------------------------------------------------
    UPDATE 
      buh.loan_transact  
    SET 
      active = 0
    WHERE 
      loan_key = _loan_key
      AND
      transact_date = _date;
	-- -------------------------------------------------------------------------      


	-- Обнуляем активность всех балансовых величин для текущего займа в указанный период
	-- -------------------------------------------------------------------------
    UPDATE 
      buh.balance_value  
    SET 
      active = 0
    WHERE 
      loan_key = _loan_key
      AND
      transact_date = _date;
	-- -------------------------------------------------------------------------
      
    RETURN 1;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function clean_loan_period_activity (OID = 466387) : 
--
CREATE FUNCTION buh.clean_loan_period_activity (
  _loan_key integer,
  _date_start date,
  _date_end date
)
RETURNS integer
AS 
$body$
/*
<history_list>
<item><date_m>10.10.2017</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE

BEGIN

	-- Обнуляем активность всех транзакций для текущего займа в указанный период
	-- -------------------------------------------------------------------------
    UPDATE 
      buh.loan_transact  
    SET 
      active = 0
    WHERE 
      loan_key = _loan_key
      AND
      transact_date >= _date_start
      AND
      transact_date <= _date_end;
	-- -------------------------------------------------------------------------      


	-- Обнуляем активность всех балансовых величин для текущего займа в указанный период
	-- -------------------------------------------------------------------------
    UPDATE 
      buh.balance_value  
    SET 
      active = 0
    WHERE 
      loan_key = _loan_key
      AND
      transact_date >= _date_start
      AND
      transact_date <= _date_end;
	-- -------------------------------------------------------------------------
      
    RETURN 1;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function fn__get_buh_report (OID = 466388) : 
--
CREATE FUNCTION buh.fn__get_buh_report (
  _date date,
  out loan_key integer,
  out subdivision_key integer,
  out traffic character varying,
  out total_balance_b numeric,
  out loan_balance_b numeric,
  out loan_balance_b_overdue numeric,
  out loan_balance_b_due numeric,
  out interest_balance_b numeric,
  out fine_balance_b numeric,
  out revenue_0_first numeric,
  out revenue_0_second numeric,
  out revenue_1_first numeric,
  out revenue_1_second numeric,
  out revenue_for_delay_0_first numeric,
  out revenue_for_delay_0_second numeric,
  out discount_0_first numeric,
  out discount_0_second numeric,
  out discount_1_first numeric,
  out discount_1_second numeric,
  out discount_0_first_for_delay numeric,
  out discount_0_second_for_delay numeric,
  out fine_calc numeric,
  out debt_relief_death_loan numeric,
  out debt_relief_death_interest numeric,
  out debt_relief_death_interest_for_delay numeric,
  out debt_relief_death_fine numeric,
  out debt_relief_cession_loan numeric,
  out debt_relief_cession_interest numeric,
  out debt_relief_cession_interest_for_delay numeric,
  out debt_relief_cession_fine numeric,
  out debt_relief_collector_loan numeric,
  out debt_relief_collector_interest numeric,
  out debt_relief_collector_interest_for_delay numeric,
  out debt_relief_collector_fine numeric,
  out debt_relief_exclusive_cases_loan numeric,
  out debt_relief_exclusive_cases_interest numeric,
  out debt_relief_exclusive_cases_interest_for_delay numeric,
  out debt_relief_exclusive_cases_fine numeric,
  out insurance_calc_revenue numeric,
  out payment_loan_current numeric,
  out payment_loan_avance numeric,
  out payment_interest_current numeric,
  out payment_interest_avance numeric,
  out payment_interest_for_delay_current numeric,
  out payment_fine numeric,
  out payment_advance_payment numeric,
  out payment_paid_more numeric,
  out payment_unfound numeric,
  out total_balance_e numeric,
  out loan_balance_e numeric,
  out loan_balance_e_overdue numeric,
  out loan_balance_e_due numeric,
  out interest_balance_e numeric,
  out fine_balance_e numeric
)
RETURNS SETOF record
AS 
$body$
DECLARE
	v__rec RECORD;
    v__rec_loans RECORD;
    v__rec_month_b RECORD;
    v__loan_key INTEGER;
    v__count_balance_value INTEGER;
    v__month_start_date DATE;
    v__month_end_date DATE;
    is_return_next BOOLEAN;
    revenue_0_summ NUMERIC;
    revenue_1_summ NUMERIC;
    revenue_for_delay_0_summ NUMERIC;
    _fine_sum NUMERIC;
    v__contract VARCHAR;
    is_first_contract BOOLEAN;
    v__contract_last_2_simbols VARCHAR;
    
    debt_relief_death_loan_summ NUMERIC;
    debt_relief_death_interest_summ NUMERIC;
    debt_relief_death_interest_for_delay_summ NUMERIC;
    payment_loan_current_summ NUMERIC;
    payment_interest_current_summ NUMERIC;
    payment_interest_for_delay_current_summ NUMERIC;
    payment_fine_summ NUMERIC;
BEGIN
	v__month_start_date = date_trunc('month', _date);

/*
Проверка что дта  - отчетная дата
или приводилась 
Лимит

условия отбора
*/
	-- Проверка даты на то, что она  - конец месяца
	-- =========================================================================
    v__month_end_date = v__month_start_date + interval '1 month' - interval '1 day';
    IF v__month_end_date != _date THEN
    	_date = v__month_end_date;
    END IF;
	-- =========================================================================
    --RAISE NOTICE '%',v__month_start_date;
    --RAISE NOTICE '%',v__month_end_date;


	FOR v__rec_loans IN
	    SELECT DISTINCT 
        	t.loan_key
        FROM
        	buh.balance_value t
        JOIN loans l ON t.loan_key = l.loan_key    
        WHERE
        	t.transact_date >=v__month_start_date
            AND 
            t.transact_date <=v__month_end_date
            AND
            t.active = 1
            --временный костыль навсегда
            AND t.client_balance_e <= 0
            
            --
        --    AND t.loan_key > 1000000
        --AND l.subdivision_key = 116
        --LIMIT 100
    LOOP
    	is_return_next = FALSE;
	    v__loan_key = v__rec_loans.loan_key;
    	SELECT 
	        *
        FROM
        	buh.balance_value t
        WHERE 
        	t.transact_date = v__month_end_date
            AND
            t.loan_key = v__loan_key
            AND
            t.active = 1
        INTO
        	v__rec;
        
	    GET DIAGNOSTICS v__count_balance_value = ROW_COUNT;
        
        IF v__count_balance_value = 0 THEN
            SELECT 
                *
            FROM
                buh.balance_value t
            WHERE 
                t.transact_date <= v__month_end_date
                AND
                t.transact_date >= v__month_start_date
                AND
                t.loan_key = v__loan_key
                AND
                t.active = 1
            ORDER BY
            	t.transact_date DESC
            LIMIT 
            	1
            INTO
                v__rec;
                
			GET DIAGNOSTICS v__count_balance_value = ROW_COUNT;
        END IF;
        
        IF v__count_balance_value > 0 THEN

    		loan_key = v__rec.loan_key;
            subdivision_key = 0;
            traffic = 'аннуитетный';
            total_balance_b = 0;
            loan_balance_b = 0;
            loan_balance_b_overdue = 0;
            loan_balance_b_due = 0;
            interest_balance_b = 0;
            fine_balance_b = 0;
            revenue_0_first = 0;
            revenue_0_second = 0;
            revenue_1_first = 0;
            revenue_1_second = 0;
            revenue_for_delay_0_first = 0;
            revenue_for_delay_0_second = 0;
            discount_0_first = 0;
            discount_0_second = 0;
            discount_1_first = 0;
            discount_1_second = 0;
            discount_0_first_for_delay = 0;
            discount_0_second_for_delay = 0;
            fine_calc = 0;
            debt_relief_death_loan = 0;
            debt_relief_death_interest = 0;
            debt_relief_death_interest_for_delay = 0;
            debt_relief_death_fine = 0;
            debt_relief_cession_loan = 0;
            debt_relief_cession_interest = 0;
            debt_relief_cession_interest_for_delay = 0;
            debt_relief_cession_fine = 0;
            debt_relief_collector_loan = 0;
            debt_relief_collector_interest = 0;
            debt_relief_collector_interest_for_delay = 0;
            debt_relief_collector_fine = 0;
            debt_relief_exclusive_cases_loan = 0;
            debt_relief_exclusive_cases_interest = 0;
            debt_relief_exclusive_cases_interest_for_delay = 0;
            debt_relief_exclusive_cases_fine = 0;
            insurance_calc_revenue = 0;
            payment_loan_current = 0;
            payment_loan_avance = 0;
            payment_interest_current = 0;
            payment_interest_avance = 0;
            payment_interest_for_delay_current = 0;
            payment_fine = 0;
            payment_advance_payment = 0;
            payment_paid_more = 0;
            payment_unfound = 0;
            total_balance_e = 0;
            loan_balance_e = 0;
            loan_balance_e_overdue = 0;
            loan_balance_e_due = 0;
            interest_balance_e = 0;
            fine_balance_e = 0;
        
        
            SELECT 
                *
            FROM
                buh.balance_value t
            WHERE 
                t.transact_date = v__month_start_date
                AND
                t.loan_key = v__loan_key
                AND
                t.active = 1
            LIMIT 
            	1
            INTO
                v__rec_month_b;
        
        
        	/*
        	loan_b + 
            intere_b_b
            od_fine_b
            */
            

            loan_balance_b_overdue = v__rec_month_b.od_loan_b;
            loan_balance_b_due = v__rec_month_b.loan_b;
            loan_balance_b = loan_balance_b_overdue + loan_balance_b_due;            
            interest_balance_b = v__rec_month_b.od_overpayment_planned_b +  v__rec_month_b.od_overpayment_for_delay_b + v__rec_month_b.n_overpayment_planned_b;
            fine_balance_b = v__rec_month_b.od_fine_b;
            total_balance_b = loan_balance_b + interest_balance_b + fine_balance_b;

            
            loan_balance_e_overdue = v__rec.od_loan_e;
            loan_balance_e_due = v__rec.loan_e;
            loan_balance_e = loan_balance_e_overdue + loan_balance_e_due;
            interest_balance_e  = v__rec.od_overpayment_planned_e +  v__rec.od_overpayment_for_delay_e + v__rec.n_overpayment_planned_e;
            fine_balance_e = v__rec.od_fine_e;
            total_balance_e = loan_balance_e + interest_balance_e + fine_balance_e;

      
			payment_advance_payment = v__rec.client_balance_e;
            
			is_return_next = TRUE;

        END IF;
        
        IF is_return_next = FALSE THEN
        	CONTINUE;
        END IF;
        
        -- =====================================================================
        is_first_contract = FALSE;
        SELECT t.subdivision_key, t.contract
        FROM public.loans t
        WHERE t.loan_key = v__rec.loan_key
        INTO subdivision_key, v__contract;
        
        v__contract = COALESCE(v__contract,'');
        v__contract_last_2_simbols = right(v__contract, 2);
        IF v__contract_last_2_simbols = '01' THEN
	        is_first_contract = TRUE;
        END IF;
        -- =====================================================================
        
        
        
        -- Плановый доход процентов за указаный период
        -- =====================================================================
		SELECT
            SUM(t.summ)
        FROM
        	buh.loan_transact t
        WHERE
        	t.loan_key = v__rec.loan_key
            AND
            t.transact_date >= v__month_start_date
            AND
            t.transact_date <= v__month_end_date
            AND
            t.account_transaction_key = 10 -- Начисление плановых процентов
        INTO
        	revenue_0_summ;
        -- =====================================================================



        
        
        -- Проценты доначисленные за просрочку за указаный период
        -- =====================================================================
		SELECT
            SUM(t.summ)
        FROM
        	buh.loan_transact t
        WHERE
        	t.loan_key = v__rec.loan_key
            AND
            t.transact_date >= v__month_start_date
            AND
            t.transact_date <= v__month_end_date
            AND
            t.account_transaction_key IN
	            ( 
    	        	12, --Начисление пени за просрочку
                    11 --Доначисление процентов за просрочку
	            )
        INTO
        	revenue_for_delay_0_summ;
        -- =====================================================================
            
        
        --плановый доход процентов  
        IF is_first_contract THEN
            revenue_0_first = revenue_0_summ;
            revenue_0_second = 0;
        ELSE
            revenue_0_first = 0;
            revenue_0_second = revenue_0_summ;
        END IF;


        --досрочный доход - ?
        revenue_1_first = 0;
        revenue_1_second = 0;
        
        
        --проценты доначисленные за просрочку
        IF is_first_contract THEN
            revenue_for_delay_0_first = revenue_for_delay_0_summ;
            revenue_for_delay_0_second = 0;
        ELSE
            revenue_for_delay_0_first = 0;
            revenue_for_delay_0_second = revenue_for_delay_0_summ;
        END IF;
        
        SELECT SUM(t.summ)
        FROM buh.loan_transact t
        WHERE t.loan_key = v__rec.loan_key
            AND t.transact_date >= v__month_start_date
            AND t.transact_date <= v__month_end_date
            AND t.account_transaction_key IN
	            ( 
    	        	12 --Начисление пени за просрочку
	            )
        INTO _fine_sum;       
        fine_calc = _fine_sum;
        

        -- Списание долга (тело) в случае смерти
        -- =====================================================================
		SELECT
            SUM(t.summ)
        FROM
        	buh.loan_transact t
        WHERE
        	t.loan_key = v__rec.loan_key
            AND
            t.transact_date >= v__month_start_date
            AND
            t.transact_date <= v__month_end_date
            AND
            t.account_transaction_key IN
	            ( 
    	        	26 
	            )
        INTO
        	debt_relief_death_loan_summ;
        -- =====================================================================

        

        -- Списание долга (проценты) в случае смерти
        -- =====================================================================
		SELECT
            SUM(t.summ)
        FROM
        	buh.loan_transact t
        WHERE
        	t.loan_key = v__rec.loan_key
            AND
            t.transact_date >= v__month_start_date
            AND
            t.transact_date <= v__month_end_date
            AND
            t.account_transaction_key IN
	            ( 
    	        	52
	            )
        INTO
        	debt_relief_death_interest_summ;
        -- =====================================================================
        

        

        -- Списание долга (проценты за просрочку) в случае смерти
        -- =====================================================================
		SELECT
            SUM(t.summ)
        FROM
        	buh.loan_transact t
        WHERE
        	t.loan_key = v__rec.loan_key
            AND
            t.transact_date >= v__month_start_date
            AND
            t.transact_date <= v__month_end_date
            AND
            t.account_transaction_key IN
	            ( 
    	        	28
	            )
        INTO
        	debt_relief_death_interest_for_delay_summ;
        -- =====================================================================
        
        
        
        debt_relief_death_loan = COALESCE(debt_relief_death_loan_summ,0);
        debt_relief_death_interest = COALESCE(debt_relief_death_interest_summ,0);
        debt_relief_death_interest_for_delay = COALESCE(debt_relief_death_interest_for_delay_summ,0);
        debt_relief_death_fine = 0;
        
        
        
        
        
        -- Погашение срочного долга_тело
        -- =====================================================================
		SELECT
            SUM(t.summ)
        FROM
        	buh.loan_transact t
        WHERE
        	t.loan_key = v__rec.loan_key
            AND
            t.transact_date >= v__month_start_date
            AND
            t.transact_date <= v__month_end_date
            AND
            t.account_transaction_key IN
	            ( 
    	        	13
	            )
        INTO
        	payment_loan_current_summ;
        -- =====================================================================
        
       
        --  Погашение срочного долга_плановые условно непросроченные проценты
        -- =====================================================================
		SELECT
            SUM(t.summ)
        FROM
        	buh.loan_transact t
        WHERE
        	t.loan_key = v__rec.loan_key
            AND
            t.transact_date >= v__month_start_date
            AND
            t.transact_date <= v__month_end_date
            AND
            t.account_transaction_key IN
	            ( 
    	        	47
	            )
        INTO
        	payment_interest_current_summ;
        -- =====================================================================
        
        
        

        --  Погашение просроченного долга_ плановые проценты
        -- =====================================================================
		SELECT
            SUM(t.summ)
        FROM
        	buh.loan_transact t
        WHERE
        	t.loan_key = v__rec.loan_key
            AND
            t.transact_date >= v__month_start_date
            AND
            t.transact_date <= v__month_end_date
            AND
            t.account_transaction_key IN
	            ( 
    	        	48
	            )
        INTO
        	payment_interest_for_delay_current_summ;
        -- =====================================================================
        
        

        --  Погашение пени
        -- =====================================================================
		SELECT
            SUM(t.summ)
        FROM
        	buh.loan_transact t
        WHERE
        	t.loan_key = v__rec.loan_key
            AND
            t.transact_date >= v__month_start_date
            AND
            t.transact_date <= v__month_end_date
            AND
            t.account_transaction_key IN
	            ( 
    	        	19
	            )
        INTO
        	payment_fine_summ;
        -- =====================================================================


        

        --  Погашение пени
        -- =====================================================================
		SELECT
            SUM(t.summ)
        FROM
        	buh.loan_transact t
        WHERE
        	t.loan_key = v__rec.loan_key
            AND
            t.transact_date >= v__month_start_date
            AND
            t.transact_date <= v__month_end_date
            AND
            t.account_transaction_key IN
	            ( 
    	        	19
	            )
        INTO
        	payment_fine_summ;
        -- =====================================================================



        
        payment_loan_current = COALESCE(payment_loan_current_summ,0);
        payment_loan_avance = 0;
        payment_interest_current = COALESCE(payment_interest_current_summ,0);
        payment_interest_avance = 0;
        payment_interest_for_delay_current = COALESCE(payment_interest_for_delay_current_summ,0);
        payment_fine = COALESCE(payment_fine_summ,0);
        payment_paid_more = 0;
        payment_unfound = 0;
        
        

		IF is_return_next = TRUE THEN
			RETURN NEXT;        
        END IF;
    END LOOP;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_account_info (OID = 466390) : 
--
CREATE FUNCTION buh.get_account_info (
  _account_key integer,
  _date date = now(),
  out code character varying
)
RETURNS character varying
AS 
$body$
DECLARE
	v__rec RECORD;
    v__code VARCHAR;
BEGIN
	SELECT *
    FROM buh.account t
    WHERE t."aссount_key" = _account_key
    INTO v__rec;
    
    IF FOUND THEN
        SELECT t.code
        FROM buh.account_code t
        WHERE 
        	t.account_key = _account_key
        	AND 
        	_date >= t.date_start 
            AND
            _date <= t.date_end
        INTO v__code;
        
        IF FOUND THEN
        	code = COALESCE(v__code,'');
        ELSE
	        RAISE EXCEPTION 'Не найден код счета #% (%)',_account_key,_date;
        END IF;
    ELSE
    	--RAISE EXCEPTION 'Не найден счет #'||_account_key;
        RAISE EXCEPTION 'Не найден счет #%',_account_key;
    END IF;
    
    RETURN;
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function get_loan_summ_for_date (OID = 466391) : 
--
CREATE FUNCTION buh.get_loan_summ_for_date (
  _loan_key integer,
  _date date
)
RETURNS numeric
AS 
$body$
/*
<description>
========================================================================================
           Хранимка воззвращает сумму по графику начисления тела займа, если указанная дата не
           является датой очередного платежа - тогда возвращаем -1
========================================================================================
</description>
<history_list>
	<item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
	<item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
  v__summ NUMERIC;
  v__count_repayment_schedules INTEGER;
  
  
  v__max_date DATE;
  v__max_date_period DATE;
  
  v__summ_max_date NUMERIC;
  v__count_days INTEGER;
  v__summ_dif NUMERIC;
  v__permissible_loan_key INTEGER;
BEGIN

	v__summ = 0;
    
    
    

    -- Получаем v__permissible_loan_key
    -- -------------------------------------------------------------------------
    v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key,_date);
    -- -------------------------------------------------------------------------
    
    

    -- Данные по графику начисления тела займа
    -- -------------------------------------------------------------------------
    SELECT SUM(t.loan_sum)
    FROM 
    	asuz_db.repayment_schedules t 
    LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
    	on ro.repayment_schedule_key=t.repayment_schedule_key
    WHERE 
    	COALESCE(ro.payment_date_ori,t.payment_date) = _date AND
        t.permissible_loan_key = v__permissible_loan_key
    INTO 
    	v__summ;
    
    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

    
    SELECT SUM(t.loan_sum)
    FROM 
    	asuz_db.repayment_schedules t 
    WHERE 
    	t.payment_date = _date AND
        t.permissible_loan_key = v__permissible_loan_key
    INTO 
    	v__summ;
    */
    v__summ = COALESCE(v__summ,0);
    -- -------------------------------------------------------------------------
    
    
    
	-- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_repayment_schedules = ROW_COUNT;
    -- -------------------------------------------------------------------------
    
    
    
    -- Если в этот день есть по графику платежей оплата - возвращаем, если нет то -1
    -- -------------------------------------------------------------------------
    IF v__count_repayment_schedules != 1 THEN
    	v__summ = -1;
    END IF;
    -- -------------------------------------------------------------------------
    
        
  	RETURN v__summ;
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function get_max_overpayment_for_delay_summ (OID = 466392) : 
--
CREATE FUNCTION buh.get_max_overpayment_for_delay_summ (
  _loan_key integer,
  _date date
)
RETURNS numeric
AS 
$body$
/*
<description>
	Функция возвращает предел просроченной переплаты
        
    ограничения:
    	сумма процентов за просрочку НЕ БОЛЕЕ , чем 200% от ТЕЛА ЗАЙМА (общего при выдаче)
        
        Если займ выдан раньше 01.01.2017, то возвращаем 1000000
        Если займ выдан позже чем 27.01.2019, то возвращаем 1000000
</description>
<history_list>
    <item><date_m>21.02.2019</date_m><task_n>473770</task_n><author>Тихоненко В.В.</author></item>
    <item><date_m>26.06.2019</date_m><task_n>509863</task_n><author>Тихоненко В.В.</author></item>
  	<item><date_m>07.02.2020</date_m><task_n>587292</task_n><author>Тихоненко В.В.</author></item>

</history_list>



*/
DECLARE
	v__result NUMERIC;
    v__loan_sum_limit NUMERIC;
    v__loan_sum NUMERIC;
    v__bv__n_overpayment_planned_e NUMERIC;
    v__bv__od_overpayment_planned_e NUMERIC;
    v__bv__od_overpayment_for_delay_e NUMERIC;
    v__bv__od_fine_e NUMERIC;
    v__total_summ NUMERIC;
    v__rec_balance_value RECORD;
    v__debet NUMERIC;
    v__start NUMERIC;
    v__loan_creation_date DATE;
    v__loan_debt_loan NUMERIC; -- Оставшаяся сумма непогашенного тела зайта   (просроченного и непросроченного)
    v__permissible_loan_key INTEGER;
    v__sum__overpayment_sum  NUMERIC;
    v__od_overpayment_planned_b  NUMERIC; --Баланс по просроченным процентам (od_overpayment - Проср. % плановые на начало)
    v__od_overpayment_for_delay_b  NUMERIC; --Проср. % доначисленные на начало
    v__total_overpayment_sum_current  NUMERIC; -- Текущее количество процентов, которые могут быть
BEGIN
	v__result = 0;
    


    -- -------------------------------------------------------------------------
    SELECT *
    FROM buh.balance_value bv
    WHERE 
    	bv.loan_key = _loan_key AND 
        bv.active = 1 AND
        bv.transact_date = _date
    INTO 
    	v__rec_balance_value;
    -- -------------------------------------------------------------------------
    
    
    SELECT l.creation_date::DATE
    FROM asuz_db.loans l
    WHERE l.loan_key = _loan_key
    INTO v__loan_creation_date;
    
    
	-- Считаем сумму на начало дня
    SELECT 
    	t.od_overpayment_for_delay_b, 
        t.od_loan_b,
        t.od_overpayment_planned_b,
        t.od_overpayment_for_delay_b
        
    FROM buh.balance_value t
    WHERE 
        t.loan_key = _loan_key
        AND
        t.active = 1 
        AND
        t.transact_date = _date
    INTO 
    	v__start, 
        v__loan_sum,
        v__od_overpayment_planned_b, -- Баланс по просроченным процентам
        v__od_overpayment_for_delay_b; -- Проср. % доначисленные на начало (76.03.03)
    v__start = COALESCE(v__start,0);
    
    
    
    /*
     *  Оставшаяся сумма непогашенного тела зайта   (просроченного и непросроченного)
     *  	Баланс по телу просроченному и баланс по телу непросроченному (плюс)
     *			loan_b+n_loan_b + od_loan_b
     *
     *  Новый алгоритм согласно задаче #509863 jn 26/06/2019
     */
    -- ====================================================================================
    SELECT 
    	COALESCE(t.loan_b,0) + COALESCE(t.n_loan_b,0) + COALESCE(t.od_loan_b,0)
    FROM buh.balance_value t
    WHERE 
        t.loan_key = _loan_key
        AND
        t.active = 1 
        AND
        t.transact_date = _date
    INTO v__loan_debt_loan;
    -- ====================================================================================    
    
    
    
    /*
     *  Текущее количество процентов, которые могут быть
     *    			считается как 
     *           		остаток по непогашенным процентам по займу (просроченным и непросроченным) - все до конца займа проценты
	 *							Брать все оставшиеся проценты по графику платежей - (суммируем  asuz_db.repayment_schedules.overpayment_sum где payment_date больше или равно текущей дате расчета - )
     *                       	начиная с периода в котором идет расчет
     *                           плюс 
     *                           Баланс по просроченным процентам (od_overpayment - Проср. % плановые на начало)
     *                           
     *
     *                   плюс
     *                   остаток по непогашенным процентам за просрочку (взять баланс)
     *                   		(od_overpayment_for_delay - Проср. % доначисленные на начало )
     *
     *  Новый алгоритм согласно задаче #509863 jn 26/06/2019
     */
    -- ====================================================================================
    
    
        -- Получаем v__permissible_loan_key
        -- -------------------------------------------------------------------------
        /*

        SELECT t.permissible_loan_key
        FROM asuz_db.loans t
        WHERE t.loan_key = _loan_key
        INTO v__permissible_loan_key;	
    	
        Код верен в том случае, если данные из АСУЗа корректные. 
        Но если они битые, тогжа стоит применять
        buh.get_permissible_loan_for_date
        Согласно задаче #587292 


        */
        v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key,_date);
        -- -------------------------------------------------------------------------
            
            
        -- Проверяем, является ли текущая дата - датой очередного платежа
        -- -------------------------------------------------------------------------
        SELECT 
        	SUM(t.overpayment_sum)
        FROM 
        	asuz_db.repayment_schedules t 
        LEFT JOIN 
        	asuz_db.repayment_schedules_payment_date_ori ro on ro.repayment_schedule_key=t.repayment_schedule_key
        WHERE 
        		t.loan_key = _loan_key
        	AND 
            	t.permissible_loan_key = v__permissible_loan_key
        	AND 
        		COALESCE(ro.payment_date_ori, t.payment_date) >= _date
        INTO 
        	v__sum__overpayment_sum;
        -- -------------------------------------------------------------------------

		v__total_overpayment_sum_current = 	COALESCE(v__sum__overpayment_sum,0) + 
        																COALESCE(v__od_overpayment_planned_b,0) + 
                                                                        COALESCE(v__od_overpayment_for_delay_b,0);
    -- ====================================================================================
    /*
    
    _loan_debt_loan = Оставшаяся сумма непогашенного тела зайта   (просроченного и непросроченного)
    	Баланс по телу просроченному и баланс по телу непросроченному (плюс)
			loan_b+n_loan_b + od_loan_b
        
        
    _total_overpayment_sum_current= текущее количество процентов, которые могут быть
    			считается как 
                		остаток по непогашенным процентам по займу (просроченным и непросроченным) - все до конца займа проценты
								Брать все оставшиеся проценты по графику платежей - (суммируем  asuz_db.repayment_schedules.overpayment_sum где payment_date больше или равно текущей дате расчета - )
                            	начиная с периода в котором идет расчет
                                плюс 
                                Баланс по просроченным процентам (od_overpayment - Проср. % плановые на начало)
                                

                        плюс
                        остаток по непогашенным процентам за просрочку (взять баланс)
                        		(od_overpayment_for_delay - Проср. % доначисленные на начало )



    _total_overpayment_sum_current = loan_debt_overpayment_planned(_rec.loan_key, _calc_date) + loan_debt_overpayment_for_delay(_rec.loan_key, _calc_date) + _ovepayment_for_delay;
      _loan_debt_loan = loan_debt_loan(_rec.loan_key, _calc_date);
      IF _total_overpayment_sum_current/_loan_debt_loan  >= _limit_overpayment_for_delay/100 THEN
        _ovepayment_for_delay = 0;--при достижении предела текущей переплаты сумму обнуляем
      END IF;
      
      
    */
    



	-- Считаем дебетовую сумму
    SELECT sum(l.summ)
    FROM buh.loan_transact l
    join buh.account_transaction a on a.account_transaction_key=l.account_transaction_key
    where 
        l.loan_key=_loan_key AND 
        a.account_transaction_key = 11 AND--только проценты за просрочку
        l.active=1 AND 
		l.transact_date=_date
    INTO v__debet;
    v__debet = COALESCE(v__debet,0);

    v__total_summ = v__start  + v__debet;
    
    

	v__loan_sum = v__loan_debt_loan;
	v__loan_sum_limit = v__loan_sum * 2;
    
    
    /*
     *
     Сейчас неактуально согласно задаче #509863 jn 26/06/2019
    IF v__loan_sum_limit > v__total_summ THEN
	 v__result = v__loan_sum_limit - v__total_summ;
    END IF;
    */
    
	/*
     *
     *  Новый алгоритм согласно задаче #509863 jn 26/06/2019
     */
    IF v__loan_sum_limit > v__total_overpayment_sum_current THEN
      	v__result = v__loan_sum_limit - v__total_overpayment_sum_current;
	END IF;
    
	-- Если займ выдан раньше 01.01.2017, то возвращаем 1000000
	IF v__loan_creation_date < '01.01.2017' THEN
    	v__result = 1000000;
    END IF;
    
	-- Если займ выдан раньше 01.01.2017, то возвращаем 1000000
	IF v__loan_creation_date > '27.01.2019' THEN
    	v__result = 1000000;
    END IF;
    
    
    RETURN v__result;
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function get_max_overpayment_summ (OID = 466393) : 
--
CREATE FUNCTION buh.get_max_overpayment_summ (
  _loan_key integer,
  _date date
)
RETURNS numeric
AS 
$body$
/*
<description>
	Функция возвращает предел общей переплаты
        
    ограничения:
    	Начисление плановых процентов + Доначисление процентов за просрочку  НЕ БОЛЕЕ , 
       -- чем 300% от ТЕЛА ЗАЙМА (общего при выдаче), если займ выдан после '01.01.2017' 
       -- чем 400% от ТЕЛА ЗАЙМА (общего при выдаче), если займ выдан до '01.01.2017' 
    Если займ выдан после 28.01.2019 - тогда предел общей переплаты составляет 
    	Начисление плановых процентов + Доначисление процентов за просрочку + Начисление пени за просрочку НЕ БОЛЕЕ , 
           250% от ТЕЛА ЗАЙМА (общего при выдаче)
</description>
<history_list>
    <item><date_m>21.02.2019</date_m><task_n>473770</task_n><author>Тихоненко В.В.</author></item>
</history_list>



*/
DECLARE
	v__result NUMERIC;
    v__loan_sum_max NUMERIC; -- Сумма займа помноженная на коэффициент, который определяет максимальную переплату
    v__loan_sum NUMERIC;
    v__bv__n_overpayment_planned_e NUMERIC;
    v__bv__od_overpayment_planned_e NUMERIC;
    v__bv__od_overpayment_for_delay_e NUMERIC;
    v__bv__od_fine_e NUMERIC;
    v__total_summ NUMERIC; -- Общая сумма оплаты (вычисленная в зависимости от даты создания займа)
    v__total_summ_300_400 NUMERIC; -- Общая сумма оплаты (проведенных транзакций для ограничений 300 и 400 % (включает плановые и % за просрочку)
    v__total_summ_250 NUMERIC; -- Общая сумма оплаты (проведенных транзакций для ограничений 250 % (включает плановые и % за просрочку и пени)
    v__rec_balance_value RECORD;
    v__debet NUMERIC;
    v__start NUMERIC;
    v__loan_creation_date DATE;
BEGIN
	v__result = 0;
    


    -- -------------------------------------------------------------------------
    SELECT *
    FROM buh.balance_value bv
    WHERE 
    	bv.loan_key = _loan_key AND 
        bv.active = 1 AND
        bv.transact_date = _date
    INTO 
    	v__rec_balance_value;
    -- -------------------------------------------------------------------------
    
    
    SELECT l.loan_sum, l.creation_date::DATE
    FROM asuz_db.loans l
    WHERE l.loan_key = _loan_key
    INTO v__loan_sum, v__loan_creation_date;
    
    
	-- Считаем сумму на начало дня
    SELECT t.all_loan_percents_b
    FROM buh.balance_value t
    WHERE 
        t.loan_key = _loan_key
        AND
        t.active = 1 
        AND
        t.transact_date = _date
    INTO v__start;
    v__start = COALESCE(v__start,0);
    
    
    


	-- Считаем дебетовую сумму
    -- ============================================================================
    SELECT 
    	sum(l.summ)
    FROM 
    	buh.loan_transact l
    join buh.account_transaction a on a.account_transaction_key=l.account_transaction_key
    where 
        l.loan_key=_loan_key AND 
        a.account_transaction_key IN (
        		10, -- Начисление плановых процентов
                11, -- Доначисление процентов за просрочку
                12  -- Начисление пени за просрочку
            ) AND
        l.active=1 AND 
		l.transact_date=_date
    INTO v__debet;
    v__debet = COALESCE(v__debet,0);
    -- ============================================================================


	/**
     * Здесь мы расчитали сумму оплат за предыдущие даты и за расчитываемый день
     * все начисленные
     *     10, -- Начисление плановых процентов
     *     11, -- Доначисление процентов за просрочку
     *     12  -- Начисление пени за просрочку
     * И это ограничение нам подходит только для займов, выданных после 28.01.2019
     * Это ограничение введено согласно задаче https://redmine.vivadengi.ru/issues/456444#note-20
     */
    v__total_summ_250 = v__start  + v__debet;
    
	/**
     * Для займов, выданных не позднее чем 28.01.2019 (<= - меньше либо равно)
     * сумма плановых % и процентов за просрочку не должна превышать 300 и 400% соответственно
     * Это ограничение введено согласно задаче https://redmine.vivadengi.ru/issues/456444#note-20
     */
     v__total_summ_300_400 = buh.get_all_transact_summ (
            _loan_key,
            '10,11',
        		--10, -- Начисление плановых процентов
                -- 11 -- Доначисление процентов за просрочку
            _date
          );

	-- Если займ выдан позже 01.01.2017, то коэффициент 3, иначе 4
	IF v__loan_creation_date > '01.01.2017' THEN
    	v__loan_sum_max = v__loan_sum * 3;
        -- Это ограничение введено согласно задаче https://redmine.vivadengi.ru/issues/456444#note-20
         v__total_summ = v__total_summ_300_400;
        /* 
        	Если займ выдан после 28.01.2019 - тогда предел общей переплаты
            составляе 250% от первоначальной суммы займа
        */
        IF v__loan_creation_date > '28.01.2019' THEN
    		v__loan_sum_max = v__loan_sum * 2.5;
            -- Это ограничение введено согласно задаче https://redmine.vivadengi.ru/issues/456444#note-20
             v__total_summ = v__total_summ_250;
        END IF;
    ELSE
    	v__loan_sum_max = v__loan_sum * 4;
        -- Это ограничение введено согласно задаче https://redmine.vivadengi.ru/issues/456444#note-20
        v__total_summ = v__total_summ_300_400;
    END IF;
	
    
    IF v__loan_sum_max > v__total_summ THEN
	    v__result = v__loan_sum_max - v__total_summ;
    END IF;
    
    RETURN v__result;
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function get_op_balance (OID = 466394) : 
--
CREATE FUNCTION buh.get_op_balance (
  _loan_key integer,
  _date date,
  _account_key integer,
  _balance_value_field character varying
)
RETURNS numeric
AS 
$body$
DECLARE
	v__start NUMERIC;
    v__debet NUMERIC;
    v__credit NUMERIC;
    v__summ NUMERIC;
    v__sql VARCHAR;

BEGIN


	-- Считаем операционный баланс
	-- Начало  + дебет - кредит
    -- -------------------------------------------------------------------------
	-- Считаем сумму на начало дня
    v__sql = '
        SELECT t.'|| _balance_value_field ||'
        FROM buh.balance_value t
        WHERE 
        	t.loan_key = '|| COALESCE(_loan_key::VARCHAR,'0') ||' AND
	        t.active = 1 AND
        	t.transact_date = '''|| COALESCE(_date::VARCHAR,'') ||'''
        ';
    EXECUTE v__sql 
    INTO v__start;
    v__start = COALESCE(v__start,0);
    --RAISE NOTICE 'v__start %', v__start;

	-- Считаем дебетовую сумму
    SELECT sum(l.summ)
    FROM buh.loan_transact l
    join buh.account_transaction a on a.account_transaction_key=l.account_transaction_key
    where 
        l.loan_key=_loan_key AND 
        a.account_key_debet=_account_key AND 
        l.active=1 AND 
		l.transact_date=_date
    INTO v__debet;
    v__debet = COALESCE(v__debet,0);
    --RAISE NOTICE 'v__debet %', v__debet;

	-- Считаем кредитовую сумму
    SELECT sum(l.summ)
    FROM buh.loan_transact l
    join buh.account_transaction a on a.account_transaction_key=l.account_transaction_key
    where 
	    l.loan_key = _loan_key
    	AND a.account_key_credit=_account_key
	    AND l.active=1
    	AND l.transact_date=_date
    INTO v__credit;
    v__credit = COALESCE(v__credit,0);
    --RAISE NOTICE 'v__credit %', v__credit;

	-- Сумма на текущий день (Операционный баланс)
    v__summ = v__start  + v__debet - v__credit;
    -- -------------------------------------------------------------------------
    --RAISE NOTICE 'v__summ %', v__summ;
    
    RETURN v__summ;

END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function get_op_balance_all_percents (OID = 466395) : 
--
CREATE FUNCTION buh.get_op_balance_all_percents (
  _loan_key integer,
  _date date,
  _mode integer = 0
)
RETURNS numeric
AS 
$body$
DECLARE
	v__start NUMERIC;
    v__summ NUMERIC;
    v__debet NUMERIC;
    v__sum_overpayment_sum NUMERIC;
BEGIN
/*
<description>
	Хранимка для генерации операционного баланса всех начисленных процентов на указанный день по указанному займу
<description>
<history_list>
	<item>
    	<date_m>22.01.2019</date_m><task_n>456444</task_n><author>Тихоненко В.</author>
        <changes>
			В отличие от предыдущей версии, где бралось во внимание лишь
                11, -- Доначисление процентов за просрочку
                12  -- Начисление пени за просрочку
            теперь ссчитаем также плановые проценты
           		10, -- Начисление плановых процентов
                11, -- Доначисление процентов за просрочку
                12  -- Начисление пени за просрочку
        </changes>
    </item>
</history_list>
*/
	-- Считаем операционный баланс
	-- Начало  + дебет - кредит
    -- -------------------------------------------------------------------------
	-- Считаем сумму на начало дня
    SELECT t.all_loan_percents_b
    FROM buh.balance_value t
    WHERE 
        t.loan_key = _loan_key
        AND
        t.active = 1 
        AND
        t.transact_date = _date
    INTO v__start;
    v__start = COALESCE(v__start,0);
    --RAISE NOTICE 'v__start %', v__start;




	-- Считаем дебетовую сумму
    -- 11 = Доначисление процентов за просрочку
    -- 12 = Начисление пени за просрочку
    SELECT sum(l.summ)
    FROM buh.loan_transact l
    join buh.account_transaction a on a.account_transaction_key=l.account_transaction_key
    where 
        l.loan_key=_loan_key AND 
        a.account_transaction_key IN (
       	-- 11,12
        	-- Правки по задаче #456444
        		10, -- Начисление плановых процентов
                11, -- Доначисление процентов за просрочку
                12  -- Начисление пени за просрочку
        	) AND
        l.active=1 AND 
		l.transact_date=_date
    INTO v__debet;
    v__debet = COALESCE(v__debet,0);
    
    
    
    IF _mode = 0 THEN
        SELECT
            sum(rs.overpayment_sum)
        FROM
            public.loans l
        JOIN
            public.repayment_schedules rs ON rs.permissible_loan_key = l.permissible_loan_key
        WHERE
            l.loan_key = _loan_key
        INTO
            v__sum_overpayment_sum;
            
        v__sum_overpayment_sum = COALESCE(v__sum_overpayment_sum,0);
        
        v__summ = v__start  + v__debet + v__sum_overpayment_sum;    
    ELSE

	    v__summ = v__start  + v__debet;
        
    END IF;
    -- -------------------------------------------------------------------------
    --RAISE NOTICE 'v__summ %', v__summ;
    
    RETURN v__summ;

END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function get_overpayment_sum_for_date (OID = 466396) : 
--
CREATE FUNCTION buh.get_overpayment_sum_for_date (
  _loan_key integer,
  _date date
)
RETURNS numeric
AS 
$body$
/*
<description>
========================================================================================
           	Получаем проценты на указанную дату по займу
========================================================================================
</description>
<history_list>
	<item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
	<item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
	<item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
	<item><date_m>07.02.2020</date_m><task_n>587292</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/

DECLARE
  v__min_date DATE;
  v__max_date DATE;
  v__max_date_period DATE;
  v__min_date_period DATE;
  v__summ NUMERIC;
  v__summ_max_date NUMERIC;
  v__count_days INTEGER;
  v__summ_dif NUMERIC;
  v__creation_date DATE;
  v__permissible_loan_key INTEGER;
  v__bv__n_overpayment_planned_e NUMERIC;
  
    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

  	v__rec__first_payment_schedule RECORD; -- Признак того, что необходимо при расчете брать первоначальный график платежей, из договора
	*/
BEGIN

	v__summ = 0;

    -- Получаем v__permissible_loan_key
    -- Получаем дату создания займа
    -- -------------------------------------------------------------------------
    /*
      
      Код верен в том случае, если данные из АСУЗа корректные. 
      Но если они битые, тогжа стоит применять
      buh.get_permissible_loan_for_date
      Согласно задаче #587292 

      */
      SELECT 
      --	t.permissible_loan_key,  
          t.creation_date
      FROM 
          asuz_db.loans t
      WHERE 
          t.loan_key = _loan_key
      INTO 
      --	v__permissible_loan_key, 
          v__creation_date;
          
      v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key,_date);
    -- -------------------------------------------------------------------------

    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

	-- Получаем настройку - нужно ли брать первоначальны график платежей, 
    -- тот что при заклчении договора
	-- -------------------------------------------------------------------------
    v__rec__first_payment_schedule  = buh.get_calc_first_payment_schedule(_loan_key);
	-- -------------------------------------------------------------------------
	*/

    	
    -- Получаем минимальную дату из тех, что больше указанной даты
    -- -------------------------------------------------------------------------
    SELECT MIN(COALESCE(ro.payment_date_ori,t.payment_date))
    FROM asuz_db.repayment_schedules t 
    LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
    	on ro.repayment_schedule_key=t.repayment_schedule_key
    WHERE t.permissible_loan_key = v__permissible_loan_key
    AND COALESCE(ro.payment_date_ori,t.payment_date) >= _date
    INTO v__max_date_period;
    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

    SELECT MIN(t.payment_date)
    FROM asuz_db.repayment_schedules t 
    WHERE t.permissible_loan_key = v__permissible_loan_key
    AND t.payment_date >= _date
    INTO v__max_date_period;
    */
    -- -------------------------------------------------------------------------



    
    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

	-- Получаем минимальную дату из тех, что больше указанной даты
    -- если стоит настройка
    -- Рассчитать по первому графику платежей на момент выдачи
    -- -------------------------------------------------------------------------
	IF v__rec__first_payment_schedule.result = 1 THEN
      SELECT MIN(t.payment_date)
      FROM asuz_db.tmp_payment_schedules t 
      WHERE 
      	t.application_key = v__rec__first_payment_schedule.application_key
      	AND 
        t.payment_date >= _date
	  INTO v__max_date_period;
    END IF;
    */
    -- -------------------------------------------------------------------------



        
    -- Получаем минимальную дату из тех, что больше указанной даты
    -- -------------------------------------------------------------------------
    SELECT MAX(COALESCE(ro.payment_date_ori,t.payment_date))
    FROM asuz_db.repayment_schedules t 
    LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
    	on ro.repayment_schedule_key=t.repayment_schedule_key
    WHERE t.permissible_loan_key = v__permissible_loan_key
    AND COALESCE(ro.payment_date_ori,t.payment_date) < _date
    INTO v__min_date_period;
    
    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

    SELECT MAX(t.payment_date)
    FROM asuz_db.repayment_schedules t 
    WHERE t.permissible_loan_key = v__permissible_loan_key
    AND t.payment_date < _date
    INTO v__min_date_period;
    */
    v__min_date_period = COALESCE(v__min_date_period,v__creation_date);
    -- -------------------------------------------------------------------------
    
    
    
    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

	-- Получаем минимальную дату из тех, что больше указанной даты
    -- если стоит настройка
    -- Рассчитать по первому графику платежей на момент выдачи
    -- -------------------------------------------------------------------------
	IF v__rec__first_payment_schedule.result = 1 THEN
      SELECT max(t.payment_date)
      FROM asuz_db.tmp_payment_schedules t 
      WHERE 
      	t.application_key = v__rec__first_payment_schedule.application_key
      	AND 
        t.payment_date < _date
	  INTO v__max_date;
      
      v__min_date_period = COALESCE(v__min_date_period,v__creation_date);
    END IF;
    -- -------------------------------------------------------------------------
	*/

    
    
    
    -- Ввиду того, что минимальная дата - "Невходящая граница", инкрементируем дату
    -- -------------------------------------------------------------------------    
    v__min_date_period = v__min_date_period + '1 day'::interval;
    -- -------------------------------------------------------------------------    
        
        
    -- Получаем сумму процентов на дату v__max_date_period
    -- -------------------------------------------------------------------------
    SELECT SUM(t.overpayment_sum)
    FROM asuz_db.repayment_schedules t 
    LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
    	on ro.repayment_schedule_key=t.repayment_schedule_key
    WHERE t.permissible_loan_key = v__permissible_loan_key
    AND COALESCE(ro.payment_date_ori,t.payment_date) = v__max_date_period
    INTO v__summ_max_date;

    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

    SELECT SUM(t.overpayment_sum)
    FROM asuz_db.repayment_schedules t 
    WHERE t.permissible_loan_key = v__permissible_loan_key
    AND t.payment_date = v__max_date_period
    INTO v__summ_max_date;
    */
    -- -------------------------------------------------------------------------
    
    
    
    
        
    
    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

	-- Получаем сумму процентов на дату v__max_date_period
    -- если стоит настройка
    -- Рассчитать по первому графику платежей на момент выдачи
    -- -------------------------------------------------------------------------
	IF v__rec__first_payment_schedule.result = 1 THEN
      SELECT SUM(t.overpayment_sum)
      FROM asuz_db.tmp_payment_schedules t 
      WHERE 
      	t.application_key = v__rec__first_payment_schedule.application_key
      	AND 
        t.payment_date = v__max_date_period
	  INTO v__summ_max_date;
    END IF;
    -- -------------------------------------------------------------------------
	*/
        
        
    -- Получаем поличество дней между началом пересчета и концом
    -- -------------------------------------------------------------------------
    SELECT COUNT (*) 
    FROM generate_series(v__min_date_period, v__max_date_period, interval '1' day) 
    INTO v__count_days;
    -- -------------------------------------------------------------------------
        
        
        
    -- Получаем сумму начисления на каждый день
    -- -------------------------------------------------------------------------
    v__summ = trunc(v__summ_max_date/ v__count_days,2);
    
    -- Балансовое значение на текущий день по непросроченным плановым процентам
   	v__bv__n_overpayment_planned_e    = buh.get_op_balance(_loan_key, _date,57,'n_overpayment_planned_b');
    
    
    IF _date = v__max_date_period THEN
    /*
        v__summ_dif	= v__summ*(v__count_days-1);
        v__summ = v__summ_max_date - v__summ_dif;
    */
    	v__summ = v__summ_max_date - v__bv__n_overpayment_planned_e;
    END IF;
    -- -------------------------------------------------------------------------
    
  	RETURN v__summ;
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function get_overpayment_base (OID = 466397) : 
--
CREATE FUNCTION buh.get_overpayment_base (
  _loan_key integer,
  _date date
)
RETURNS numeric
AS 
$body$
/*
<description>
	Функция возвращает базу для начисления вледующих величин
    	-- "Доначисленные проценты за просрочку на тело займа"
        -- "Начисление пени за просрочку"
</description>
<history_list>
    <item><date_m>21.02.2019</date_m><task_n>473770</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__loan_sum NUMERIC;
    v__loan_creation_date DATE;
    v__result NUMERIC;
    v__od_loan_b NUMERIC;
    v__n_loan_b NUMERIC;
    v__loan_b NUMERIC;
    v__rec_balance_value RECORD;
BEGIN
/*
Если займ выдан до 01,01,2017: 
база для начисления доначисленных процентов и пени - ВЕСЬ остаток долга по ТЕЛУ займа (и просроченному, и нет)
с 01,01,2017 включительно только просроченное ТЕЛО

*/

	v__result = 0; 
    


    -- -------------------------------------------------------------------------
    SELECT *
    FROM buh.balance_value bv
    WHERE 
    	bv.loan_key = _loan_key AND 
        bv.active = 1 AND
        bv.transact_date = _date
    INTO 
    	v__rec_balance_value;
    -- -------------------------------------------------------------------------


    IF COALESCE(v__rec_balance_value.od_loan_b::INTEGER,0) != 0 THEN
    
    
        SELECT l.loan_sum, l.creation_date
        FROM asuz_db.loans l
        WHERE l.loan_key = _loan_key
        INTO v__loan_sum, v__loan_creation_date;
            
        /*
        IF v__loan_creation_date < '01.01.2017' THEN
            -- база для начисления доначисленных процентов и пени - ВЕСЬ остаток долга 
            -- по ТЕЛУ займа (и просроченному, и нет)    	
            
            -- 58.03.02 = 67 = Проср. тело займа на начало (58.03.02)
            v__od_loan_b = v__rec_balance_value.od_loan_b;
            
            -- 58.03.01 = 53
            v__n_loan_b = v__rec_balance_value.n_loan_b;
            --оставшееся тело займ(непросроченное)
            v__loan_b = v__rec_balance_value.loan_b;
            v__result = COALESCE(v__od_loan_b,0) + COALESCE(v__n_loan_b,0) + COALESCE(v__loan_b,0);
        ELSE
            -- с 01,01,2017 включительно только просроченное ТЕЛО    
            -- 58.03.02 = 67 = Проср. тело займа на начало (58.03.02)
            v__od_loan_b = v__rec_balance_value.od_loan_b;
            v__result = COALESCE(v__od_loan_b,0);
            
        END IF;
        */
          
        -- 58.03.02 = 67 = Проср. тело займа на начало (58.03.02)
        v__od_loan_b = v__rec_balance_value.od_loan_b;
        v__result = COALESCE(v__od_loan_b,0);
        
    END IF;


    
    RETURN v__result;
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function get_permissible_loan_for_date (OID = 466398) : 
--
CREATE FUNCTION buh.get_permissible_loan_for_date (
  _loan_key integer,
  _date date,
  _mode integer = 0
)
RETURNS integer
AS 
$body$
/*
<description>
	Хранимка возвращает permissible_loan_key для указанного займа на конкретную дату
</description>
<history_list>
<item><date_m>03.02.2020</date_m><task_n>587292</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
  _result INTEGER;
  v__rec__permissible_loans RECORD;
BEGIN
  _result = 0;
  
  IF _mode = 1 THEN
    SELECT f.value::integer
    FROM facts.facts f
    WHERE f.loan_key = _loan_key AND date_trunc('day', f.fact_date) < _date AND f.fact_name_key = 2
    ORDER BY f.fact_date DESC
    LIMIT 1
    INTO _result;  
  ELSE
    SELECT f.value::integer
    FROM facts.facts f
    WHERE f.loan_key = _loan_key AND date_trunc('day', f.fact_date) <= _date AND f.fact_name_key = 2
    ORDER BY f.fact_date DESC
    LIMIT 1
    INTO _result;
  END IF;
  
  /*

  */
  --
  IF NOT FOUND THEN
  --
    PERFORM f.value::integer
    FROM facts.facts f
    WHERE f.loan_key = _loan_key AND f.fact_name_key = 2;
    --если нет фактов  - берем из loans
    IF NOT FOUND THEN
      SELECT l.permissible_loan_key
      FROM asuz_db.loans l
      WHERE l.loan_key = _loan_key
      INTO _result;
    
    ELSE
    --ищем первый факт изменения графика
      SELECT f.value::integer
      FROM facts.facts f
      WHERE f.loan_key = _loan_key AND f.fact_name_key = 2
      ORDER BY f.fact_date ASC
      LIMIT 1
      INTO _result;
      
      SELECT pl.key
      FROM asuz_db.permissible_loans pl
      WHERE pl.loan_key = _loan_key AND pl.key < _result
      ORDER BY pl.key DESC
      LIMIT 1
      INTO _result;
    
    
    END IF;
  
  ELSE -- 
  
    SELECT * 
    FROM   asuz_db.permissible_loans  pl
    WHERE pl.key = _result 
    INTO
      v__rec__permissible_loans;
  
  	IF NOT FOUND THEN
          
        SELECT pl.key
        FROM asuz_db.permissible_loans  pl
        WHERE pl.loan_key = _loan_key
        AND pl.version_date <= _date
        ORDER BY pl.key DESC
        LIMIT 1
        INTO _result;
      
    END IF;
  
  END IF;
  
  
  
  
  RETURN _result;  
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function get_separate_subdivision (OID = 466399) : 
--
CREATE FUNCTION buh.get_separate_subdivision (
  _loan_key integer,
  _date_start date
)
RETURNS character varying
AS 
$body$
DECLARE
	v__separate_subdivision VARCHAR;
    v__subdivision_key INTEGER;
BEGIN
	-- Получаем подразделение
	-- -----------------------------------------------------
  	SELECT t.subdivision_key
    FROM asuz_db.loans t
    WHERE t.loan_key = _loan_key
    INTO v__subdivision_key;
        
    SELECT separate_subdivision
    FROM public.get_separate_subdivision(v__subdivision_key,_date_start)
    INTO v__separate_subdivision;

    v__separate_subdivision = COALESCE(v__separate_subdivision,'');
	-- -----------------------------------------------------
    
    RETURN v__separate_subdivision;
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function get_table_transactions_extended (OID = 466400) : 
--
CREATE FUNCTION buh.get_table_transactions_extended (
  out id integer,
  out name character varying,
  out debet character varying,
  out credit character varying,
  out description character varying
)
RETURNS SETOF record
AS 
$body$
DECLARE
	v__rec RECORD;
    v__new_code VARCHAR;
    v__old_code VARCHAR;
BEGIN

	FOR v__rec IN
    	SELECT *
        FROM buh.account_transaction t
        ORDER BY
        	t."order" ASC
    LOOP

		id = v__rec."order";
        name = v__rec.name;
        
    	v__new_code = buh.get_account_info(v__rec.account_key_debet,'01.01.2099');
    	v__old_code = buh.get_account_info(v__rec.account_key_debet,'01.01.1970');
        debet = '{'||COALESCE(v__old_code,'')||'} => {'||COALESCE(v__new_code,'')||'}';
        
    	v__new_code = buh.get_account_info(v__rec.account_key_credit,'01.01.2099');
    	v__old_code = buh.get_account_info(v__rec.account_key_credit,'01.01.1970');
        credit = '{'||COALESCE(v__old_code,'')||'} => {'||COALESCE(v__new_code,'')||'}';
        
        description = COALESCE(v__rec.description,'');
    	RETURN NEXT;
    END LOOP;

	RETURN;
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function is_correct_balance_value (OID = 466401) : 
--
CREATE FUNCTION buh.is_correct_balance_value (
  _loan_key integer,
  _date date
)
RETURNS boolean
AS 
$body$
DECLARE
    v__rec_balance_value RECORD;
    v__count_balance_value INTEGER;
    v__message VARCHAR;
    v__result BOOLEAN;
    v__count_log INTEGER;
BEGIN
	v__result = TRUE;

/*
Закомментировано ввид большой нагрузки при выборке
    -- -------------------------------------------------------------------------
    SELECT 
    	COUNT(*) 
    FROM 
    	buh.transact_calc_log t
    WHERE 
    	t.date = _date AND 
        t.loan_key = _loan_key AND 
        t.procedure = 'is_correct_balance_value'
    INTO
    	v__count_log;
    -- -------------------------------------------------------------------------
    
    
    IF v__count_log > 0 THEN
        v__result = FALSE;
        RETURN v__result;
    END IF;
*/

    -- -------------------------------------------------------------------------
    SELECT *
    FROM buh.balance_value bv
    WHERE 
    	bv.loan_key = _loan_key AND 
        bv.active = 1 AND
        bv.transact_date = _date
    INTO 
    	v__rec_balance_value;
    -- -------------------------------------------------------------------------



	-- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_balance_value = ROW_COUNT;
    -- -------------------------------------------------------------------------
    
    
    -- Если балансовых величин не найдено = Ошибка
	-- -------------------------------------------------------------------------
    IF v__count_balance_value = 0 THEN
        v__message = 'Балансовых величин найдено '||COALESCE(v__count_balance_value::VARCHAR,'')||' для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' is_correct_balance_value';
        PERFORM buh.add_transact_calc_log(_loan_key,_date,'is_correct_balance_value',v__message);
        v__result =  FALSE;
    END IF;
	-- -------------------------------------------------------------------------



    -- Если балансовых величин больше 1 = Ошибка
	-- -------------------------------------------------------------------------
    IF v__count_balance_value > 1 THEN
        v__message = 'Нарушена структура данных balance_value для loan_key='||COALESCE(_loan_key::VARCHAR,'')||' на '||COALESCE(_date::VARCHAR,'')||' is_correct_balance_value';
        PERFORM buh.add_transact_calc_log(_loan_key,_date,'is_correct_balance_value',v__message);
        v__result =  FALSE;
    END IF;
	-- -------------------------------------------------------------------------
    
    


    RETURN v__result;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function is_date_in_repayment_shedules (OID = 466402) : 
--
CREATE FUNCTION buh.is_date_in_repayment_shedules (
  _loan_key integer,
  _date date
)
RETURNS boolean
AS 
$body$
/*
<description>
========================================================================================
	Хранимка возвращает - является ли дата датой очередного платежа
========================================================================================
</description>
<history_list>
  <item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2020</date_m><task_n>587292</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__result BOOLEAN;
    v__permissible_loan_key INTEGER;
    v__count INTEGER;
BEGIN

	v__result = FALSE;


	-- Получаем v__permissible_loan_key
    -- -------------------------------------------------------------------------
    /*

    SELECT t.permissible_loan_key
    FROM asuz_db.loans t
    WHERE t.loan_key = _loan_key
    INTO v__permissible_loan_key;	
	
    Код верен в том случае, если данные из АСУЗа корректные. 
    Но если они битые, тогжа стоит применять
	buh.get_permissible_loan_for_date
    Согласно задаче #587292 


    */
    v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key,_date);
    -- -------------------------------------------------------------------------
    
    
    
    -- Проверяем, является ли текущая дата - датой очередного платежа
    -- -------------------------------------------------------------------------
    SELECT count(*)
    FROM asuz_db.repayment_schedules t 
    LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
    	on ro.repayment_schedule_key=t.repayment_schedule_key
    WHERE  t.permissible_loan_key = v__permissible_loan_key
    AND COALESCE(ro.payment_date_ori,t.payment_date) = _date
    INTO v__count;
    
    /*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
    
    SELECT count(*)
    FROM asuz_db.repayment_schedules t
    WHERE t.permissible_loan_key = v__permissible_loan_key
    AND t.payment_date = _date
    INTO v__count;
    */
    -- -------------------------------------------------------------------------
    
    
    
    
    -- Если да (теущая дата = дата платежа)
    -- -------------------------------------------------------------------------
    IF v__count > 0 THEN
	    v__result = TRUE;
    END IF;
    -- -------------------------------------------------------------------------
    
    
	RETURN v__result;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function is_date_in_repayment_shedules_annuitet (OID = 466403) : 
--
CREATE FUNCTION buh.is_date_in_repayment_shedules_annuitet (
  _loan_key integer,
  _date date
)
RETURNS boolean
AS 
$body$
/*
<description>
========================================================================================
	Хранимка возвращает - является ли дата датой очередного платежа (делалось для ануитетной ставки)
========================================================================================
</description>
<history_list>
	<item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
  	<item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
	<item><date_m>07.02.2020</date_m><task_n>587292</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__result BOOLEAN;
    v__permissible_loan_key INTEGER;
    v__count INTEGER;
BEGIN

	v__result = FALSE;


	-- Получаем v__permissible_loan_key
    -- -------------------------------------------------------------------------
    /*

    SELECT t.permissible_loan_key
    FROM asuz_db.loans t
    WHERE t.loan_key = _loan_key
    INTO v__permissible_loan_key;	
	
    Код верен в том случае, если данные из АСУЗа корректные. 
    Но если они битые, тогжа стоит применять
	buh.get_permissible_loan_for_date
    Согласно задаче #587292 


    */
    v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key,_date);
    -- -------------------------------------------------------------------------
    
    
    
    -- Проверяем, является ли текущая дата - датой аннуитета
    -- -------------------------------------------------------------------------
    SELECT count(*)
    FROM asuz_db.repayment_schedules t 
    LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
    	on ro.repayment_schedule_key=t.repayment_schedule_key
    WHERE  t.permissible_loan_key = v__permissible_loan_key
    AND COALESCE(ro.payment_date_ori,t.payment_date) = _date
    INTO v__count;
    
    /*
    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
    
    SELECT count(*)
    FROM asuz_db.repayment_schedules t
    WHERE t.permissible_loan_key = v__permissible_loan_key
    AND t.payment_date = _date
    INTO v__count;
    */
    -- -------------------------------------------------------------------------
    
    
    
    
    -- Если да (теущая дата = дата аннуитета)
    -- -------------------------------------------------------------------------
    IF v__count > 0 THEN
	    v__result = TRUE;
    END IF;
    -- -------------------------------------------------------------------------
    
    
	RETURN v__result;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function is_loan_close_date (OID = 466404) : 
--
CREATE FUNCTION buh.is_loan_close_date (
  _loan_key integer,
  _date date
)
RETURNS boolean
AS 
$body$
/*
<history_list>
<item><date_m>10.10.2017</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__rec_fact RECORD;
    v__count_fact INTEGER;
    v__ret BOOLEAN;
BEGIN
	v__ret = FALSE;


    -- Получаем запись офакте закрытия указанного займа за данную дату
	-- -------------------------------------------------------------------------
    SELECT * 
    FROM facts.facts t
    WHERE 
        t.fact_name_key = 3 AND 
        t.loan_key = _loan_key AND 
        t.fact_date = _date
    INTO 
        v__rec_fact;
	-- -------------------------------------------------------------------------


	-- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_fact = ROW_COUNT;
    -- -------------------------------------------------------------------------
    
    IF v__count_fact > 0 THEN
	    v__ret = TRUE;
    END IF;
    
    RETURN v__ret;
    
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function is_transact_calc_error (OID = 466405) : 
--
CREATE FUNCTION buh.is_transact_calc_error (
  _loan_key integer,
  _date date
)
RETURNS boolean
AS 
$body$
/*
<history_list>
<item><date_m>10.10.2017</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__count INTEGER;
BEGIN
  	SELECT 
    	count(*) 
    FROM 
    	buh.transact_calc_log t
  	WHERE 
    	t.loan_key = _loan_key AND
  		t.date = _date
  	INTO 
    	v__count;
  
  	IF v__count>0 THEN
  		RETURN true;
  	END IF;
  	
    RETURN false;  
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function set_stat_tables_count_b (OID = 466406) : 
--
CREATE FUNCTION buh.set_stat_tables_count_b (
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
DECLARE
	v__balance_value_count_b INTEGER;
	v__loans_count INTEGER;
	v__repayment_schedules_count INTEGER;
	v__permissible_loans_count INTEGER;
	v__separate_subdivision_count INTEGER;
	v__loan_transact_count_b INTEGER;
	v__transact_calc_log_count_b INTEGER;
    
    v__stat_tables_count_key INTEGER;
    v__max_record INTEGER;
    v__record RECORD;
BEGIN
	result = 1;
    result_str = '';
	RETURN;
	result = -1;
    result_str = '';

    SELECT MAX(s.stat_tables_count_key)
    FROM buh.stat_tables_count s
	INTO v__max_record;
    
    v__max_record = COALESCE(v__max_record,0);
    
    IF v__max_record != 0 THEN
        SELECT *
        FROM buh.stat_tables_count s
        INTO v__record;
    END IF;



	SELECT COUNT(*)
    FROM buh.balance_value t
    INTO v__balance_value_count_b;

	SELECT COUNT(*)
    FROM asuz_db.loans t
    INTO v__loans_count;

	SELECT COUNT(*)
    FROM asuz_db.repayment_schedules t
    INTO v__repayment_schedules_count;
    
	SELECT COUNT(*)
    FROM asuz_db.permissible_loans t
    INTO v__permissible_loans_count;
    
	SELECT COUNT(*)
    FROM public.separate_subdivision t
    INTO v__separate_subdivision_count;
    
	SELECT COUNT(*)
    FROM buh.loan_transact t
    INTO v__loan_transact_count_b;
    
	SELECT COUNT(*)
    FROM buh.transact_calc_log t
    INTO v__transact_calc_log_count_b;    
    
    
    IF v__max_record != 0 THEN
    	IF v__loans_count < COALESCE(v__record.loans_count,0) OR v__loans_count = 0 THEN
            result_str = 'public.loans меньше чем в предыдущем рассчете ('||COALESCE(v__loans_count,0)||')';
            RETURN;
        END IF;
        
    	IF 	v__repayment_schedules_count < COALESCE(v__record.repayment_schedules_count,0) OR 
        	v__repayment_schedules_count = 0 THEN
            result_str = 'public.repayment_schedules меньше чем в предыдущем рассчете ('||COALESCE(v__repayment_schedules_count,0)||')';
            RETURN;
        END IF;
        
    	IF 	v__permissible_loans_count < COALESCE(v__record.permissible_loans_count,0) OR 
        	v__permissible_loans_count = 0 THEN
            result_str = 'public.permissible_loans меньше чем в предыдущем рассчете ('||COALESCE(v__permissible_loans_count,0)||')';
            RETURN;
        END IF;
        
    	IF 	v__separate_subdivision_count < COALESCE(v__record.separate_subdivision_count,0) OR 
        	v__separate_subdivision_count = 0 THEN
            result_str = 'public.separate_subdivision меньше чем в предыдущем рассчете ('||COALESCE(v__separate_subdivision_count,0)||')';
            RETURN;
        END IF;
        
        
        
    END IF;
    

    INSERT INTO 
      buh.stat_tables_count
    (
      balance_value_count_b,
      balance_value_count_e,
      loans_count,
      repayment_schedules_count,
      permissible_loans_count,
      separate_subdivision_count,
      loan_transact_count_b,
      loan_transact_count_e,
      transact_calc_log_count_b,
      transact_calc_log_count_e
    ) 
    VALUES (
      v__balance_value_count_b,
      0,
      v__loans_count,
      v__repayment_schedules_count,
      v__permissible_loans_count,
      v__separate_subdivision_count,
      v__loan_transact_count_b,
      0,
      v__transact_calc_log_count_b,
      0
    )
    RETURNING
    	stat_tables_count_key
    INTO
    	v__stat_tables_count_key;
        
	result = v__stat_tables_count_key;
    result_str = 'OK';
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact (OID = 471143) : 
--
SET search_path = facts, pg_catalog;
CREATE FUNCTION facts.register_fact (
  _obj_key integer,
  _obj_type_key integer,
  _fact_name_key integer,
  _fact_date timestamp without time zone,
  _loan_key integer,
  _value numeric,
  _user_key integer,
  _subdivision_key integer,
  _kod_podr character varying,
  _additional_value integer = NULL::integer,
  _additional_value2 integer = NULL::integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>31.07.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list>
*/
BEGIN
  INSERT INTO 
      "facts"."facts"
    (
  obj_key,
  obj_type,
  fact_name_key,
  fact_date,
  loan_key,
  value,
  user_key,
  subdivision_key,
  kod_podr,
  additional_value,
  additional_value2
    ) 
    VALUES (
  _obj_key,
  _obj_type_key,
  _fact_name_key,
  _fact_date,
  _loan_key,
  _value,
  _user_key,
  _subdivision_key,
  _kod_podr,
  _additional_value,
  _additional_value2
    )
    RETURNING fact_key
    INTO result;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_1 (OID = 471144) : 
--
CREATE FUNCTION facts.register_fact_1 (
  _loan_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>31.07.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list>
*/
DECLARE
  _creation_date date;
  _loan_sum numeric;
  _subdivision_key int;
  _kod_podr varchar;
  _non_cash int;
  _user_key int;
BEGIN
  PERFORM loan_key FROM asuz_db.loans
  WHERE loan_key = _loan_key;

  IF NOT FOUND THEN
    result = -1;
    result_str = 'Займ не найден'::varchar;
    return;
  END IF;

  SELECT l.creation_date, l.loan_sum, l.subdivision_key, (setting_value(l.subdivision_key,'separate_subdivision', l.creation_date)).value as kod_podr, l.non_cash 
  FROM asuz_db.loans l
  WHERE l.loan_key = _loan_key
  INTO _creation_date, _loan_sum, _subdivision_key, _kod_podr, _non_cash;

  _user_key = public.user_key();

  SELECT rf.result, rf.result_str FROM facts.register_fact(_loan_key, 1, 1, _creation_date::timestamp, _loan_key, _loan_sum, _user_key, _subdivision_key, _kod_podr, _non_cash::int) rf
  INTO result, result_str;

  EXCEPTION
WHEN OTHERS THEN
  INSERT INTO logging.log_errors(error_text,error_site) VALUES(SQLERRM,'facts.register_fact_1');
  RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_10_13 (OID = 471145) : 
--
CREATE FUNCTION facts.register_fact_10_13 (
  _lmc_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>07.08.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list>
*/
DECLARE
  _creation_date timestamp;
  _value numeric;
  _subdivision_key int;
  _kod_podr varchar;
  _user_key int;
  _loan_key int;
  _obj_key int;
  _obj_type int;
  _additional_value int;

  _sum_cor_loan numeric;
  _sum_cor_overpayment numeric;
  _sum_cor_overpayment_for_delay numeric;
  _sum_cor_fine numeric;
BEGIN
  PERFORM * FROM asuz_db.loans_manual_correction lmc
  WHERE lmc.lmc_key = _lmc_key;

  IF NOT FOUND THEN
    result = -1;
    result_str = 'Коррекция не найдена'::varchar;
    return;
  END IF;

  SELECT lmc.sum_cor_loan * -1, lmc.sum_cor_overpayment * -1, lmc.sum_cor_overpayment_for_delay * -1, lmc.sum_cor_fine * -1, lmc.creation_date, l.loan_key, l.subdivision_key, (setting_value(l.subdivision_key,'separate_subdivision', lmc.creation_date::date)).value, lmc.correction_type
  FROM asuz_db.loans_manual_correction lmc
  LEFT JOIN asuz_db.loans l on l.loan_key = lmc.loan_key
  WHERE lmc.lmc_key = _lmc_key
  INTO _sum_cor_loan, _sum_cor_overpayment, _sum_cor_overpayment_for_delay, _sum_cor_fine, _creation_date, _loan_key, _subdivision_key, _kod_podr, _additional_value;

  _user_key = public.user_key();
  
  IF _sum_cor_loan != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 10, _creation_date::timestamp, _loan_key, _sum_cor_loan, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;
  IF _sum_cor_overpayment != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 11, _creation_date::timestamp, _loan_key, _sum_cor_overpayment, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;
  IF _sum_cor_overpayment_for_delay != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 12, _creation_date::timestamp, _loan_key, _sum_cor_overpayment_for_delay, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;
  IF _sum_cor_fine != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 13, _creation_date::timestamp, _loan_key, _sum_cor_fine, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;

  EXCEPTION
WHEN OTHERS THEN
  INSERT INTO logging.log_errors(error_text,error_site) VALUES(SQLERRM,'facts.register_fact_10_13');
  RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_16 (OID = 471146) : 
--
CREATE FUNCTION facts.register_fact_16 (
  _loan_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>08.08.2017</date_m><task_n>360930</task_n><author>Власов С.А.</author></item>
</history_list>
*/
DECLARE
  _creation_date timestamp;
  _loan_sum numeric;
  _subdivision_key int;
  _kod_podr varchar;
  _non_cash int;
  _user_key int;
BEGIN
  PERFORM loan_key FROM asuz_db.loans
  WHERE loan_key = _loan_key AND canceled = TRUE;

  IF NOT FOUND THEN
    result = -1;
    result_str = 'Займ не найден или не отменен'::varchar;
    return;
  END IF;

  SELECT CURRENT_TIMESTAMP, l.loan_sum * -1, l.subdivision_key, (setting_value(l.subdivision_key,'separate_subdivision', l.creation_date)).value as kod_podr, l.non_cash
  FROM asuz_db.loans l
  WHERE l.loan_key = _loan_key
  INTO _creation_date, _loan_sum, _subdivision_key, _kod_podr, _non_cash;

  _user_key = public.user_key();

  SELECT rf.result, rf.result_str FROM facts.register_fact(_loan_key, 1, 16, _creation_date, _loan_key, _loan_sum, _user_key, _subdivision_key, _kod_podr, _non_cash::int) rf
  INTO result, result_str;

  EXCEPTION
WHEN OTHERS THEN
  INSERT INTO logging.log_errors(error_text,error_site) VALUES(SQLERRM,'facts.register_fact_16');
  RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_17_20_pdp (OID = 471147) : 
--
CREATE FUNCTION facts.register_fact_17_20_pdp (
  _lmc_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>08.12.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list>
*/
DECLARE
  _creation_date timestamp;
  _value numeric;
  _subdivision_key int;
  _kod_podr varchar;
  _user_key int;
  _loan_key int;
  _obj_key int;
  _obj_type int;
  _additional_value int;

  _sum_cor_loan numeric;
  _sum_cor_overpayment numeric;
  _sum_cor_overpayment_for_delay numeric;
  _sum_cor_fine numeric;
BEGIN
  PERFORM * FROM asuz_db.loans_manual_correction lmc
  WHERE lmc.lmc_key = _lmc_key;

  IF NOT FOUND THEN
    result = -1;
    result_str = 'Коррекция не найдена'::varchar;
    return;
  END IF;

  SELECT 0, lmcrs.sum_cor_overpayment, 0, 0, 
  	lmc.creation_date, l.loan_key, l.subdivision_key, (setting_value(l.subdivision_key,'separate_subdivision', lmc.creation_date::date)).value, lmc.correction_type
  FROM asuz_db.loans_manual_correction lmc
  LEFT JOIN asuz_db.loans l on l.loan_key = lmc.loan_key
  LEFT JOIN asuz_db.loans_manual_correction_rs lmcrs on lmcrs.lmc_key = lmc.lmc_key
  WHERE lmc.lmc_key = _lmc_key AND lmc.correction_type = 4
  ORDER BY lmcrs.repayment_schedule_key asc
  limit 1
  INTO _sum_cor_loan, _sum_cor_overpayment, _sum_cor_overpayment_for_delay, _sum_cor_fine, _creation_date, _loan_key, _subdivision_key, _kod_podr, _additional_value;

  _user_key = public.user_key();
  
  IF _sum_cor_loan != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 17, _creation_date::timestamp, _loan_key, _sum_cor_loan, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;
  IF _sum_cor_overpayment != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 18, _creation_date::timestamp, _loan_key, _sum_cor_overpayment, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;
  IF _sum_cor_overpayment_for_delay != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 19, _creation_date::timestamp, _loan_key, _sum_cor_overpayment_for_delay, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;
  IF _sum_cor_fine != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 20, _creation_date::timestamp, _loan_key, _sum_cor_fine, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;

  EXCEPTION
WHEN OTHERS THEN
  INSERT INTO logging.log_errors(error_text,error_site) VALUES(SQLERRM,'facts.register_fact_17_20_pdp');
  RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_2 (OID = 471148) : 
--
CREATE FUNCTION facts.register_fact_2 (
  _permissible_loan_key integer,
  _permissible_loan_date timestamp without time zone,
  _permissible_loan_type integer = 0,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>31.07.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list> 
*/
DECLARE
  _subdivision_key int;
  _kod_podr varchar;
  _loan_key int;
  _user_key int;
BEGIN
  PERFORM loan_key FROM asuz_db.permissible_loans
  WHERE key = _permissible_loan_key;

  IF NOT FOUND THEN
    result = -1;
    result_str = 'График платежей не найден'::varchar;
    return;
  END IF;

/*
  SELECT pl.loan_key, l.subdivision_key, (setting_value(l.subdivision_key,'separate_subdivision', _permissible_loan_date::date)).value as kod_podr
  FROM asuz_db.permissible_loans pl
  LEFT JOIN asuz_db.loans l on pl.loan_key = l.loan_key
  WHERE pl.key = _permissible_loan_key
  INTO _loan_key, _subdivision_key, _kod_podr;
*/
/*
  SELECT 
  	pl.loan_key, 
    l.subdivision_key, 
    (setting_value(l.subdivision_key,'separate_subdivision', _permissible_loan_date::date)).value as kod_podr
  FROM 
  	asuz_db.permissible_loans pl
  LEFT JOIN 
  	asuz_db.loans l on pl.loan_key = l.loan_key
  WHERE 
  	pl.key = _permissible_loan_key
  INTO 
  	_loan_key, _subdivision_key, _kod_podr;
    */
    
  SELECT 
  	pl.loan_key
  FROM 
  	asuz_db.permissible_loans pl
  WHERE 
  	pl.key = _permissible_loan_key
  INTO 
  	_loan_key;
    
  SELECT 
    l.subdivision_key, 
    (setting_value(l.subdivision_key,'separate_subdivision', _permissible_loan_date::date)).value as kod_podr
  FROM 
  	asuz_db.loans l
  WHERE 
  	l.loan_key = _loan_key
  INTO 
  	_subdivision_key, _kod_podr;


  _user_key = public.user_key();

  SELECT rf.result, rf.result_str FROM facts.register_fact(_permissible_loan_key, 2, 2, _permissible_loan_date, _loan_key, _permissible_loan_key::numeric, _user_key, _subdivision_key, _kod_podr, _permissible_loan_type) rf
  INTO result, result_str;

  EXCEPTION
WHEN OTHERS THEN
  INSERT INTO logging.log_errors(error_text,error_site) VALUES(SQLERRM,'facts.register_fact_2');
  RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_2_fix (OID = 471149) : 
--
CREATE FUNCTION facts.register_fact_2_fix (
  out result integer,
  out result_str character varying
)
RETURNS SETOF record
AS 
$body$
/*<history_list>
<item><date_m>31.07.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list> 
*/
DECLARE 
  	_obj_key int;
  	_fact_key int;
  	_count int;
  	_type int;
  	_fact_date date;
  	_loan_key int;
BEGIN
  FOR _obj_key, _fact_key, _fact_date, _loan_key IN
    SELECT f.obj_key, f.fact_key, f.fact_date, f.loan_key from facts.facts f
    WHERE f.fact_name_key = 2 --AND f.loan_key = 963066
  LOOP
    select count(*) from 
		(select agreement_date from asuz_db.loan_restructuring_history
		where loan_key = _loan_key
		union
		select partial_early_date from asuz_db.partial_early_agreement
		where loan_key = _loan_key) t
    INTO _count;

    IF  _count = 0 THEN
    	UPDATE facts.facts f SET additional_value = 0
    	WHERE f.fact_key = _fact_key;
 	ELSE
      select * from 
		(select agreement_date from asuz_db.loan_restructuring_history
		where loan_key = _loan_key AND permissible_loan_key = _obj_key
		union
		select partial_early_date from asuz_db.partial_early_agreement
		where loan_key = _loan_key AND old_permissible_loan_key = _obj_key) t
        INTO _fact_date;  
        
        _fact_date = COALESCE(_fact_date, '2100.10.01');
        --raise notice '%',_fact_date;
          
		select type from (
			select agreement_date as date, 1 as type from asuz_db.loan_restructuring_history
			where loan_key = _loan_key and agreement_date < _fact_date
			union
			select partial_early_date as date, 2 as type from asuz_db.partial_early_agreement
			where loan_key = _loan_key and partial_early_date < _fact_date) t
		order by date desc
		limit 1
		INTO _type;

		IF _type is not null THEN
	    	UPDATE facts.facts f SET additional_value = _type
	    	WHERE f.fact_key = _fact_key;
	    ELSE
	    	UPDATE facts.facts f SET additional_value = 0
	    	WHERE f.fact_key = _fact_key;
		END IF;
    END IF;
  END LOOP;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_2_fix_partial (OID = 471150) : 
--
CREATE FUNCTION facts.register_fact_2_fix_partial (
  out result integer,
  out result_str character varying
)
RETURNS SETOF record
AS 
$body$
/*<history_list>
<item><date_m>19.12.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list> 
*/
DECLARE 
    _obj_key int;
    _fact_key int;
    _count int;
    _type int;
    _fact_date date;
    _loan_key int;
    _overpayment_sum numeric;
    _loan_sum NUMERIC;
    _payment_date date;
    _add_val2 INTEGER;
BEGIN
  FOR _obj_key, _fact_key, _fact_date, _loan_key, _add_val2 IN
    SELECT f.obj_key, f.fact_key, CASE WHEN f.additional_value2 = -2 THEN f.creation_date  ELSE f.fact_date END, f.loan_key, f.additional_value2 
    FROM facts.facts f
    WHERE f.fact_name_key = 2 and f.additional_value = 2 --AND f.loan_key = 1017008
  LOOP

    PERFORM * from asuz_db.repayment_schedules rs
    where rs.permissible_loan_key = _obj_key;

    IF NOT FOUND THEN
      UPDATE facts.facts SET additional_value2 = -1
      WHERE obj_key = _obj_key;
    ELSE
      SELECT rs.overpayment_sum, rs.loan_sum, rs.payment_date FROM asuz_db.repayment_schedules rs
      where rs.permissible_loan_key = _obj_key AND rs.payment_date >= _fact_date
      ORDER BY rs.repayment_schedule_key asc
      LIMIT 1 OFFSET 1
      INTO _overpayment_sum, _loan_sum, _payment_date;

      IF _overpayment_sum = 0 AND _loan_sum > 0  THEN
        UPDATE facts.facts SET fact_date = _payment_date,
            additional_value2 = null
        WHERE obj_key = _obj_key;
      ELSE
        SELECT rs.overpayment_sum, rs.loan_sum, rs.payment_date FROM asuz_db.repayment_schedules rs
        where rs.permissible_loan_key = _obj_key AND rs.payment_date < _fact_date
        ORDER BY rs.repayment_schedule_key DESC
        LIMIT 1
        INTO _overpayment_sum, _loan_sum, _payment_date;        

        IF _overpayment_sum = 0 AND _loan_sum > 0 THEN
          UPDATE facts.facts SET fact_date = _payment_date,
            additional_value2 = null
          WHERE obj_key = _obj_key;
        ELSE
          UPDATE facts.facts SET additional_value2 = -2
          WHERE obj_key = _obj_key;
        END IF;
      END IF; 
    END IF;
  END LOOP;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_3 (OID = 471151) : 
--
CREATE FUNCTION facts.register_fact_3 (
  _loan_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>31.07.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list>
*/
DECLARE
  _closed_date date;
  _subdivision_key int;
  _kod_podr varchar;
  _user_key int;
  _early_repayment bool;
  _debt_relieft_type int;
BEGIN
  PERFORM loan_key FROM asuz_db.loans
  WHERE loan_key = _loan_key;

  IF NOT FOUND THEN
    result = -1;
    result_str = 'Займ не найден'::varchar;
    return;
  END IF;

  SELECT l.closed_date, l.subdivision_key, (setting_value(l.subdivision_key,'separate_subdivision', l.closed_date)).value as kod_podr, l.early_repayment, l.debt_relieft_type 
  FROM asuz_db.loans l
  WHERE l.loan_key = _loan_key
  INTO _closed_date, _subdivision_key, _kod_podr, _early_repayment, _debt_relieft_type;

  _user_key = public.user_key();

  SELECT rf.result, rf.result_str FROM facts.register_fact(_loan_key, 1, 3, _closed_date::timestamp, _loan_key, 0::numeric, _user_key, _subdivision_key, _kod_podr, (CASE WHEN _early_repayment::int is null THEN 0 ELSE _early_repayment::int END), _debt_relieft_type) rf
  INTO result, result_str;

  EXCEPTION
WHEN OTHERS THEN
  INSERT INTO logging.log_errors(error_text,error_site) VALUES(SQLERRM,'facts.register_fact_3');
  RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_4 (OID = 471152) : 
--
CREATE FUNCTION facts.register_fact_4 (
  _document_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>02.08.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list>
*/
DECLARE
  _creation_date date;
  _value numeric;
  _subdivision_key int;
  _kod_podr varchar;
  _user_key int;
  _loan_key int;
  _obj_key int;
  _obj_type int;
  _additional_value int;
  v__payment_key INTEGER;
BEGIN

	SELECT pp.payment_key 
    FROM asuz_db.payment_part pp
  	WHERE pp.document_key =  _document_key 
    INTO v__payment_key;

    IF NOT FOUND THEN
      result = -1;
      result_str = 'Платеж не найден'::varchar;
      return;
    END IF;
    
  	PERFORM * FROM asuz_db.payment p
  	WHERE
  		p.payment_key = v__payment_key
  		and (p.payment_sum is not null AND p.payment_sum > 0)
  		AND
  		COALESCE(p.required_date,p.payment_date) < '01.09.2017';

    IF NOT FOUND THEN
      result = -1;
      result_str = 'Платеж не найден'::varchar;
      return;
    END IF;

/*
  PERFORM * FROM asuz_db.payment p
  JOIN asuz_db.payment_part pp ON p.payment_key = pp.payment_key
  WHERE pp.document_key =  _document_key 
  and (p.payment_sum is not null AND p.payment_sum > 0)
  AND
  COALESCE(p.required_date,p.payment_date) < '01.09.2017';
*/

  IF NOT FOUND THEN
    result = -1;
    result_str = 'Платеж не найден'::varchar;
    return;
  END IF;

/*
    SELECT 
    	COALESCE(p.required_date,p.payment_date), 
     	l.loan_key, 
        l.subdivision_key, 
    	(setting_value(l.subdivision_key,'separate_subdivision', COALESCE(p.required_date,p.payment_date)::date)).value, 
        (CASE WHEN pp.document_key > 0 THEN pp.document_key ELSE pp.document_key * -1 END), 
        (CASE WHEN pp.document_key > 0 THEN 5 ELSE 6 END)
    FROM 
    	asuz_db.payment p
    JOIN 
    	asuz_db.payment_part pp ON p.payment_key = pp.payment_key
    JOIN 
    	asuz_db.loans l ON p.loan_key = l.loan_key
    WHERE 
        pp.document_key =  _document_key 
        AND 
        (p.payment_sum is not null 
        AND 
        p.payment_sum > 0)
        AND 
        COALESCE(p.required_date,p.payment_date) < '01.09.2017'
    INTO _creation_date, _loan_key, _subdivision_key, _kod_podr, _obj_key, _obj_type;
*/
    SELECT 
        (CASE WHEN pp.document_key > 0 THEN pp.document_key ELSE pp.document_key * -1 END), 
        (CASE WHEN pp.document_key > 0 THEN 5 ELSE 6 END),
        pp.payment_key
    FROM 
    	asuz_db.payment_part pp
    WHERE 
        pp.document_key =  _document_key
    INTO _obj_key, _obj_type, v__payment_key;	



    SELECT 
    	COALESCE(p.required_date,p.payment_date), 
        p.loan_key
    FROM 
    	asuz_db.payment p
    WHERE 
        p.payment_key =  v__payment_key 
        AND 
        (p.payment_sum is not null 
        AND 
        p.payment_sum > 0)
        AND 
        COALESCE(p.required_date,p.payment_date) < '01.09.2017'
    INTO _creation_date, _loan_key;



    SELECT 
        l.subdivision_key, 
    	(setting_value(l.subdivision_key,'separate_subdivision', _creation_date::date)).value
    FROM 
    	asuz_db.loans l 
    WHERE 
        l.loan_key =  _loan_key 
    INTO _subdivision_key, _kod_podr;



	if _obj_type = 5 THEN
        SELECT cco.payment_sum
        FROM asuz_db.credit_cash_orders cco
        WHERE cco.order_key  = _document_key
        INTO _value;
    ELSE
        SELECT rr.payment_sum
        FROM asuz_db.register_records rr 
        WHERE register_records_key = -1*_document_key
        INTO _value;
    END IF;


/*
	SELECT rr.payment_sum
	FROM asuz_db.register_records rr 
	WHERE register_records_key = -1*document_key

	FROM asuz_db.payment p
	WHERE p.loan_key = _loan_key

в процедуру facts.generate_facts_by_loan
COALESCE(p.required_date,p.payment_date) < 01.09.2017
*/

  _user_key = public.user_key();
  IF _obj_type = 5 THEN
    _additional_value = 0;
  ELSE 
    _additional_value = 1;
  END IF;

  SELECT rf.result, rf.result_str FROM facts.register_fact(_obj_key, _obj_type, 4, _creation_date::timestamp, _loan_key, _value, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
  INTO result, result_str;

  EXCEPTION
WHEN OTHERS THEN
  INSERT INTO logging.log_errors(error_text,error_site) VALUES(SQLERRM,'facts.register_fact_4');
  RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_5 (OID = 471153) : 
--
CREATE FUNCTION facts.register_fact_5 (
  _document_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>02.08.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list>
*/
DECLARE
  _creation_date date;
  _value numeric;
  _subdivision_key int;
  _kod_podr varchar;
  _user_key int;
  _loan_key int;
  _obj_key int;
  _obj_type int;
  _additional_value int;
BEGIN
  PERFORM * FROM payments.payment p
  JOIN asuz_db.payment_part pp ON p.payment_key = pp.payment_key
  JOIN asuz_db.loans l ON p.loan_key = l.loan_key
  JOIN asuz_db.received_payment_by_loan rpl ON pp.document_key = rpl.order_key AND rpl.loan_key = l.loan_key
  WHERE pp.document_key =  _document_key and (p.payment_sum is not null AND p.payment_sum > 0);

  IF NOT FOUND THEN
    result = -1;
    result_str = 'Платеж не найден'::varchar;
    return;
  END IF;

  SELECT rpl.payment_sum * -1, l.loan_key, l.subdivision_key, (setting_value(l.subdivision_key,'separate_subdivision', COALESCE(p.required_date,p.payment_date)::date)).value, (CASE WHEN pp.document_key > 0 THEN pp.document_key ELSE pp.document_key * -1 END), (CASE WHEN pp.document_key > 0 THEN 5 ELSE 6 END)
  FROM asuz_db.payment p
  JOIN asuz_db.payment_part pp ON p.payment_key = pp.payment_key
  JOIN asuz_db.loans l ON p.loan_key = l.loan_key
  JOIN asuz_db.received_payment_by_loan rpl ON pp.document_key = rpl.order_key AND rpl.loan_key = l.loan_key
  WHERE pp.document_key =  _document_key and (p.payment_sum is not null AND p.payment_sum > 0)
  INTO _value, _loan_key, _subdivision_key, _kod_podr, _obj_key, _obj_type;

  _creation_date = clock_timestamp()::timestamp;
  _user_key = public.user_key();
  IF _obj_type = 5 THEN
    _additional_value = 0;
  ELSE 
    _additional_value = 1;
  END IF;

  SELECT rf.result, rf.result_str FROM facts.register_fact(_obj_key, _obj_type, 5, _creation_date::timestamp, _loan_key, _value, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
  INTO result, result_str;

  EXCEPTION
WHEN OTHERS THEN
  INSERT INTO logging.log_errors(error_text,error_site) VALUES(SQLERRM,'facts.register_fact_5');
  RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_6_9 (OID = 471154) : 
--
CREATE FUNCTION facts.register_fact_6_9 (
  _lmc_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>07.08.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list>
*/
DECLARE
  _creation_date timestamp;
  _value numeric;
  _subdivision_key int;
  _kod_podr varchar;
  _user_key int;
  _loan_key int;
  _obj_key int;
  _obj_type int;
  _additional_value int;

  _sum_cor_loan numeric;
  _sum_cor_overpayment numeric;
  _sum_cor_overpayment_for_delay numeric;
  _sum_cor_fine numeric;
BEGIN
  PERFORM * FROM asuz_db.loans_manual_correction lmc
  WHERE lmc.lmc_key = _lmc_key;

  IF NOT FOUND THEN
    result = -1;
    result_str = 'Коррекция не найдена'::varchar;
    return;
  END IF;

/*
  SELECT lmc.sum_cor_loan, lmc.sum_cor_overpayment, lmc.sum_cor_overpayment_for_delay, lmc.sum_cor_fine, lmc.creation_date, 
  l.loan_key, l.subdivision_key, (setting_value(l.subdivision_key,'separate_subdivision', lmc.creation_date::date)).value, lmc.correction_type
  FROM asuz_db.loans_manual_correction lmc
  LEFT JOIN asuz_db.loans l on l.loan_key = lmc.loan_key
  WHERE lmc.lmc_key = _lmc_key
  INTO _sum_cor_loan, _sum_cor_overpayment, _sum_cor_overpayment_for_delay, _sum_cor_fine, _creation_date, _loan_key, _subdivision_key, _kod_podr, _additional_value;
*/
/*
  SELECT 
  	lmc.sum_cor_loan, 
    lmc.sum_cor_overpayment, 
    lmc.sum_cor_overpayment_for_delay,
    lmc.sum_cor_fine, 
    lmc.creation_date, 
  	l.loan_key, 
    l.subdivision_key, 
    (setting_value(l.subdivision_key,'separate_subdivision', lmc.creation_date::date)).value, 
    lmc.correction_type
  FROM 
  	asuz_db.loans_manual_correction lmc
  LEFT JOIN 
  	asuz_db.loans l on l.loan_key = lmc.loan_key
  WHERE 
  	lmc.lmc_key = _lmc_key
  INTO 
  	_sum_cor_loan, _sum_cor_overpayment, _sum_cor_overpayment_for_delay, _sum_cor_fine, _creation_date, _loan_key, _subdivision_key, _kod_podr, _additional_value;
*/



  SELECT 
  	lmc.sum_cor_loan, 
    lmc.sum_cor_overpayment, 
    lmc.sum_cor_overpayment_for_delay,
    lmc.sum_cor_fine, 
    lmc.creation_date, 
	lmc.loan_key,
    lmc.correction_type
  FROM 
  	asuz_db.loans_manual_correction lmc
  WHERE 
  	lmc.lmc_key = _lmc_key
  INTO 
  	_sum_cor_loan, 
    _sum_cor_overpayment, 
    _sum_cor_overpayment_for_delay, 
    _sum_cor_fine, 
    _creation_date, 
    _loan_key,
    _kod_podr, 
    _additional_value;




  SELECT  
    l.subdivision_key, 
    (setting_value(l.subdivision_key,'separate_subdivision',_creation_date::date)).value
  FROM 
  	asuz_db.loans l
  WHERE 
  	l.loan_key = _loan_key
  INTO 
  	 _subdivision_key, _kod_podr;




  _user_key = public.user_key();
  
  IF _sum_cor_loan != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 6, _creation_date::timestamp, _loan_key, _sum_cor_loan, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;
  IF _sum_cor_overpayment != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 7, _creation_date::timestamp, _loan_key, _sum_cor_overpayment, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;
  IF _sum_cor_overpayment_for_delay != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 8, _creation_date::timestamp, _loan_key, _sum_cor_overpayment_for_delay, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;
  IF _sum_cor_fine != 0 THEN
    SELECT rf.result, rf.result_str FROM facts.register_fact(_lmc_key, 7, 9, _creation_date::timestamp, _loan_key, _sum_cor_fine, _user_key, _subdivision_key, _kod_podr, _additional_value) rf
    INTO result, result_str;
  END IF;

  EXCEPTION
WHEN OTHERS THEN
  INSERT INTO logging.log_errors(error_text,error_site) VALUES(SQLERRM,'facts.register_fact_6_9');
  RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_6_9_fix (OID = 471155) : 
--
CREATE FUNCTION facts.register_fact_6_9_fix (
  out result integer,
  out result_str character varying
)
RETURNS SETOF record
AS 
$body$
/*<history_list>
<item><date_m>31.07.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list> 
*/
DECLARE 
	_loan_key int;
	_agreement_date	date;
	_count int;
	_lmc_key int;
BEGIN
	FOR _loan_key, _agreement_date IN
		SELECT lrh.loan_key, lrh.agreement_date from asuz_db.loan_restructuring_history lrh
		WHERE lrh.agreement_date >= '2017-09-01' AND lrh.order_key is null AND lrh.discount_amount_payment > 0
	LOOP
		SELECT count(*) FROM asuz_db.loans_manual_correction lmc
		WHERE lmc.loan_key = _loan_key and lmc.creation_date::date = _agreement_date and lmc.comment = 'скидка'
		INTO _count;

		IF _count = 0 OR _count > 1 THEN
			RETURN QUERY SELECT -1, ('Для ' || _loan_key || ' записей: '||_count)::varchar;
		ELSE
			SELECT lmc.lmc_key FROM asuz_db.loans_manual_correction lmc
			WHERE lmc.loan_key = _loan_key and lmc.creation_date::date = _agreement_date and lmc.comment = 'скидка'
			INTO _lmc_key;

			--регистрация событий (фактов)
  			PERFORM * FROM facts.register_fact_6_9(_lmc_key);

			RETURN QUERY SELECT 1, _loan_key::varchar;
		END IF;
	END LOOP;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_6_9_fix2 (OID = 471156) : 
--
CREATE FUNCTION facts.register_fact_6_9_fix2 (
  out result integer,
  out result_str character varying
)
RETURNS SETOF record
AS 
$body$
/*<history_list>
<item><date_m>31.07.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list> 
*/
DECLARE 
  _lmc_key int;
  _correction_type int;
BEGIN
  FOR _lmc_key IN
    SELECT f.obj_key from facts.facts f
    WHERE f.fact_name_key = 7 AND f.fact_date >= '2017-09-01' AND f.additional_value = 4
  LOOP
  	SELECT lmc.correction_type FROM asuz_db.loans_manual_correction lmc 
  	WHERE lmc.lmc_key = _lmc_key
  	INTO _correction_type;

  	IF _correction_type != 4 THEN
  		UPDATE facts.facts set additional_value = _correction_type
  		WHERE obj_key = _lmc_key;
  	ELSE
	    PERFORM * from facts.facts f
	    WHERE f.fact_name_key in (17, 18, 19, 20) AND f.obj_key = _lmc_key;

	    IF NOT FOUND THEN
	  		PERFORM * FROM facts.register_fact_17_20_pdp(_lmc_key);
	  	END IF;
	END IF;
  END LOOP;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_6_9_fix3 (OID = 471157) : 
--
CREATE FUNCTION facts.register_fact_6_9_fix3 (
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>19.12.2017</date_m><task_n>374320</task_n><author>Власов С.А.</author></item>
</history_list> 
*/
DECLARE 
  	_lmc_key int;
  	_count int;
BEGIN
  _count = 0;
  FOR _lmc_key IN
    SELECT lmc.lmc_key from asuz_db.loans_manual_correction lmc
    WHERE lmc.comment = 'Задержка в обработке платежа' AND lmc.creation_date between '2017-09-01' and '2017-10-17' --AND f.loan_key = 963066
  LOOP
	  --регистрация событий (фактов)
	  PERFORM * FROM facts.register_fact_6_9(_lmc_key);
	  _count = _count + 1;
  END LOOP;
  result = _count;

END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function register_fact_6_9_fix4 (OID = 471158) : 
--
CREATE FUNCTION facts.register_fact_6_9_fix4 (
  out result integer,
  out result_str character varying
)
RETURNS SETOF record
AS 
$body$
/*<history_list>
<item><date_m>31.07.2017</date_m><task_n>360968</task_n><author>Власов С.А.</author></item>
</history_list> 
*/
DECLARE 
  _lmc_key int;
  _correction_type int;
BEGIN
  FOR _lmc_key IN
    SELECT f.obj_key from facts.facts f
    WHERE f.fact_name_key IN (6,8,9) AND f.fact_date >= '2017-09-01' AND f.additional_value = 4
  LOOP
  	SELECT lmc.correction_type FROM asuz_db.loans_manual_correction lmc 
  	WHERE lmc.lmc_key = _lmc_key
  	INTO _correction_type;

  	IF _correction_type != 4 THEN
  		UPDATE facts.facts set additional_value = _correction_type
  		WHERE obj_key = _lmc_key;
	END IF;
  END LOOP;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function app_data_load (OID = 471187) : 
--
SET search_path = repair_data, pg_catalog;
CREATE FUNCTION repair_data.app_data_load (
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
DECLARE
_application_key integer;
_app_creation_date date;
_process_key INTEGER; 
_process_status INTEGER;
_was_101 BOOLEAN; 
_last_101 TIMESTAMP; 
_was_113 BOOLEAN; 
_last_113 TIMESTAMP; 
_issued_by_app BOOLEAN; 
_loan_key INTEGER; 
_issued_by_loan BOOLEAN; 
_possible_loan_key INTEGER; 
_has_insurance BOOLEAN; 
_insurance_status INTEGER;
_insurance_contract_payment_key INTEGER;
_subdivision_key INTEGER;
_user_113 INTEGER;
_verification_result INTEGER;
BEGIN
	DELETE FROM repair_data.app_data;
    
    result=0;
    
	FOR _application_key, _app_creation_date IN
    select distinct t.application_key , t.creation_date
    from (
      --все апп, которые были на 101й процедуре, но у которых нет лоана
      select distinct a.application_key , a.creation_date
      from loan_issue.application a
      join b_processes.process_log pl 
          on pl.process_key=a.process_key and pl.procedure_key=101
      left join loans l on l.application_key=a.application_key
      where /*a.issued=1 and*/ l.loan_key is null
      and a.creation_date>'01.01.2018'
      and a.creation_date <'13.01.2018'
      union all
      --все апп, которые были на 200й процедуре, стоит признак выдано в апп, но не стоит в лоанс
      select distinct a.application_key , a.creation_date
      from loan_issue.application a
      join b_processes.process_log pl 
          on pl.process_key=a.process_key and pl.procedure_key=200
      join loans l on l.application_key=a.application_key
      where coalesce(a.issued,0)=1 and (coalesce(l.issued,false) = false)
      and a.creation_date>'01.01.2018'
      and a.creation_date <'13.01.2018') as t 
	LOOP
    	_process_key=null;
        _process_status=null;
        _was_101=false; 
        _last_101=null; 
        _was_113=null; 
        _last_113=null; 
        _issued_by_app=null; 
        _loan_key=null; 
        _issued_by_loan=null; 
        _possible_loan_key=null; 
        _has_insurance=false; 
        _insurance_status=null;
        _subdivision_key=null;
        
        SELECT a.process_key, a.issued::BOOLEAN, a.reg_subdiv, a.verification_result
        FROM loan_issue.application a
        WHERE a.application_key=_application_key
        INTO _process_key, _issued_by_app, _subdivision_key, _verification_result;
        
        SELECT l.loan_key, l.issued FROM public.loans l
        WHERE l.application_key=_application_key
        INTO _loan_key, _issued_by_loan;
        
        SELECT p.status FROM b_processes.process p
        WHERE p.process_key=_process_key
        INTO _process_status;
        
        SELECT p."end" FROM b_processes.process_log p
        WHERE p.process_key=_process_key
        	  AND p.procedure_key=101
        ORDER BY p.log_key desc LIMIT 1
        INTO _last_101;
        
        IF found THEN
        	_was_101=true;
        END IF;
        
        SELECT p."end", p.user_key 
        FROM b_processes.process_log p
        WHERE p.process_key=_process_key
        	  AND p.procedure_key=113
        ORDER BY p.log_key desc LIMIT 1
        INTO _last_113, _user_113;
        
        IF found THEN
        	_was_113=true;
        END IF;
        
        SELECT i.insurance_contract_payment_key, i.loan_key, i.contract_status_key
        FROM insurance.insurance i
        WHERE i.process_key=_process_key
        INTO _insurance_contract_payment_key, _possible_loan_key, _insurance_status;
        
        IF _insurance_contract_payment_key=2 THEN
        	_has_insurance=true;
        END IF;
    
    	INSERT INTO repair_data.app_data (application_key, app_creation_date, process_key, process_status,
        	was_101, last_101, was_113, last_113, issued_by_app, loan_key, issued_by_loan, 
  			possible_loan_key, has_insurance, insurance_status, subdivision_key, user_113, verification_result)
        VALUES (_application_key, _app_creation_date, _process_key, _process_status,
        	_was_101, _last_101, _was_113, _last_113, _issued_by_app, _loan_key, _issued_by_loan, 
  			_possible_loan_key, _has_insurance, _insurance_status, _subdivision_key, _user_113, _verification_result);
            
    	result=result+1;
    END LOOP;

	result_str='';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function app_data_online_load (OID = 471188) : 
--
CREATE FUNCTION repair_data.app_data_online_load (
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
DECLARE
_rec record;
BEGIN
	DELETE FROM repair_data.app_data_online;
	result=0;

	FOR _rec IN
    select distinct a.application_key, a.process_key, a.creation_date, aa.account, a.online_possibility, a.withdrawal_method,
    t.master_object_key::INTEGER as possible_loan_key, a.reg_subdiv, t.creation_date as last_113, l.loan_key, a.issued::BOOLEAN as issued_by_app, l.issued as issued_by_loan
    from loan_issue.application a
    join loan_issue.application_account aa on aa.application_key=a.application_key
    left join paym.tasks t on strpos(t.xml_send,aa.account)>0
    left join loans l on l.application_key=a.application_key
    where /*a.issued=1 and l.loan_key is null
    and*/ a.sales_channel_key=9
    and coalesce(a.issued,0)=1 and (coalesce(l.issued,false) = false)
    and a.creation_date>'01.01.2018'
    and a.creation_date <'13.01.2018'
    LOOP
    	if _rec.withdrawal_method in (0,2) then
        	SELECT p."end" FROM b_processes.process_log p
            WHERE p.process_key=_rec.process_key
            	AND p.procedure_key=7210
           	INTO _rec.last_113;
        end if;
        
    	IF _rec.withdrawal_method=0 THEN
        	---нал
            select eco.loan_key from (
            select borrower_fio(a.borrower_key) as fio, a.application_key
            from loan_issue.application a
            where a.application_key=_rec.application_key)as t
            join public.expenditure_cash_orders eco on t.fio=eco.borrower_name
            where eco.date_issue::date='12.01.2018'
            INTO _rec.possible_loan_key;
        ELSIF _rec.withdrawal_method=2 THEN
        	--безнал
            select rs.loan_key
            from loan_issue.application a
            join cft.req_card rc on rc.process_key=a.process_key and rc.type_data=4
            join cft.req_send rs on strpos(rc.xml_out,rs.oid)>0
            where a.application_key=_rec.application_key
            INTO _rec.possible_loan_key;
        END IF;
    
    	INSERT INTO 
        repair_data.app_data_online(application_key, process_key,
        		creation_date, account, online_possibility, withdrawal_method,
        		possible_loan_key, subdivision_key, last_113, loan_key, issued_by_app, issued_by_loan) 
      	VALUES (_rec.application_key, _rec.process_key, 
        		_rec.creation_date, _rec.account, _rec.online_possibility, _rec.withdrawal_method,
        		_rec.possible_loan_key, _rec.reg_subdiv, _rec.last_113, _rec.loan_key, _rec.issued_by_app, _rec.issued_by_loan);
        result=result+1;
    END LOOP;
    
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function fact_4_repair (OID = 471189) : 
--
CREATE FUNCTION repair_data.fact_4_repair (
)
RETURNS integer
AS 
$body$
DECLARE
/*<history_list>
<item><date_m>12.02.2018</date_m><task_n></task_n><author>Власов С.А.</author></item>
</history_list>
*/
	_document_key int;
	_document_key_for_check int;
  	_count int;
BEGIN
  _count = 0;
	FOR _document_key, _document_key_for_check IN
		select pp.document_key, (CASE WHEN pp.document_key > 0 THEN pp.document_key ELSE pp.document_key * -1 END) from payments.payment p
		left join payments.payment_part pp on p.payment_key = pp.payment_key
		where payment_date = '2018-01-12'
	LOOP
		PERFORM * FROM facts.facts f
		WHERE f.obj_key = _document_key_for_check and f.fact_name_key = 4;

		IF NOT FOUND THEN
			--регистрация событий (фактов)
	  		PERFORM * FROM facts.register_fact_4(_document_key);

  			_count = _count + 1;
	  	END IF;
	END LOOP;

  return _count;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function fact_6_9_repair (OID = 471190) : 
--
CREATE FUNCTION repair_data.fact_6_9_repair (
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*<history_list>
<item><date_m>12.02.2018</date_m><task_n></task_n><author>Власов С.А.</author></item>
</history_list> 
*/
DECLARE 
  	_lmc_key int;
  	_count int;
BEGIN
  _count = 0;
  FOR _lmc_key IN
    SELECT lmc.lmc_key from public.loans_manual_correction lmc
    WHERE lmc.creation_date = '2018-01-12'
  LOOP
	PERFORM * FROM facts.facts f
	WHERE f.obj_key = _lmc_key and f.fact_name_key in (6, 7, 8, 9);

	IF NOT FOUND THEN
		--регистрация событий (фактов)
		PERFORM * FROM facts.register_fact_6_9(_lmc_key);
		PERFORM * FROM facts.register_fact_17_20_pdp(_lmc_key);

  		_count = _count + 1;
	END IF;
  END LOOP;
  result = _count;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function proceed_app_data (OID = 471191) : 
--
CREATE FUNCTION repair_data.proceed_app_data (
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
DECLARE
_rec record;
_borrower_key integer;
_loan_purpose varchar;
_req_sum numeric;
_req_term integer;
_user_name varchar;
_term_days integer;
_reject_reason integer;
BEGIN
	IF db_type()='WORK' THEN
    	raise exception 'Отключено выполнение на БОЮ!';
    END IF;
    
    FOR _rec IN
    SELECT * FROM repair_data.app_data a
    LOOP
        SELECT a.borrower_key, a.loan_purpose, a.req_sum, a.req_term
        FROM loan_issue.application a
        WHERE a.application_key=_rec.application_key
        INTO _borrower_key, _loan_purpose, _req_sum, _req_term;
                    
        _user_name=user_fio(_rec.user_113);
                    
        SELECT te.term_days FROM product.terms te
        WHERE te.term_key = _req_term
        INTO _term_days;
    
    	IF COALESCE(_rec.issued_by_app,false) THEN
        	--Согласно данным в аппликейшн - займ был выдан
            IF _rec.loan_key is null THEN
            --Если нет записи в лоанс - добавляем ее
            	INSERT INTO public.loans(loan_key, borrower_key, application_key, for_what_purpose, 
                		requested_amount, requested_term, calc_date, calc_user_name, 
                        subdivision_key, reject_reason, requested_period)
	  			VALUES(_rec.possible_loan_key, _borrower_key, _rec.application_key, _loan_purpose, 
                		_req_sum, _req_term, _rec.last_113, _user_name, 
                        _rec.subdivision_key, 99, _term_days);
            END IF;
            
            UPDATE repair_data.app_data a
            SET proceed = 1
            WHERE a.process_key=_rec.process_key;
            
            IF _rec.issued_by_loan is null THEN
            	perform repair_data.set_contract_parametrs(_rec.process_key);
                UPDATE repair_data.app_data a
                SET proceed = 2
                WHERE a.process_key=_rec.process_key;
            END IF;
            
            
        ELSE
        	--Согласно данным в аппликейшн - займ не был выдан
            IF _rec.was_113 THEN
            	--было принятие решения
            	IF _rec.verification_result=-1 THEN
                	_reject_reason=-100;
                ELSE 
                	_reject_reason=99;
                END IF;
                --добавляем запись в лоанс
                INSERT INTO public.loans(borrower_key, application_key, for_what_purpose, 
                		requested_amount, requested_term, calc_date, calc_user_name, 
                		subdivision_key, reject_reason, requested_period)
                VALUES(_borrower_key, _rec.application_key, _loan_purpose, 
                		_req_sum, _req_term, _rec.last_113, _user_name, 
                		_rec.subdivision_key, _reject_reason, _term_days);
                UPDATE repair_data.app_data a
                SET proceed = 3
                WHERE a.process_key=_rec.process_key;
            END IF;
            
        END IF;
    END LOOP;
    
    
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function proceed_app_data_online (OID = 471192) : 
--
CREATE FUNCTION repair_data.proceed_app_data_online (
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
DECLARE
_rec record;
_borrower_key integer;
_loan_purpose varchar;
_req_sum numeric;
_req_term integer;
_user_name varchar;
_term_days integer;
_reject_reason integer;
BEGIN
	IF db_type()='WORK' THEN
    	raise exception 'Отключено выполнение на БОЮ!';
    END IF;
    
    FOR _rec IN
    SELECT * FROM repair_data.app_data_online a
    LOOP
        SELECT a.borrower_key, a.loan_purpose, a.req_sum, a.req_term
        FROM loan_issue.application a
        WHERE a.application_key=_rec.application_key
        INTO _borrower_key, _loan_purpose, _req_sum, _req_term;
                    
        _user_name=user_fio(online_back.get_online_back_user_key());
                    
        SELECT te.term_days FROM product.terms te
        WHERE te.term_key = _req_term
        INTO _term_days;
    
    	--Согласно данным в аппликейшн - займ был выдан
        IF _rec.loan_key is null THEN
            --Если нет записи в лоанс - добавляем ее
           	INSERT INTO public.loans(loan_key, borrower_key, application_key, for_what_purpose, 
               		requested_amount, requested_term, calc_date, calc_user_name, 
                    subdivision_key, reject_reason, requested_period)
	  		VALUES(_rec.possible_loan_key, _borrower_key, _rec.application_key, _loan_purpose, 
               		_req_sum, _req_term, _rec.last_113, _user_name, 
                    _rec.subdivision_key, 99, _term_days);
        END IF;
        
        perform repair_data.set_contract_parametrs(_rec.process_key);
            
        UPDATE repair_data.app_data_online a
        SET proceed = 1
        WHERE a.process_key=_rec.process_key;
             
    END LOOP;
    
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function repayment_schedule_make (OID = 471193) : 
--
CREATE FUNCTION repair_data.repayment_schedule_make (
  _loan_key integer,
  _agreement_key integer
)
RETURNS integer
AS 
$body$
DECLARE
/*<history_list>
</history_list>
*/
  _rec record;
  _current_overpayment_original_without_prepaid NUMERIC(8,2);
  _loan_sum numeric(8,2);
  _loan_overpayment numeric(8,2);
  _number_of_payment integer;
  _creation_date date;
  _date_repayment date;
  _period integer;
  _overpayment NUMERIC(8,2);
  _tmp_sum NUMERIC(8,2);
  _sign  integer;
  _refunds NUMERIC(8,2);
  _type_repayment_schedule_key INTEGER;
  _rate_rs NUMERIC;
  _limit_term INTEGER;
  _psk NUMERIC;
  _psk_result INTEGER;
  _psk_result_str VARCHAR;

 _payment_period INTERVAL;
BEGIN
  DELETE FROM receiving_payments.repayment_schedules rs
  WHERE rs.loan_key = _loan_key;

  SELECT pl.*, te.max_period_of_days
  FROM public.loan_restructuring_history pl
  LEFT JOIN product.tarifs t ON pl.tariff_key = t.tarif_key
  LEFT JOIN product.terms te ON t.term_key = te.term_key
  WHERE pl.agreement_key = _agreement_key
  INTO _rec;

  _limit_term = COALESCE(_rec.max_period_of_days,0);

  --прошлые платежи
  INSERT INTO receiving_payments.repayment_schedules(
   loan_key, payment_date, loan_sum, overpayment_sum,
   delay, debtor, returned, permissible_loan_key,
   returned_date, old_key)
  SELECT _loan_key, res.date_repayment, CASE WHEN rs.returned=true THEN rs.loan_sum ELSE res.paid_loan END, rs.overpayment_sum,
   rs.delay, rs.debtor, rs.returned, 0,
   rs.returned_date,  res.rs_key
  FROM repayment_schedules_detail(_loan_key) as res
  JOIN repayment_schedules rs ON res.rs_key=rs.repayment_schedule_key
  WHERE res.date_repayment < '2018-01-12'
  ORDER BY res.date_repayment;

  INSERT INTO receiving_payments.repayment_schedules(
   loan_key, payment_date, loan_sum, overpayment_sum,
   delay, debtor, returned, permissible_loan_key,
   returned_date, old_key)
  VALUES(_loan_key, '2018-01-12', _rec.prepaid_loan, _rec.current_overpayment_original, FALSE, FALSE, FALSE, 0, null, 0);

  --новый график
  _creation_date = _rec.agreement_date;
  _payment_period = _rec.payment_period;

  SELECT COALESCE(pl.type_repayment_schedule_key, 1)
  FROM loans l
  JOIN permissible_loans pl ON l.permissible_loan_key = pl.key
  WHERE l.loan_key = _loan_key
  INTO _type_repayment_schedule_key;

  --для режима графика аннуитет - пересчитываем ставку
  IF _type_repayment_schedule_key = 2 THEN
    _rate_rs = product.calc_annuity_tariff(_rec.loan_rate, _rec.nmb_of_payments, _payment_period, '2018-01-12', _limit_term);
  ELSE
    _rate_rs = _rec.loan_rate;
  END IF;

  INSERT INTO receiving_payments.repayment_schedules( loan_key, payment_date,
       loan_sum, overpayment_sum, delay, debtor, returned, permissible_loan_key)
  SELECT _loan_key, rs.payment_date,
    rs.loan_sum_rs, rs.overpayment_sum_rs, false,false,false,0
  FROM repayment_schedule_calc(_rec.loan_sum,--сумма займа
  _rate_rs,--ставка в день по займу c учетом способа формирования графика платежей
  _rec.nmb_of_payments,--кол-во платежей по графику
  _type_repayment_schedule_key,--способ формирования графика платежей
  _payment_period,--периодичность выплат по графику
  _rec.fixed_payment_day,--фиксированный день платежа
  '2018-01-12',
  _limit_term
  ) AS rs;

  SELECT SUM(rs.loan_sum + rs.overpayment_sum), SUM(rs.overpayment_sum)
  FROM receiving_payments.repayment_schedules rs
  WHERE rs.loan_key = _loan_key  AND rs.payment_date>'2018-01-12'
  INTO _refunds, _overpayment;

  RETURN 1;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function set_contract_parametrs (OID = 471194) : 
--
CREATE FUNCTION repair_data.set_contract_parametrs (
  _process_key integer
)
RETURNS integer
AS 
$body$
DECLARE
/*<history_list>
<item><date_m>10.03.2015</date_m><task_n>220340</task_n><author>Пономаренко А.Б.</author></item>
<item><date_m>23.05.2016</date_m><task_n>289582</task_n><author>Пономаренко А.Б.</author></item>
<item><date_m>09.06.2017</date_m><task_n>357133</task_n><author>Пономаренко А.Б.</author></item>
<item><date_m>13.12.2017</date_m><task_n>382307</task_n><author>Власов С.А.</author></item>
</history_list>
*/
	_app_key int;
	_borrower_key int;
	_creation_date date;
	_withdrawal_method int;
	_serial varchar;
	_number varchar;
	_number1 varchar;
	_number2 varchar;
	_contract_number varchar;
    _loan_fine_rate numeric;
	_loan_maximum_delay int;
	_log_key int;
	_calc_user_key int;
	_calc_date date;
	_calc_user_name varchar;
	_req_sum numeric;
	_req_term int;
	_loan_purpose varchar;
	_reg_subdiv int;
	_req_term_days int;
	_loan_sum int;
	_loan_term_key int;
    _fixed_payment_day INTEGER;
	_loan_term_days int;
	_rate float;
	_overpayment int;
	_refunds int;
	_tariff_key int;
	_nmb_of_payments int;
	_payment numeric;
	_used_discount numeric;
	_loan_key int;
	_permissible_loan_key int;
	_payment_date date;
	_overpayment_sum numeric;
    _result int;
    _discount_loalty_perc numeric;
    _loan_sum_rs numeric;
    _overpayment_sum_rs numeric;
    _rate_rs NUMERIC;
    _type_repayment_schedule_key INTEGER;  
    _sales_channel_online INTEGER;  
    _sales_channel INTEGER;
    _claim INTEGER;
BEGIN
	--определяем параметры, необходимые для работы хранимки
	SELECT b.borrower_key, a.withdrawal_method, b.pass_series, b.pass_number, 
    	a.application_key, a.reg_subdiv, a.sales_channel_key, a.claim_right
    FROM loan_issue.application a
    JOIN public.t_borrowers b on b.borrower_key = a.borrower_key
    WHERE a.process_key = _process_key
    INTO _borrower_key, _withdrawal_method, _serial, _number, 
    	_app_key, _reg_subdiv, _sales_channel,_claim;
    
    SELECT loan_issue.get_borrower_key_by_application(_app_key)
	INTO _borrower_key;

	SELECT * from "public".setting_value_n('fine_rate')
	INTO _loan_fine_rate;

	SELECT * from "public".setting_value_n('max_period_delay')
	INTO _loan_maximum_delay;
    
    SELECT pl1.loan_sum, pl1.loan_term_key, pl1.fixed_payment_day 
    FROM loan_issue.tmp_permissible_loan pl1
    WHERE pl1.application_key = _app_key
    INTO _loan_sum, _loan_term_key, _fixed_payment_day;
    
    SELECT loan_key, creation_date FROM loans
    WHERE application_key = _app_key
    INTO _loan_key, _creation_date;
    
    _sales_channel_online = online_back.get_online_back_prod_param_val('SALES_CHANNEL')::INTEGER;
    /*IF _sales_channel = _sales_channel_online THEN
      PERFORM loan_issue_online.calculate_loan_by_application(_app_key, _loan_sum, _loan_term_key, _fixed_payment_day);
    ELSE
      -- обновляем таблицы с параметрами займа.
      PERFORM loan_issue.calculate_loan_by_application(_process_key, _loan_sum, _loan_term_key, _fixed_payment_day);
    END IF;*/
       
	--создать запись в таблице public.permissible_loans
    SELECT pl.loan_sum, pl.loan_term_days, pl.rate, pl.overpayment, pl.refunds, 
	pl.tarif_key, pl.nmb_of_payments, pl.payment, pl.used_discount, pl.loan_term_key, pl.discount_loalty_perc,
    pl.fixed_payment_day, pl.rate_rs, pl.type_repayment_schedule_key
    FROM loan_issue.application a
	LEFT JOIN loan_issue.tmp_permissible_loan pl on a.application_key = pl.application_key
	LEFT JOIN product.terms te on te.term_key = a.req_term
	WHERE a.application_key = _app_key
	INTO _loan_sum, _loan_term_days, _rate, _overpayment, _refunds, _tariff_key, 
	_nmb_of_payments, _payment, _used_discount, _loan_term_key, _discount_loalty_perc,
    _fixed_payment_day, _rate_rs, _type_repayment_schedule_key ;
    
    DELETE FROM public.permissible_loans
    WHERE loan_key = _loan_key;
    
    INSERT INTO public.permissible_loans(loan_key, tariff_key, period_key, period, max_amount, rate, refunds, overpayment, nmb_of_payments, sum_payment, discount_amount, discount_loalty_perc, fixed_payment_day, type_repayment_schedule_key, rate_rs, version_number, version_date, version_type)
	VALUES(_loan_key, _tariff_key, _loan_term_key, _loan_term_days, _loan_sum, _rate, _refunds, _overpayment, _nmb_of_payments, _payment, _used_discount, _discount_loalty_perc, _fixed_payment_day, _type_repayment_schedule_key, _rate_rs, 0, _creation_date, 0)
	RETURNING key
	INTO _permissible_loan_key;
    
	--создать запись в таблице public.repayment_schedules
    DELETE FROM public.repayment_schedules rs
    WHERE rs.loan_key = _loan_key;
    
	FOR _payment_date, _loan_sum_rs, _overpayment_sum_rs IN
	SELECT payment_date, loan_sum, overpayment_sum 
    FROM loan_issue.tmp_payment_schedules ps
	WHERE ps.application_key = _app_key
    ORDER BY ps.key
	LOOP
		INSERT INTO "public".repayment_schedules(loan_key, payment_date, loan_sum, overpayment_sum, permissible_loan_key)
		VALUES(_loan_key, _payment_date, _loan_sum_rs, _overpayment_sum_rs, _permissible_loan_key);
	END LOOP;
  
    --Определить номер нового договора
	SELECT '12.01.2018'::date
	INTO _creation_date;
    
    IF _process_key = 12153673 THEN
      _creation_date = '2017.06.10';
    END IF;

	_number1 = 'Z' || _serial || _number;

	SELECT lpad((SELECT (COUNT(*)+1)::varchar from loans l
	WHERE l.borrower_key = _borrower_key AND l.issued = true::BOOLEAN),	2,'0')
	INTO _number2;
	
	_contract_number = _number1 || _number2;
    
   -- raise notice '% % % % %', _number1, _number2, _contract_number, _creation_date, _withdrawal_method;

	--Установить новые параметры договора
	UPDATE public.loans l
    SET loan_term = _loan_term_days, loan_sum = _loan_sum,  loan_rate = _rate, loan_plan_overpayment = _overpayment, loan_plan_to_returns = _refunds, 
    loan_fine_rate = _loan_fine_rate, loan_maximum_delay = _loan_maximum_delay, creation_date = _creation_date, 
    non_cash = _withdrawal_method, contract = _contract_number, permissible_loan_key = _permissible_loan_key
    , claim_right=coalesce(_claim,l.claim_right)
    WHERE loan_key = _loan_key;
    
	--
    UPDATE loans l
    SET issued=TRUE,
        claim_right=coalesce(_claim,l.claim_right)
    WHERE l.loan_key = _loan_key;
    
    ----регистрация событий (фактов)
    PERFORM * FROM facts.register_fact_1(_loan_key);

    PERFORM * FROM facts.register_fact_2(_permissible_loan_key, _creation_date::timestamp);

	RETURN 1;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function type_2_3_fix (OID = 471196) : 
--
CREATE FUNCTION repair_data.type_2_3_fix (
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
DECLARE
	_loan_key int;
	_list_recovery_key int;
BEGIN
	FOR _loan_key, _list_recovery_key IN 
		select lrt.loan_key, lrt.list_recovery_key from repair_data.list_recovery_type_2_3 lrt
		WHERE lrt.repair_type in (2, 3) and lrt.status = 0
         -- AND lrt.loan_key = 1024389
	LOOP
		--проверка погашения графика платежей
		PERFORM repayment_schedules_check_return(_loan_key);
		--проводим проверку займа на полное погашение и закрываем его если это так
		PERFORM loan_standart_closing(_loan_key);

		UPDATE repair_data.list_recovery_type_2_3 set status = 1
		WHERE list_recovery_key = _list_recovery_key;
	END LOOP;

	result = 1;
	result_str='';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function type_4_fix (OID = 471197) : 
--
CREATE FUNCTION repair_data.type_4_fix (
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
DECLARE
	_loan_key int;
	_list_recovery_key int;
	_version_date date;
	_permissible_loan_key int;
	_repayment_schedule_key int;
	_agreement_key int;
	_max_repayment_schedule_key int;
	_min_repayment_schedule_key int;
	_old_permissible_loan_key int;
BEGIN
	FOR _loan_key, _list_recovery_key IN 
		select lrt.loan_key, lrt.list_recovery_key from repair_data.list_recovery_type_2_3 lrt
		WHERE lrt.repair_type = 4 and lrt.status = 0
          --AND lrt.loan_key = 1122634
	LOOP
		PERFORM * FROM public.loan_restructuring_history lrh
		WHERE lrh.loan_key = _loan_key and lrh.agreement_date > '2018-01-12';

		IF FOUND THEN
			UPDATE repair_data.list_recovery_type_2_3 set status = -5
			WHERE list_recovery_key = _list_recovery_key;
			CONTINUE;
		END IF;

		INSERT INTO permissible_loans(
			loan_key, tariff_key, period_key, period,
			max_amount, rate, refunds, overpayment,
			nmb_of_payments, bonus, tuning, discount_amount,
			sum_payment, discount_loalty_perc, fixed_payment_day,
			rate_rs, type_repayment_schedule_key, version_number, version_date, version_type
		)
		select ra.loan_key,ra.tariff_key, ra.credit_product_key, ra.loan_term,
		    ra.loan_sum, ra.loan_rate, ra.loan_returns, ra.loan_overpayment,
		    ra.nmb_of_payments, pl.bonus, pl.tuning, ra.discount_amount_rs,
		    ra.new_sum_payment, ra.discount_loalty_perc, ra.fixed_payment_day,
		    ra.rate_rs, ra.type_repayment_schedule_key, ra.agreement_number, ra.agreement_date, 1
		from public.loan_restructuring_history ra
		LEFT JOIN permissible_loans pl ON ra.permissible_loan_key=pl.key
		WHERE ra.loan_key = _loan_key and ra.agreement_date = '2018-01-12'
		returning permissible_loans.key, permissible_loans.version_date
		INTO _permissible_loan_key, _version_date;

		select ra.agreement_key, ra.permissible_loan_key
		from public.loan_restructuring_history ra
		WHERE ra.loan_key = _loan_key and ra.agreement_date = '2018-01-12'
		INTO _agreement_key, _old_permissible_loan_key;

		PERFORM * FROM repair_data.repayment_schedule_make(_loan_key, _agreement_key);

		SELECT max(repayment_schedule_key), min(repayment_schedule_key) FROM receiving_payments.repayment_schedules rs
		WHERE rs.loan_key = _loan_key
		INTO _max_repayment_schedule_key, _min_repayment_schedule_key;

		FOR _repayment_schedule_key IN
			select distinct(t.repayment_schedule_key) from (
				select mo.repayment_schedule_key from public.movement_of_money mo
				left join payments.payment_part pp on mo.order_key = pp.document_key
				left join payments.payment p on p.payment_key = pp.payment_key
				WHERE p.loan_key = _loan_key
				UNION		
				select lmc.repayment_schedule_key from public.loans_manual_correction_rs lmc
				WHERE lmc.loan_key = _loan_key
			) t
			order by t.repayment_schedule_key
		LOOP
			IF _min_repayment_schedule_key > _max_repayment_schedule_key THEN
				exit;
			END IF;

			UPDATE receiving_payments.repayment_schedules set repayment_schedule_key = _repayment_schedule_key
			WHERE repayment_schedule_key = _min_repayment_schedule_key;

			_min_repayment_schedule_key = _min_repayment_schedule_key + 1;
		END LOOP;

		IF _min_repayment_schedule_key > _max_repayment_schedule_key THEN
			UPDATE repair_data.list_recovery_type_2_3 set status = -6
			WHERE list_recovery_key = _list_recovery_key;

			CONTINUE;
		END IF;

		--перенести новый график
		INSERT INTO repayment_schedules(repayment_schedule_key, loan_key, payment_date,
			loan_sum, overpayment_sum, delay, debtor, returned,
			permissible_loan_key, returned_date)
		SELECT rs.repayment_schedule_key, rs.loan_key, rs.payment_date,
			rs.loan_sum, rs.overpayment_sum, rs.delay, rs.debtor, rs.returned,
			_permissible_loan_key, rs.returned_date
		FROM receiving_payments.repayment_schedules rs
		WHERE rs.loan_key=_loan_key;

		DELETE FROM receiving_payments.repayment_schedules rs
		WHERE rs.loan_key=_loan_key;

		update public.loans set permissible_loan_key = _permissible_loan_key
		WHERE loan_key = _loan_key;

		DELETE FROM public.calculations c
		where c.repayment_schedule_key in (
			select rs.repayment_schedule_key from public.repayment_schedules rs
			where rs.permissible_loan_key = _old_permissible_loan_key
		) and c.calc_date > '2018-01-12';

		PERFORM * FROM facts.register_fact_2(_permissible_loan_key, _version_date, 1);

		UPDATE repair_data.list_recovery_type_2_3 set status = 1
		WHERE list_recovery_key = _list_recovery_key;
	END LOOP;

	result = 1;
	result_str='';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function fill_facts_by_loan (OID = 471319) : 
--
SET search_path = facts, pg_catalog;
CREATE FUNCTION facts.fill_facts_by_loan (
  _loan_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>10.10.2017</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__loan RECORD;
    v__count_loans INTEGER;
    v__rec__generate_facts_by_loan RECORD;
    v__rec RECORD;
    v__count_fact INTEGER;
    v__rec__facts RECORD; -- Запись с выбранным фактом
BEGIN
	

	SELECT * FROM asuz_db.loans l
    WHERE l.loan_key = _loan_key
    INTO v__loan;

	-- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_loans = ROW_COUNT;
    -- -------------------------------------------------------------------------
    
    -- Проверяем на наличие указанного займа
    -- -------------------------------------------------------------------------
    IF v__count_loans != 1 THEN
        result = -1;
        result_str = 'Указанный займ не найден';
        RETURN;
    END IF;
    -- -------------------------------------------------------------------------

    
	-- Удаляем имеющиеся факты для текущего лоана    
    -- -------------------------------------------------------------------------
	DELETE FROM facts.facts f WHERE f.loan_key = _loan_key;
    -- -------------------------------------------------------------------------

    
    
    IF v__loan.creation_date < '01.09.2017' THEN

    
    	--Выдача, смена версии графика платежей, оплаты и коррекции
        /*
        Генерация фактов 1,2,4,6_9
        */
		SELECT * FROM facts.generate_facts_by_loan(_loan_key) 
        INTO v__rec__generate_facts_by_loan;
        
    END IF;
    
    
    
       
        -- Сделано через LOOP и DISTINCT потому как в АСУЗ присутствуют задвоенные данные
        -- такой вот костыль
        FOR v__rec IN 
            SELECT DISTINCT
              f.fact_key,
              f.fact_date,
              f.fact_name_key,
              f.obj_type,
              f.obj_key,
              f.loan_key,
              f.value,
              f.user_key,
              f.subdivision_key,
              f.kod_podr,
              f.additional_value,
              f.additional_value2
            FROM asuz_db.facts f
            WHERE f.loan_key = _loan_key
            AND f.fact_date >='01.09.2017'
        LOOP
            INSERT INTO 
              facts.facts
            (
              fact_date,
              fact_name_key,
              obj_type,
              obj_key,
              loan_key,
              value,
              user_key,
              subdivision_key,
              kod_podr,
              additional_value,
              additional_value2,
              creation_date
            )
            VALUES (
              v__rec.fact_date,
              v__rec.fact_name_key,
              v__rec.obj_type,
              v__rec.obj_key,
              v__rec.loan_key,
              v__rec.value,
              v__rec.user_key,
              v__rec.subdivision_key,
              v__rec.kod_podr,
              v__rec.additional_value,
              v__rec.additional_value2,
              now()
            );
            
        END LOOP;
    
    
    
    /*
    INSERT INTO 
      facts.facts
    (
      fact_date,
      fact_name_key,
      obj_type,
      obj_key,
      loan_key,
      value,
      user_key,
      subdivision_key,
      kod_podr,
      additional_value,
      additional_value2,
      creation_date
    )
    SELECT 
      fact_date,
      fact_name_key,
      obj_type,
      obj_key,
      loan_key,
      value,
      user_key,
      subdivision_key,
      kod_podr,
      additional_value,
      additional_value2,
      creation_date
    FROM asuz_db.facts f
    WHERE f.loan_key = _loan_key;
    */
    
    -- Проеряем, есть ли факт выдачи займа (есть такие что без факта), тогда генерим
    -- =========================================================================
    
    -- Получаем факты выдачи займа
    -- -------------------------------------------------------------------------
    SELECT * 
    FROM facts.facts t
    WHERE
        t.fact_name_key = 1 AND 
        t.loan_key = _loan_key AND 
        t.fact_date = v__loan.creation_date
    INTO 
    	v__rec__facts;
    -- -------------------------------------------------------------------------
    
    
	-- Получаем количество полученных записей    
    -- -------------------------------------------------------------------------
    GET DIAGNOSTICS v__count_fact = ROW_COUNT;
    -- -------------------------------------------------------------------------
    
    IF v__count_fact = 0 THEN
		PERFORM facts.register_fact_1(_loan_key);
    END IF;
    
    
    -- =========================================================================
    

	result = 1;
	result_str = 'OK';
    
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function generate_facts_by_loan (OID = 471324) : 
--
CREATE FUNCTION facts.generate_facts_by_loan (
  _loan_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
           Хранимка для генерации фактов для займов до 01_09_2017
</description>
<history_list>
	<item><date_m>03.05.2018</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
    <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__loan RECORD;
    v__rec__permissible_loan RECORD;
    v__rec RECORD;
    
    v__now TIMESTAMP;
    v__startcalc TIMESTAMP;
    v__key INTEGER;
BEGIN
    -- Очищаем таблицу
    DELETE FROM loan_calc_history.timelog WHERE true;   

    -- Вставляем данные о вызываемой хранимке
    v__now = clock_timestamp();
    INSERT INTO loan_calc_history.timelog
    ( description, startcalc, endcalc, timecalc )
    VALUES 
    ( 'fill_facts_by_loan', v__now, NULL, NULL )
    RETURNING key
    INTO v__key; 
    v__startcalc =  clock_timestamp();  


	-- Регистрируем выдачу займа (1)
	PERFORM facts.register_fact_1(_loan_key);
    
    
    

    -- Получаем данные по указанному займу
    -- -------------------------------------------------------------------------
	SELECT 
    *
    FROM asuz_db.loans l
    WHERE l.loan_key = _loan_key
    INTO v__loan;
    -- -------------------------------------------------------------------------
    
    
 



    
    
    
    -- Смена версии графика платежей (2)
    -- -------------------------------------------------------------------------
    FOR v__rec IN
        SELECT 
            * 
        FROM
            asuz_db.permissible_loans pl
        WHERE 
        	pl.loan_key = _loan_key
            AND 
            pl.version_date < '01.09.2017'
        ORDER BY
        	pl.key DESC
    LOOP 
        PERFORM facts.register_fact_2(
            v__rec.key, 
            v__rec.version_date,
            v__rec.version_type);
    END LOOP;
    -- -------------------------------------------------------------------------
    

    -- 

            
            
	-- Поступление средств по договору займа
    FOR v__rec IN
        SELECT * 
        FROM asuz_db.payment_part pp
        WHERE 
        	pp.payment_key IN
              (
                  SELECT p.payment_key
                  FROM asuz_db.payment p
                  WHERE p.loan_key = _loan_key
                  AND p.payment_date < '01.09.2017'
              )
    LOOP
    	PERFORM  facts.register_fact_4(v__rec.document_key);    
    END LOOP;
    

    -- Коррекции (русные коррекции займа)      
    FOR v__rec IN
        SELECT * FROM
        asuz_db.loans_manual_correction lmc WHERE
        lmc.loan_key = _loan_key	
        AND
        lmc.creation_date < '01.09.2017'
    LOOP
    	PERFORM  facts.register_fact_6_9(v__rec.lmc_key);   
    END LOOP;

    UPDATE loan_calc_history.timelog 
    SET endcalc = clock_timestamp(), timecalc = age(clock_timestamp(),v__startcalc)
    WHERE  key = v__key; 

    
	result = 1;
    result_str = 'OK';
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function setting_value (OID = 471401) : 
--
SET search_path = public, pg_catalog;
CREATE FUNCTION public.setting_value (
  _subdivision_key integer,
  _name character varying,
  _date date,
  out value character varying,
  out data_type character varying,
  out status integer
)
RETURNS record
AS 
$body$
DECLARE
/*<history_list>
<item><date_m>05.08.2015</date_m><task_n>235436</task_n><author>Пономаренко А.Б.</author></item>
</history_list>*/
  owner_key integer;
BEGIN
  if _subdivision_key = 0 then
    select null, sn.setting_data_type
    from asuz_db.setting_name sn
    where sn.setting_name=_name
    into value, data_type;
    status=-1;
    return;
    return;
  end if;

  SELECT sv.value, sn.setting_data_type
  FROM asuz_db.setting_name sn
  JOIN asuz_db.setting_values sv ON sn.setting_key=sv.setting_name_key
  WHERE sn.setting_name=_name AND sv.subdivision_key=_subdivision_key
    AND COALESCE(sv.date_b,'2012.01.01')<=_date
  ORDER BY COALESCE(sv.date_b,'2012.01.01') DESC
  LIMIT 1
  INTO value, data_type;

  if found then
    status = 2;
    return;
  else
    select s.subdivision_owner_key from asuz_db.t_subdivisions s
    where s.subdivision_key = _subdivision_key
    into owner_key;
    if not found then
      raise exception 'Владелец не найден для subdivision_key =%',_subdivision_key;
    end if;

    select * from  setting_value(owner_key, _name, _date)
    into value, data_type, status;
    if status<>-1 then
      status=1;
    end if;
    return;
  end if;

END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function setting_value (OID = 471402) : 
--
CREATE FUNCTION public.setting_value (
  _subdivision_key integer,
  _name character varying
)
RETURNS TABLE (
  value character varying,
  data_type character varying,
  status integer
)
AS 
$body$
DECLARE
/*<history_list>
<item><date_m>05.08.2015</date_m><task_n>235436</task_n><author>Пономаренко А.Б.</author></item>
</history_list>*/
  owner_key integer;
BEGIN
  SELECT *
  FROM public.setting_value(_subdivision_key,_name,CURRENT_DATE)
  INTO   value, data_type, status;

  RETURN NEXT;
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function borrower_fio (OID = 471543) : 
--
CREATE FUNCTION public.borrower_fio (
  _borrower_key integer
)
RETURNS character varying
AS 
$body$
DECLARE
/*<history_list>
<item><date_m>27.10.2015</date_m><task_n>263184</task_n><author>Пономаренко А.Б.</author></item>
</history_list>
*/
  _result varchar;
BEGIN
  select b.last_name||' '||b.name||COALESCE(' '||NULLIF(b.patronimic,''),'')
  from asuz_db.t_borrowers b
  where b.borrower_key = _borrower_key
  into _result;
  return COALESCE(_result,'');
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function get_loan_balances (OID = 471575) : 
--
SET search_path = facts, pg_catalog;
CREATE FUNCTION facts.get_loan_balances (
  _contract_number character varying,
  out "1_planned_date_payment" date,
  out "2_plained_summ_schedule" numeric,
  out "3_debt_on_schedule" numeric,
  out "4_repayment_date" date,
  out "5_paid_up_principal_debt" numeric,
  out "6_overdue_principal_debt" numeric,
  out "7_debt_on_schedule" numeric,
  out "8_repayment_date" date,
  out "9_interest_repayment" numeric,
  out "10_overdue_interest" numeric,
  out "11_start_of_accrual_period" date,
  out "12_end_of_accrual_period" date,
  out "13_base_for_charging__overdue_principal" numeric,
  out "14_interest_charges" numeric,
  out "15_maturity_date" date,
  out "16_interest_exempted" numeric,
  out "17_debt" numeric,
  out "18_start_of_accrual_period" date,
  out "19_end_of_accrual_period" date,
  out "20_base_for_charging__overdue_principal" numeric,
  out "21_accrued_penalty" numeric,
  out "22_maturity_date" date,
  out "23_redeemable_forfeit" numeric,
  out "24_debt" numeric,
  out "25_amount_of_payments_received" numeric,
  out "26_total_debt" numeric
)
RETURNS SETOF record
AS 
$body$
DECLARE
	v__rec RECORD;
    v__rec__loan RECORD;
BEGIN

	SELECT * FROM asuz_db.loans l
	where l.contract = _contract_number
    INTO v__rec__loan;

	IF NOT FOUND THEN
		RETURN;
	END IF;
    
	PERFORM * FROM buh.balance_value bv WHERE
        bv.loan_key = v__rec__loan.loan_key
        AND
        bv.active = 1;

	IF NOT FOUND THEN
		RETURN;
	END IF;
    
    /* Брать либо из repayment_schedules*/
    /*
    FOR v__rec IN
	    SELECT * FROM asuz_db.repayment_schedules rs 
    	WHERE rs.loan_key = v__rec__loan.loan_key
    LOOP
    	/*
			Вероятно, на даты пранируемой оплаты делать выборку из balance_value
            и там получать балансовые величины
        */
    END LOOP;
    */

	/* Брать либо из balance_value*/
	FOR v__rec IN
    	SELECT * FROM buh.balance_value bv WHERE
        bv.loan_key = v__rec__loan.loan_key
        AND
        bv.active = 1
    LOOP
          
    
		"1_planned_date_payment" = NOW(); -- Плановая дата платежа по договору
        "2_plained_summ_schedule" = 0; -- Плановый платеж по графику (% и основной долг)
        "3_debt_on_schedule" = v__rec.n_loan_e; -- Основной долг (руб.)	  Задолженность по графику  
        "4_repayment_date" = NOW(); -- Основной долг (руб.)	  Дата погашения
        "5_paid_up_principal_debt" = 0; -- Основной долг (руб.)	  Погашенный основной долг 
        "6_overdue_principal_debt" = v__rec.od_loan_e; -- Основной долг (руб.)	  Просроченный основной долг\
        "7_debt_on_schedule" = v__rec.n_overpayment_planned_e; -- Проценты (руб.) Задолженность по графику
        "8_repayment_date" = NOW(); -- Проценты (руб.) Дата погашения
        "9_interest_repayment" = 0; -- Проценты (руб.) Погашенные проценты
        "10_overdue_interest" = v__rec.od_overpayment_planned_e; -- Проценты (руб.) Просроченные проценты
        "11_start_of_accrual_period" = NOW(); -- Проценты на просроченный основной долг (руб.)	Начало периода начисления
        "12_end_of_accrual_period" = NOW(); -- Проценты на просроченный основной долг (руб.) Конец периода начисления
        "13_base_for_charging__overdue_principal" = 0; -- Проценты на просроченный основной долг (руб.)	База для начисления (Просроченный основной долг)
        "14_interest_charges" = v__rec.od_overpayment_for_delay_e; -- Проценты на просроченный основной долг (руб.)	Начисленные проценты
        "15_maturity_date" = NOW(); -- Проценты на просроченный основной долг (руб.)  Дата погашения
        "16_interest_exempted" = 0; -- Проценты на просроченный основной долг (руб.)  Погашенные проценты
        "17_debt" = 0; -- Проценты на просроченный основной долг (руб.) Долг
        "18_start_of_accrual_period" = NOW(); -- Неустойка на просроченный основной долг (руб.)	Начало периода начисления

        "19_end_of_accrual_period" = NOW(); -- Неустойка на просроченный основной долг (руб.)	Конец периода начисления
        "20_base_for_charging__overdue_principal" = 0; -- Неустойка на просроченный основной долг (руб.)	База для начисления (Просроченный основной долг)
        "21_accrued_penalty" = v__rec.od_fine_e; -- Неустойка на просроченный основной долг (руб.)	Начисленная неустойка
        "22_maturity_date" = NOW(); -- Неустойка на просроченный основной долг (руб.)	Дата погашения
        "23_redeemable_forfeit" = 0; -- Неустойка на просроченный основной долг (руб.)	Погашенная неустойка
        "24_debt" = 0; -- Неустойка на просроченный основной долг (руб.)	Долг
        "25_amount_of_payments_received" = 0; -- Сумма поступивших платежей (руб.)
        "26_total_debt" = v__rec.client_balance_e; -- Итого задолженность (руб.)


        
        
/*
        "1_planned_date_payment" date,
        "2_plained_summ_schedule" numeric,
        "3_debt_on_schedule" numeric,
        "4_repayment_date" date,
        "5_paid_up_principal_debt" numeric,
        "6_overdue_principal_debt" numeric,
        "7_debt_on_schedule" numeric,
        "8_repayment_date" date,
        "9_interest_repayment" numeric,
        "10_overdue_interest" numeric,
        "11_start_of_accrual_period" date,
        "12_end_of_accrual_period" date,
        "13_base_for_charging__overdue_principal" numeric,
        "14_interest_charges" numeric,
        "15_maturity_date" date,
        "16_interest_exempted" numeric,
        "17_debt" numeric,
        "18_start_of_accrual_period" date,
        "19_end_of_accrual_period" date,
        "20_base_for_charging__overdue_principal" numeric,
        "21_accrued_penalty" numeric,
        "22_maturity_date" date,
        "23_redeemable_forfeit" numeric,
        "24_debt" numeric,
        "25_amount_of_payments_received" numeric,
        "26_total_debt" numeric
*/
		RETURN NEXT;
    
    END LOOP;

  

	RETURN NEXT;	

	RETURN;
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function check_correct_contract_number (OID = 471659) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE FUNCTION loan_calc_history.check_correct_contract_number (
  _contract_number character varying,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>03.05.2018</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__rec_loan RECORD;
BEGIN

	PERFORM * 
    FROM asuz_db.loans l
	WHERE 
    	l.contract = _contract_number
        AND
        l.issued = TRUE;

	IF NOT FOUND THEN
        result = -1;
        result_str = 'Займ не найден';
		RETURN;
	END IF;
    
    SELECT * 
    FROM asuz_db.loans l
    WHERE 
    	l.contract = _contract_number
        AND
        l.issued = TRUE
    INTO v__rec_loan;
    
    result = v__rec_loan.loan_key;
    result_str = 'OK';
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_rate_by_creation_date (OID = 472736) : 
--
SET search_path = buh, pg_catalog;
CREATE FUNCTION buh.get_rate_by_creation_date (
  _loan_key integer
)
RETURNS numeric
AS 
$body$
/*
<description>
           Хранимка для получения процентной ставки в зависимости от даты
           до 30.03.2016 - мы берем процентные ставки согласно справочнику (из таблицы buh.loan_percent)
           Справочник нам предоставлен центробанком
</description>
<history_list>
    <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
    <item><date_m>21.02.2019</date_m><task_n>473770</task_n><author>Тихоненко В.В.</author></item>

</history_list>
*/
DECLARE
	v__creation_date TIMESTAMP;
    v__loan_term INTEGER;
    v__month INTEGER;
    v__overpayment_base NUMERIC;
    v__year INTEGER;
    v__permissible_loan_key INTEGER;
    v__rate NUMERIC;
    v__koeff NUMERIC;
    v__rec__permissible_loans RECORD;
BEGIN

    --для определения версии алгоритма начисления
    SELECT l.creation_date, l.loan_term
    FROM asuz_db.loans l
    WHERE l.loan_key = _loan_key
    INTO v__creation_date, v__loan_term;

	IF v__creation_date < '30.03.2016' THEN
    
        v__month = extract(month from v__creation_date);
        v__year = extract(year from v__creation_date);
            
        SELECT lp.value FROM
        buh.loan_percent lp
        WHERE 
            lp.month = v__month
            AND
            lp.year = v__year
            AND
            lp.days_max>= v__loan_term
            AND
            lp.days_min<=v__loan_term
        LIMIT 1
        INTO
            v__koeff;
            
        IF FOUND then 
        	v__koeff = round(v__koeff/365, 3);
	        RETURN v__koeff;    
        END IF; 
        
    END IF;
    
    

    

	-- Получаем % ставка по договору (в день)	
    -- =========================================================================
	v__permissible_loan_key = buh.get_permissible_loan_for_date(_loan_key, v__creation_date::DATE, 0);
    
	SELECT *
    FROM asuz_db.permissible_loans t
    WHERE t.key = v__permissible_loan_key
    INTO v__rec__permissible_loans;
    
    -- Согласно обсуждению от 01_02_2019 закомментированный ниже код посчитали некорректным
    -- Потому как в случае с неануитетным графиком поле permissible_loans.rate_rs не будет заполнено.
    v__rate = COALESCE(v__rec__permissible_loans.rate_rs,v__rec__permissible_loans.rate);
    /*
    -- Если график аннуитетный
    IF v__rec__permissible_loans.type_repayment_schedule_key = 2 THEN
    	v__rate = COALESCE(v__rec__permissible_loans.rate,v__rec__permissible_loans.rate_rs);
    -- Если график не аннуитетный
    ELSE
    	v__rate = COALESCE(v__rec__permissible_loans.rate_rs,v__rec__permissible_loans.rate);
    END IF; 
    */
    
    /*
    	Согласно обсуждению от 21_02_2019 с Костей Крпским и Александром Пономеренко
        принято решение вернуть первоначальную версию
        Обсуждение возникл на основании задачи https://redmine.vivadengi.ru/issues/473770
        Есть вероятность что теперь будут проблемы с отображением процентов по расчету для банка,
        потому как для банка мы в шапке показываем те проценты, которые в договоре
    */
    -- Если график аннуитетный
    IF v__rec__permissible_loans.type_repayment_schedule_key = 2 THEN
    	v__rate = v__rec__permissible_loans.rate;
    -- Если график не аннуитетный
    ELSE
    	v__rate = COALESCE(v__rec__permissible_loans.rate_rs,v__rec__permissible_loans.rate);
    END IF; 
    -- =========================================================================

    RETURN v__rate;
    
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_calc_state_duty (OID = 472910) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE FUNCTION loan_calc_history.get_calc_state_duty (
  _lv_key integer,
  out state_duty numeric,
  out state_duty_penalty numeric,
  out od_loan_e numeric,
  out od_overpayment_planned_e numeric,
  out od_overpayment_for_delay_e numeric,
  out od_fine_e numeric,
  out gosp numeric,
  out gosp_penalty numeric,
  out creation_date timestamp without time zone,
  out description character varying
)
RETURNS record
AS 
$body$
DECLARE
	v__rec_loan_version RECORD;
    v__result1 NUMERIC;
    v__result2 NUMERIC;
    v__gosp NUMERIC;
    v__gosp_penalty NUMERIC;
BEGIN

	
    SELECT * FROM loan_calc_history.loan_version lv
    WHERE lv.loan_version_key = _lv_key
    INTO v__rec_loan_version;
    
    
    creation_date = v__rec_loan_version.creation_date;
    description = v__rec_loan_version.description;

    SELECT
    	bvh.od_loan_e + bvh.od_overpayment_planned_e+bvh.od_overpayment_for_delay_e ,
        bvh.od_loan_e + bvh.od_overpayment_planned_e + bvh.od_overpayment_for_delay_e + bvh.od_fine_e,
        bvh.od_loan_e,
        bvh.od_overpayment_planned_e,
        bvh.od_overpayment_for_delay_e,
        bvh.od_fine_e
    FROM 
    	loan_calc_history.balance_value_history bvh
    WHERE 
    	bvh.loan_version_key = _lv_key
    	AND
        bvh.transact_date = v__rec_loan_version.creation_date::DATE
    INTO
    	state_duty, 
        state_duty_penalty,
        od_loan_e,
        od_overpayment_planned_e,
        od_overpayment_for_delay_e,
        od_fine_e
        ;
        
    --state_duty = round(state_duty);
    --state_duty_penalty = round(state_duty_penalty);

	v__gosp = 0;
    
    --до 20 000 рублей - 4 процента цены иска, но не менее 400 рублей;
	IF state_duty <=20000 THEN
    	v__gosp = state_duty*0.04;
        v__gosp = round(v__gosp, 2);
        IF v__gosp < 400 THEN
        	v__gosp = 400;
        END IF;
    END IF;
    
    --от 20 001 рубля до 100 000 рублей - 800 рублей плюс 3 процента суммы, превышающей 20 000 рублей;
	IF state_duty <=100000 AND state_duty >=20001 THEN
    	v__gosp = (state_duty - 20000)*0.03 + 800;
        v__gosp = round(v__gosp, 2);
    END IF;
    
    --от 100 001 рубля до 200 000 рублей - 3 200 рублей плюс 2 процента суммы, превышающей 100 000 рублей;
	IF state_duty <=200000 AND state_duty >=100001 THEN
    	v__gosp = (state_duty - 100000)*0.02 + 3200;
        v__gosp = round(v__gosp, 2);
    END IF; 
    
    --от 200 001 рубля до 1 000 000 рублей - 5 200 рублей плюс 1 процент суммы, превышающей 200 000 рублей;
	IF state_duty <=1000000 AND state_duty >=200001 THEN
    	v__gosp = (state_duty - 200000)*0.01 + 5200;
        v__gosp = round(v__gosp, 2);
    END IF; 
    
    -- свыше 1 000 000 рублей - 13 200 рублей плюс 0,5 процента суммы, превышающей 1 000 000 рублей, но не более 60 000 рублей;
	IF state_duty >=1000001 THEN
    	v__gosp = (state_duty - 1000000)*0.005 + 13200;
        v__gosp = round(v__gosp, 2);
        IF v__gosp > 60000 THEN
	        v__gosp = 60000;
        END IF;
    END IF; 
    
    
    
    
    
    
    
    
    
    
    
	v__gosp_penalty = 0;
    
    --до 20 000 рублей - 4 процента цены иска, но не менее 400 рублей;
	IF state_duty_penalty <=20000 THEN
    	v__gosp_penalty = state_duty_penalty*0.04;
        v__gosp_penalty = round(v__gosp_penalty, 2);
        IF v__gosp_penalty < 400 THEN
        	v__gosp_penalty = 400;
        END IF;
    END IF;
    
    --от 20 001 рубля до 100 000 рублей - 800 рублей плюс 3 процента суммы, превышающей 20 000 рублей;
	IF state_duty_penalty <=100000 AND state_duty_penalty >=20001 THEN
    	v__gosp_penalty = (state_duty_penalty - 20000)*0.03 + 800;
        v__gosp_penalty = round(v__gosp_penalty, 2);
    END IF;
    
    --от 100 001 рубля до 200 000 рублей - 3 200 рублей плюс 2 процента суммы, превышающей 100 000 рублей;
	IF state_duty_penalty <=200000 AND state_duty_penalty >=100001 THEN
    	v__gosp_penalty = (state_duty_penalty - 100000)*0.02 + 3200;
        v__gosp_penalty = round(v__gosp_penalty, 2);
    END IF; 
    
    --от 200 001 рубля до 1 000 000 рублей - 5 200 рублей плюс 1 процент суммы, превышающей 200 000 рублей;
	IF state_duty_penalty <=1000000 AND state_duty_penalty >=200001 THEN
    	v__gosp_penalty = (state_duty_penalty - 200000)*0.01 + 5200;
        v__gosp_penalty = round(v__gosp_penalty, 2);
    END IF; 
    
    -- свыше 1 000 000 рублей - 13 200 рублей плюс 0,5 процента суммы, превышающей 1 000 000 рублей, но не более 60 000 рублей;
	IF state_duty_penalty >=1000001 THEN
    	v__gosp_penalty = (state_duty_penalty - 1000000)*0.005 + 13200;
        v__gosp_penalty = round(v__gosp_penalty, 2);
        IF v__gosp_penalty > 60000 THEN
	        v__gosp_penalty = 60000;
        END IF;
    END IF; 
    

    gosp = v__gosp;
    gosp_penalty = v__gosp_penalty;



    


    
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function create_calc_sequence (OID = 473057) : 
--
CREATE FUNCTION loan_calc_history.create_calc_sequence (
  _loan_key integer,
  _name character varying,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
DECLARE
	v__sql VARCHAR;
    v__rec__loan RECORD;
    v__days_count INTEGER; -- Общее количество дней, за кооторое считаем займ
    v__rec_dates RECORD; -- Запись для перебора дат
BEGIN
  
    v__sql = 'DROP SEQUENCE IF EXISTS '||COALESCE(_name,'')||';';
    EXECUTE v__sql;
	
    v__sql = 'CREATE SEQUENCE '||COALESCE(_name,'')||' START 1;';
    EXECUTE v__sql;
    
    
    SELECT * FROM asuz_db.loans l
    WHERE l.loan_key = _loan_key
    INTO v__rec__loan;
    
    v__days_count = 0;
	FOR v__rec_dates IN
		select generate_series(v__rec__loan.creation_date::DATE, NOW()::DATE, interval '1' day) 
    LOOP
	    v__days_count = v__days_count + 1;
        
        -- Если текущая дата, является датой закрытия займа
        -- ---------------------------------------------------------------------
        IF buh.is_loan_close_date(_loan_key,v__rec_dates.generate_series::DATE) THEN 
        	EXIT;
        END IF;
        -- ---------------------------------------------------------------------
        
    END LOOP;
    
    result = v__days_count;
    result_str = COALESCE(_name,'');
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function calc_loan_period (OID = 473066) : 
--
SET search_path = buh, pg_catalog;
CREATE FUNCTION buh.calc_loan_period (
  _loan_key integer,
  _date_start date,
  _date_end date,
  _loan_version_key integer,
  _sequence character varying,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
           Хранимка для расчета по займу для периода в http://collection-calc-dev.vivadengi.ru/
</description>
<history_list>
  <item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
  	v__arr_functions VARCHAR[];
    v__call_function VARCHAR;
    v__sql VARCHAR;
    v__rec RECORD;
    v__rec_dates RECORD;
    v__stat_tables_count_key INTEGER;
    v__rec__stat_tables_count RECORD;
    v__key INTEGER;
    v__startcalc TIMESTAMP;
    v__diff INTEGER;
    v__now TIMESTAMP;
    v__days_count INTEGER; -- Общее количество дней, за кооторое считаем займ
    v__counter INTEGER; -- Счетчик
    v__nopeni INTEGER; -- Необходимость исключить из расчета пени
BEGIN

	v__rec__stat_tables_count = buh.set_stat_tables_count_b();
	v__stat_tables_count_key = v__rec__stat_tables_count.result;

	-- Установка неактивыми данных на текущую дату.
    -- После тестирования - убрать.
	-- -------------------------------------------------------------------------
    PERFORM buh.clean_loan_period_activity(_loan_key,_date_start,_date_end);
	-- -------------------------------------------------------------------------
    
    
    
	-- Устанавливаем связь между текущим расчетом и версией расчета
    -- Для того чтобы в самом алгоритме использовать настройки указанной версии расчета
	-- -----------------------------------------------------------------------------------------------------------------------------------
	PERFORM buh.add_current_calc(_loan_key, _loan_version_key);
	-- -----------------------------------------------------------------------------------------------------------------------------------    
    
    


	result = 1;
	result_str = 'OK';

	-- Определяем необходимость расчета пени
    -- -------------------------------------------------------------------------
    SELECT lv.nopeni FROM loan_calc_history.loan_version lv
    WHERE lv.loan_version_key = _loan_version_key
    INTO v__nopeni;
    v__nopeni = COALESCE(v__nopeni,0);
    -- -------------------------------------------------------------------------
	
	IF v__nopeni = 1 THEN
        -- Определяем список функция для их вызова в цикле
        -- ---------------------------------------------------------------------
        v__arr_functions = ARRAY[
            'calc_loan__0', --Переносим балансовую величину с предыдуще годня
            /*
            1. Учесть выдачу займа и создать первую балансовую запись.
            Берем все факты "выдача" за период 5 дней и для каждого факта проверяем наличие проводки "выдача займа".
            Создаем проводку для выдачи и запись в таблице баланс с нулевыми значениями.
            */
            'calc_loan__1',
               
            
            /*      
            2. Учесть начисление плановых процентов.
            Найти временной интервал по графику обязательств где мы находимся на дату анализа
            Определяем кол-во дней в интервале и сумму плановых процентов в очередном обязательстве.
            Делим одно на второе и получаем сумму процентов плановых к начислению за день.
            */
            'calc_loan__2',
            
            'calc_loan__3', --3. Учет поступлений (поиск факта, регистрация факта в виде проводки.)
                              --4. Учет коррекций
            'calc_loan__4_1', --4.1. Коррекция основного долга
            'calc_loan__4_2', --4.2. Коррекция процентов плановых     
            'calc_loan__4_3', --4.3. Коррекция доначисленные процентов     

                              --5. Погашение обязательств (проверка баланса счета клиента 76.09)
            'calc_loan__5_1', --5.1. Погашение просроченные проценты (вначале доначисленные)
            'calc_loan__5_2', --5.2. Погашение просроченное тело
                              --5.1 - 5.3. выполняется ежедневно.
            'calc_loan__5_4', --5.4. Погашение плановые проценты
            'calc_loan__5_5', --5.5. Погашение плановое тело
                              --5.4.-5.5. выполняется строго в дату исполнения очередных обязательств
            'calc_loan__6', --6. расчет суммы просроченного долга и плановых процентов
            'calc_loan__7' --7. расчет баланса на конец дня
        ];
        -- ---------------------------------------------------------------------
        
    ELSE
        -- Определяем список функция для их вызова в цикле
        -- ---------------------------------------------------------------------
        v__arr_functions = ARRAY[
            'calc_loan__0', --Переносим балансовую величину с предыдуще годня
            /*
            1. Учесть выдачу займа и создать первую балансовую запись.
            Берем все факты "выдача" за период 5 дней и для каждого факта проверяем наличие проводки "выдача займа".
            Создаем проводку для выдачи и запись в таблице баланс с нулевыми значениями.
            */
            'calc_loan__1',
            
            /*        
            1.1. Расчет доначисленных % и пени
            Смотрим баланс 58.03.02 на начало опер. дня, и делаем расчет % и пени на конец опер. дня.
            */
            'calc_loan__1_1',      
            
            /*      
            2. Учесть начисление плановых процентов.
            Найти временной интервал по графику обязательств где мы находимся на дату анализа
            Определяем кол-во дней в интервале и сумму плановых процентов в очередном обязательстве.
            Делим одно на второе и получаем сумму процентов плановых к начислению за день.
            */
            'calc_loan__2',
            
            'calc_loan__3', --3. Учет поступлений (поиск факта, регистрация факта в виде проводки.)
                              --4. Учет коррекций
            'calc_loan__4_1', --4.1. Коррекция основного долга
            'calc_loan__4_2', --4.2. Коррекция процентов плановых     
            'calc_loan__4_3', --4.3. Коррекция доначисленные процентов     
            'calc_loan__4_4', --4.4. Коррекция пени
                              --5. Погашение обязательств (проверка баланса счета клиента 76.09)
            'calc_loan__5_1', --5.1. Погашение просроченные проценты (вначале доначисленные)
            'calc_loan__5_2', --5.2. Погашение просроченное тело
            'calc_loan__5_3', --5.3. Погашение просроченные пени
                              --5.1 - 5.3. выполняется ежедневно.
            'calc_loan__5_4', --5.4. Погашение плановые проценты
            'calc_loan__5_5', --5.5. Погашение плановое тело
                              --5.4.-5.5. выполняется строго в дату исполнения очередных обязательств
            'calc_loan__6', --6. расчет суммы просроченного долга и плановых процентов
            'calc_loan__7' --7. расчет баланса на конец дня
        ];
        -- ---------------------------------------------------------------------
        
    END IF;


    
    
    
    
    v__days_count = 0;
	FOR v__rec_dates IN
		select generate_series(_date_start, _date_end, interval '1' day) 
    LOOP
	    v__days_count = v__days_count + 1;
    END LOOP;


	v__counter = 0;    
    
	-- Перебираем циклом все даты
	-- -------------------------------------------------------------------------
	FOR v__rec_dates IN
		select generate_series(_date_start, _date_end, interval '1' day) 
    LOOP
            
        
    	-- Перебираем циклом функции в массиве
		-- ---------------------------------------------------------------------
        FOREACH v__call_function IN ARRAY v__arr_functions
        LOOP 


            -- Если это первый день, то переносить балансовую величину не нужно
			-- -----------------------------------------------------------------               
        	IF 	COALESCE(v__call_function,'') = 'calc_loan__0' AND 
            	_date_start = v__rec_dates.generate_series     THEN
            	--CONTINUE;
            END IF;
			-- -----------------------------------------------------------------
               
            
            -- Вставляем данные о вызываемой хранимке
            --BEGIN
			v__now = clock_timestamp();
            INSERT INTO 
              loan_calc_history.timelog
            (
              description,
              startcalc,
              endcalc,
              timecalc
            )
            VALUES (
              v__call_function,
              v__now,
              NULL,
              NULL
            )
            RETURNING
            	key
            INTO
            	v__key; 
            --COMMIT;
            v__startcalc =  clock_timestamp();          
            
            
            -- ФОрмируем и вызываем функцию
			-- -----------------------------------------------------------------
            v__sql = 'SELECT * FROM buh.'||COALESCE(v__call_function,'')||'('||COALESCE(_loan_key::VARCHAR,'')||','''||COALESCE(v__rec_dates.generate_series::VARCHAR,'')||''')';
            EXECUTE v__sql 
            INTO result, result_str;
			-- -----------------------------------------------------------------
            
            



            UPDATE 
              loan_calc_history.timelog 
            SET 
              endcalc = clock_timestamp(),
              timecalc = age(clock_timestamp(),v__startcalc)
            WHERE 
            	key = v__key;   
                
                
                
                
                
                
            
            -- Если функция отработала с ошибкой - выходим из рассчета
			-- -----------------------------------------------------------------
            IF result = -1 THEN 
            	--RETURN; 
                CONTINUE;
            END IF;
			-- -----------------------------------------------------------------
            
      	END LOOP;
		-- ---------------------------------------------------------------------
        
        
        
        -- Если текущая дата, является датой закрытия займа
        -- ---------------------------------------------------------------------
        IF buh.is_loan_close_date(_loan_key,v__rec_dates.generate_series::DATE) THEN 
        	EXIT;
        END IF;
        -- ---------------------------------------------------------------------
        

		IF COALESCE(_sequence,'') != '' THEN
	        PERFORM nextval(_sequence);
        END IF;
		

/*
BEGIN
        INSERT INTO 
          loan_calc_history.progress
        (
          loan_version_key,
          total_days,
          current_day
        )
        VALUES (
          _loan_version_key,
          v__days_count,
          v__counter
        );     EXCEPTION
WHEN OTHERS THEN
END;
   */
   
        
        
        

    END LOOP;
	-- -------------------------------------------------------------------------

 
/*

*/


    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function drop_calc_sequence (OID = 473075) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE FUNCTION loan_calc_history.drop_calc_sequence (
  _name character varying,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
           Хранимка для удаления последовательности, которая используется для прогресса
</description>
<history_list>
    <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
    <item><date_m>27.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__sql VARCHAR;
    v__rec RECORD;
BEGIN
  
    v__sql = 'DROP SEQUENCE IF EXISTS '||COALESCE(_name,'')||';';
    EXECUTE v__sql;
    
    FOR v__rec IN
    
        SELECT 
        t.sequence_schema,
        t.sequence_name
        FROM 
        information_schema.sequences t
        WHERE
        t.sequence_name LIKE 'sequence%'
    
    LOOP
        v__sql = 'DROP SEQUENCE IF EXISTS '||COALESCE(v__rec.sequence_name,'')||';';
        EXECUTE v__sql;
    
    END LOOP;
    
    result = 1;
    result_str = COALESCE(_name,'');
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_calc_sequence (OID = 473076) : 
--
CREATE FUNCTION loan_calc_history.get_calc_sequence (
  _name character varying,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
DECLARE
	v__sql VARCHAR;
    v__rec__loan RECORD;
    v__days_count INTEGER; -- Общее количество дней, за кооторое считаем займ
    v__rec_dates RECORD; -- Запись для перебора дат
BEGIN
  
    v__sql = 'SELECT last_value FROM '||COALESCE(_name,'')||';';
    EXECUTE v__sql
    INTO result;
 
    --result_str = COALESCE(_name,'');
    result_str = v__sql;
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function __tmp (OID = 473262) : 
--
SET search_path = facts, pg_catalog;
CREATE FUNCTION facts.__tmp (
  out result integer
)
RETURNS integer
AS 
$body$
DECLARE
 	v__rec RECORD;
BEGIN
         DELETE FROM facts.facts f WHERE f.loan_key = 593892   ; 
   
	-- Поступление средств по договору займа
    FOR v__rec IN
        SELECT * 
        FROM asuz_db.payment_part pp
        WHERE 
        	pp.payment_key IN
              (
                  SELECT p.payment_key
                  FROM asuz_db.payment p
                  WHERE p.loan_key = 593892
              )
    LOOP
    	PERFORM  facts.register_fact_4(v__rec.document_key);    
    END LOOP;
    
    result = 1;
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_calc_history_data (OID = 473430) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE FUNCTION loan_calc_history.get_calc_history_data (
  lv_key integer,
  out npp integer,
  out f1_reporting_date date,
  out f2_scheduled_payment_on_schedule numeric,
  out f3_amount_of_payments_received numeric,
  out f4_debt_on_schedule numeric,
  out f5_deducted_principal_debt numeric,
  out f6_overdue_principal numeric,
  out f7_debt_on_schedule numeric,
  out f8_interest_exempted numeric,
  out f9_overdue_interest numeric,
  out f10_start_of_accrual_period date,
  out f11_end_of_accrual_period date,
  out f12_base_for_charging numeric,
  out f13_interest_charges numeric,
  out f14_interest_exempted numeric,
  out f15_interest_on_overdue_principal_debt numeric,
  out f16_total_debt numeric,
  out f17_penalty_start_of_accrual_period date,
  out f18_penalty_end_of_accrual_period date,
  out f19_penalty_base_for_charging numeric,
  out f20_penalty_charges numeric,
  out f21_penalty_exempted numeric,
  out f22_penalty_on_overdue_principal_debt numeric,
  out flags_operations character varying
)
RETURNS SETOF record
AS 
$body$
/*
<description>
========================================================================================
	Получаем расчетные даанные для суда
========================================================================================
</description>
<history_list>
  <item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__rec__loan_version RECORD;
    v__rec__loans RECORD;
    v__rec__permissible_loans RECORD;
    v__rec__repayment_schedules RECORD;
    v__rec RECORD;
    
    v__dates_array DATE [];
    v_prev_date DATE;
    v__flag VARCHAR;
    v__npp INTEGER;
    
    v__balance_2 NUMERIC;
    v__balance_1 NUMERIC;
    
    
      
    v__total_f1 date;
    v__total_f2 numeric;
    v__total_f3 numeric;
    v__total_f4 numeric;
    v__total_f5 numeric;
    v__total_f6 numeric;
    v__total_f7 numeric;
    v__total_f8 numeric;
    v__total_f9 numeric;
    v__total_f10 date;
    v__total_f11 date;
    v__total_f12 numeric;
    v__total_f13 numeric;
    v__total_f14 numeric;
    v__total_f15 numeric;
    v__total_f16 numeric;
    v__total_f17 date;
    v__total_f18 date;
    v__total_f19 numeric;
    v__total_f20 numeric;
    v__total_f21 numeric;
    v__total_f22 numeric;
    
    v__records_count INTEGER;
BEGIN

    v__total_f1  = NULL;
    v__total_f2  = 0;
    v__total_f3  = 0;
    v__total_f4  = 0;
    v__total_f5  = 0;
    v__total_f6  = NULL;
    v__total_f7  = 0;
    v__total_f8  = 0;
    v__total_f9  = NULL;
    v__total_f10 = NULL;
    v__total_f11 = NULL;
    v__total_f12 = NULL;
    v__total_f13 = 0;
    v__total_f14 = 0;
    v__total_f15 = NULL;
    v__total_f16 = NULL;
    v__total_f17 = NULL;
    v__total_f18 = NULL;
    v__total_f19 = NULL;
    v__total_f20 = 0;
    v__total_f21 = 0;
    v__total_f22 = NULL;

	-- Получаем данные версии рассчета
	SELECT * FROM loan_calc_history.loan_version lv 
    WHERE lv.loan_version_key = lv_key
	INTO v__rec__loan_version;
    
    SELECT * FROM asuz_db.loans l
    WHERE l.loan_key = v__rec__loan_version.loan_key
    INTO v__rec__loans;
    
    SELECT * FROM asuz_db.permissible_loans pl
    WHERE pl.key = v__rec__loans.permissible_loan_key
    INTO v__rec__permissible_loans;
    
    
    SELECT * FROM asuz_db.repayment_schedules rs
    WHERE rs.permissible_loan_key = v__rec__loans.permissible_loan_key
    INTO  v__rec__repayment_schedules;

        

    
    
    npp = NULL;
    v__npp = 0;
    v_prev_date = NULL;
    v__records_count = 0;
    FOR v__rec IN
    	
    	SELECT edate,flag, f1, f2, f3 FROM 
        (
        
            SELECT 
            	COALESCE(ro.payment_date_ori,rs.payment_date) as edate, 
            	1 AS flag, 
                rs.loan_sum + rs.overpayment_sum as f1,
                rs.loan_sum as f2,
                rs.overpayment_sum as f3 
            FROM asuz_db.repayment_schedules rs
            LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
                on ro.repayment_schedule_key=rs.repayment_schedule_key
            WHERE rs.permissible_loan_key = v__rec__loans.permissible_loan_key
            
            /*
		    Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)
            
            SELECT 
            	rs.payment_date as edate, 
            	1 AS flag, 
                rs.loan_sum + rs.overpayment_sum as f1,
                rs.loan_sum as f2,
                rs.overpayment_sum as f3 
            FROM asuz_db.repayment_schedules rs
            WHERE rs.permissible_loan_key = v__rec__loans.permissible_loan_key
            */
            
            
            UNION
            
            SELECT 
            	lth.transact_date  as edate, 
            	2,
                0,
                0,
                0
            FROM loan_calc_history.loan_transact_history lth 
            WHERE lth.loan_version_key = lv_key
            AND lth.account_transaction_key IN (3,45)
            
            UNION
            
            SELECT 
            	MAX(bvh.transact_date) as edate, 
                3 ,
                0,
                0,
                0
            FROM loan_calc_history.balance_value_history bvh 
            WHERE bvh.loan_version_key = lv_key
            
        ) as tmp_table
        ORDER BY edate, flag ASC

	LOOP
    	flags_operations = '';
        
    	v__records_count = v__records_count + 1;
        IF v__rec.edate::DATE = v_prev_date THEN
            CONTINUE;
        END IF;

		npp = NULL;
        f2_scheduled_payment_on_schedule = NULL;
        f4_debt_on_schedule = NULL;
        f7_debt_on_schedule = NULL;
    	IF v__rec.flag = 1 THEN
	        flags_operations = flags_operations || '1 + ';
            v__npp = v__npp + 1;
            npp = v__npp;
            f2_scheduled_payment_on_schedule = v__rec.f1;
            f4_debt_on_schedule = v__rec.f2;
            f7_debt_on_schedule = v__rec.f3;
        END IF;
        

        SELECT 
			SUM(lth.summ)
        FROM loan_calc_history.loan_transact_history lth 
        WHERE 
        	lth.loan_version_key = lv_key
	        AND lth.account_transaction_key IN (3,45)
	        AND lth.transact_date = v__rec.edate
        INTO
	        f3_amount_of_payments_received;
            
            
        
        SELECT 
			SUM(lth.summ)
        FROM loan_calc_history.loan_transact_history lth 
        WHERE 
        	lth.loan_version_key = lv_key
	        AND lth.account_transaction_key IN (13, 15)
	        AND lth.transact_date = v__rec.edate
        INTO
	        f5_deducted_principal_debt;
            
            
            
        
        SELECT 
			SUM(lth.summ)
        FROM loan_calc_history.loan_transact_history lth 
        WHERE 
        	lth.loan_version_key = lv_key
	        AND lth.account_transaction_key IN (47, 48)
	        AND lth.transact_date = v__rec.edate
        INTO
	        f8_interest_exempted;
            
            
            
        
        SELECT 
			SUM(lth.summ)
        FROM loan_calc_history.loan_transact_history lth 
        WHERE 
        	lth.loan_version_key = lv_key
	        AND lth.account_transaction_key IN (17)
	        AND lth.transact_date = v__rec.edate
        INTO
	        f14_interest_exempted;
        




            
        
        SELECT 
			SUM(lth.summ)
        FROM loan_calc_history.loan_transact_history lth 
        WHERE 
        	lth.loan_version_key = lv_key
	        AND lth.account_transaction_key IN (19)
	        AND lth.transact_date = v__rec.edate
        INTO
	        f21_penalty_exempted;
        

        
        
        
        SELECT 
			bvh.od_loan_e, --для 6 поля 
			bvh.od_overpayment_planned_e, --для 9 поля 
			bvh.od_overpayment_for_delay_e, --для 15 поля 
			bvh.od_fine_e, --для 22 поля 
            bvh.od_loan_e + bvh.od_overpayment_planned_e + bvh.od_overpayment_for_delay_e --для 16 поля
        FROM loan_calc_history.balance_value_history bvh 
        WHERE 
        	bvh.loan_version_key = lv_key
	        AND bvh.transact_date = v__rec.edate
        INTO
            f6_overdue_principal,
            f9_overdue_interest,
            f15_interest_on_overdue_principal_debt,
            f22_penalty_on_overdue_principal_debt,
            f16_total_debt;
        
		--RAISE NOTICE 'date = %',v__rec.edate;
		--RAISE NOTICE 'f22_penalty_on_overdue_principal_debt = %',f22_penalty_on_overdue_principal_debt;



        f18_penalty_end_of_accrual_period = NULL;
        f19_penalty_base_for_charging = NULL;
        f10_start_of_accrual_period = NULL;
        f11_end_of_accrual_period = NULL;
        f12_base_for_charging = NULL;
        f13_interest_charges = NULL;
        f17_penalty_start_of_accrual_period = NULL;
        f20_penalty_charges = NULL;
        
        IF v_prev_date IS NOT NULL THEN
                
                
            SELECT 
                bvh.od_overpayment_for_delay_e
            FROM 
                loan_calc_history.balance_value_history bvh
            WHERE
                bvh.transact_date = v_prev_date::DATE-- + interval '1 day'
                AND
                bvh.loan_version_key = lv_key
            INTO 
                v__balance_1;



            
            
            SELECT 
                bvh.od_overpayment_for_delay_e
            FROM 
                loan_calc_history.balance_value_history bvh
            WHERE
                bvh.transact_date = v__rec.edate::DATE
                AND
                bvh.loan_version_key = lv_key
            INTO 
                v__balance_2;
                

            f13_interest_charges  = v__balance_2 - v__balance_1;
            f13_interest_charges = f13_interest_charges + COALESCE(f14_interest_exempted,0);
            
            IF f13_interest_charges > 0 THEN
	        	f10_start_of_accrual_period = v_prev_date::DATE + interval '1 day';
		        f11_end_of_accrual_period = v__rec.edate::DATE;
		        f12_base_for_charging = f6_overdue_principal;
                
                SELECT 
                bvh.od_loan_b
            FROM 
                loan_calc_history.balance_value_history bvh
            WHERE
                bvh.transact_date = v__rec.edate::DATE
                AND
                bvh.loan_version_key = lv_key
            INTO f12_base_for_charging;
            END IF;
            
            
            
                
            SELECT 
                bvh.od_fine_e
            FROM 
                loan_calc_history.balance_value_history bvh
            WHERE
                bvh.transact_date = v_prev_date::DATE --+ interval '1 day'
                AND
                bvh.loan_version_key = lv_key
            INTO 
                v__balance_1;
            
            
            SELECT 
                bvh.od_fine_e
            FROM 
                loan_calc_history.balance_value_history bvh
            WHERE
                bvh.transact_date = v__rec.edate::DATE
                AND
                bvh.loan_version_key = lv_key
            INTO 
                v__balance_2;
                
                
            f20_penalty_charges   = v__balance_2 - v__balance_1;
            f20_penalty_charges = f20_penalty_charges + COALESCE(f21_penalty_exempted,0);
                         
            --raise notice '%,%,%',v__balance_2, v__balance_1, f20_penalty_charges;
               SELECT bvh.od_loan_b
                FROM loan_calc_history.balance_value_history bvh
                WHERE bvh.transact_date = v__rec.edate::DATE
                 AND bvh.loan_version_key = lv_key
                INTO f19_penalty_base_for_charging;            
            --IF f20_penalty_charges > 0 THEN
            IF f19_penalty_base_for_charging > 0 THEN
	        	f17_penalty_start_of_accrual_period = v_prev_date::DATE + interval '1 day';
                f18_penalty_end_of_accrual_period = v__rec.edate::DATE;
                --f19_penalty_base_for_charging = f6_overdue_principal;
                 
            END IF;
        END IF;
        
        
        
        
        


        v__total_f6 = f6_overdue_principal;
        v__total_f9 = f9_overdue_interest;
        v__total_f15 = f15_interest_on_overdue_principal_debt;
        v__total_f16 = f16_total_debt;
        v__total_f22 = f22_penalty_on_overdue_principal_debt;


        v__total_f2 = v__total_f2 + COALESCE(f2_scheduled_payment_on_schedule,0);
        v__total_f3  = v__total_f3 + COALESCE(f3_amount_of_payments_received,0);
        v__total_f4  = v__total_f4 + COALESCE(f4_debt_on_schedule,0);
        v__total_f5  = v__total_f5 + COALESCE(f5_deducted_principal_debt,0);
        v__total_f7  = v__total_f7 + COALESCE(f7_debt_on_schedule,0);
        v__total_f8  = v__total_f8 + COALESCE(f8_interest_exempted,0);
        v__total_f13  = v__total_f13 + COALESCE(f13_interest_charges,0);
        v__total_f14  = v__total_f14 + COALESCE(f14_interest_exempted,0);
        v__total_f20  = v__total_f20 + COALESCE(f20_penalty_charges,0);
        v__total_f21 = v__total_f21 + COALESCE(f21_penalty_exempted,0);

        

    	v__dates_array = array_append(v__dates_array, v__rec.edate::DATE);
        f1_reporting_date = v__rec.edate::DATE;
        RETURN NEXT;
        
        
        v_prev_date = v__rec.edate::DATE;
    END LOOP;
	
    
    IF v__records_count > 0 THEN
        npp = -1;
        f1_reporting_date                      = v__total_f1;
        f2_scheduled_payment_on_schedule       = v__total_f2;
        f3_amount_of_payments_received         = v__total_f3;
        f4_debt_on_schedule                    = v__total_f4;
        f5_deducted_principal_debt             = v__total_f5;
        f6_overdue_principal                   = v__total_f6;
        f7_debt_on_schedule                    = v__total_f7;
        f8_interest_exempted                   = v__total_f8;
        f9_overdue_interest                    = v__total_f9;
        f10_start_of_accrual_period            = v__total_f10;
        f11_end_of_accrual_period              = v__total_f11;
        f12_base_for_charging                  = v__total_f12;
        f13_interest_charges                   = v__total_f13;
        f14_interest_exempted                  = v__total_f14;
        f15_interest_on_overdue_principal_debt = v__total_f15;
        f16_total_debt                         = v__total_f16;
        f17_penalty_start_of_accrual_period    = v__total_f17;
        f18_penalty_end_of_accrual_period      = v__total_f18;
        f19_penalty_base_for_charging          = v__total_f19;
        f20_penalty_charges                    = v__total_f20;
        f21_penalty_exempted                   = v__total_f21;
        f22_penalty_on_overdue_principal_debt  = v__total_f22;
		RETURN NEXT;
    END IF;
	
    
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_loan_special_offers (OID = 482624) : 
--
CREATE FUNCTION loan_calc_history.get_loan_special_offers (
  _lv_key integer,
  out start_date date,
  out special_offer_name character varying
)
RETURNS SETOF record
AS 
$body$
/*
<arguments>
	<in>
    	<item><name>_lv_key</name><description>Ключ версии расчета</description></item>
    </in>
	<out>
    	<item><name>period_b</name><description>Дата начала действия акции</description></item>
        <item><name>period_e</name><description>Дата конца действия акции</description></item>
        <item><name>special_offer_name</name><description>Название акции</description></item>
    </out>
</arguments>
<description>
	Получение списка акций для указаннго займа. Используется в collection-calc.
</description>
<history_list>
  <item>
      <date_m>04.07.2018</date_m>
      <task_n>402432</task_n>
      <author>Тихоненко В.В.</author>
  </item>
</history_list>
*/
DECLARE
	v__rec_list_participants RECORD; -- Список акций для указанного займа
    v__count_loan_version INTEGER; -- КОличество найденных версий расчета по указанному ключу
	v__rec__loan_version RECORD; -- Запись о версии рассчета
BEGIN

	-- Получаем запись о версии рассчета
	SELECT * FROM loan_calc_history.loan_version lv 
    WHERE lv.loan_version_key = _lv_key
	INTO v__rec__loan_version;
    
	-- Получаем количество найденных записей
    GET DIAGNOSTICS v__count_loan_version = ROW_COUNT;
    
    -- Если не нашли указанные расчет - выходим из хранимки и ничего не возвращаем
    IF v__count_loan_version = 0 THEN
    	RETURN; 
    END IF;
    
    
	-- Перебираем список акций для указанного займа
	FOR v__rec_list_participants IN
            
        SELECT 
          	so.start_date,
          	sot.special_offer_name
        FROM
        	asuz_db.special_offers so
        JOIN 	
        	asuz_db.special_offers_types sot on so.process_name_key = sot.id
        WHERE 
        	so.loan_key = v__rec__loan_version.loan_key

        	AND 
            so.used = true
		ORDER BY 
        	so.start_date ASC

        
    LOOP
  		start_date = v__rec_list_participants.start_date::DATE;
  		special_offer_name = v__rec_list_participants.special_offer_name;        
        RETURN NEXT;
    END LOOP;

	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_calc_state_duty_br (OID = 578496) : 
--
CREATE FUNCTION loan_calc_history.get_calc_state_duty_br (
  _lv_key integer,
  out state_duty numeric,
  out state_duty_penalty numeric,
  out od_loan_e numeric,
  out od_overpayment_planned_e numeric,
  out od_overpayment_for_delay_e numeric,
  out od_fine_e numeric,
  out gosp numeric,
  out gosp_penalty numeric,
  out creation_date timestamp without time zone,
  out description character varying,
  out od_overpayment_total numeric,
  out client_balance_e numeric,
  out total_income_payments numeric,
  out total_pays_percents numeric,
  out total_pays_body numeric,
  out total_pays_peni numeric
)
RETURNS record
AS 
$body$
/*
<description>
========================================================================================
           Хранимка для получения данных в шапке расчета для банк
========================================================================================
</description>
<history_list>
    <item><date_m>23.01.2019</date_m><task_n>466273</task_n><author>Тихоненко В.</author></item>
    <item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__rec_loan_version RECORD;
    v__result1 NUMERIC;
    v__result2 NUMERIC;
    v__gosp NUMERIC;
    v__gosp_penalty NUMERIC;
BEGIN

	
    SELECT * FROM loan_calc_history.loan_version lv
    WHERE lv.loan_version_key = _lv_key
    INTO v__rec_loan_version;
    
    
    creation_date = v__rec_loan_version.creation_date;
    description = v__rec_loan_version.description;

    SELECT
    	bvh.od_loan_e + bvh.od_overpayment_planned_e+bvh.od_overpayment_for_delay_e ,
        bvh.od_loan_e + bvh.od_overpayment_planned_e + bvh.od_overpayment_for_delay_e + bvh.od_fine_e,
        bvh.od_loan_e,
        bvh.od_overpayment_planned_e,
        bvh.od_overpayment_for_delay_e,
        bvh.od_fine_e,
        
        bvh.client_balance_e,
        bvh.od_overpayment_planned_e+bvh.od_overpayment_for_delay_e
    FROM 
    	loan_calc_history.balance_value_history bvh
    WHERE 
    	bvh.loan_version_key = _lv_key
    	AND
        bvh.transact_date = v__rec_loan_version.creation_date::DATE
    INTO
    	state_duty, 
        state_duty_penalty,
        od_loan_e,
        od_overpayment_planned_e,
        od_overpayment_for_delay_e,
        od_fine_e,
        client_balance_e,
        od_overpayment_total
        
--        total_debt -- Общая сумма задолженности:
        ;
        
    --state_duty = round(state_duty);
    --state_duty_penalty = round(state_duty_penalty);

	v__gosp = 0;
    
    --до 20 000 рублей - 4 процента цены иска, но не менее 400 рублей;
	IF state_duty <=20000 THEN
    	v__gosp = state_duty*0.04;
        v__gosp = round(v__gosp, 2);
        IF v__gosp < 400 THEN
        	v__gosp = 400;
        END IF;
    END IF;
    
    --от 20 001 рубля до 100 000 рублей - 800 рублей плюс 3 процента суммы, превышающей 20 000 рублей;
	IF state_duty <=100000 AND state_duty >=20001 THEN
    	v__gosp = (state_duty - 20000)*0.03 + 800;
        v__gosp = round(v__gosp, 2);
    END IF;
    
    --от 100 001 рубля до 200 000 рублей - 3 200 рублей плюс 2 процента суммы, превышающей 100 000 рублей;
	IF state_duty <=200000 AND state_duty >=100001 THEN
    	v__gosp = (state_duty - 100000)*0.02 + 3200;
        v__gosp = round(v__gosp, 2);
    END IF; 
    
    --от 200 001 рубля до 1 000 000 рублей - 5 200 рублей плюс 1 процент суммы, превышающей 200 000 рублей;
	IF state_duty <=1000000 AND state_duty >=200001 THEN
    	v__gosp = (state_duty - 200000)*0.01 + 5200;
        v__gosp = round(v__gosp, 2);
    END IF; 
    
    -- свыше 1 000 000 рублей - 13 200 рублей плюс 0,5 процента суммы, превышающей 1 000 000 рублей, но не более 60 000 рублей;
	IF state_duty >=1000001 THEN
    	v__gosp = (state_duty - 1000000)*0.005 + 13200;
        v__gosp = round(v__gosp, 2);
        IF v__gosp > 60000 THEN
	        v__gosp = 60000;
        END IF;
    END IF; 
    
    
    
    
    
    
    
    
    
    
    
	v__gosp_penalty = 0;
    
    --до 20 000 рублей - 4 процента цены иска, но не менее 400 рублей;
	IF state_duty_penalty <=20000 THEN
    	v__gosp_penalty = state_duty_penalty*0.04;
        v__gosp_penalty = round(v__gosp_penalty, 2);
        IF v__gosp_penalty < 400 THEN
        	v__gosp_penalty = 400;
        END IF;
    END IF;
    
    --от 20 001 рубля до 100 000 рублей - 800 рублей плюс 3 процента суммы, превышающей 20 000 рублей;
	IF state_duty_penalty <=100000 AND state_duty_penalty >=20001 THEN
    	v__gosp_penalty = (state_duty_penalty - 20000)*0.03 + 800;
        v__gosp_penalty = round(v__gosp_penalty, 2);
    END IF;
    
    --от 100 001 рубля до 200 000 рублей - 3 200 рублей плюс 2 процента суммы, превышающей 100 000 рублей;
	IF state_duty_penalty <=200000 AND state_duty_penalty >=100001 THEN
    	v__gosp_penalty = (state_duty_penalty - 100000)*0.02 + 3200;
        v__gosp_penalty = round(v__gosp_penalty, 2);
    END IF; 
    
    --от 200 001 рубля до 1 000 000 рублей - 5 200 рублей плюс 1 процент суммы, превышающей 200 000 рублей;
	IF state_duty_penalty <=1000000 AND state_duty_penalty >=200001 THEN
    	v__gosp_penalty = (state_duty_penalty - 200000)*0.01 + 5200;
        v__gosp_penalty = round(v__gosp_penalty, 2);
    END IF; 
    
    -- свыше 1 000 000 рублей - 13 200 рублей плюс 0,5 процента суммы, превышающей 1 000 000 рублей, но не более 60 000 рублей;
	IF state_duty_penalty >=1000001 THEN
    	v__gosp_penalty = (state_duty_penalty - 1000000)*0.005 + 13200;
        v__gosp_penalty = round(v__gosp_penalty, 2);
        IF v__gosp_penalty > 60000 THEN
	        v__gosp_penalty = 60000;
        END IF;
    END IF; 
    

    gosp = v__gosp;
    gosp_penalty = v__gosp_penalty;



    -- = od_overpayment_planned_e + od_overpayment_for_delay_e;
    
    
    
    
    
    
    -- Получаем сумму всех платежей за все время
    -- ============================================================================      
    SELECT 
    	SUM(t.summ)
    FROM 
    	loan_calc_history.loan_transact_history  t
    WHERE
    	t.loan_version_key = _lv_key
    -- AND t.active = 1 
    --         AND t.loan_key = 863577
    AND
    	-- 45; --Нал(additional_value = 0) 
        -- 3; --Безнал (additional_value = 1)
	    t.account_transaction_key IN (3,45)
	INTO
    	total_income_payments;
    -- ============================================================================      







    
    -- Из них в погашение процентов
    -- ============================================================================      
    SELECT 
    	SUM(t.summ)
    FROM 
    	loan_calc_history.loan_transact_history  t
    WHERE
    	t.loan_version_key = _lv_key
    -- AND t.active = 1 
    --         AND t.loan_key = 863577
    AND
    	-- 17; -- Погашение просроченные доначисленные проценты
        -- 48; -- Погашение просроченного долга_ плановые проценты
        -- 47; -- Погашение срочного долга_плановые проценты
        -- 76; -- Погашение срочного долга_плановые проценты при закрытии
	    t.account_transaction_key IN (17,48, 47, 76)
	INTO
    	total_pays_percents;
    -- ============================================================================      



    -- в погашение основного долга:
    -- ============================================================================      
    SELECT 
    	SUM(t.summ)
    FROM 
    	loan_calc_history.loan_transact_history  t
    WHERE
    	t.loan_version_key = _lv_key
    -- AND t.active = 1 
    --         AND t.loan_key = 863577
    AND
    	-- 13; -- Погашение срочного долга_тело
        -- 15; -- Погашение просроченного долга_ тело
	    t.account_transaction_key IN (13,15)
	INTO
    	total_pays_body;
    -- ============================================================================      






    -- в погашение пени
    -- ============================================================================      
    SELECT 
    	SUM(t.summ)
    FROM 
    	loan_calc_history.loan_transact_history  t
    WHERE
    	t.loan_version_key = _lv_key
    -- AND t.active = 1 
    --         AND t.loan_key = 863577
    AND
    	-- 19; -- Погашение пени
	    t.account_transaction_key IN (19)
	INTO
    	total_pays_peni;
    -- ============================================================================      

      /**
       * Если Остаток задолженности получается отрицательный - показываем ноль для банка
       * Обсудили 26_03_2019 с Константином и было принято такое решение
       * 
       */
      IF state_duty_penalty < 0 THEN
      	state_duty_penalty = 0;
      END IF;

      IF od_loan_e < 0 THEN
      	od_loan_e = 0;
      END IF;

      IF od_overpayment_total < 0 THEN
      	od_overpayment_total = 0;
      END IF;

      IF od_fine_e < 0 THEN
      	od_fine_e = 0;
      END IF;

    
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_loan_info_br (OID = 581442) : 
--
SET search_path = facts, pg_catalog;
CREATE FUNCTION facts.get_loan_info_br (
  _lv_key integer,
  out result integer,
  out result_str character varying,
  out fio character varying,
  out contract_number character varying,
  out loan_date date,
  out loan_summ numeric,
  out percent_contract numeric,
  out percent_loan_overdue numeric,
  out planned_close_date date,
  out loan_rate numeric,
  out rate numeric,
  out loan_restructuring_count integer,
  out loan_restructuring_dates character varying,
  out term_days integer,
  out close_date character varying,
  out year_rate numeric
)
RETURNS record
AS 
$body$
/*
<description>
           Хранимка для получения данных в шапке расчета для банк
</description>
<history_list>
    <item><date_m>23.01.2019</date_m><task_n>466273</task_n><author>Власов С.А.</author></item>
    <item><date_m>31.01.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__rec_loan RECORD;
    v__loan_key INTEGER;
    v__permissible_loan_key INTEGER;
    v__rate NUMERIC;
    v__rate_first  NUMERIC; -- Процентная ставка по первоначальному графику платежей
    v__rec_restructuring RECORD;
    v__rec__permissible_loans RECORD;
    v__rec__first__permissible_loans RECORD;
BEGIN

	-- Проверяем существование версии рассчета
    -- =========================================================================
	PERFORM * FROM loan_calc_history.loan_version lv
    WHERE lv.loan_version_key = _lv_key;

	IF NOT FOUND THEN
        result = -1;
        result_str = 'Займ не найден';
		RETURN;
	END IF;
    -- =========================================================================
    
    
    

	-- Получаем ключ займа
    -- =========================================================================
    SELECT lv.loan_key FROM loan_calc_history.loan_version lv
    WHERE lv.loan_version_key = _lv_key
    INTO v__loan_key;
    -- =========================================================================



	-- Получаем данные о займе
    -- =========================================================================
    SELECT * 
    FROM asuz_db.loans l
    WHERE l.loan_key = v__loan_key
    INTO v__rec_loan;
    -- =========================================================================
    
    
    
    
    
	-- Получаем % ставка по договору (в день - первоначальную, та что шла при заключении займа)	
    -- =========================================================================
	v__permissible_loan_key = buh.get_permissible_loan_for_date(v__loan_key, v__rec_loan.creation_date::DATE, 0);
    
	SELECT *
    FROM asuz_db.permissible_loans t
    WHERE t.key = v__permissible_loan_key
    INTO v__rec__permissible_loans;
    
    -- Если график аннуитетный
    IF v__rec__permissible_loans.type_repayment_schedule_key = 2 THEN
    	v__rate = COALESCE(v__rec__permissible_loans.rate,v__rec__permissible_loans.rate_rs);
    -- Если график не аннуитетный
    ELSE
    	v__rate = COALESCE(v__rec__permissible_loans.rate_rs,v__rec__permissible_loans.rate);
    END IF; 
    -- =========================================================================
    
    
    
    -- Получаем первоначальный график платежей (первый по ID)
    -- =========================================================================
	SELECT *
    FROM asuz_db.permissible_loans t
    WHERE t.key IN
    	(
        	SELECT 
            	MIN(tmp.key)
            FROM
            	asuz_db.permissible_loans tmp
            WHERE
            	tmp.loan_key = v__loan_key
        )
    INTO v__rec__first__permissible_loans;
    
    /*
    rate_rs - аннуитетная ставка в день
    rate - переплата в день
    Поменять в COALESCE сказал Михаил Кофанов
        
    */
        
    -- Согласно обсуждению от 01_02_2019 закомментированный ниже код посчитали некорректным
    -- Потому как в случае с неануитетным графиком поле permissible_loans.rate_rs не будет заполнено.
    v__rate_first = COALESCE(v__rec__first__permissible_loans.rate_rs,v__rec__first__permissible_loans.rate);

	/*
    -- Если график аннуитетный
    IF v__rec__first__permissible_loans.type_repayment_schedule_key = 2 THEN
    	
        v__rate_first = COALESCE(v__rec__first__permissible_loans.rate_rs,v__rec__first__permissible_loans.rate);
    -- Если график не аннуитетный
    ELSE
    	v__rate_first = COALESCE(v__rec__first__permissible_loans.rate,v__rec__first__permissible_loans.rate_rs);
    END IF; 
    */
    -- =========================================================================


    
    
    
    loan_restructuring_count = 0;
    loan_restructuring_dates = '';
    FOR v__rec_restructuring IN
        SELECT * FROM asuz_db.loan_restructuring_history lrh
        WHERE lrh.loan_key = v__loan_key
        ORDER BY lrh.agreement_date ASC
    LOOP
        IF loan_restructuring_count = 0 THEN
            loan_restructuring_dates = loan_restructuring_dates || 
                                       '' ||
                                       to_char(v__rec_restructuring.agreement_date, 'DD.MM.YYYY');
		ELSE
            loan_restructuring_dates = loan_restructuring_dates || 
                                       ';' ||
                                       to_char(v__rec_restructuring.agreement_date, 'DD.MM.YYYY');
        END IF;

	    loan_restructuring_count = loan_restructuring_count + 1;

    END LOOP;

    
    
    planned_close_date = v__rec_loan.creation_date + v__rec_loan.loan_term;
	rate = buh.get_rate_by_creation_date(v__loan_key);
    fio = public.borrower_fio(v__rec_loan.borrower_key);
    contract_number = v__rec_loan.contract;
    loan_date = v__rec_loan.creation_date;
    loan_summ = v__rec_loan.loan_sum;
    percent_contract = 0;
    percent_loan_overdue = 0;
    --loan_rate = v__rate;
    --loan_rate = v__rec__first__permissible_loans.rate; -- Ставка дневная
    loan_rate = v__rate_first;
	--year_rate = rate * 365;
    year_rate = v__rate_first * 365; -- Ставка аннуитет за год
    --year_rate = COALESCE(v__rec__permissible_loans.rate_rs,v__rec_loan.loan_rate) * 365;
    --year_rate = COALESCE(v__rec__permissible_loans.rate,v__rec_loan.loan_rate) * 365;
    --year_rate = rate;

        
    result = 1;
    result_str = 'OK';
    term_days = v__rec_loan.loan_term;
    close_date = (CASE WHEN v__rec_loan.closed_date is null THEN 'Не погашен' ELSE to_char(v__rec_loan.closed_date, 'DD.MM.YYYY') END);
    
    
    RETURN;
    
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_calc_history_data_bank (OID = 584598) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE FUNCTION loan_calc_history.get_calc_history_data_bank (
  lv_key integer,
  out f1_reporting_date date,
  out f2_date_diff_from_start integer,
  out f2_1_loan numeric,
  out f2_2__loan_rate__balance_date numeric,
  out f3_od_n_overpayment_all numeric,
  out f4_overpayment_all_summ numeric,
  out f5_fine_per_day numeric,
  out f6_other_payments numeric,
  out f7_pays_all numeric,
  out f8_pays_body numeric,
  out f9_pays_percent numeric,
  out f10_pays_peni numeric,
  out f11_pays_other character varying,
  out f12_balance_all numeric,
  out f13_balance_body numeric,
  out f14_balance_percents_all numeric,
  out f15_balance_fine numeric,
  out f16_total_debt numeric,
  out f17_penalty_start_of_accrual_period date,
  out f18_penalty_end_of_accrual_period date,
  out f19_penalty_base_for_charging numeric,
  out f20_penalty_charges numeric,
  out f21_penalty_exempted numeric,
  out f22_penalty_on_overdue_principal_debt numeric,
  out flags_operations character varying
)
RETURNS SETOF record
AS 
$body$
/*
<description>
========================================================================================
           Хранимка для получения расчета для банка
========================================================================================
</description>
<history_list>
    <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
    <item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__rec__loan_version RECORD;
    v__rec__loans RECORD;
    v__rec__permissible_loans RECORD;
    v__rec__repayment_schedules RECORD;
    v__rec RECORD;
    
    v__dates_array DATE [];
    v_prev_date DATE;
    v__flag VARCHAR;
    v__npp INTEGER;
    
    v__balance_2 NUMERIC;
    v__balance_1 NUMERIC;
    
    
      
    v__total_f1 date;
    v__total_f2 numeric;
    v__total_f3 numeric;
    v__total_f4 numeric;
    v__total_f5 numeric;
    v__total_f6 numeric;
    v__total_f7 numeric;
    v__total_f8 numeric;
    v__total_f9 numeric;
    v__total_f10 date;
    v__total_f11 date;
    v__total_f12 numeric;
    v__total_f13 numeric;
    v__total_f14 numeric;
    v__total_f15 numeric;
    v__total_f16 numeric;
    v__total_f17 date;
    v__total_f18 date;
    v__total_f19 numeric;
    v__total_f20 numeric;
    v__total_f21 numeric;
    v__total_f22 numeric;
    
    v__records_count INTEGER;
    f11_pays_other__temp numeric; 
    v__permissible_loan_key__current_date INTEGER; -- Ключ графика платежей на указанную дату
    v__rec__permissible_loans__current_date RECORD; -- Запись о графике платежей на дату транзакции
    v__rate__current_date numeric; -- Процентная ставка на  дату транзакции согласно графику платежей
    
    v__fact_close_date DATE; -- Дата закрытия займа
BEGIN

    v__total_f4  = 0;
/*
    v__total_f1  = NULL;
    v__total_f2  = 0;
    v__total_f3  = 0;
    v__total_f4  = 0;
    v__total_f5  = 0;
    v__total_f6  = NULL;
    v__total_f7  = 0;
    v__total_f8  = 0;
    v__total_f9  = NULL;
    v__total_f10 = NULL;
    v__total_f11 = NULL;
    v__total_f12 = NULL;
    v__total_f13 = 0;
    v__total_f14 = 0;
    v__total_f15 = NULL;
    v__total_f16 = NULL;
    v__total_f17 = NULL;
    v__total_f18 = NULL;
    v__total_f19 = NULL;
    v__total_f20 = 0;
    v__total_f21 = 0;
    v__total_f22 = NULL;
*/
	-- Получаем данные версии рассчета
	SELECT * FROM loan_calc_history.loan_version lv 
    WHERE lv.loan_version_key = lv_key
	INTO v__rec__loan_version;
    
    SELECT * 
    FROM asuz_db.loans l
    WHERE l.loan_key = v__rec__loan_version.loan_key
    INTO v__rec__loans;
    
    SELECT * FROM asuz_db.permissible_loans pl
    WHERE pl.key = v__rec__loans.permissible_loan_key
    INTO v__rec__permissible_loans;
    
    
    SELECT * FROM asuz_db.repayment_schedules rs
    WHERE rs.permissible_loan_key = v__rec__loans.permissible_loan_key
    INTO  v__rec__repayment_schedules;




	FOR v__rec IN
    	SELECT	
        	*

        FROM
        	loan_calc_history.balance_value_history bvh 
        WHERE 
        	bvh.loan_version_key = lv_key
        ORDER BY
        	bvh.transact_date ASC
    LOOP
    	
    
        f1_reporting_date                      = v__rec.transact_date;

        f2_date_diff_from_start				= v__rec.transact_date::DATE - v__rec__loans.creation_date::DATE;
        
        
        -- Если это первый день - то пропускаем
        -- -------------------------------------------------------------------------------------------------------------------------------
        IF v__rec.transact_date::DATE = v__rec__loans.creation_date::DATE THEN
        	CONTINUE;
        END IF;
        -- -------------------------------------------------------------------------------------------------------------------------------        
        
        f2_1_loan								   = COALESCE(v__rec.loan_e,0) +COALESCE(v__rec.od_loan_e,0);

        
        
        
        f2_2__loan_rate__balance_date = 0;
        -- Получаем процентную ставку на день транзакции
        -- =========================================================================
        -- Получаем график платежей на текущую дату 
        v__permissible_loan_key__current_date = buh.get_permissible_loan_for_date(v__rec__loans.loan_key, v__rec.transact_date::DATE, 0);

        SELECT *
        FROM asuz_db.permissible_loans t
        WHERE t.key = v__permissible_loan_key__current_date
        INTO v__rec__permissible_loans__current_date;


        /*
        rate_rs - аннуитетная ставка в день
        rate - переплата в день
        Поменять в COALESCE сказал Михаил Кофанов
        
        */
        
    	-- Согласно обсуждению от 01_02_2019 закомментированный ниже код посчитали некорректным
    	-- Потому как в случае с неануитетным графиком поле permissible_loans.rate_rs не будет заполнено.
        v__rate__current_date = COALESCE(v__rec__permissible_loans__current_date.rate_rs,v__rec__permissible_loans__current_date.rate);
        f2_2__loan_rate__balance_date = v__rate__current_date;
        -- =========================================================================

          
      
      
      
      

		-- Начислено: Сумма начисленных процентов в день, руб.  (аланоаые + доначисленные)
    	f3_od_n_overpayment_all			   			     = 0; 
        -- ============================================================================
        SELECT 
            SUM(t.summ)
        FROM 
            loan_calc_history.loan_transact_history  t
        WHERE
              t.loan_version_key = lv_key
              AND 
              t.active = 1 
              AND
              t.transact_date =v__rec.transact_date
        AND
            -- 10; -- Начисление плановых процентов
            -- 11; -- Доначисление процентов за просрочку на тело займа
            
            -- 58; -- Корректировка не выясненного платежа по процентам просроченным
            -- 59
            -- 60
            -- 61
            -- 69
            
            -- 71
            -- 72
            -- 73
            -- 74
            -- 75
            t.account_transaction_key IN (10,11
            /*
            ,
            58,59,60,61,69, -- Коррективки
            71,72,73,74,75 -- Коррективки
            */
            )
        INTO
            f3_od_n_overpayment_all;
        f3_od_n_overpayment_all = COALESCE(f3_od_n_overpayment_all,0);
        -- ============================================================================
        

		-- Начислено:Сумма процентов накопительным итогом, руб.
        f4_overpayment_all_summ       = v__rec.all_loan_percents_e; 
        f4_overpayment_all_summ       = COALESCE(f4_overpayment_all_summ,0);
        
        -- Начислено: Сумма штрафов в день, руб.
        /*
        f5_fine_per_day 					  = COALESCE(v__rec.od_fine_e,0) - COALESCE(v__rec.od_fine_b,0); 
        */
        f5_fine_per_day			   			     = 0; 
        -- ============================================================================
        SELECT 
            SUM(t.summ)
        FROM 
            loan_calc_history.loan_transact_history  t
        WHERE
              t.loan_version_key = lv_key
              AND 
              t.active = 1 
              AND
              t.transact_date =v__rec.transact_date
        AND
            -- 12; -- Начисление пени за просрочку
            t.account_transaction_key IN (12)
        INTO
            f5_fine_per_day;
        f5_fine_per_day = COALESCE(f5_fine_per_day,0);
        -- ============================================================================
        
        
        f6_other_payments				   = 0; -- Начислено:Сумма иных платежей в день, руб. (с указанием назначения платежа)
        
        f7_pays_all				   			     = 0; -- Оплачено: Всего
        
        -- Получаем сумму всех платежей за все время
        -- ============================================================================      
        SELECT 
            SUM(t.summ)
        FROM 
            loan_calc_history.loan_transact_history  t
        WHERE
            t.loan_version_key = lv_key
            AND 
            t.active = 1 
            AND
            t.transact_date =v__rec.transact_date
        AND
            -- 45; --Нал(additional_value = 0) 
            -- 3; --Безнал (additional_value = 1)
            t.account_transaction_key IN (3,45)
        INTO
            f7_pays_all;
        f7_pays_all = COALESCE(f7_pays_all,0);
        -- ============================================================================      



    	f8_pays_body			   			     = 0; -- Оплачено: В счет процентов
        -- в погашение основного долга:
        -- ============================================================================      
        SELECT 
            SUM(t.summ)
        FROM 
            loan_calc_history.loan_transact_history  t
        WHERE
            t.loan_version_key = lv_key
            AND 
            t.active = 1 
            AND
            t.transact_date =v__rec.transact_date
        AND
            -- 13; -- Погашение срочного долга_тело
            -- 15; -- Погашение просроченного долга_ тело
            t.account_transaction_key IN (13,15)
        INTO
            f8_pays_body;
      	f8_pays_body = COALESCE(f8_pays_body,0);
        -- ============================================================================      






    	f9_pays_percent			   			     = 0; -- Оплачено: В счет процентов
      -- Из них в погашение процентов
      -- ============================================================================      
      SELECT 
          SUM(t.summ)
      FROM 
          loan_calc_history.loan_transact_history  t
      WHERE
            t.loan_version_key = lv_key
            AND 
            t.active = 1 
            AND
            t.transact_date =v__rec.transact_date
      AND
          -- 17; -- Погашение просроченные доначисленные проценты
          -- 48; -- Погашение просроченного долга_ плановые проценты
          -- 47; -- Погашение срочного долга_плановые проценты
          -- 76; -- Погашение срочного долга_плановые проценты при закрытии
          t.account_transaction_key IN (17,48, 47, 76)
      INTO
          f9_pays_percent;
      f9_pays_percent = COALESCE(f9_pays_percent,0);
      -- ============================================================================      




    	f10_pays_peni			   			     = 0; -- Оплачено: В счет штрафов
      -- в погашение пени
      -- ============================================================================      
      SELECT 
          SUM(t.summ)
      FROM 
          loan_calc_history.loan_transact_history  t
      WHERE
            t.loan_version_key = lv_key
            AND 
            t.active = 1 
            AND
            t.transact_date =v__rec.transact_date
      AND
          -- 19; -- Погашение пени
          t.account_transaction_key IN (19)
      INTO
          f10_pays_peni;
      f10_pays_peni = COALESCE(f10_pays_peni,0);
      -- ============================================================================      



















	  f11_pays_other			   			     = '0'; -- Оплачено: В счет иных платежей (с указанием назначения платежа)


      
      
      f12_balance_all = 0; -- Остаток задолженности: Всего
      f12_balance_all = COALESCE(v__rec.od_loan_e,0) + -- Проср. тело займа на конец (58.03.02)
  						         	COALESCE(v__rec.n_loan_e,0)+ -- Непросроченое тело займа на конец (58.03.01)
      							 	COALESCE(v__rec.od_overpayment_planned_e,0) + --Проср. % плановые на конец
      							 	COALESCE(v__rec.od_overpayment_for_delay_e,0) + --Проср. % доначисленные на конец (76.03.03)                                 
                                 	COALESCE(v__rec.od_fine_e,0); --Проср. % пени на конец (76.05.02)

	  -- Остаток задолженности: Основной долг	
      f13_balance_body = COALESCE(v__rec.od_loan_e,0) + -- Проср. тело займа на конец (58.03.02)
      								 COALESCE(v__rec.n_loan_e,0);  --+  -- Непросроченое тело займа на конец (58.03.01)
                                 --    COALESCE(v__rec.loan_e,0); -- Тело займа на конец 58.03
      f13_balance_body = COALESCE(f13_balance_body,0);
      
    
      -- Остаток задолженности: Проценты
      f14_balance_percents_all = COALESCE(v__rec.od_overpayment_planned_e,0) + --Проср. % плановые на конец
                                                COALESCE(v__rec.od_overpayment_for_delay_e,0); --Проср. % доначисленные на конец (76.03.03)
                                                      
      f14_balance_percents_all = COALESCE(f14_balance_percents_all,0);  
      
      
      
        -- Остаток задолженности: Штрафы
        f15_balance_fine 					  = COALESCE(v__rec.od_fine_e,0); 
      
      
      
      /**
       * Если Остаток задолженности получается отрицательный - показываем ноль для банка
       * Обсудили 26_03_2019 с Константином и было принято такое решение
       * 
       */
      /*
      IF f12_balance_all < 0 THEN
      	f12_balance_all = 0;
      END IF;

      IF f13_balance_body < 0 THEN
      	f13_balance_body = 0;
      END IF;

      IF f14_balance_percents_all < 0 THEN
      	f14_balance_percents_all = 0;
      END IF;

      IF f15_balance_fine < 0 THEN
      	f15_balance_fine = 0;
      END IF;
      */
      
      
    /*
    Оговорили 26_03_2019 с Костей, что поля будем считать следующим образом
    3 - 10 = 15
    6 - 11 = 16 
    */
    /*
    f13_balance_body = f2_1_loan - f8_pays_body;
    f14_balance_percents_all = f4_overpayment_all_summ - f9_pays_percent;
    */  
      
        /*
         * Если займ закрыт и существует факт закрытия займа (сгенерированный или скопированный из оригинала
         * (на примере займа Z340005190301 (loan_key = 1211015)- тогда для последнего дня устанавливаем нулевые значения 
         * по остатку долга и оставшимся суммам в полях "Остаток задолженности"
         * При этом, даже если согласно  расчетам collection-calc остался долг - в отчете для банка он не будет отражен
         * 20_03_2019 проведена конференция с заказчиками. Задача https://redmine.vivadengi.ru/issues/480575
         * На конфе было принято решение вручную, для конкретных ситуаций проставить ноль
         */
      	-- ==================================================================================
        SELECT
        	f.fact_date
        FROM
        	facts.facts f
        WHERE
        	f.loan_key = v__rec__loans.loan_key
            AND
            f.fact_name_key = 3 --Закрытие займа
        INTO
        	v__fact_close_date;
            
        IF FOUND THEN
            IF v__rec__loans.returned = TRUE AND v__rec.transact_date = v__fact_close_date::DATE  THEN
                f2_1_loan = 0;
                f12_balance_all = 0;
                f13_balance_body = 0;
                f14_balance_percents_all = 0;
                f15_balance_fine = 0;
            END IF;
        END IF;
      	-- ==================================================================================

      
      	

		RETURN NEXT;
    END LOOP;

    
	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function lock_calculation (OID = 587069) : 
--
CREATE FUNCTION loan_calc_history.lock_calculation (
  _loan_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
           Хранимка для создания блокировок при параллельных расчетов
</description>
<history_list>
    <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__rec__lock_calculation RECORD;
BEGIN
    SELECT 
      *
    FROM 
      loan_calc_history.lock_calculation  lc
    WHERE
        lc.loan_key = _loan_key
     	AND
        lc.create_date > clock_timestamp() - interval '2 minute'	
    INTO
    	v__rec__lock_calculation;
    
    IF FOUND THEN
    	result = -1;
        result_str = 'Указанный займ сейчас рассчитывается другим пользователем';
        RETURN;
    END IF;
    
    DELETE FROM 
    	loan_calc_history.lock_calculation 
    WHERE 
    	loan_key = _loan_key;
            
    INSERT INTO 
      loan_calc_history.lock_calculation
    (
      loan_key
    )
    VALUES (
      _loan_key
    );        


    result = 1;
    result_str = 'OK';
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function lock_calculation_drop (OID = 587090) : 
--
CREATE FUNCTION loan_calc_history.lock_calculation_drop (
  _loan_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
           Хранимка для удаления блокировок при параллельных расчетов
</description>
<history_list>
    <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__rec__lock_calculation RECORD;
BEGIN
 
    
    DELETE FROM 
    	loan_calc_history.lock_calculation
    WHERE 
    	loan_key = _loan_key;
            


    result = 1;
    result_str = 'OK';
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function start_us_calc (OID = 588944) : 
--
CREATE FUNCTION loan_calc_history.start_us_calc (
  _loan_key integer,
  _description character varying = ''::character varying,
  _nopeni integer = 0,
  _sequence character varying = ''::character varying,
  _first_payment_schedule integer = 0,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
           Хранимка для старта расчета
</description>
<history_list>
	<item><date_m>03.05.2018</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
    <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
  	v__rec__loan_version RECORD;
    v__rec__loan RECORD;
    v__rec_calc_loan_period RECORD;
    v__count INTEGER;
    v__rec RECORD;
    
    v__key INTEGER;
    v__startcalc TIMESTAMP;
    v__diff INTEGER;
    v__now TIMESTAMP;
BEGIN



	-- Проверяем существование sequence
    -- ================================================
	PERFORM loan_calc_history.drop_calc_sequence (_sequence);
    PERFORM loan_calc_history.create_calc_sequence(_loan_key, _sequence);
    -- ================================================



    -- Очищаем таблицу
    DELETE FROM loan_calc_history.timelog WHERE true;   


    -- Вставляем данные о вызываемой хранимке
    v__now = clock_timestamp();
    INSERT INTO 
      loan_calc_history.timelog
    (
      description,
      startcalc,
      endcalc,
      timecalc
    )
    VALUES (
      'fill_facts_by_loan',
      v__now,
      NULL,
      NULL
    )
    RETURNING
        key
    INTO
        v__key; 
    v__startcalc =  clock_timestamp();    

	--
  	SELECT * FROM loan_calc_history.add_loan_version(
    		_loan_key,
            _description,
            _nopeni,
            _first_payment_schedule
        )
    INTO v__rec__loan_version;
    
    IF v__rec__loan_version.result = -1 THEN
    	result = -1;
        result_str = v__rec__loan_version.result_str;
		RETURN;
    END IF;
    
    SELECT * FROM asuz_db.loans l
    WHERE l.loan_key = _loan_key
    INTO v__rec__loan;
    
    -- Где-то тут генерируем факты
	PERFORM * FROM facts.fill_facts_by_loan(_loan_key);    
    
    
    
    
    
    
    UPDATE 
      loan_calc_history.timelog 
    SET 
      endcalc = clock_timestamp(),
      timecalc = age(clock_timestamp(),v__startcalc)
    WHERE 
        key = v__key; 
        
        
        
        
        
    -- Вставляем данные о вызываемой хранимке
    v__now = clock_timestamp();
    INSERT INTO 
      loan_calc_history.timelog
    (
      description,
      startcalc,
      endcalc,
      timecalc
    )
    VALUES (
      'DELETE balances',
      v__now,
      NULL,
      NULL
    )
    RETURNING
        key
    INTO
        v__key; 
    v__startcalc =  clock_timestamp();    
    
    
    
    
    SELECT count(*) FROM facts.facts f WHERE
    f.loan_key = _loan_key
    INTO v__count;
    
    RAISE NOTICE 'Количество фактов = %',v__count;
    --RETURN;
    
    -- Очищаем транзакции
    DELETE FROM buh.loan_transact WHERE TRUE;
    
    -- Предварительно очищаем данные по балансовым величинам
    DELETE FROM buh.balance_value bv WHERE bv.loan_key != _loan_key;
    DELETE FROM buh.balance_value bv WHERE 
    	bv.loan_key = _loan_key
        AND
        bv.transact_date >= v__rec__loan.creation_date;
    
    
    
    
    
    
    
    
    
    UPDATE 
      loan_calc_history.timelog 
    SET 
      endcalc = clock_timestamp(),
      timecalc = age(clock_timestamp(),v__startcalc)
    WHERE 
        key = v__key;
        







        
    -- Вставляем данные о вызываемой хранимке
    v__now = clock_timestamp();
    INSERT INTO 
      loan_calc_history.timelog
    (
      description,
      startcalc,
      endcalc,
      timecalc
    )
    VALUES (
      'calc_loan_period',
      v__now,
      NULL,
      NULL
    )
    RETURNING
        key
    INTO
        v__key; 
    v__startcalc =  clock_timestamp();  









        
    RAISE NOTICE 'creation_date = %',v__rec__loan.creation_date::DATE;
         
    
    -- Вызываем рассчет для Учетного слоя
	SELECT * FROM buh.calc_loan_period(
    				_loan_key, 
                    v__rec__loan.creation_date::DATE, 
    				NOW()::DATE,
                    v__rec__loan_version.result,
                    _sequence)
    INTO v__rec_calc_loan_period;  
    
    
    
    
    
    
    
    
    
    UPDATE 
      loan_calc_history.timelog 
    SET 
      endcalc = clock_timestamp(),
      timecalc = age(clock_timestamp(),v__startcalc)
    WHERE 
        key = v__key;
    
    
    
    
    
    
    
    
    
    
    
    
	-- Полученный рассчет переносим в историю рассчетов
    FOR v__rec IN
    	SELECT * FROM buh.loan_transact WHERE TRUE
    LOOP
        INSERT INTO 
          loan_calc_history.loan_transact_history
        (
          loan_key,
          separate_subdivision_key,
          summ,
          fact_key,
          account_transaction_key,
          transact_date,
          create_date,
          active,
          loan_version_key
        )
        VALUES (
          v__rec.loan_key,
          v__rec.separate_subdivision_key,
          v__rec.summ,
          v__rec.fact_key,
          v__rec.account_transaction_key,
          v__rec.transact_date,
          v__rec.create_date,
          v__rec.active,
          v__rec__loan_version.result
        );    	
    END LOOP;
    
	FOR v__rec IN
        SELECT * FROM buh.balance_value bv WHERE 
            bv.loan_key = _loan_key
            AND
            bv.transact_date >= v__rec__loan.creation_date
    LOOP
        INSERT INTO 
          loan_calc_history.balance_value_history
        (
          loan_key,
          od_loan_b,
          od_loan_e,
          od_overpayment_planned_b,
          od_overpayment_planned_e,
          od_overpayment_for_delay_b,
          od_overpayment_for_delay_e,
          od_fine_b,
          od_fine_e,
          transact_date,
          create_date,
          n_loan_b,
          n_loan_e,
          n_overpayment_planned_b,
          n_overpayment_planned_e,
          loan_b,
          loan_e,
          client_balance_b,
          client_balance_e,
          active,
          opened,
          n_overpayment_conditionally_b,
          n_overpayment_conditionally_e,
          all_loan_percents_b,
          all_loan_percents_e,
          loan_version_key
        )
        VALUES (
          v__rec.loan_key,
          v__rec.od_loan_b,
          v__rec.od_loan_e,
          v__rec.od_overpayment_planned_b,
          v__rec.od_overpayment_planned_e,
          v__rec.od_overpayment_for_delay_b,
          v__rec.od_overpayment_for_delay_e,
          v__rec.od_fine_b,
          v__rec.od_fine_e,
          v__rec.transact_date,
          v__rec.create_date,
          v__rec.n_loan_b,
          v__rec.n_loan_e,
          v__rec.n_overpayment_planned_b,
          v__rec.n_overpayment_planned_e,
          v__rec.loan_b,
          v__rec.loan_e,
          v__rec.client_balance_b,
          v__rec.client_balance_e,
          v__rec.active,
          v__rec.opened,
          v__rec.n_overpayment_conditionally_b,
          v__rec.n_overpayment_conditionally_e,
          v__rec.all_loan_percents_b,
          v__rec.all_loan_percents_e,
          v__rec__loan_version.result
        );        
    END LOOP;
    
    result = v__rec__loan_version.result;
    result_str = 'OK';
    RETURN;
      
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function add_loan_version (OID = 588947) : 
--
CREATE FUNCTION loan_calc_history.add_loan_version (
  _loan_key integer,
  _description character varying = ''::character varying,
  _nopeni integer = 0,
  _first_payment_schedule integer = 0,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
           Хранимка для добавления номера версии расчета
</description>
<history_list>
  <item><date_m>07.05.2018</date_m><task_n>402432</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
  	v__loans_count INTEGER;
BEGIN

	-- Получаем количество займов с указанным ключем
	SELECT count(*)
    FROM asuz_db.loans l
    WHERE l.loan_key = _loan_key
    INTO v__loans_count;
    
    IF v__loans_count != 1 THEN
    	result = -1;
        result = 'Указанный займ не найден';
        RETURN;
    END IF;

    INSERT INTO 
      loan_calc_history.loan_version
    (
      loan_key,
      description,
      nopeni,
      first_payment_schedule
    )
    VALUES (
      _loan_key,
      _description,
      _nopeni,
      _first_payment_schedule
    )
    RETURNING
        loan_version_key
    INTO
        result;  
    
    result_str = 'OK';
    
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_loan_calc_history (OID = 588962) : 
--
CREATE FUNCTION loan_calc_history.get_loan_calc_history (
  _contract_number character varying,
  out loan_version_key integer,
  out loan_key integer,
  out creation_date timestamp without time zone,
  out description character varying,
  out nopeni integer,
  out first_payment_schedule integer
)
RETURNS SETOF record
AS 
$body$
/*
<history_list>
<item><date_m>03.05.2018</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__rec_loan RECORD;
    v__rec RECORD;
    v__count INTEGER;
BEGIN


	PERFORM * FROM asuz_db.loans l
	where l.contract = _contract_number
        AND
        l.issued = TRUE;

	IF NOT FOUND THEN
		RETURN;
	END IF;
    
    

    SELECT * 
    FROM asuz_db.loans l
    WHERE l.contract = _contract_number
        AND
        l.issued = TRUE
    INTO v__rec_loan;
    
    SELECT count(*)
    FROM loan_calc_history.loan_version lv
    WHERE lv.loan_key = v__rec_loan.loan_key
    INTO v__count;
    
    raiSE NOTICE 'v__count = %', v__count;
	IF v__count = 0 THEN
        raiSE NOTICE 'zz';

		RETURN;
	END IF;
    
    FOR v__rec IN
    	SELECT *
        FROM loan_calc_history.loan_version lv
        WHERE lv.loan_key = v__rec_loan.loan_key
        ORDER BY lv.creation_date DESC
    LOOP
        loan_version_key = v__rec.loan_version_key;
        loan_key = v__rec.loan_key;
        creation_date = v__rec.creation_date;
        description = v__rec.description;
        nopeni = v__rec.nopeni;
        first_payment_schedule = v__rec.first_payment_schedule;
        
        RETURN NEXT;
    END LOOP;
    
    
    RETURN;
    
END;
$body$
LANGUAGE plpgsql
STABLE SECURITY DEFINER;
--
-- Definition for function add_current_calc (OID = 588989) : 
--
SET search_path = buh, pg_catalog;
CREATE FUNCTION buh.add_current_calc (
  _loan_key integer,
  _loan_version_key integer,
  out result integer,
  out result_str character varying
)
RETURNS record
AS 
$body$
/*
<description>
	Функция добавляем метку о выполняемом сейчас расчете займ  + версия расчета
    для того, чтобы во время расчета можно было поднять настройки текущей версии расчета
</description>
<history_list>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
BEGIN


DELETE FROM	buh.current_calc
WHERE loan_key = _loan_key;

INSERT INTO 
  buh.current_calc
(
  loan_key,
  loan_version_key
)
VALUES (
  _loan_key,
  _loan_version_key
)
RETURNING
	current_calc_key
INTO
	result;
result_str = 'OK';
RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_calc_first_payment_schedule (OID = 589000) : 
--
CREATE FUNCTION buh.get_calc_first_payment_schedule (
  _loan_key integer,
  out result integer,
  out application_key integer
)
RETURNS record
AS 
$body$
/*
<description>
	Получаем настройку Рассчитать по первому графику платежей на момент выдачи и application_key
</description>
<history_list>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__lv_key INTEGER;
    v__rec__loans RECORD;
    v__count INTEGER;
    v__application_key INTEGER;
BEGIN
	SELECT
    	cc.loan_version_key
    FROM
    	buh.current_calc cc
    WHERE
    	cc.loan_key = _loan_key
    INTO
    	v__lv_key;
    
    
    IF NOT FOUND THEN
    	result = 0;
        RETURN;
    END IF;
    
    SELECT 
    	lv.first_payment_schedule
    FROM
    	loan_calc_history.loan_version lv
    WHERE
    	lv.loan_version_key = v__lv_key
    INTO
    	result;
     
    
    IF NOT FOUND THEN
    	result = 0;
        RETURN;
    END IF;
    

   	result = COALESCE(result,0);
    
    
    IF result = 1 THEN
    	SELECT 
        	 l.application_key
        FROM
        	asuz_db.loans l
        WHERE
        	l.loan_key = _loan_key
        INTO
        	v__application_key;
             
        v__application_key = coalesce(v__application_key,0);
        application_key = v__application_key;
               
        IF NOT FOUND THEN
            result = 0;
            RETURN;
        END IF;
        
        SELECT count(t.payment_date)
        FROM asuz_db.tmp_payment_schedules t 
        WHERE t.application_key = v__application_key
        INTO v__count;
        
        IF v__count = 0 THEN
            result = 0;
            RETURN;
        END IF;
        
    END IF;

END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_balance_value_history (OID = 592300) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE FUNCTION loan_calc_history.get_balance_value_history (
  _lv_key integer,
  out balance_value_key integer,
  out loan_key integer,
  out od_loan_b numeric,
  out od_loan_e numeric,
  out od_overpayment_planned_b numeric,
  out od_overpayment_planned_e numeric,
  out od_overpayment_for_delay_b numeric,
  out od_overpayment_for_delay_e numeric,
  out od_fine_b numeric,
  out od_fine_e numeric,
  out transact_date date,
  out create_date timestamp without time zone,
  out n_loan_b numeric,
  out n_loan_e numeric,
  out n_overpayment_planned_b numeric,
  out n_overpayment_planned_e numeric,
  out loan_b numeric,
  out loan_e numeric,
  out client_balance_b numeric,
  out client_balance_e numeric,
  out active integer,
  out opened boolean,
  out n_overpayment_conditionally_b numeric,
  out n_overpayment_conditionally_e numeric,
  out all_loan_percents_b numeric,
  out all_loan_percents_e numeric,
  out loan_version_key integer
)
RETURNS SETOF record
AS 
$body$
/*
<history_list>
<item><date_m>14.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
  v__rec__bvh RECORD;
BEGIN
	FOR v__rec__bvh IN
        SELECT * FROM
            loan_calc_history.balance_value_history bvh
        WHERE
            bvh.loan_version_key = _lv_key
        ORDER BY
            bvh.transact_date ASC
	LOOP
              
        balance_value_key = v__rec__bvh.balance_value_key;
        loan_key = v__rec__bvh.loan_key;
        od_loan_b = v__rec__bvh.od_loan_b;
        od_loan_e = v__rec__bvh.od_loan_e;
        od_overpayment_planned_b = v__rec__bvh.od_overpayment_planned_b;
        od_overpayment_planned_e = v__rec__bvh.od_overpayment_planned_e;
        od_overpayment_for_delay_b = v__rec__bvh.od_overpayment_for_delay_b;
        od_overpayment_for_delay_e = v__rec__bvh.od_overpayment_for_delay_e;
        od_fine_b = v__rec__bvh.od_fine_b;
        od_fine_e = v__rec__bvh.od_fine_e;
        transact_date = v__rec__bvh.transact_date;
        create_date = v__rec__bvh.create_date;
        n_loan_b = v__rec__bvh.n_loan_b;
        n_loan_e = v__rec__bvh.n_loan_e;
        n_overpayment_planned_b = v__rec__bvh.n_overpayment_planned_b;
        n_overpayment_planned_e = v__rec__bvh.n_overpayment_planned_e;
        loan_b = v__rec__bvh.loan_b;
        loan_e = v__rec__bvh.loan_e;
        client_balance_b = v__rec__bvh.client_balance_b;
        client_balance_e = v__rec__bvh.client_balance_e;
        active = v__rec__bvh.active;
        opened = v__rec__bvh.opened;
        n_overpayment_conditionally_b = v__rec__bvh.n_overpayment_conditionally_b;
        n_overpayment_conditionally_e = v__rec__bvh.n_overpayment_conditionally_e;
        all_loan_percents_b = v__rec__bvh.all_loan_percents_b;
        all_loan_percents_e = v__rec__bvh.all_loan_percents_e;
        loan_version_key = v__rec__bvh.loan_version_key;

    
    	RETURN NEXT;
    END LOOP;
    
    RETURN;
    
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_loan_transact_history (OID = 592336) : 
--
CREATE FUNCTION loan_calc_history.get_loan_transact_history (
  _lv_key integer,
  out loan_transact_key integer,
  out loan_key integer,
  out separate_subdivision_key character varying,
  out summ numeric,
  out fact_key integer,
  out account_transaction_key integer,
  out transact_date date,
  out create_date timestamp without time zone,
  out active integer,
  out loan_version_key integer,
  out transactname character varying
)
RETURNS SETOF record
AS 
$body$
/*
<history_list>
<item><date_m>14.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
  v__rec__ltr RECORD;
BEGIN
	FOR v__rec__ltr IN
        SELECT 
            at.name as transactname,
            ltr.*
        FROM
            loan_calc_history.loan_transact_history ltr
        LEFT JOIN
            buh.account_transaction at
            ON
                at.account_transaction_key = ltr.account_transaction_key
        WHERE
            ltr.loan_version_key = _lv_key
        ORDER BY
            ltr.transact_date ASC
	LOOP
    
	    transactname  = v__rec__ltr.transactname;
        loan_transact_key  = v__rec__ltr.loan_transact_key;
        loan_key  = v__rec__ltr.loan_key;
        separate_subdivision_key  = v__rec__ltr.separate_subdivision_key;
        summ  = v__rec__ltr.summ;
        fact_key  = v__rec__ltr.fact_key;
        account_transaction_key  = v__rec__ltr.account_transaction_key;
        transact_date  = v__rec__ltr.transact_date;
        create_date   = v__rec__ltr.create_date;
        active   = v__rec__ltr.active;
        loan_version_key   = v__rec__ltr.loan_version_key;
    
              /*
        balance_value_key = v__rec__bvh.balance_value_key;
        loan_key = v__rec__bvh.loan_key;
        od_loan_b = v__rec__bvh.od_loan_b;
        od_loan_e = v__rec__bvh.od_loan_e;
        od_overpayment_planned_b = v__rec__bvh.od_overpayment_planned_b;
        od_overpayment_planned_e = v__rec__bvh.od_overpayment_planned_e;
        od_overpayment_for_delay_b = v__rec__bvh.od_overpayment_for_delay_b;
        od_overpayment_for_delay_e = v__rec__bvh.od_overpayment_for_delay_e;
        od_fine_b = v__rec__bvh.od_fine_b;
        od_fine_e = v__rec__bvh.od_fine_e;
        transact_date = v__rec__bvh.transact_date;
        create_date = v__rec__bvh.create_date;
        n_loan_b = v__rec__bvh.n_loan_b;
        n_loan_e = v__rec__bvh.n_loan_e;
        n_overpayment_planned_b = v__rec__bvh.n_overpayment_planned_b;
        n_overpayment_planned_e = v__rec__bvh.n_overpayment_planned_e;
        loan_b = v__rec__bvh.loan_b;
        loan_e = v__rec__bvh.loan_e;
        client_balance_b = v__rec__bvh.client_balance_b;
        client_balance_e = v__rec__bvh.client_balance_e;
        active = v__rec__bvh.active;
        opened = v__rec__bvh.opened;
        n_overpayment_conditionally_b = v__rec__bvh.n_overpayment_conditionally_b;
        n_overpayment_conditionally_e = v__rec__bvh.n_overpayment_conditionally_e;
        all_loan_percents_b = v__rec__bvh.all_loan_percents_b;
        all_loan_percents_e = v__rec__bvh.all_loan_percents_e;
        loan_version_key = v__rec__bvh.loan_version_key;
*/
    
    	RETURN NEXT;
    END LOOP;
    
    RETURN;
    
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_loan_info (OID = 592345) : 
--
SET search_path = facts, pg_catalog;
CREATE FUNCTION facts.get_loan_info (
  _lv_key integer,
  out result integer,
  out result_str character varying,
  out fio character varying,
  out contract_number character varying,
  out loan_date date,
  out loan_summ numeric,
  out percent_contract numeric,
  out percent_loan_overdue numeric,
  out planned_close_date date,
  out loan_rate numeric,
  out rate numeric,
  out loan_restructuring_count integer,
  out loan_restructuring_dates character varying,
  out loan_key integer
)
RETURNS record
AS 
$body$
/*
<history_list>
<item><date_m>03.05.2018</date_m><task_n></task_n><author>Тихоненко В.В.</author></item>
<item><date_m>14.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__rec_loan RECORD;
    v__loan_key INTEGER;
    v__permissible_loan_key INTEGER;
    v__rate NUMERIC;
    v__rec_restructuring RECORD;
    v__rec__permissible_loans RECORD;
BEGIN

	-- Проверяем существование версии рассчета
    -- =========================================================================
	PERFORM * FROM loan_calc_history.loan_version lv
    WHERE lv.loan_version_key = _lv_key;

	IF NOT FOUND THEN
        result = -1;
        result_str = 'Займ не найден';
		RETURN;
	END IF;
    -- =========================================================================
    
    
    

	-- Получаем ключ займа
    -- =========================================================================
    SELECT lv.loan_key FROM loan_calc_history.loan_version lv
    WHERE lv.loan_version_key = _lv_key
    INTO v__loan_key;
    -- =========================================================================



	-- Получаем данные о займе
    -- =========================================================================
    SELECT * 
    FROM asuz_db.loans l
    WHERE l.loan_key = v__loan_key
    INTO v__rec_loan;
    -- =========================================================================
    
    
    
    
    
	-- Получаем % ставка по договору (в день)	
    -- =========================================================================
	v__permissible_loan_key = buh.get_permissible_loan_for_date(v__loan_key, v__rec_loan.creation_date::DATE, 0);
    
	SELECT *
    FROM asuz_db.permissible_loans t
    WHERE t.key = v__permissible_loan_key
    INTO v__rec__permissible_loans;
    
    -- Если график аннуитетный
    IF v__rec__permissible_loans.type_repayment_schedule_key = 2 THEN
    	v__rate = COALESCE(v__rec__permissible_loans.rate,v__rec__permissible_loans.rate_rs);
    -- Если график не аннуитетный
    ELSE
    	v__rate = COALESCE(v__rec__permissible_loans.rate_rs,v__rec__permissible_loans.rate);
    END IF; 
    -- =========================================================================
    
    
    loan_restructuring_count = 0;
    loan_restructuring_dates = '';
    FOR v__rec_restructuring IN
        SELECT * FROM asuz_db.loan_restructuring_history lrh
        WHERE lrh.loan_key = v__loan_key
        ORDER BY lrh.agreement_date ASC
    LOOP
        IF loan_restructuring_count = 0 THEN
            loan_restructuring_dates = loan_restructuring_dates || 
                                       '' ||
                                       to_char(v__rec_restructuring.agreement_date, 'DD.MM.YYYY');
		ELSE
            loan_restructuring_dates = loan_restructuring_dates || 
                                       ';' ||
                                       to_char(v__rec_restructuring.agreement_date, 'DD.MM.YYYY');
        END IF;

	    loan_restructuring_count = loan_restructuring_count + 1;

    END LOOP;

    
    
    planned_close_date = v__rec_loan.creation_date + v__rec_loan.loan_term;
	rate = buh.get_rate_by_creation_date(v__loan_key);
    fio = public.borrower_fio(v__rec_loan.borrower_key);
    contract_number = v__rec_loan.contract;
    loan_date = v__rec_loan.creation_date;
    loan_summ = v__rec_loan.loan_sum;
    percent_contract = 0;
    percent_loan_overdue = 0;
    loan_rate = v__rate;
    loan_key = v__rec_loan.loan_key;
    result = 1;
    result_str = 'OK';
    
    
    RETURN;
    
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_loan_facts (OID = 592346) : 
--
CREATE FUNCTION facts.get_loan_facts (
  _loan_key integer,
  out fact_key integer,
  out fact_date timestamp without time zone,
  out fact_name character varying,
  out obj_type integer,
  out obj_key integer,
  out value numeric,
  out transact_date date
)
RETURNS SETOF record
AS 
$body$
/*
<history_list>
<item><date_m>14.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
  v__rec__f RECORD;

BEGIN

	FOR v__rec__f IN
        SELECT 
            fn.fact_name,
            f.*
        FROM 
          facts.facts f
        LEFT JOIN
            facts.fact_names fn ON
            fn.fact_name_key = f.fact_name_key
        WHERE
            f.loan_key = _loan_key
        ORDER BY
            f.fact_date ASC
	LOOP
    
         fact_key  = v__rec__f.fact_key;
         fact_date  = v__rec__f.fact_date;
         fact_name  = v__rec__f.fact_name;
         obj_type  = v__rec__f.obj_type;
         obj_key  = v__rec__f.obj_key;
         value  = v__rec__f.value;
         transact_date = v__rec__f.fact_date::DATE;
  
    	RETURN NEXT;
    END LOOP;

	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_asuz_loan_facts (OID = 592347) : 
--
CREATE FUNCTION facts.get_asuz_loan_facts (
  _loan_key integer,
  out fact_key integer,
  out fact_date timestamp without time zone,
  out fact_name character varying,
  out obj_type integer,
  out obj_key integer,
  out value numeric,
  out transact_date date
)
RETURNS SETOF record
AS 
$body$
/*
<history_list>
<item><date_m>14.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
  v__rec__f RECORD;

BEGIN

	FOR v__rec__f IN
        SELECT 
            fn.fact_name,
            f.*
        FROM 
          asuz_db.facts f
        LEFT JOIN
            facts.fact_names fn ON
            fn.fact_name_key = f.fact_name_key
        WHERE
            f.loan_key = _loan_key
        ORDER BY
            f.fact_date ASC
	LOOP
    
         fact_key  = v__rec__f.fact_key;
         fact_date  = v__rec__f.fact_date;
         fact_name  = v__rec__f.fact_name;
         obj_type  = v__rec__f.obj_type;
         obj_key  = v__rec__f.obj_key;
         value  = v__rec__f.value;
         transact_date = v__rec__f.fact_date::DATE;
  
    	RETURN NEXT;
    END LOOP;

	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_repayment_schedules (OID = 592768) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE FUNCTION loan_calc_history.get_repayment_schedules (
  inout _loan_key integer,
  out repayment_schedule_key integer,
  out loan_key integer,
  out payment_date date,
  out loan_sum numeric,
  out overpayment_sum numeric,
  out delay boolean,
  out debtor boolean,
  out returned boolean,
  out permissible_loan_key integer,
  out returned_date date
)
RETURNS SETOF record
AS 
$body$
/*
<description>
========================================================================================
	Получаем график платежей
========================================================================================
</description>
<history_list>
  <item><date_m>14.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
  <item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__count INTEGER;
    v__rec RECORD; 
BEGIN


	FOR v__rec IN
            /*
    	SELECT 
        	t.*
    	FROM 
        	asuz_db.repayment_schedules t
    	WHERE 
        	t.loan_key = _loan_key
		ORDER BY 
        	t.repayment_schedule_key ASC
            */
            
    	SELECT 
        	t.*,
            COALESCE(ro.payment_date_ori,t.payment_date) as cpayment_date
    	FROM 
        	asuz_db.repayment_schedules t
        LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
            on ro.repayment_schedule_key=t.repayment_schedule_key
    	WHERE 
        	t.loan_key = _loan_key
		ORDER BY 
        	t.repayment_schedule_key ASC

    LOOP
        repayment_schedule_key = v__rec.repayment_schedule_key;
        loan_key = v__rec.loan_key;
        payment_date = v__rec.cpayment_date;
        loan_sum = v__rec.loan_sum;
        overpayment_sum = v__rec.overpayment_sum;
        delay = v__rec.delay;
        debtor = v__rec.debtor;
        returned = v__rec.returned;
        permissible_loan_key = v__rec.permissible_loan_key;
        returned_date = v__rec.returned_date;
    
        RETURN NEXT;
    END LOOP;

	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_permissible_loans (OID = 592773) : 
--
CREATE FUNCTION loan_calc_history.get_permissible_loans (
  inout _loan_key integer,
  out key integer,
  out loan_key integer,
  out version_date date,
  out rate numeric,
  out rate_rs numeric
)
RETURNS SETOF record
AS 
$body$
/*
<history_list>
<item><date_m>14.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
    v__count INTEGER;
    v__rec RECORD; 
BEGIN


	FOR v__rec IN
            
    	SELECT 
        	t.*
    	FROM 
        	asuz_db.permissible_loans t
    	WHERE 
        	t.loan_key = _loan_key
		ORDER BY 
        	t.version_date ASC

    LOOP
        key = v__rec.key;
        loan_key = v__rec.loan_key;
        version_date = v__rec.version_date;
        rate = v__rec.rate;
        rate_rs = v__rec.rate_rs;
    
        RETURN NEXT;
    END LOOP;

	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_peni__60_days_date_enable (OID = 604343) : 
--
SET search_path = buh, pg_catalog;
CREATE FUNCTION buh.get_peni__60_days_date_enable (
  _loan_key integer,
  _date date,
  out result boolean
)
RETURNS boolean
AS 
$body$
/*
<description>
========================================================================================
	Хранимка используется для расчета максимальной даты насичисления пени. Потому как для займов, 
    которые выданы до 01.07.2014 мы считаем неустойку только в том случае, если балансовая величина 
    просроченного тела займа больше ноля. Но при этом с даты последнего изменения 
    (согласно графику платежей) балансовой величины просроченного тела займа - пени не 
    должны начислять более 60 дней
========================================================================================
</description>
<history_list>
    <item><date_m>13.03.2019</date_m><task_n>477118</task_n><author>Тихоненко В.В.</author></item>
	<item><date_m>25.03.2019</date_m><task_n>480575</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__creation_date DATE; -- Дата создания займа
    v__rec_balance_value RECORD; -- Содержит запись о балансовых величинах из таблицы buh.balance_value
    v__rec_loan RECORD; -- Содержит запись о текущем займе из таблицы loans
	v__last_change_bv_od_loan_date DATE; -- Дата последнего изменения просроченного тела займа согласно графику платежей
    
    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

    v__rec__first_payment_schedule RECORD; -- Признак того, что необходимо при расчете брать первоначальный график платежей, из договора
	*/
    
    v__max_payment_date_plus_60 DATE; --Максимальная дата начисления пени
BEGIN





	-- Получаем данные о текущем займе
	-- -------------------------------------------------------------------------
    SELECT *  FROM asuz_db.loans l
    WHERE l.loan_key = _loan_key
    INTO v__rec_loan;
	-- -------------------------------------------------------------------------
    
    
    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

	-- Получаем настройку - нужно ли брать первоначальны график платежей, 
    -- тот что при заклчении договора
	-- -------------------------------------------------------------------------
    v__rec__first_payment_schedule  = buh.get_calc_first_payment_schedule(_loan_key);
	-- -------------------------------------------------------------------------
	*/

	-- Дата последнего изменения просроченного тела займа согласно графику платежей
    -- -------------------------------------------------------------------------
    SELECT 
    	MAX(bv.transact_date)
    FROM buh.balance_value bv
    WHERE      
        bv.loan_key = _loan_key
        AND
        bv.active = 1
        AND
    	bv.transact_date <= _date
        AND
        bv.od_loan_b != bv.od_loan_e
        AND
        bv.od_loan_e > 0
        AND
        bv.transact_date IN
        (
            SELECT COALESCE(ro.payment_date_ori,t.payment_date)
            FROM asuz_db.repayment_schedules t 
            LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
                on ro.repayment_schedule_key=t.repayment_schedule_key
            WHERE 
            	t.permissible_loan_key = v__rec_loan.permissible_loan_key
        
        /*
	  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

            SELECT t.payment_date
            FROM asuz_db.repayment_schedules t 
            WHERE 
            	t.permissible_loan_key = v__rec_loan.permissible_loan_key
        */
        )
    INTO
		v__last_change_bv_od_loan_date;
    -- -------------------------------------------------------------------------

    
    /*
  	Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

	-- Если стоит настройка "Рассчитать по первому графику платежей на момент выдачи"
    -- тогда расчитываем дату последнего изменения просроченного тела займа согласно первому графику платежей
    -- -------------------------------------------------------------------------
	IF v__rec__first_payment_schedule.result = 1 THEN
        SELECT 
            MAX(bv.transact_date)
        FROM buh.balance_value bv
        WHERE
            bv.loan_key = _loan_key
            AND
	        bv.active = 1
	        AND
            bv.transact_date <= _date
            AND
            bv.od_loan_b != bv.od_loan_e
            AND
            bv.od_loan_e > 0
            AND
            bv.transact_date IN
            (
                SELECT t.payment_date
                FROM asuz_db.tmp_payment_schedules t 
                WHERE 
                    t.application_key = v__rec__first_payment_schedule.application_key
            )
        INTO
            v__last_change_bv_od_loan_date;    
    END IF;
    -- -------------------------------------------------------------------------
    */
    
    
    IF NOT FOUND THEN
	    v__last_change_bv_od_loan_date = v__rec_loan.creation_date;
        
        SELECT MAX(COALESCE(ro.payment_date_ori,t.payment_date))
        FROM asuz_db.repayment_schedules t 
        LEFT JOIN asuz_db.repayment_schedules_payment_date_ori ro 
            on ro.repayment_schedule_key=t.repayment_schedule_key
        WHERE 
            t.permissible_loan_key = v__rec_loan.permissible_loan_key
        INTO
             v__last_change_bv_od_loan_date;
            /*
            Закомментировал согласно задаче №480575  (если задачу примут - в последствии можно удалить)

        	IF v__rec__first_payment_schedule.result = 1 THEN

                SELECT t.payment_date
                FROM asuz_db.tmp_payment_schedules t 
                WHERE 
                    t.application_key = v__rec__first_payment_schedule.application_key
                INTO
                	 v__last_change_bv_od_loan_date;
                     
            ELSE
            
                SELECT MAX(t.payment_date)
                FROM asuz_db.repayment_schedules t 
                WHERE 
                    t.permissible_loan_key = v__rec_loan.permissible_loan_key
                INTO
                	 v__last_change_bv_od_loan_date;
            END IF;
			*/
    END IF;
    
    -- Добавляем к дате последнего платежа 60 дней  
    v__max_payment_date_plus_60 = v__last_change_bv_od_loan_date::DATE + INTERVAL '60 day';
    
    IF _date <= v__max_payment_date_plus_60  THEN
    	result = TRUE;
    	RETURN;
    ELSE
    	result = FALSE;
    	RETURN;
    END IF;

    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function findfuncs (OID = 609154) : 
--
SET search_path = public, pg_catalog;
CREATE FUNCTION public.findfuncs (
  name,
  text
)
RETURNS text[]
AS 
$body$
    SELECT ARRAY(
        SELECT DISTINCT quote_ident(n.nspname) || '.' || quote_ident(p.proname) AS pname
          FROM pg_catalog.pg_proc p
          JOIN pg_catalog.pg_namespace n ON p.pronamespace = n.oid
         WHERE n.nspname = $1
           AND p.proname ~ $2
         ORDER BY pname
    );
$body$
LANGUAGE sql;
--
-- Definition for function get_all_transact_summ (OID = 616051) : 
--
SET search_path = buh, pg_catalog;
CREATE FUNCTION buh.get_all_transact_summ (
  _loan_key integer,
  _account_transaction_keys_csv character varying,
  _max_date date,
  out summ numeric
)
RETURNS numeric
AS 
$body$
/*
<description>
	Функция возвращает сумму всех транзакций (по ключу транзакции) до указанной даты для указаннного займа
</description>
<history_list>
    <item><date_m>10.04.2019</date_m><task_n>466541</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__account_transaction_keys INTEGER[];
BEGIN
	v__account_transaction_keys = string_to_array(_account_transaction_keys_csv,',');
	-- Считаем дебетовую сумму
    -- ============================================================================
    SELECT 
    	sum(l.summ)
    FROM 
    	buh.loan_transact l
    join buh.account_transaction a on a.account_transaction_key=l.account_transaction_key
    where 
        l.loan_key=_loan_key AND 
        a.account_transaction_key = ANY (	v__account_transaction_keys) AND
        l.active=1 AND 
		l.transact_date <= _max_date
    INTO summ;
    summ = COALESCE(summ,0);
    -- ============================================================================
    
    RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Definition for function get_tmp_payment_schedules (OID = 697601) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE FUNCTION loan_calc_history.get_tmp_payment_schedules (
  _loan_key integer,
  out key integer,
  out application_key integer,
  out payment_date date,
  out loan_sum integer,
  out overpayment_sum integer
)
RETURNS SETOF record
AS 
$body$
/*
<description>
	Получение первоначального графика платежей
</description>
<history_list>
	<item><date_m>14.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
	<item><date_m>25.07.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/
DECLARE
	v__application_key integer;
    v__count INTEGER;
    v__rec RECORD; 
BEGIN
   
  
    
    
    SELECT 
         l.application_key
    FROM
        asuz_db.loans l
    WHERE
        l.loan_key = _loan_key
    INTO
        v__application_key;
             
    v__application_key = coalesce(v__application_key,0);
               
    IF NOT FOUND THEN
        RETURN;
    END IF;
        
    
    
	-- Перебираем список 
	FOR v__rec IN
            
    	SELECT 
        	t.*
    	FROM 
        	asuz_db.tmp_payment_schedules t 
    	WHERE 
        	t.application_key = v__application_key
		ORDER BY 
        	t.payment_date ASC

        
    LOOP
        key  = v__rec.key;
        application_key   = v__rec.application_key;
        payment_date  = v__rec.payment_date;
        loan_sum   = v__rec.loan_sum::INTEGER;
        overpayment_sum  = v__rec.overpayment_sum::INTEGER;
        RETURN NEXT;
    END LOOP;

	RETURN;
END;
$body$
LANGUAGE plpgsql
SECURITY DEFINER;
--
-- Structure for table account (OID = 466192) : 
--
SET search_path = buh, pg_catalog;
CREATE TABLE buh.account (
    "aссount_key" serial NOT NULL,
    name character varying
)
WITH (oids = false);
--
-- Structure for table account_code (OID = 466203) : 
--
CREATE TABLE buh.account_code (
    account_code_key serial NOT NULL,
    account_key integer,
    code character varying,
    date_start date,
    date_end date
)
WITH (oids = false);
--
-- Structure for table account_transaction (OID = 466214) : 
--
CREATE TABLE buh.account_transaction (
    account_transaction_key serial NOT NULL,
    account_transaction_1c_key integer,
    name character varying,
    account_key_debet integer,
    account_key_credit integer,
    description character varying,
    "order" integer,
    storno integer DEFAULT 0
)
WITH (oids = false);
--
-- Structure for table account_transaction_1c (OID = 466228) : 
--
CREATE TABLE buh.account_transaction_1c (
    account_transaction_key serial NOT NULL,
    name character varying,
    account_key_debet integer,
    account_key_credit integer,
    description character varying,
    "order" integer,
    storno integer DEFAULT 0
)
WITH (oids = false);
--
-- Structure for table stat_tables_count (OID = 466240) : 
--
CREATE TABLE buh.stat_tables_count (
    stat_tables_count_key serial NOT NULL,
    create_date timestamp without time zone DEFAULT now(),
    user_key integer DEFAULT public.user_key(),
    balance_value_count_b integer,
    balance_value_count_e integer,
    loans_count integer,
    repayment_schedules_count integer,
    permissible_loans_count integer,
    separate_subdivision_count integer,
    loan_transact_count_b integer,
    loan_transact_count_e integer,
    transact_calc_log_count_b integer,
    transact_calc_log_count_e integer
)
WITH (oids = false);
--
-- Structure for table balance_value (OID = 466250) : 
--
CREATE TABLE buh.balance_value (
    balance_value_key serial NOT NULL,
    loan_key integer,
    od_loan_b numeric,
    od_loan_e numeric,
    od_overpayment_planned_b numeric,
    od_overpayment_planned_e numeric,
    od_overpayment_for_delay_b numeric,
    od_overpayment_for_delay_e numeric,
    od_fine_b numeric,
    od_fine_e numeric,
    transact_date date,
    create_date timestamp without time zone DEFAULT now(),
    n_loan_b numeric,
    n_loan_e numeric,
    n_overpayment_planned_b numeric,
    n_overpayment_planned_e numeric,
    loan_b numeric,
    loan_e numeric,
    client_balance_b numeric,
    client_balance_e numeric,
    active integer DEFAULT 1,
    opened boolean DEFAULT true,
    n_overpayment_conditionally_b numeric,
    n_overpayment_conditionally_e numeric,
    all_loan_percents_b numeric,
    all_loan_percents_e numeric
)
WITH (oids = false);
--
-- Structure for table loan_transact (OID = 466265) : 
--
CREATE TABLE buh.loan_transact (
    loan_transact_key serial NOT NULL,
    loan_key integer,
    separate_subdivision_key character varying,
    summ numeric,
    fact_key integer,
    account_transaction_key integer,
    transact_date date,
    create_date timestamp without time zone DEFAULT now(),
    active integer DEFAULT 1
)
WITH (oids = false);
--
-- Structure for table transact_calc_log (OID = 466280) : 
--
CREATE TABLE buh.transact_calc_log (
    transact_calc_log_key serial NOT NULL,
    loan_key integer,
    date date,
    procedure character varying,
    message character varying,
    create_date timestamp without time zone DEFAULT now()
)
WITH (oids = false);
--
-- Structure for table fact_names (OID = 466291) : 
--
SET search_path = facts, pg_catalog;
CREATE TABLE facts.fact_names (
    fact_name_key integer NOT NULL,
    fact_name character varying,
    func_register_fact character varying
)
WITH (oids = false);
--
-- Structure for table facts (OID = 466299) : 
--
CREATE TABLE facts.facts (
    fact_key serial NOT NULL,
    fact_date timestamp(6) without time zone,
    fact_name_key integer,
    obj_type integer,
    obj_key integer,
    loan_key integer,
    value numeric,
    user_key integer,
    subdivision_key integer,
    kod_podr character varying,
    additional_value integer,
    additional_value2 integer,
    creation_date timestamp without time zone DEFAULT (clock_timestamp())::timestamp without time zone NOT NULL
)
WITH (oids = false);
--
-- Structure for table separate_subdivision (OID = 466331) : 
--
SET search_path = public, pg_catalog;
CREATE TABLE public.separate_subdivision (
    saparate_subdivision_key serial NOT NULL,
    separate_subdivision character varying,
    subdivision_key integer,
    date_b date
)
WITH (oids = false);
--
-- Definition for foreign data wrapper postgresql_fdw (OID = 471076) : 
--
CREATE FOREIGN DATA WRAPPER postgresql_fdw
  NO HANDLER
  VALIDATOR postgresql_fdw_validator;
--
-- Definition for foreign data wrapper postgres_fdw (OID = 471077) : 
--
CREATE FOREIGN DATA WRAPPER postgres_fdw
  HANDLER postgres_fdw_handler
  VALIDATOR postgres_fdw_validator;
--
-- Definition for foreign server asuz01 (OID = 471078) : 
--
CREATE SERVER asuz01
  FOREIGN DATA WRAPPER postgres_fdw
  OPTIONS (
    hostaddr '10.255.254.252',
    dbname 'asuz');
--
-- Definition for user mapping collection_db_owner (OID = 471079) : 
--
CREATE USER MAPPING FOR collection_db_owner
  SERVER asuz01
  OPTIONS (
    "user" 'asuz_fdw',
    password 'z5UzTD83yt');
--
-- Structure for table app_data (OID = 471162) : 
--
SET search_path = repair_data, pg_catalog;
CREATE TABLE repair_data.app_data (
    app_data_key serial NOT NULL,
    application_key integer,
    app_creation_date date,
    process_key integer,
    process_status integer,
    was_101 boolean,
    last_101 timestamp without time zone,
    was_113 boolean,
    last_113 timestamp without time zone,
    issued_by_app boolean,
    loan_key integer,
    issued_by_loan boolean,
    possible_loan_key integer,
    has_insurance boolean,
    insurance_status integer,
    subdivision_key integer,
    user_113 integer,
    verification_result integer,
    proceed integer DEFAULT 0,
    new_loan_key integer
)
WITH (oids = false);
--
-- Structure for table app_data_online (OID = 471171) : 
--
CREATE TABLE repair_data.app_data_online (
    app_data_online_key serial NOT NULL,
    application_key integer,
    process_key integer,
    creation_date date,
    account character varying,
    online_possibility integer,
    withdrawal_method integer,
    possible_loan_key integer,
    subdivision_key integer,
    proceed integer DEFAULT 0,
    last_113 timestamp without time zone,
    loan_key integer,
    issued_by_app boolean,
    issued_by_loan boolean
)
WITH (oids = false);
--
-- Structure for table list_recovery_type_2_3 (OID = 471183) : 
--
CREATE TABLE repair_data.list_recovery_type_2_3 (
    list_recovery_key serial NOT NULL,
    loan_key integer,
    repair_type integer,
    status integer
)
WITH (oids = false);
--
-- Structure for table log_errors (OID = 471239) : 
--
SET search_path = logging, pg_catalog;
CREATE TABLE logging.log_errors (
    error_key serial NOT NULL,
    error_text text,
    error_time timestamp without time zone DEFAULT now(),
    error_site character varying
)
WITH (oids = false);
--
-- Definition for user mapping palexander (OID = 471314) : 
--
CREATE USER MAPPING FOR palexander
  SERVER asuz01
  OPTIONS (
    "user" 'asuz_fdw',
    password 'z5UzTD83yt');
--
-- Structure for table loan_version (OID = 471599) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE TABLE loan_calc_history.loan_version (
    loan_version_key serial NOT NULL,
    loan_key integer,
    creation_date timestamp without time zone DEFAULT (clock_timestamp())::timestamp without time zone,
    description character varying,
    nopeni integer DEFAULT 0,
    first_payment_schedule integer DEFAULT 0
)
WITH (oids = false);
--
-- Structure for table balance_value_history (OID = 471611) : 
--
CREATE TABLE loan_calc_history.balance_value_history (
    balance_value_key serial NOT NULL,
    loan_key integer,
    od_loan_b numeric,
    od_loan_e numeric,
    od_overpayment_planned_b numeric,
    od_overpayment_planned_e numeric,
    od_overpayment_for_delay_b numeric,
    od_overpayment_for_delay_e numeric,
    od_fine_b numeric,
    od_fine_e numeric,
    transact_date date,
    create_date timestamp without time zone DEFAULT now(),
    n_loan_b numeric,
    n_loan_e numeric,
    n_overpayment_planned_b numeric,
    n_overpayment_planned_e numeric,
    loan_b numeric,
    loan_e numeric,
    client_balance_b numeric,
    client_balance_e numeric,
    active integer DEFAULT 1,
    opened boolean DEFAULT true,
    n_overpayment_conditionally_b numeric,
    n_overpayment_conditionally_e numeric,
    all_loan_percents_b numeric,
    all_loan_percents_e numeric,
    loan_version_key integer
)
WITH (oids = false);
--
-- Structure for table loan_transact_history (OID = 471626) : 
--
CREATE TABLE loan_calc_history.loan_transact_history (
    loan_transact_key serial NOT NULL,
    loan_key integer,
    separate_subdivision_key character varying,
    summ numeric,
    fact_key integer,
    account_transaction_key integer,
    transact_date date,
    create_date timestamp without time zone DEFAULT now(),
    active integer DEFAULT 1,
    loan_version_key integer
)
WITH (oids = false);
--
-- Structure for table t_borrowers (OID = 472352) : 
--
SET search_path = asuz_db_local, pg_catalog;
CREATE TABLE asuz_db_local.t_borrowers (
    borrower_key integer NOT NULL,
    settlement_key integer NOT NULL,
    regular_client boolean,
    do_not_notify boolean,
    last_name character varying NOT NULL,
    name character varying NOT NULL,
    patronimic character varying,
    birthday date NOT NULL,
    pass_series character varying NOT NULL,
    pass_number character varying NOT NULL,
    pass_issue character varying NOT NULL,
    pass_date date NOT NULL,
    pass_code character varying NOT NULL,
    email character varying,
    reg_city character varying,
    reg_postcode character varying,
    reg_street character varying,
    reg_house character varying,
    reg_housing character varying,
    reg_flat character varying,
    actual_address boolean,
    actual_city character varying,
    actual_postcode character varying,
    actual_street character varying,
    actual_house character varying,
    actual_housing character varying,
    actual_flat character varying,
    martial_status integer,
    spouse_last_name character varying,
    spouse_name character varying,
    spouse_patronimiic character varying,
    spouse_birthday date,
    have_children boolean,
    nmb_of_children integer,
    date_of_divorce date,
    regular_profit numeric NOT NULL,
    pensioner boolean,
    standing integer,
    nmb_of_work integer,
    work character varying,
    work_address character varying,
    work_post character varying,
    work_comment character varying,
    work2 character varying,
    work2_address character varying,
    work2_post character varying,
    work2_comment character varying,
    spouse_regular_profit numeric,
    spouse_works integer,
    spouse_work character varying,
    spouse_work_address character varying,
    guarantor_last_name character varying,
    guarantor_name character varying,
    guarantor_patronimic character varying,
    guarantor_who character varying,
    guarantor2_last_name character varying,
    guarantor2_name character varying,
    guarantor2_patronimic character varying,
    guarantor2_who character varying,
    blacklist boolean,
    blacklist_date date,
    blacklist_comment character varying,
    individual_entrepreneur boolean,
    ie_ogrn character varying,
    ie_inn character varying,
    ie_comment text,
    discount_enabled boolean,
    discount_card_type integer,
    discount_card_number character varying,
    collector_comment text,
    user_name character varying DEFAULT "session_user"(),
    creation_date timestamp without time zone DEFAULT now(),
    ie_business_state boolean,
    employment_history boolean,
    discount_recommended_borrower_card_number character varying,
    gender character varying(1),
    regular_profit_unofficial numeric,
    source_information integer,
    blacklist_external boolean DEFAULT false,
    in_blacklist_before_date date,
    subdivision_key integer,
    reg_city_key bigint,
    actual_city_key bigint,
    collector_address integer,
    snils character varying,
    snils_check_mode integer DEFAULT 0,
    birthplace character varying
)
WITH (oids = false);
--
-- Structure for table t_subdivisions (OID = 472362) : 
--
CREATE TABLE asuz_db_local.t_subdivisions (
    subdivision_key integer NOT NULL,
    subdivision_name character varying,
    subdivision_type_key integer,
    subdivision_owner_key integer,
    dependent_subdivisions integer[],
    minioffice boolean DEFAULT false,
    included_in_service_office integer
)
WITH (oids = false);
--
-- Structure for table credit_cash_orders (OID = 472369) : 
--
CREATE TABLE asuz_db_local.credit_cash_orders (
    order_key integer NOT NULL,
    order_number character varying,
    order_type integer,
    loan_key integer NOT NULL,
    payment_sum numeric NOT NULL,
    borrower_name character varying NOT NULL,
    payment_date timestamp without time zone DEFAULT now() NOT NULL,
    user_name character varying DEFAULT "session_user"() NOT NULL,
    fixed boolean DEFAULT false,
    separate_subdivision_key character varying,
    subdivision_key integer,
    face3d character varying,
    discount numeric
)
WITH (oids = false);
--
-- Structure for table timelog (OID = 472402) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE TABLE loan_calc_history.timelog (
    key serial NOT NULL,
    description character varying,
    startcalc timestamp without time zone,
    endcalc timestamp without time zone,
    timecalc interval
)
WITH (oids = false);
--
-- Structure for table loan_percent (OID = 472556) : 
--
SET search_path = buh, pg_catalog;
CREATE TABLE buh.loan_percent (
    loan_percent_key serial NOT NULL,
    month integer,
    value numeric,
    days_min integer,
    days_max integer,
    year integer
)
WITH (oids = false);
--
-- Structure for table progress (OID = 472916) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE TABLE loan_calc_history.progress (
    progress_key serial NOT NULL,
    loan_version_key integer,
    total_days integer,
    current_day integer,
    creation_date timestamp without time zone DEFAULT clock_timestamp()
)
WITH (oids = false);
--
-- Definition for sequence test (OID = 472974) : 
--
SET search_path = public, pg_catalog;
CREATE SEQUENCE public.test
    START WITH 2147483647
    INCREMENT BY -1
    MAXVALUE 2147483647
    MINVALUE -9223372036854775807
    CACHE 1;
--
-- Definition for sequence serial (OID = 472985) : 
--
CREATE SEQUENCE public.serial
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence zozo (OID = 473072) : 
--
CREATE SEQUENCE public.zozo
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence facts_fact_key_seq (OID = 474537) : 
--
CREATE SEQUENCE public.facts_fact_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence list_participants_key_seq (OID = 474554) : 
--
CREATE SEQUENCE public.list_participants_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Structure for table list_participants_register (OID = 474556) : 
--
CREATE TABLE public.list_participants_register (
    list_key integer DEFAULT nextval('register_list_participants_list_key_seq'::regclass) NOT NULL,
    description character varying,
    closed_for_processing boolean,
    type_of_action integer,
    period_b date,
    period_e date
)
WITH (oids = false);
--
-- Structure for table special_offers_types (OID = 474564) : 
--
CREATE TABLE public.special_offers_types (
    special_offer_type_key integer,
    special_offer_name character varying,
    special_offer_description character varying,
    start_date date,
    end_date date,
    procedure_name character varying,
    id integer,
    only_new_client boolean,
    min_term_of_loan integer,
    max_term_of_loan integer,
    direct_discount boolean DEFAULT false,
    offer_category integer
)
WITH (oids = false);
--
-- Structure for table loan_restructuring_history (OID = 474587) : 
--
CREATE TABLE public.loan_restructuring_history (
    agreement_key integer,
    loan_key integer,
    agreement_date date,
    agreement_number integer,
    loan_sum numeric,
    loan_overpayment_original numeric,
    loan_overpayment numeric,
    loan_rate numeric,
    loan_term numeric,
    loan_returns numeric,
    nmb_of_payments integer,
    tariff_key integer,
    credit_product_key integer,
    discount_unused numeric,
    discount_compensation numeric,
    discount_special_offers numeric,
    discount_amount_overdue_fine numeric,
    discount_amount_overdue_overpayment numeric,
    discount_amount numeric,
    discount_amount_rs numeric,
    discount_amount_payment numeric,
    overdue_loand numeric,
    overdue_overpayment numeric,
    overdue_penalty numeric,
    overdue_overpayment_original numeric,
    overdue_penalty_original numeric,
    current_loan numeric,
    current_overpayment numeric,
    current_overpayment_original numeric,
    total_payment numeric,
    total_payment_original numeric,
    taken_money numeric,
    order_key integer,
    b_process_key integer,
    spec_offer_key integer,
    user_name character varying,
    permissible_loan_key integer,
    prepaid_loan numeric,
    prepaid_overpayment numeric,
    discount_loalty_perc numeric,
    new_sum_payment numeric,
    type_special_restructuring integer,
    fixed_payment_day integer,
    new_permissible_loan_key integer,
    payment_period interval,
    overdue_overpayment_for_delay_original numeric,
    overdue_overpayment_for_delay numeric,
    discount_amount_overdue_overpayment_for_delay numeric,
    rate_rs numeric,
    type_repayment_schedule_key integer,
    psk numeric,
    spec_offer_sum_paym numeric
)
WITH (oids = false);
--
-- Definition for foreign table special_offers (OID = 482618) : 
--
SET search_path = asuz_db, pg_catalog;
CREATE FOREIGN TABLE asuz_db.special_offers (
    spec_offer_key integer NOT NULL,
    regist_restr_key integer,
    latest_date_for_payment date,
    contract character varying,
    overpayment_to_pay numeric,
    fine_to_pay numeric,
    used boolean,
    loan_key integer,
    redmine_task character varying,
    process_name_key integer,
    start_date date
)
SERVER asuz01
OPTIONS (
  schema_name 'public',
  table_name 'special_offers');
--
-- Structure for table lock_calculation (OID = 586998) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE TABLE loan_calc_history.lock_calculation (
    lock_calculation_key serial NOT NULL,
    loan_key integer,
    create_date timestamp without time zone DEFAULT clock_timestamp()
)
WITH (oids = false);
--
-- Structure for table tmp_payment_schedules (OID = 588921) : 
--
SET search_path = asuz_db, pg_catalog;
CREATE TABLE asuz_db.tmp_payment_schedules (
    key serial NOT NULL,
    application_key integer,
    payment_date date,
    loan_sum integer,
    overpayment_sum integer
)
WITH (oids = false);
--
-- Structure for table current_calc (OID = 588974) : 
--
SET search_path = buh, pg_catalog;
CREATE TABLE buh.current_calc (
    current_calc_key serial NOT NULL,
    loan_key integer,
    loan_version_key integer,
    create_date timestamp without time zone DEFAULT clock_timestamp()
)
WITH (oids = false);
--
-- Structure for table t_xmin_usage (OID = 591871) : 
--
SET search_path = dwh, pg_catalog;
CREATE TABLE dwh.t_xmin_usage (
    name_table character varying(255),
    max_xmin bigint,
    mdate timestamp without time zone,
    type_id character varying(10)
)
WITH (oids = false);
--
-- Structure for table repayment_schedules_payment_date_ori (OID = 609142) : 
--
SET search_path = asuz_db, pg_catalog;
CREATE TABLE asuz_db.repayment_schedules_payment_date_ori (
    repayment_schedule_key integer,
    payment_date_ori date
)
WITH (oids = false);
--
-- Definition for sequence borrowers_borrower_key_seq (OID = 792391) : 
--
CREATE SEQUENCE asuz_db.borrowers_borrower_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence repayment_schedules_repayment_schedule_key_seq (OID = 792393) : 
--
CREATE SEQUENCE asuz_db.repayment_schedules_repayment_schedule_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence loans_loan_key_seq (OID = 792395) : 
--
CREATE SEQUENCE asuz_db.loans_loan_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence loans_manual_correction_lmc_key_seq (OID = 792397) : 
--
CREATE SEQUENCE asuz_db.loans_manual_correction_lmc_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence credit_cash_orders_order_key_seq (OID = 792399) : 
--
CREATE SEQUENCE asuz_db.credit_cash_orders_order_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence payment_payment_key_seq (OID = 792401) : 
--
CREATE SEQUENCE asuz_db.payment_payment_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence payment_part_payment_part_key_seq (OID = 792403) : 
--
CREATE SEQUENCE asuz_db.payment_part_payment_part_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence loans_manual_correction_rs_lmc_rs_key_seq (OID = 792405) : 
--
CREATE SEQUENCE asuz_db.loans_manual_correction_rs_lmc_rs_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence permissible_loans_key_seq (OID = 792407) : 
--
CREATE SEQUENCE asuz_db.permissible_loans_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence setting_name_setting_key_seq (OID = 792409) : 
--
CREATE SEQUENCE asuz_db.setting_name_setting_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence setting_values_setting_value_key_seq (OID = 792411) : 
--
CREATE SEQUENCE asuz_db.setting_values_setting_value_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence subdivisions_subdivision_key_seq (OID = 792413) : 
--
CREATE SEQUENCE asuz_db.subdivisions_subdivision_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for sequence partial_early_agreement_partial_early_agreement_key_seq (OID = 792415) : 
--
CREATE SEQUENCE asuz_db.partial_early_agreement_partial_early_agreement_key_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Structure for table t_borrowers (OID = 792425) : 
--
CREATE TABLE asuz_db.t_borrowers (
    borrower_key integer DEFAULT nextval('borrowers_borrower_key_seq'::regclass) NOT NULL,
    settlement_key integer NOT NULL,
    regular_client boolean,
    do_not_notify boolean,
    last_name character varying NOT NULL,
    name character varying NOT NULL,
    patronimic character varying,
    birthday date NOT NULL,
    pass_series character varying NOT NULL,
    pass_number character varying NOT NULL,
    pass_issue character varying NOT NULL,
    pass_date date NOT NULL,
    pass_code character varying NOT NULL,
    email character varying,
    reg_city character varying,
    reg_postcode character varying,
    reg_street character varying,
    reg_house character varying,
    reg_housing character varying,
    reg_flat character varying,
    actual_address boolean,
    actual_city character varying,
    actual_postcode character varying,
    actual_street character varying,
    actual_house character varying,
    actual_housing character varying,
    actual_flat character varying,
    martial_status integer,
    spouse_last_name character varying,
    spouse_name character varying,
    spouse_patronimiic character varying,
    spouse_birthday date,
    have_children boolean,
    nmb_of_children integer,
    date_of_divorce date,
    regular_profit numeric NOT NULL,
    pensioner boolean,
    standing integer,
    nmb_of_work integer,
    work character varying,
    work_address character varying,
    work_post character varying,
    work_comment character varying,
    work2 character varying,
    work2_address character varying,
    work2_post character varying,
    work2_comment character varying,
    spouse_regular_profit numeric,
    spouse_works integer,
    spouse_work character varying,
    spouse_work_address character varying,
    guarantor_last_name character varying,
    guarantor_name character varying,
    guarantor_patronimic character varying,
    guarantor_who character varying,
    guarantor2_last_name character varying,
    guarantor2_name character varying,
    guarantor2_patronimic character varying,
    guarantor2_who character varying,
    blacklist boolean,
    blacklist_date date,
    blacklist_comment character varying,
    individual_entrepreneur boolean,
    ie_ogrn character varying,
    ie_inn character varying,
    ie_comment text,
    discount_enabled boolean,
    discount_card_type integer,
    discount_card_number character varying,
    collector_comment text,
    user_name character varying DEFAULT "session_user"(),
    creation_date timestamp without time zone DEFAULT now(),
    ie_business_state boolean,
    employment_history boolean,
    discount_recommended_borrower_card_number character varying,
    gender character varying(1),
    regular_profit_unofficial numeric,
    source_information integer,
    blacklist_external boolean DEFAULT false,
    in_blacklist_before_date date,
    subdivision_key integer,
    reg_city_key bigint,
    actual_city_key bigint,
    collector_address integer,
    snils character varying,
    snils_check_mode integer DEFAULT 0,
    birthplace character varying
)
WITH (oids = false);
--
-- Structure for table repayment_schedules (OID = 792436) : 
--
CREATE TABLE asuz_db.repayment_schedules (
    repayment_schedule_key integer DEFAULT nextval('repayment_schedules_repayment_schedule_key_seq'::regclass) NOT NULL,
    loan_key integer,
    payment_date date,
    loan_sum numeric,
    overpayment_sum numeric,
    delay boolean,
    debtor boolean,
    returned boolean,
    permissible_loan_key integer,
    returned_date date
)
WITH (oids = false);
--
-- Structure for table loans (OID = 792443) : 
--
CREATE TABLE asuz_db.loans (
    loan_key integer DEFAULT nextval('loans_loan_key_seq'::regclass) NOT NULL,
    borrower_key integer NOT NULL,
    for_what_purpose character varying,
    cause_of_failure character varying,
    priority_1 character varying,
    priority_2 character varying,
    priority_3 character varying,
    requested_amount numeric,
    requested_term integer,
    second_loan boolean,
    type_credit_product_key integer,
    test_pass_age boolean,
    test_pass_citizenship boolean,
    test_pass_registration boolean,
    test_pass_martial_status boolean,
    test_pass_nmb_of_children boolean,
    test_state_business integer,
    test_phone_mobile_phone boolean,
    test_phone_mobile_phone_spouse boolean,
    test_phone_third_person boolean,
    test_adds_call_home_phone boolean,
    test_adds_call_spouse boolean,
    test_adds_call_third_person boolean,
    test_fixed_income_call_work boolean,
    test_fixed_income_call_spouse boolean,
    test_fixed_income_call_third_person boolean,
    test_fixed_income_proof_of_income boolean,
    test_fixed_income_business_card boolean,
    test_fixed_income_pass boolean,
    test_fixed_income_civil_servant boolean,
    test_fixed_income_pension_certificate boolean,
    test_fixed_income_savings_book boolean,
    test_revenue_proof_of_income boolean,
    test_revenue_civil_servant boolean,
    test_revenue_call_spouse boolean,
    test_revenue_cash_book boolean,
    test_revenue_registration_vehicle boolean,
    test_revenue_pension_certificate boolean,
    test_revenue_savings_book boolean,
    test_revenue_declaration boolean,
    face_control integer,
    health integer,
    resolution integer,
    resolution_description text,
    loan_term integer,
    loan_sum numeric,
    loan_rate numeric,
    loan_plan_overpayment numeric,
    loan_plan_to_returns numeric,
    loan_fine_rate numeric,
    loan_penalty numeric,
    loan_maximum_delay integer,
    approved_user boolean,
    additional_confirmation boolean,
    additional_user character varying,
    approved_system integer,
    contract character varying,
    principial_contract integer,
    creation_date date DEFAULT ('now'::text)::date,
    printing_documents integer,
    issued boolean,
    delay boolean,
    debtor boolean,
    returned boolean,
    canceled boolean,
    canceled_decription character varying,
    user_name character varying DEFAULT "session_user"(),
    date_work timestamp without time zone DEFAULT now(),
    permissible_loan_key integer,
    agreed boolean,
    additional_agreement integer,
    requested_period integer,
    non_cash integer,
    calc_date timestamp without time zone,
    calc_user_name character varying,
    pre_agreed integer,
    test_utility_bills boolean,
    test_no_debt_another_organization boolean,
    loyalty_programm boolean,
    subdivision_key integer,
    test_adds_paid_receipt_terminal boolean DEFAULT false,
    reject_reason integer,
    test_revenue_osago boolean,
    cft_card_key integer,
    test_birth_certificate boolean DEFAULT false,
    test_valid_foreign_passport boolean DEFAULT false,
    test_house_book boolean DEFAULT false,
    test_statement_of_salary_account boolean DEFAULT false,
    cancel_reason character varying,
    restructuring integer,
    early_repayment boolean,
    application_key integer,
    closed_date date,
    debt_relieft_type integer DEFAULT 0,
    current_dpd integer DEFAULT 0,
    calc_stop_date date,
    calc_stop_reason character varying,
    claim_right integer,
    refinance integer
)
WITH (oids = false);
--
-- Structure for table loans_manual_correction (OID = 792460) : 
--
CREATE TABLE asuz_db.loans_manual_correction (
    lmc_key integer DEFAULT nextval('loans_manual_correction_lmc_key_seq'::regclass) NOT NULL,
    sum_cor_loan numeric,
    sum_cor_fine numeric,
    sum_cor_penalty numeric,
    loan_key integer,
    sum_cor_overpayment numeric,
    creation_date timestamp without time zone,
    comment character varying,
    user_name character varying,
    cco_key integer,
    backdated integer,
    reason integer,
    correction_type integer,
    sum_cor_overpayment_for_delay numeric
)
WITH (oids = false);
--
-- Structure for table register_records (OID = 792467) : 
--
CREATE TABLE asuz_db.register_records (
    register_records_key serial NOT NULL,
    register_key integer,
    payment_sum numeric,
    payment_date date,
    payer character varying,
    contract_number character varying,
    loan_key integer,
    date_work timestamp without time zone DEFAULT now(),
    user_name character varying DEFAULT "session_user"(),
    processed integer,
    fee numeric
)
WITH (oids = false);
--
-- Structure for table credit_cash_orders (OID = 792475) : 
--
CREATE TABLE asuz_db.credit_cash_orders (
    order_key integer DEFAULT nextval('credit_cash_orders_order_key_seq'::regclass) NOT NULL,
    order_number character varying,
    order_type integer,
    loan_key integer NOT NULL,
    payment_sum numeric NOT NULL,
    borrower_name character varying NOT NULL,
    payment_date timestamp without time zone DEFAULT now() NOT NULL,
    user_name character varying DEFAULT "session_user"() NOT NULL,
    fixed boolean DEFAULT false,
    separate_subdivision_key character varying,
    subdivision_key integer,
    face3d character varying,
    discount numeric
)
WITH (oids = false);
--
-- Structure for table payment (OID = 792485) : 
--
CREATE TABLE asuz_db.payment (
    payment_key integer DEFAULT nextval('payment_payment_key_seq'::regclass) NOT NULL,
    payment_date date,
    nmb_parts integer,
    payment_sum numeric,
    fixed boolean,
    loan_key integer,
    discount_reserved numeric,
    required_date date,
    date_changes date
)
WITH (oids = false);
--
-- Structure for table payment_part (OID = 792492) : 
--
CREATE TABLE asuz_db.payment_part (
    payment_part_key integer DEFAULT nextval('payment_part_payment_part_key_seq'::regclass) NOT NULL,
    payment_key integer,
    document_key integer
)
WITH (oids = false);
--
-- Structure for table facts (OID = 792496) : 
--
CREATE TABLE asuz_db.facts (
    fact_key serial NOT NULL,
    fact_date timestamp(6) without time zone,
    fact_name_key integer,
    obj_type integer,
    obj_key integer,
    loan_key integer,
    value numeric,
    user_key integer,
    subdivision_key integer,
    kod_podr character varying,
    additional_value integer,
    additional_value2 integer,
    creation_date timestamp without time zone DEFAULT (clock_timestamp())::timestamp without time zone NOT NULL
)
WITH (oids = false);
--
-- Structure for table loan_restructuring_history (OID = 792503) : 
--
CREATE TABLE asuz_db.loan_restructuring_history (
    agreement_key integer,
    loan_key integer,
    agreement_date date,
    agreement_number integer,
    loan_sum numeric,
    loan_overpayment_original numeric,
    loan_overpayment numeric,
    loan_rate numeric,
    loan_term numeric,
    loan_returns numeric,
    nmb_of_payments integer,
    tariff_key integer,
    credit_product_key integer,
    discount_unused numeric,
    discount_compensation numeric,
    discount_special_offers numeric,
    discount_amount_overdue_fine numeric,
    discount_amount_overdue_overpayment numeric,
    discount_amount numeric,
    discount_amount_rs numeric,
    discount_amount_payment numeric,
    overdue_loand numeric,
    overdue_overpayment numeric,
    overdue_penalty numeric,
    overdue_overpayment_original numeric,
    overdue_penalty_original numeric,
    current_loan numeric,
    current_overpayment numeric,
    current_overpayment_original numeric,
    total_payment numeric,
    total_payment_original numeric,
    taken_money numeric,
    order_key integer,
    b_process_key integer,
    spec_offer_key integer,
    user_name character varying,
    permissible_loan_key integer,
    prepaid_loan numeric,
    prepaid_overpayment numeric,
    discount_loalty_perc numeric,
    new_sum_payment numeric,
    type_special_restructuring integer,
    fixed_payment_day integer,
    new_permissible_loan_key integer,
    payment_period interval,
    overdue_overpayment_for_delay_original numeric,
    overdue_overpayment_for_delay numeric,
    discount_amount_overdue_overpayment_for_delay numeric,
    rate_rs numeric,
    type_repayment_schedule_key integer,
    psk numeric,
    spec_offer_sum_paym numeric
)
WITH (oids = false);
--
-- Structure for table loans_manual_correction_rs (OID = 792509) : 
--
CREATE TABLE asuz_db.loans_manual_correction_rs (
    lmc_rs_key integer DEFAULT nextval('loans_manual_correction_rs_lmc_rs_key_seq'::regclass) NOT NULL,
    lmc_key integer,
    repayment_schedule_key integer,
    sum_cor_loan numeric,
    sum_cor_overpayment numeric,
    sum_cor_fine numeric,
    sum_cor_penalty numeric,
    date_distribution date,
    date_accounting date,
    loan_key integer,
    reverse_object_key integer DEFAULT 0,
    sum_cor_overpayment_for_delay numeric
)
WITH (oids = false);
--
-- Structure for table permissible_loans (OID = 792517) : 
--
CREATE TABLE asuz_db.permissible_loans (
    key integer DEFAULT nextval('permissible_loans_key_seq'::regclass) NOT NULL,
    loan_key integer,
    tariff_key integer,
    period_key integer,
    period integer,
    max_amount numeric,
    rate numeric,
    refunds numeric,
    overpayment numeric,
    nmb_of_payments integer,
    sum_payment numeric,
    bonus numeric(8,2),
    tuning integer,
    discount_amount numeric,
    discount_loalty_perc numeric,
    fixed_payment_day integer,
    rate_rs numeric,
    type_repayment_schedule_key integer,
    version_number integer,
    version_date date,
    version_type integer,
    psk_percent numeric(8,3)
)
WITH (oids = false);
--
-- Structure for table setting_name (OID = 792524) : 
--
CREATE TABLE asuz_db.setting_name (
    setting_key integer DEFAULT nextval('setting_name_setting_key_seq'::regclass) NOT NULL,
    setting_name character varying,
    setting_data_type character varying,
    setting_comment character varying
)
WITH (oids = false);
--
-- Structure for table setting_values (OID = 792531) : 
--
CREATE TABLE asuz_db.setting_values (
    setting_value_key integer DEFAULT nextval('setting_values_setting_value_key_seq'::regclass) NOT NULL,
    setting_name_key integer,
    subdivision_key integer,
    value character varying,
    date_b date DEFAULT ('now'::text)::date,
    user_name character varying DEFAULT "session_user"(),
    date_work timestamp without time zone DEFAULT now()
)
WITH (oids = false);
--
-- Structure for table t_subdivisions (OID = 792541) : 
--
CREATE TABLE asuz_db.t_subdivisions (
    subdivision_key integer DEFAULT nextval('subdivisions_subdivision_key_seq'::regclass) NOT NULL,
    subdivision_name character varying,
    subdivision_type_key integer,
    subdivision_owner_key integer,
    dependent_subdivisions integer[],
    minioffice boolean DEFAULT false,
    included_in_service_office integer
)
WITH (oids = false);
--
-- Structure for table partial_early_agreement (OID = 792549) : 
--
CREATE TABLE asuz_db.partial_early_agreement (
    partial_early_agreement_key integer DEFAULT nextval('partial_early_agreement_partial_early_agreement_key_seq'::regclass) NOT NULL,
    loan_key integer,
    partial_early_date date,
    old_permissible_loan_key integer,
    start_date_new_repayment_schedule date,
    agreed_payment_sum numeric,
    payment_date date[],
    payment_sum integer[],
    payment_key integer[],
    status integer,
    fixed_date date,
    cancelled boolean DEFAULT false
)
WITH (oids = false);
--
-- Structure for table list_participants (OID = 792557) : 
--
CREATE TABLE asuz_db.list_participants (
    key serial NOT NULL,
    list_key integer,
    special_offer_type_key integer,
    loan_key integer,
    to_pay_overpayment numeric,
    to_pat_total numeric,
    to_pay_overpayment_actual numeric,
    to_pay_total_actual numeric,
    discount numeric,
    period_b date,
    period_e date,
    paid boolean,
    created_special boolean,
    to_pay_loan numeric,
    to_pay_fine numeric,
    to_pay_loan_actual numeric,
    to_pay_fine_actual numeric
)
WITH (oids = false);
--
-- Structure for table list_participants_register (OID = 792563) : 
--
CREATE TABLE asuz_db.list_participants_register (
    list_key integer DEFAULT nextval('register_list_participants_list_key_seq'::regclass) NOT NULL,
    description character varying,
    closed_for_processing boolean,
    type_of_action integer,
    period_b date,
    period_e date
)
WITH (oids = false);
--
-- Structure for table special_offers_types (OID = 792569) : 
--
CREATE TABLE asuz_db.special_offers_types (
    special_offer_type_key integer,
    special_offer_name character varying,
    special_offer_description character varying,
    start_date date,
    end_date date,
    procedure_name character varying,
    id integer,
    only_new_client boolean,
    min_term_of_loan integer,
    max_term_of_loan integer,
    direct_discount boolean DEFAULT false,
    offer_category integer
)
WITH (oids = false);
--
-- Definition for sequence sequence5e99e796d5589 (OID = 2916174) : 
--
SET search_path = public, pg_catalog;
CREATE SEQUENCE public.sequence5e99e796d5589
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Definition for index idx__buh_account_transaction__account_key_credit__1 (OID = 466224) : 
--
SET search_path = buh, pg_catalog;
CREATE INDEX idx__buh_account_transaction__account_key_credit__1 ON account_transaction USING btree (account_key_credit);
--
-- Definition for index idx__buh_account_transaction__account_key_debet__1 (OID = 466225) : 
--
CREATE INDEX idx__buh_account_transaction__account_key_debet__1 ON account_transaction USING btree (account_key_debet);
--
-- Definition for index idx__buh_balance_value__loan_key__transact_date__opened__1 (OID = 466262) : 
--
CREATE INDEX idx__buh_balance_value__loan_key__transact_date__opened__1 ON balance_value USING btree (loan_key, transact_date, opened);
--
-- Definition for index idx__buh_loan_transact__account_transaction_key__1 (OID = 466276) : 
--
CREATE INDEX idx__buh_loan_transact__account_transaction_key__1 ON loan_transact USING btree (account_transaction_key);
--
-- Definition for index idx__buh_loan_transact__loan_key__transact_datee__fact_key__1 (OID = 466277) : 
--
CREATE INDEX idx__buh_loan_transact__loan_key__transact_datee__fact_key__1 ON loan_transact USING btree (loan_key, transact_date, fact_key);
--
-- Definition for index documents_idx (OID = 466307) : 
--
SET search_path = facts, pg_catalog;
CREATE INDEX documents_idx ON facts USING btree (fact_name_key, obj_type, obj_key);
--
-- Definition for index documents_idx2 (OID = 466308) : 
--
CREATE INDEX documents_idx2 ON facts USING btree (loan_key, fact_date, fact_name_key);
--
-- Definition for index idx__public_separate_subdivision__date_b__1 (OID = 466340) : 
--
SET search_path = public, pg_catalog;
CREATE INDEX idx__public_separate_subdivision__date_b__1 ON separate_subdivision USING btree (date_b);
--
-- Definition for index idx__public_separate_subdivision__separate_subdivision__1 (OID = 466341) : 
--
CREATE INDEX idx__public_separate_subdivision__separate_subdivision__1 ON separate_subdivision USING btree (separate_subdivision);
--
-- Definition for index idx__public_separate_subdivision__subdivision_key__1 (OID = 466342) : 
--
CREATE INDEX idx__public_separate_subdivision__subdivision_key__1 ON separate_subdivision USING btree (subdivision_key);
--
-- Definition for index balance_value_history_idx__buh_balance_value__loan_key__transac (OID = 471623) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE INDEX balance_value_history_idx__buh_balance_value__loan_key__transac ON balance_value_history USING btree (loan_key, transact_date, opened);
--
-- Definition for index loan_transact_history_idx__buh_loan_transact__account_transacti (OID = 471637) : 
--
CREATE INDEX loan_transact_history_idx__buh_loan_transact__account_transacti ON loan_transact_history USING btree (account_transaction_key);
--
-- Definition for index loan_transact_history_idx__buh_loan_transact__loan_key__transac (OID = 471638) : 
--
CREATE INDEX loan_transact_history_idx__buh_loan_transact__loan_key__transac ON loan_transact_history USING btree (loan_key, transact_date, fact_key);
--
-- Definition for index balance_value_history_idx (OID = 578594) : 
--
CREATE INDEX balance_value_history_idx ON balance_value_history USING btree (loan_version_key);
--
-- Definition for index loan_transact_history_idx (OID = 579032) : 
--
CREATE INDEX loan_transact_history_idx ON loan_transact_history USING btree (loan_version_key);
--
-- Definition for index loan_transact_history_idx5 (OID = 579033) : 
--
CREATE INDEX loan_transact_history_idx5 ON loan_transact_history USING btree (loan_version_key, transact_date, active, account_transaction_key);
--
-- Definition for index asuz_db_tmp_payment_schedules_idx1 (OID = 588927) : 
--
SET search_path = asuz_db, pg_catalog;
CREATE INDEX asuz_db_tmp_payment_schedules_idx1 ON tmp_payment_schedules USING btree (application_key);
--
-- Definition for index repayment_schedules_payment_date_ori_idx (OID = 609145) : 
--
CREATE INDEX repayment_schedules_payment_date_ori_idx ON repayment_schedules_payment_date_ori USING btree (repayment_schedule_key);
--
-- Definition for index repayment_schedules_payment_date_ori_idx1 (OID = 609603) : 
--
CREATE INDEX repayment_schedules_payment_date_ori_idx1 ON repayment_schedules_payment_date_ori USING btree (payment_date_ori);
--
-- Definition for index repayment_schedules_payment_date_ori_idx2 (OID = 609605) : 
--
CREATE INDEX repayment_schedules_payment_date_ori_idx2 ON repayment_schedules_payment_date_ori USING btree (repayment_schedule_key, payment_date_ori);
--
-- Definition for index documents_idx (OID = 792702) : 
--
CREATE INDEX documents_idx ON facts USING btree (fact_name_key, obj_type, obj_key);
--
-- Definition for index documents_idx2 (OID = 792704) : 
--
CREATE INDEX documents_idx2 ON facts USING btree (loan_key, fact_date, fact_name_key);
--
-- Definition for index documents_idx3 (OID = 792705) : 
--
CREATE INDEX documents_idx3 ON facts USING btree (fact_key);
--
-- Definition for index payment_idx (OID = 792706) : 
--
CREATE INDEX payment_idx ON payment USING btree (loan_key, payment_date);
--
-- Definition for index payment_idx1 (OID = 792707) : 
--
CREATE INDEX payment_idx1 ON payment USING btree (payment_key);
--
-- Definition for index payment_part_idx (OID = 792708) : 
--
CREATE INDEX payment_part_idx ON payment_part USING btree (document_key);
--
-- Definition for index payment_part_idx1 (OID = 792709) : 
--
CREATE INDEX payment_part_idx1 ON payment_part USING btree (payment_key);
--
-- Definition for index credit_cash_orders_idx (OID = 792710) : 
--
CREATE INDEX credit_cash_orders_idx ON credit_cash_orders USING btree (order_type, subdivision_key);
--
-- Definition for index credit_cash_orders_idx1 (OID = 792711) : 
--
CREATE INDEX credit_cash_orders_idx1 ON credit_cash_orders USING btree (loan_key);
--
-- Definition for index credit_cash_orders_sep_sd_key_idx (OID = 792712) : 
--
CREATE INDEX credit_cash_orders_sep_sd_key_idx ON credit_cash_orders USING btree (separate_subdivision_key);
--
-- Definition for index loan_restructuring_history_idx (OID = 792719) : 
--
CREATE INDEX loan_restructuring_history_idx ON loan_restructuring_history USING btree (loan_key);
--
-- Definition for index loan_restructuring_history_idx2 (OID = 792720) : 
--
CREATE INDEX loan_restructuring_history_idx2 ON loan_restructuring_history USING btree (agreement_key);
--
-- Definition for index loans_idx (OID = 792721) : 
--
CREATE INDEX loans_idx ON loans USING btree (borrower_key);
--
-- Definition for index loans_idx1 (OID = 792722) : 
--
CREATE INDEX loans_idx1 ON loans USING btree (creation_date);
--
-- Definition for index loans_idx2 (OID = 792723) : 
--
CREATE INDEX loans_idx2 ON loans USING btree (contract varchar_pattern_ops);
--
-- Definition for index loans_idx_3 (OID = 792724) : 
--
CREATE INDEX loans_idx_3 ON loans USING btree (loan_key, subdivision_key, current_dpd);
--
-- Definition for index loans_idx_4 (OID = 792725) : 
--
CREATE INDEX loans_idx_4 ON loans USING btree (subdivision_key, issued, (COALESCE(returned, false)), current_dpd);
--
-- Definition for index loans_idx_5 (OID = 792727) : 
--
CREATE INDEX loans_idx_5 ON loans USING btree (permissible_loan_key);
--
-- Definition for index loans_idx_application (OID = 792728) : 
--
CREATE INDEX loans_idx_application ON loans USING btree (application_key);
--
-- Definition for index loans_manual_correction_idx (OID = 792729) : 
--
CREATE INDEX loans_manual_correction_idx ON loans_manual_correction USING btree (lmc_key);
--
-- Definition for index loans_manual_correction_idx1 (OID = 792730) : 
--
CREATE INDEX loans_manual_correction_idx1 ON loans_manual_correction USING btree (cco_key, reason);
--
-- Definition for index loans_manual_correction_idx2 (OID = 792731) : 
--
CREATE INDEX loans_manual_correction_idx2 ON loans_manual_correction USING btree (loan_key);
--
-- Definition for index loans_manual_correction_rs_idx (OID = 792732) : 
--
CREATE INDEX loans_manual_correction_rs_idx ON loans_manual_correction_rs USING btree (lmc_key);
--
-- Definition for index loans_manual_correction_rs_idx1 (OID = 792733) : 
--
CREATE INDEX loans_manual_correction_rs_idx1 ON loans_manual_correction_rs USING btree (repayment_schedule_key);
--
-- Definition for index loans_manual_correction_rs_idx2 (OID = 792734) : 
--
CREATE INDEX loans_manual_correction_rs_idx2 ON loans_manual_correction_rs USING btree (loan_key);
--
-- Definition for index loans_manual_correction_rs_idx3 (OID = 792735) : 
--
CREATE INDEX loans_manual_correction_rs_idx3 ON loans_manual_correction_rs USING btree (reverse_object_key);
--
-- Definition for index loans_manual_correction_rs_idx4 (OID = 792736) : 
--
CREATE INDEX loans_manual_correction_rs_idx4 ON loans_manual_correction_rs USING btree (lmc_rs_key);
--
-- Definition for index permissible_loans_loan_key_idx (OID = 792737) : 
--
CREATE INDEX permissible_loans_loan_key_idx ON permissible_loans USING btree (loan_key);
--
-- Definition for index permissible_loans_type_repayment_schedule_key_idx (OID = 792738) : 
--
CREATE INDEX permissible_loans_type_repayment_schedule_key_idx ON permissible_loans USING btree (type_repayment_schedule_key);
--
-- Definition for index public_credit_cash_orders_idx2 (OID = 792739) : 
--
CREATE INDEX public_credit_cash_orders_idx2 ON credit_cash_orders USING btree (date_trunc('day'::text, payment_date), user_name);
--
-- Definition for index repayment_schedules_idx (OID = 792740) : 
--
CREATE INDEX repayment_schedules_idx ON repayment_schedules USING btree (repayment_schedule_key);
--
-- Definition for index repayment_schedules_idx1 (OID = 792741) : 
--
CREATE INDEX repayment_schedules_idx1 ON repayment_schedules USING btree (loan_key);
--
-- Definition for index repayment_schedules_idx2 (OID = 792742) : 
--
CREATE INDEX repayment_schedules_idx2 ON repayment_schedules USING btree (permissible_loan_key);
--
-- Definition for index repayment_schedules_idx3 (OID = 792747) : 
--
CREATE INDEX repayment_schedules_idx3 ON repayment_schedules USING btree (permissible_loan_key, repayment_schedule_key);
--
-- Definition for index repayment_schedules_idx4 (OID = 792748) : 
--
CREATE INDEX repayment_schedules_idx4 ON repayment_schedules USING btree (permissible_loan_key, payment_date);
--
-- Definition for index repayment_schedules_returned_idx (OID = 792749) : 
--
CREATE INDEX repayment_schedules_returned_idx ON repayment_schedules USING btree (returned);
--
-- Definition for index setting_name_idx (OID = 792752) : 
--
CREATE INDEX setting_name_idx ON setting_name USING btree (setting_key);
--
-- Definition for index setting_name_idx1 (OID = 792753) : 
--
CREATE INDEX setting_name_idx1 ON setting_name USING btree (setting_name);
--
-- Definition for index setting_values_idx (OID = 792754) : 
--
CREATE INDEX setting_values_idx ON setting_values USING btree (setting_value_key);
--
-- Definition for index setting_values_idx1 (OID = 792755) : 
--
CREATE INDEX setting_values_idx1 ON setting_values USING btree (subdivision_key, setting_name_key);
--
-- Definition for index t_borrowers_idx (OID = 792756) : 
--
CREATE INDEX t_borrowers_idx ON t_borrowers USING btree (last_name varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx1 (OID = 792757) : 
--
CREATE INDEX t_borrowers_idx1 ON t_borrowers USING btree (name varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx10 (OID = 792758) : 
--
CREATE INDEX t_borrowers_idx10 ON t_borrowers USING btree (upper((guarantor_patronimic)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx11 (OID = 792759) : 
--
CREATE INDEX t_borrowers_idx11 ON t_borrowers USING btree (upper((guarantor2_last_name)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx12 (OID = 792760) : 
--
CREATE INDEX t_borrowers_idx12 ON t_borrowers USING btree (upper((guarantor2_name)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx13 (OID = 792761) : 
--
CREATE INDEX t_borrowers_idx13 ON t_borrowers USING btree (upper((guarantor2_patronimic)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx14 (OID = 792762) : 
--
CREATE INDEX t_borrowers_idx14 ON t_borrowers USING btree (borrower_key, settlement_key);
--
-- Definition for index t_borrowers_idx15 (OID = 792763) : 
--
CREATE INDEX t_borrowers_idx15 ON t_borrowers USING btree (pass_series, pass_number);
--
-- Definition for index t_borrowers_idx16 (OID = 792764) : 
--
CREATE INDEX t_borrowers_idx16 ON t_borrowers USING btree (subdivision_key);
--
-- Definition for index t_borrowers_idx17 (OID = 792765) : 
--
CREATE INDEX t_borrowers_idx17 ON t_borrowers USING btree (subdivision_key, borrower_key);
--
-- Definition for index t_borrowers_idx18 (OID = 792766) : 
--
CREATE INDEX t_borrowers_idx18 ON t_borrowers USING btree (birthday);
--
-- Definition for index t_borrowers_idx2 (OID = 792767) : 
--
CREATE INDEX t_borrowers_idx2 ON t_borrowers USING btree (patronimic varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx3 (OID = 792768) : 
--
CREATE INDEX t_borrowers_idx3 ON t_borrowers USING btree (settlement_key);
--
-- Definition for index t_borrowers_idx4 (OID = 792769) : 
--
CREATE INDEX t_borrowers_idx4 ON t_borrowers USING btree (upper((last_name)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx5 (OID = 792770) : 
--
CREATE INDEX t_borrowers_idx5 ON t_borrowers USING btree (settlement_key, borrower_key);
--
-- Definition for index t_borrowers_idx6 (OID = 792771) : 
--
CREATE INDEX t_borrowers_idx6 ON t_borrowers USING btree (upper((name)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx7 (OID = 792772) : 
--
CREATE INDEX t_borrowers_idx7 ON t_borrowers USING btree (upper((patronimic)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx8 (OID = 792773) : 
--
CREATE INDEX t_borrowers_idx8 ON t_borrowers USING btree (upper((guarantor_last_name)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx9 (OID = 792774) : 
--
CREATE INDEX t_borrowers_idx9 ON t_borrowers USING btree (upper((guarantor_name)::text) varchar_pattern_ops);
--
-- Definition for index t_subdivisions_idx (OID = 792775) : 
--
CREATE INDEX t_subdivisions_idx ON t_subdivisions USING btree (subdivision_key);
--
-- Definition for index t_subdivisions_idx1 (OID = 792776) : 
--
CREATE INDEX t_subdivisions_idx1 ON t_subdivisions USING btree (subdivision_owner_key);
--
-- Definition for index register_records_idx (OID = 792777) : 
--
CREATE INDEX register_records_idx ON register_records USING btree (register_records_key);
--
-- Definition for index register_records_idx1 (OID = 792778) : 
--
CREATE INDEX register_records_idx1 ON register_records USING btree (register_key);
--
-- Definition for index register_records_idx2 (OID = 792779) : 
--
CREATE INDEX register_records_idx2 ON register_records USING btree (loan_key);
--
-- Definition for index register_records_idx3 (OID = 792780) : 
--
CREATE INDEX register_records_idx3 ON register_records USING btree ((((-1) * register_records_key)));
--
-- Definition for index register_records_idx4 (OID = 792781) : 
--
CREATE INDEX register_records_idx4 ON register_records USING btree (contract_number);
--
-- Definition for index special_offers_list_participants_idx1 (OID = 792782) : 
--
CREATE INDEX special_offers_list_participants_idx1 ON list_participants USING btree (key);
--
-- Definition for index special_offers_list_participants_idx2 (OID = 792783) : 
--
CREATE INDEX special_offers_list_participants_idx2 ON list_participants USING btree (loan_key);
--
-- Definition for index account_pkey (OID = 466199) : 
--
SET search_path = buh, pg_catalog;
ALTER TABLE ONLY account
    ADD CONSTRAINT account_pkey
    PRIMARY KEY ("aссount_key");
--
-- Definition for index account_code_pkey (OID = 466210) : 
--
ALTER TABLE ONLY account_code
    ADD CONSTRAINT account_code_pkey
    PRIMARY KEY (account_code_key);
--
-- Definition for index account_transaction_pkey (OID = 466222) : 
--
ALTER TABLE ONLY account_transaction
    ADD CONSTRAINT account_transaction_pkey
    PRIMARY KEY (account_transaction_key);
--
-- Definition for index account_transaction_1c_pkey (OID = 466236) : 
--
ALTER TABLE ONLY account_transaction_1c
    ADD CONSTRAINT account_transaction_1c_pkey
    PRIMARY KEY (account_transaction_key);
--
-- Definition for index stat_tables_count_pkey (OID = 466246) : 
--
ALTER TABLE ONLY stat_tables_count
    ADD CONSTRAINT stat_tables_count_pkey
    PRIMARY KEY (stat_tables_count_key);
--
-- Definition for index balance_value_pkey (OID = 466260) : 
--
ALTER TABLE ONLY balance_value
    ADD CONSTRAINT balance_value_pkey
    PRIMARY KEY (balance_value_key);
--
-- Definition for index loan_transact_pkey (OID = 466274) : 
--
ALTER TABLE ONLY loan_transact
    ADD CONSTRAINT loan_transact_pkey
    PRIMARY KEY (loan_transact_key);
--
-- Definition for index transact_calc_log_pkey (OID = 466288) : 
--
ALTER TABLE ONLY transact_calc_log
    ADD CONSTRAINT transact_calc_log_pkey
    PRIMARY KEY (transact_calc_log_key);
--
-- Definition for index saparate_subdivision_pkey (OID = 466338) : 
--
SET search_path = public, pg_catalog;
ALTER TABLE ONLY separate_subdivision
    ADD CONSTRAINT saparate_subdivision_pkey
    PRIMARY KEY (saparate_subdivision_key);
--
-- Definition for index app_data_pkey (OID = 471167) : 
--
SET search_path = repair_data, pg_catalog;
ALTER TABLE ONLY app_data
    ADD CONSTRAINT app_data_pkey
    PRIMARY KEY (app_data_key);
--
-- Definition for index app_data_online_pkey (OID = 471179) : 
--
ALTER TABLE ONLY app_data_online
    ADD CONSTRAINT app_data_online_pkey
    PRIMARY KEY (app_data_online_key);
--
-- Definition for index loan_version_pkey (OID = 471607) : 
--
SET search_path = loan_calc_history, pg_catalog;
ALTER TABLE ONLY loan_version
    ADD CONSTRAINT loan_version_pkey
    PRIMARY KEY (loan_version_key);
--
-- Definition for index balance_value_history_pkey (OID = 471621) : 
--
ALTER TABLE ONLY balance_value_history
    ADD CONSTRAINT balance_value_history_pkey
    PRIMARY KEY (balance_value_key);
--
-- Definition for index loan_transact_history_pkey (OID = 471635) : 
--
ALTER TABLE ONLY loan_transact_history
    ADD CONSTRAINT loan_transact_history_pkey
    PRIMARY KEY (loan_transact_key);
--
-- Definition for index timelog_pkey (OID = 472409) : 
--
ALTER TABLE ONLY timelog
    ADD CONSTRAINT timelog_pkey
    PRIMARY KEY (key);
--
-- Definition for index loan_percent_pkey (OID = 472563) : 
--
SET search_path = buh, pg_catalog;
ALTER TABLE ONLY loan_percent
    ADD CONSTRAINT loan_percent_pkey
    PRIMARY KEY (loan_percent_key);
--
-- Definition for index progress_pkey (OID = 472921) : 
--
SET search_path = loan_calc_history, pg_catalog;
ALTER TABLE ONLY progress
    ADD CONSTRAINT progress_pkey
    PRIMARY KEY (progress_key);
--
-- Definition for index lock_calculation_pkey (OID = 587003) : 
--
ALTER TABLE ONLY lock_calculation
    ADD CONSTRAINT lock_calculation_pkey
    PRIMARY KEY (lock_calculation_key);
--
-- Definition for index tmp_payment_schedules_pkey (OID = 588925) : 
--
SET search_path = asuz_db, pg_catalog;
ALTER TABLE ONLY tmp_payment_schedules
    ADD CONSTRAINT tmp_payment_schedules_pkey
    PRIMARY KEY (key);
--
-- Definition for index current_calc_pkey (OID = 588979) : 
--
SET search_path = buh, pg_catalog;
ALTER TABLE ONLY current_calc
    ADD CONSTRAINT current_calc_pkey
    PRIMARY KEY (current_calc_key);
--
-- Definition for index borrowers_pkey (OID = 792694) : 
--
SET search_path = asuz_db, pg_catalog;
ALTER TABLE ONLY t_borrowers
    ADD CONSTRAINT borrowers_pkey
    PRIMARY KEY (borrower_key);
--
-- Definition for index credit_cash_orders_pkey (OID = 792696) : 
--
ALTER TABLE ONLY credit_cash_orders
    ADD CONSTRAINT credit_cash_orders_pkey
    PRIMARY KEY (order_key);
--
-- Definition for index loans_pkey (OID = 792698) : 
--
ALTER TABLE ONLY loans
    ADD CONSTRAINT loans_pkey
    PRIMARY KEY (loan_key);
--
-- Definition for index permissible_loans_pkey (OID = 792700) : 
--
ALTER TABLE ONLY permissible_loans
    ADD CONSTRAINT permissible_loans_pkey
    PRIMARY KEY (key);
--
-- Comments
--
COMMENT ON SCHEMA public IS 'standard public schema';
COMMENT ON LANGUAGE plpgsql IS 'PL/pgSQL procedural language';
SET search_path = buh, pg_catalog;
COMMENT ON COLUMN buh.account."aссount_key" IS 'Первичный ключ';
COMMENT ON COLUMN buh.account.name IS 'Название счета';
COMMENT ON COLUMN buh.account_code.account_code_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.account_code.account_key IS 'Ссылка на buh.account (счет)';
COMMENT ON COLUMN buh.account_code.code IS 'Код счета';
COMMENT ON COLUMN buh.account_code.date_start IS 'Дата начала действия кода';
COMMENT ON COLUMN buh.account_code.date_end IS 'Дата окончания действия кода';
COMMENT ON COLUMN buh.account_transaction.account_transaction_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.account_transaction.account_transaction_1c_key IS 'Ссылка на таблицу account_transaction_1c';
COMMENT ON COLUMN buh.account_transaction.name IS 'Название проводки';
COMMENT ON COLUMN buh.account_transaction.account_key_debet IS 'Дебет';
COMMENT ON COLUMN buh.account_transaction.account_key_credit IS 'Кредит';
COMMENT ON COLUMN buh.account_transaction.description IS 'Описание';
COMMENT ON COLUMN buh.account_transaction."order" IS 'Ключ сортировки';
COMMENT ON COLUMN buh.account_transaction.storno IS 'Сторно';
COMMENT ON COLUMN buh.account_transaction_1c.account_transaction_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.account_transaction_1c.name IS 'Название проводки';
COMMENT ON COLUMN buh.account_transaction_1c.account_key_debet IS 'Дебет';
COMMENT ON COLUMN buh.account_transaction_1c.account_key_credit IS 'Кредит';
COMMENT ON COLUMN buh.account_transaction_1c.description IS 'Описание';
COMMENT ON COLUMN buh.account_transaction_1c."order" IS 'Ключ сортировки';
COMMENT ON COLUMN buh.account_transaction_1c.storno IS 'Сторно';
COMMENT ON COLUMN buh.stat_tables_count.stat_tables_count_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.stat_tables_count.create_date IS 'Дата создания записи';
COMMENT ON COLUMN buh.stat_tables_count.user_key IS 'Ключ пользователя';
COMMENT ON COLUMN buh.stat_tables_count.balance_value_count_b IS 'Количество балансовых величин на начало рассчета';
COMMENT ON COLUMN buh.stat_tables_count.balance_value_count_e IS 'Количество балансовых величин на конец рассчета';
COMMENT ON COLUMN buh.stat_tables_count.loans_count IS 'Количество записей в таблице public.loans';
COMMENT ON COLUMN buh.stat_tables_count.repayment_schedules_count IS 'Количество записей в таблице public.repayment_schedules на момент рассчета';
COMMENT ON COLUMN buh.stat_tables_count.permissible_loans_count IS 'Количество записей в таблице public.permissible_loans';
COMMENT ON COLUMN buh.stat_tables_count.separate_subdivision_count IS 'Количество записей в таблице public.separate_subdivision';
COMMENT ON COLUMN buh.stat_tables_count.loan_transact_count_b IS 'Количество записей в таблице loan_transact на начело рассчета';
COMMENT ON COLUMN buh.stat_tables_count.loan_transact_count_e IS 'Количество записей в таблице loan_transact на конец рассчета';
COMMENT ON COLUMN buh.stat_tables_count.transact_calc_log_count_b IS 'Количество записей в таблице transact_calc_log на начало рассчета';
COMMENT ON COLUMN buh.stat_tables_count.transact_calc_log_count_e IS 'Количество записей в таблице transact_calc_log на конец рассчета';
COMMENT ON COLUMN buh.balance_value.balance_value_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.balance_value.loan_key IS 'Займ. Ссылка на public.loans';
COMMENT ON COLUMN buh.balance_value.od_loan_b IS 'Проср. тело займа на начало (58.03.02)';
COMMENT ON COLUMN buh.balance_value.od_loan_e IS 'Проср. тело займа на конец (58.03.02)';
COMMENT ON COLUMN buh.balance_value.od_overpayment_planned_b IS 'Проср. % плановые на начало (76.03.02)';
COMMENT ON COLUMN buh.balance_value.od_overpayment_planned_e IS 'Проср. % плановые на конец (76.03.02)';
COMMENT ON COLUMN buh.balance_value.od_overpayment_for_delay_b IS 'Проср. % доначисленные на начало (76.03.03)';
COMMENT ON COLUMN buh.balance_value.od_overpayment_for_delay_e IS 'Проср. % доначисленные на конец (76.03.03)';
COMMENT ON COLUMN buh.balance_value.od_fine_b IS 'Проср. % пени на начало (76.05.02)';
COMMENT ON COLUMN buh.balance_value.od_fine_e IS 'Проср. % пени на конец (76.05.02)';
COMMENT ON COLUMN buh.balance_value.transact_date IS 'Дата рассчета';
COMMENT ON COLUMN buh.balance_value.create_date IS 'Дата создания записи';
COMMENT ON COLUMN buh.balance_value.n_loan_b IS 'Непросроченое тело займа на начало (58.03.01)';
COMMENT ON COLUMN buh.balance_value.n_loan_e IS 'Непросроченое тело займа на конец (58.03.01)';
COMMENT ON COLUMN buh.balance_value.n_overpayment_planned_b IS 'Непросроченные % на начало (76.03.01)';
COMMENT ON COLUMN buh.balance_value.n_overpayment_planned_e IS 'Непросроченные % на конец (76.03.01)';
COMMENT ON COLUMN buh.balance_value.loan_b IS 'Тело займа на начало 58.03';
COMMENT ON COLUMN buh.balance_value.loan_e IS 'Тело займа на начало 58.03';
COMMENT ON COLUMN buh.balance_value.client_balance_b IS 'Баланс клиента на начало (76.09)';
COMMENT ON COLUMN buh.balance_value.client_balance_e IS 'Баланс клиента на конец (76.09)';
COMMENT ON COLUMN buh.balance_value.active IS 'Активная запись';
COMMENT ON COLUMN buh.balance_value.opened IS 'Открыт ли займ';
COMMENT ON COLUMN buh.balance_value.n_overpayment_conditionally_b IS 'Условно непросроченные проценты на начало (76.03.04)';
COMMENT ON COLUMN buh.balance_value.n_overpayment_conditionally_e IS 'Условно непросроченные проценты на конец(76.03.04)';
COMMENT ON COLUMN buh.balance_value.all_loan_percents_b IS 'Cумма всех начисленных процентов по займу на начало';
COMMENT ON COLUMN buh.balance_value.all_loan_percents_e IS 'Cумма всех начисленных процентов по займу на конец';
COMMENT ON COLUMN buh.loan_transact.loan_transact_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.loan_transact.loan_key IS 'Займ. Ссылка на public.loans';
COMMENT ON COLUMN buh.loan_transact.separate_subdivision_key IS 'Подразделение';
COMMENT ON COLUMN buh.loan_transact.summ IS 'Сумма проводки';
COMMENT ON COLUMN buh.loan_transact.fact_key IS 'Ссылка на facts.facts';
COMMENT ON COLUMN buh.loan_transact.account_transaction_key IS 'Ключ транзакции. Ссылка на buh.account_transaction';
COMMENT ON COLUMN buh.loan_transact.transact_date IS 'Дата рассчета';
COMMENT ON COLUMN buh.loan_transact.create_date IS 'Дата создания записи';
COMMENT ON COLUMN buh.loan_transact.active IS 'Активная запись';
COMMENT ON COLUMN buh.transact_calc_log.transact_calc_log_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.transact_calc_log.loan_key IS 'Ключ займа';
COMMENT ON COLUMN buh.transact_calc_log.date IS 'Дата рассчета';
COMMENT ON COLUMN buh.transact_calc_log.procedure IS 'Хранимка, в которой произошла ошибка';
COMMENT ON COLUMN buh.transact_calc_log.message IS 'Сообщение об ошибке';
COMMENT ON COLUMN buh.transact_calc_log.create_date IS 'Дата создания записи';
SET search_path = facts, pg_catalog;
COMMENT ON COLUMN facts.fact_names.fact_name_key IS 'ключ записи';
COMMENT ON COLUMN facts.fact_names.fact_name IS 'Наименование события';
COMMENT ON COLUMN facts.fact_names.func_register_fact IS 'Наименование функции для регистрации события';
COMMENT ON COLUMN facts.facts.subdivision_key IS 'Ключ подразделения';
SET search_path = public, pg_catalog;
COMMENT ON COLUMN public.separate_subdivision.saparate_subdivision_key IS 'Первичный ключ';
COMMENT ON COLUMN public.separate_subdivision.separate_subdivision IS 'Название подразделения';
COMMENT ON COLUMN public.separate_subdivision.subdivision_key IS 'Номер подразделения';
COMMENT ON COLUMN public.separate_subdivision.date_b IS 'Дата начала работы подразделения';
SET search_path = loan_calc_history, pg_catalog;
COMMENT ON COLUMN loan_calc_history.loan_version.loan_version_key IS 'Первичный ключ';
COMMENT ON COLUMN loan_calc_history.loan_version.creation_date IS 'Время создания записи';
COMMENT ON COLUMN loan_calc_history.loan_version.description IS 'Описание';
COMMENT ON COLUMN loan_calc_history.loan_version.nopeni IS 'Расчет без пени';
COMMENT ON COLUMN loan_calc_history.loan_version.first_payment_schedule IS 'Рассчитать по первому графику платежей на момент выдачи (из таблицы loan_issue.tmp_payment_schedules)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.balance_value_key IS 'Первичный ключ';
COMMENT ON COLUMN loan_calc_history.balance_value_history.loan_key IS 'Займ. Ссылка на public.loans';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_loan_b IS 'Проср. тело займа на начало (58.03.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_loan_e IS 'Проср. тело займа на конец (58.03.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_overpayment_planned_b IS 'Проср. % плановые на начало (76.03.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_overpayment_planned_e IS 'Проср. % плановые на конец (76.03.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_overpayment_for_delay_b IS 'Проср. % доначисленные на начало (76.03.03)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_overpayment_for_delay_e IS 'Проср. % доначисленные на конец (76.03.03)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_fine_b IS 'Проср. % пени на начало (76.05.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_fine_e IS 'Проср. % пени на конец (76.05.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.transact_date IS 'Дата рассчета';
COMMENT ON COLUMN loan_calc_history.balance_value_history.create_date IS 'Дата создания записи';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_loan_b IS 'Непросроченое тело займа на начало (58.03.01)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_loan_e IS 'Непросроченое тело займа на конец (58.03.01)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_overpayment_planned_b IS 'Непросроченные % на начало (76.03.01)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_overpayment_planned_e IS 'Непросроченные % на конец (76.03.01)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.loan_b IS 'Тело займа на начало 58.03';
COMMENT ON COLUMN loan_calc_history.balance_value_history.loan_e IS 'Тело займа на начало 58.03';
COMMENT ON COLUMN loan_calc_history.balance_value_history.client_balance_b IS 'Баланс клиента на начало (76.09)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.client_balance_e IS 'Баланс клиента на конец (76.09)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.active IS 'Активная запись';
COMMENT ON COLUMN loan_calc_history.balance_value_history.opened IS 'Открыт ли займ';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_overpayment_conditionally_b IS 'Условно непросроченные проценты на начало (76.03.04)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_overpayment_conditionally_e IS 'Условно непросроченные проценты на конец(76.03.04)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.all_loan_percents_b IS 'Cумма всех начисленных процентов по займу на начало';
COMMENT ON COLUMN loan_calc_history.balance_value_history.all_loan_percents_e IS 'Cумма всех начисленных процентов по займу на конец';
COMMENT ON COLUMN loan_calc_history.balance_value_history.loan_version_key IS 'Ссылка на версию рассчета';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.loan_transact_key IS 'Первичный ключ';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.loan_key IS 'Займ. Ссылка на public.loans';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.separate_subdivision_key IS 'Подразделение';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.summ IS 'Сумма проводки';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.fact_key IS 'Ссылка на facts.facts';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.account_transaction_key IS 'Ключ транзакции. Ссылка на buh.account_transaction';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.transact_date IS 'Дата рассчета';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.create_date IS 'Дата создания записи';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.active IS 'Активная запись';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.loan_version_key IS 'Ссылка на версию рассчета';
SET search_path = asuz_db_local, pg_catalog;
COMMENT ON COLUMN asuz_db_local.t_borrowers.regular_client IS 'Постоянный клиент';
COMMENT ON COLUMN asuz_db_local.t_borrowers.do_not_notify IS 'Не уведомлять о приближении даты оплаты';
COMMENT ON COLUMN asuz_db_local.t_borrowers.snils IS 'СНИЛС';
COMMENT ON COLUMN asuz_db_local.t_borrowers.snils_check_mode IS 'Каким образом был проверен СНИЛС (табл. loan_issue.snils_verification_result)';
COMMENT ON COLUMN asuz_db_local.t_borrowers.birthplace IS 'Место рождения';
SET search_path = buh, pg_catalog;
COMMENT ON COLUMN buh.loan_percent.year IS 'Год';
SET search_path = loan_calc_history, pg_catalog;
COMMENT ON COLUMN loan_calc_history.progress.progress_key IS 'Первичный ключ';
COMMENT ON COLUMN loan_calc_history.progress.loan_version_key IS 'Ключ версии расчета';
COMMENT ON COLUMN loan_calc_history.progress.total_days IS 'За сколько дней идет расчет';
COMMENT ON COLUMN loan_calc_history.progress.current_day IS 'Текущий день';
SET search_path = public, pg_catalog;
COMMENT ON COLUMN public.special_offers_types.special_offer_type_key IS 'Первый ключ таблицы';
COMMENT ON COLUMN public.special_offers_types.special_offer_name IS 'Название акции';
COMMENT ON COLUMN public.special_offers_types.special_offer_description IS 'Описание акции';
COMMENT ON COLUMN public.special_offers_types.start_date IS 'Дата начала действия акции';
COMMENT ON COLUMN public.special_offers_types.end_date IS 'Дата окончания действия акции';
COMMENT ON COLUMN public.special_offers_types.procedure_name IS 'Хранимка создания акции для клиента';
COMMENT ON COLUMN public.special_offers_types.id IS 'Идентификатор процесса';
COMMENT ON COLUMN public.special_offers_types.only_new_client IS 'Флаг действия акции только для новых клиентов';
COMMENT ON COLUMN public.special_offers_types.min_term_of_loan IS 'Минимальный срок займа';
COMMENT ON COLUMN public.special_offers_types.max_term_of_loan IS 'Максимальный срок займа';
COMMENT ON COLUMN public.special_offers_types.direct_discount IS 'Скидка на сумму';
COMMENT ON COLUMN public.special_offers_types.offer_category IS 'Категория предложения';
SET search_path = loan_calc_history, pg_catalog;
COMMENT ON TABLE loan_calc_history.lock_calculation IS '/*
<description>
           Таблица для хранения блокировок при параллельных расчетах
</description>
<history_list>
    <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/';
COMMENT ON COLUMN loan_calc_history.lock_calculation.lock_calculation_key IS 'Первичный ключ';
COMMENT ON COLUMN loan_calc_history.lock_calculation.loan_key IS 'Ключ займа, который расчитывается в текущий момент';
COMMENT ON COLUMN loan_calc_history.lock_calculation.create_date IS 'Дта создания записи';
SET search_path = buh, pg_catalog;
COMMENT ON TABLE buh.current_calc IS '/*
<description>
	В таблице зранятся метки о выполняемых сейчас расчетах займ  + версия расчета
    для того, чтобы во время расчета можно было поднять настройки текущей версии расчета</description>
<history_list>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/';
COMMENT ON COLUMN buh.current_calc.current_calc_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.current_calc.loan_key IS 'Ключ займа';
COMMENT ON COLUMN buh.current_calc.loan_version_key IS 'Ссылка на версию расчета';
COMMENT ON COLUMN buh.current_calc.create_date IS 'Дата создания записи';
SET search_path = asuz_db, pg_catalog;
COMMENT ON COLUMN asuz_db.t_borrowers.regular_client IS 'Постоянный клиент';
COMMENT ON COLUMN asuz_db.t_borrowers.do_not_notify IS 'Не уведомлять о приближении даты оплаты';
COMMENT ON COLUMN asuz_db.t_borrowers.snils IS 'СНИЛС';
COMMENT ON COLUMN asuz_db.t_borrowers.snils_check_mode IS 'Каким образом был проверен СНИЛС (табл. loan_issue.snils_verification_result)';
COMMENT ON COLUMN asuz_db.t_borrowers.birthplace IS 'Место рождения';
COMMENT ON COLUMN asuz_db.repayment_schedules.repayment_schedule_key IS 'Ключ таблицы';
COMMENT ON COLUMN asuz_db.repayment_schedules.loan_key IS 'Ключ займа';
COMMENT ON COLUMN asuz_db.repayment_schedules.payment_date IS 'Плановая дата платежа';
COMMENT ON COLUMN asuz_db.repayment_schedules.loan_sum IS 'Сумма тела займа к выплате';
COMMENT ON COLUMN asuz_db.repayment_schedules.overpayment_sum IS 'Сумма плановых процентов к выплате';
COMMENT ON COLUMN asuz_db.repayment_schedules.delay IS 'Флаг просрочки';
COMMENT ON COLUMN asuz_db.repayment_schedules.returned IS 'Флаг, что платёж полностью погашен';
COMMENT ON COLUMN asuz_db.repayment_schedules.permissible_loan_key IS 'Ключ графика платежей';
COMMENT ON COLUMN asuz_db.repayment_schedules.returned_date IS 'Дата полного закрытия платежа';
COMMENT ON COLUMN asuz_db.loans.debt_relieft_type IS 'признак списания долга
цессия, смерть клиента, банкротство и т.п.
выставляется в процедурах списания долга,
юзается для отчетности и рассчета проводок';
COMMENT ON COLUMN asuz_db.loans.claim_right IS 'Переуступка прав требований по договору займа третьим лицам (нулл - до появления признака,-1 - не согласен,1 - согласен)';
COMMENT ON COLUMN asuz_db.loans.refinance IS 'Рефинанс';
COMMENT ON COLUMN asuz_db.facts.subdivision_key IS 'Ключ подразделения';
COMMENT ON TABLE asuz_db.loan_restructuring_history IS 'Реестр доп соглашений на реструктуризацию';
COMMENT ON COLUMN asuz_db.permissible_loans.type_repayment_schedule_key IS 'Тип графика платежей';
COMMENT ON COLUMN asuz_db.special_offers_types.special_offer_type_key IS 'Первый ключ таблицы';
COMMENT ON COLUMN asuz_db.special_offers_types.special_offer_name IS 'Название акции';
COMMENT ON COLUMN asuz_db.special_offers_types.special_offer_description IS 'Описание акции';
COMMENT ON COLUMN asuz_db.special_offers_types.start_date IS 'Дата начала действия акции';
COMMENT ON COLUMN asuz_db.special_offers_types.end_date IS 'Дата окончания действия акции';
COMMENT ON COLUMN asuz_db.special_offers_types.procedure_name IS 'Хранимка создания акции для клиента';
COMMENT ON COLUMN asuz_db.special_offers_types.id IS 'Идентификатор процесса';
COMMENT ON COLUMN asuz_db.special_offers_types.only_new_client IS 'Флаг действия акции только для новых клиентов';
COMMENT ON COLUMN asuz_db.special_offers_types.min_term_of_loan IS 'Минимальный срок займа';
COMMENT ON COLUMN asuz_db.special_offers_types.max_term_of_loan IS 'Максимальный срок займа';
COMMENT ON COLUMN asuz_db.special_offers_types.direct_discount IS 'Скидка на сумму';
COMMENT ON COLUMN asuz_db.special_offers_types.offer_category IS 'Категория предложения';
