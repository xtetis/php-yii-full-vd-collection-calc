-- SQL Manager for PostgreSQL 5.9.3.51215
-- ---------------------------------------
-- Хост         : asuztest-local
-- База данных  : collection_db
-- Версия       : PostgreSQL 9.3.14 on x86_64-unknown-linux-gnu, compiled by gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-4), 64-bit



SET search_path = asuz_db, pg_catalog;
DROP INDEX IF EXISTS asuz_db.special_offers_list_participants_idx2;
DROP INDEX IF EXISTS asuz_db.special_offers_list_participants_idx1;
DROP INDEX IF EXISTS asuz_db.register_records_idx4;
DROP INDEX IF EXISTS asuz_db.register_records_idx3;
DROP INDEX IF EXISTS asuz_db.register_records_idx2;
DROP INDEX IF EXISTS asuz_db.register_records_idx1;
DROP INDEX IF EXISTS asuz_db.register_records_idx;
DROP INDEX IF EXISTS asuz_db.t_subdivisions_idx1;
DROP INDEX IF EXISTS asuz_db.t_subdivisions_idx;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx9;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx8;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx7;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx6;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx5;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx4;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx3;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx2;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx18;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx17;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx16;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx15;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx14;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx13;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx12;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx11;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx10;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx1;
DROP INDEX IF EXISTS asuz_db.t_borrowers_idx;
DROP INDEX IF EXISTS asuz_db.setting_values_idx1;
DROP INDEX IF EXISTS asuz_db.setting_values_idx;
DROP INDEX IF EXISTS asuz_db.setting_name_idx1;
DROP INDEX IF EXISTS asuz_db.setting_name_idx;
DROP INDEX IF EXISTS asuz_db.repayment_schedules_returned_idx;
DROP INDEX IF EXISTS asuz_db.repayment_schedules_idx4;
DROP INDEX IF EXISTS asuz_db.repayment_schedules_idx3;
DROP INDEX IF EXISTS asuz_db.repayment_schedules_idx2;
DROP INDEX IF EXISTS asuz_db.repayment_schedules_idx1;
DROP INDEX IF EXISTS asuz_db.repayment_schedules_idx;
DROP INDEX IF EXISTS asuz_db.public_credit_cash_orders_idx2;
DROP INDEX IF EXISTS asuz_db.permissible_loans_type_repayment_schedule_key_idx;
DROP INDEX IF EXISTS asuz_db.permissible_loans_loan_key_idx;
DROP INDEX IF EXISTS asuz_db.loans_manual_correction_rs_idx4;
DROP INDEX IF EXISTS asuz_db.loans_manual_correction_rs_idx3;
DROP INDEX IF EXISTS asuz_db.loans_manual_correction_rs_idx2;
DROP INDEX IF EXISTS asuz_db.loans_manual_correction_rs_idx1;
DROP INDEX IF EXISTS asuz_db.loans_manual_correction_rs_idx;
DROP INDEX IF EXISTS asuz_db.loans_manual_correction_idx2;
DROP INDEX IF EXISTS asuz_db.loans_manual_correction_idx1;
DROP INDEX IF EXISTS asuz_db.loans_manual_correction_idx;
DROP INDEX IF EXISTS asuz_db.loans_idx_application;
DROP INDEX IF EXISTS asuz_db.loans_idx_5;
DROP INDEX IF EXISTS asuz_db.loans_idx_4;
DROP INDEX IF EXISTS asuz_db.loans_idx_3;
DROP INDEX IF EXISTS asuz_db.loans_idx2;
DROP INDEX IF EXISTS asuz_db.loans_idx1;
DROP INDEX IF EXISTS asuz_db.loans_idx;
DROP INDEX IF EXISTS asuz_db.loan_restructuring_history_idx2;
DROP INDEX IF EXISTS asuz_db.loan_restructuring_history_idx;
DROP INDEX IF EXISTS asuz_db.credit_cash_orders_sep_sd_key_idx;
DROP INDEX IF EXISTS asuz_db.credit_cash_orders_idx1;
DROP INDEX IF EXISTS asuz_db.credit_cash_orders_idx;
DROP INDEX IF EXISTS asuz_db.payment_part_idx1;
DROP INDEX IF EXISTS asuz_db.payment_part_idx;
DROP INDEX IF EXISTS asuz_db.payment_idx1;
DROP INDEX IF EXISTS asuz_db.payment_idx;
DROP INDEX IF EXISTS asuz_db.documents_idx3;
DROP INDEX IF EXISTS asuz_db.documents_idx2;
DROP INDEX IF EXISTS asuz_db.documents_idx;
DROP INDEX IF EXISTS asuz_db.repayment_schedules_payment_date_ori_idx2;
DROP INDEX IF EXISTS asuz_db.repayment_schedules_payment_date_ori_idx1;
DROP INDEX IF EXISTS asuz_db.repayment_schedules_payment_date_ori_idx;
DROP INDEX IF EXISTS asuz_db.asuz_db_tmp_payment_schedules_idx1;
SET search_path = loan_calc_history, pg_catalog;
DROP INDEX IF EXISTS loan_calc_history.loan_transact_history_idx5;
DROP INDEX IF EXISTS loan_calc_history.loan_transact_history_idx;
DROP INDEX IF EXISTS loan_calc_history.balance_value_history_idx;
DROP INDEX IF EXISTS loan_calc_history.loan_transact_history_idx__buh_loan_transact__loan_key__transac;
DROP INDEX IF EXISTS loan_calc_history.loan_transact_history_idx__buh_loan_transact__account_transacti;
DROP INDEX IF EXISTS loan_calc_history.balance_value_history_idx__buh_balance_value__loan_key__transac;
SET search_path = public, pg_catalog;
DROP INDEX IF EXISTS public.idx__public_separate_subdivision__subdivision_key__1;
DROP INDEX IF EXISTS public.idx__public_separate_subdivision__separate_subdivision__1;
DROP INDEX IF EXISTS public.idx__public_separate_subdivision__date_b__1;
SET search_path = facts, pg_catalog;
DROP INDEX IF EXISTS facts.documents_idx2;
DROP INDEX IF EXISTS facts.documents_idx;
SET search_path = buh, pg_catalog;
DROP INDEX IF EXISTS buh.idx__buh_loan_transact__loan_key__transact_datee__fact_key__1;
DROP INDEX IF EXISTS buh.idx__buh_loan_transact__account_transaction_key__1;
DROP INDEX IF EXISTS buh.idx__buh_balance_value__loan_key__transact_date__opened__1;
DROP INDEX IF EXISTS buh.idx__buh_account_transaction__account_key_debet__1;
DROP INDEX IF EXISTS buh.idx__buh_account_transaction__account_key_credit__1;
SET search_path = asuz_db, pg_catalog;
DROP TABLE IF EXISTS asuz_db.special_offers_types;
DROP TABLE IF EXISTS asuz_db.list_participants_register;
DROP TABLE IF EXISTS asuz_db.list_participants;
DROP TABLE IF EXISTS asuz_db.partial_early_agreement;
DROP TABLE IF EXISTS asuz_db.t_subdivisions;
DROP TABLE IF EXISTS asuz_db.setting_values;
DROP TABLE IF EXISTS asuz_db.setting_name;
DROP TABLE IF EXISTS asuz_db.permissible_loans;
DROP TABLE IF EXISTS asuz_db.loans_manual_correction_rs;
DROP TABLE IF EXISTS asuz_db.loan_restructuring_history;
DROP TABLE IF EXISTS asuz_db.facts;
DROP TABLE IF EXISTS asuz_db.payment_part;
DROP TABLE IF EXISTS asuz_db.payment;
DROP TABLE IF EXISTS asuz_db.credit_cash_orders;
DROP TABLE IF EXISTS asuz_db.register_records;
DROP TABLE IF EXISTS asuz_db.loans_manual_correction;
DROP TABLE IF EXISTS asuz_db.loans;
DROP TABLE IF EXISTS asuz_db.repayment_schedules;
DROP TABLE IF EXISTS asuz_db.t_borrowers;
DROP TABLE IF EXISTS asuz_db.repayment_schedules_payment_date_ori;
SET search_path = dwh, pg_catalog;
DROP TABLE IF EXISTS dwh.t_xmin_usage;
SET search_path = buh, pg_catalog;
DROP TABLE IF EXISTS buh.current_calc;
SET search_path = asuz_db, pg_catalog;
DROP TABLE IF EXISTS asuz_db.tmp_payment_schedules;
SET search_path = loan_calc_history, pg_catalog;
DROP TABLE IF EXISTS loan_calc_history.lock_calculation;
SET search_path = asuz_db, pg_catalog;
DROP FOREIGN TABLE IF EXISTS asuz_db.special_offers;
SET search_path = public, pg_catalog;
DROP TABLE IF EXISTS public.loan_restructuring_history;
DROP TABLE IF EXISTS public.special_offers_types;
DROP TABLE IF EXISTS public.list_participants_register;
SET search_path = loan_calc_history, pg_catalog;
DROP TABLE IF EXISTS loan_calc_history.progress;
SET search_path = buh, pg_catalog;
DROP TABLE IF EXISTS buh.loan_percent;
SET search_path = loan_calc_history, pg_catalog;
DROP TABLE IF EXISTS loan_calc_history.timelog;
SET search_path = asuz_db_local, pg_catalog;
DROP TABLE IF EXISTS asuz_db_local.credit_cash_orders;
DROP TABLE IF EXISTS asuz_db_local.t_subdivisions;
DROP TABLE IF EXISTS asuz_db_local.t_borrowers;
SET search_path = loan_calc_history, pg_catalog;
DROP TABLE IF EXISTS loan_calc_history.loan_transact_history;
DROP TABLE IF EXISTS loan_calc_history.balance_value_history;
DROP TABLE IF EXISTS loan_calc_history.loan_version;
SET search_path = logging, pg_catalog;
DROP TABLE IF EXISTS logging.log_errors;
SET search_path = repair_data, pg_catalog;
DROP TABLE IF EXISTS repair_data.list_recovery_type_2_3;
DROP TABLE IF EXISTS repair_data.app_data_online;
DROP TABLE IF EXISTS repair_data.app_data;
SET search_path = public, pg_catalog;
DROP TABLE IF EXISTS public.separate_subdivision;
SET search_path = facts, pg_catalog;
DROP TABLE IF EXISTS facts.facts;
DROP TABLE IF EXISTS facts.fact_names;
SET search_path = buh, pg_catalog;
DROP TABLE IF EXISTS buh.transact_calc_log;
DROP TABLE IF EXISTS buh.loan_transact;
DROP TABLE IF EXISTS buh.balance_value;
DROP TABLE IF EXISTS buh.stat_tables_count;
DROP TABLE IF EXISTS buh.account_transaction_1c;
DROP TABLE IF EXISTS buh.account_transaction;
DROP TABLE IF EXISTS buh.account_code;
DROP TABLE IF EXISTS buh.account;
SET check_function_bodies = false;
--
-- Structure for table account (OID = 466192) : 
--
CREATE TABLE buh.account (
    "aссount_key" serial NOT NULL,
    name varchar
)
WITH (oids = false);
--
-- Structure for table account_code (OID = 466203) : 
--
CREATE TABLE buh.account_code (
    account_code_key serial NOT NULL,
    account_key integer,
    code varchar,
    date_start date,
    date_end date
)
WITH (oids = false);
--
-- Structure for table account_transaction (OID = 466214) : 
--
CREATE TABLE buh.account_transaction (
    account_transaction_key serial NOT NULL,
    account_transaction_1c_key integer,
    name varchar,
    account_key_debet integer,
    account_key_credit integer,
    description varchar,
    "order" integer,
    storno integer DEFAULT 0
)
WITH (oids = false);
--
-- Structure for table account_transaction_1c (OID = 466228) : 
--
CREATE TABLE buh.account_transaction_1c (
    account_transaction_key serial NOT NULL,
    name varchar,
    account_key_debet integer,
    account_key_credit integer,
    description varchar,
    "order" integer,
    storno integer DEFAULT 0
)
WITH (oids = false);
--
-- Structure for table stat_tables_count (OID = 466240) : 
--
CREATE TABLE buh.stat_tables_count (
    stat_tables_count_key serial NOT NULL,
    create_date timestamp without time zone DEFAULT now(),
    user_key integer DEFAULT public.user_key(),
    balance_value_count_b integer,
    balance_value_count_e integer,
    loans_count integer,
    repayment_schedules_count integer,
    permissible_loans_count integer,
    separate_subdivision_count integer,
    loan_transact_count_b integer,
    loan_transact_count_e integer,
    transact_calc_log_count_b integer,
    transact_calc_log_count_e integer
)
WITH (oids = false);
--
-- Structure for table balance_value (OID = 466250) : 
--
CREATE TABLE buh.balance_value (
    balance_value_key serial NOT NULL,
    loan_key integer,
    od_loan_b numeric,
    od_loan_e numeric,
    od_overpayment_planned_b numeric,
    od_overpayment_planned_e numeric,
    od_overpayment_for_delay_b numeric,
    od_overpayment_for_delay_e numeric,
    od_fine_b numeric,
    od_fine_e numeric,
    transact_date date,
    create_date timestamp without time zone DEFAULT now(),
    n_loan_b numeric,
    n_loan_e numeric,
    n_overpayment_planned_b numeric,
    n_overpayment_planned_e numeric,
    loan_b numeric,
    loan_e numeric,
    client_balance_b numeric,
    client_balance_e numeric,
    active integer DEFAULT 1,
    opened boolean DEFAULT true,
    n_overpayment_conditionally_b numeric,
    n_overpayment_conditionally_e numeric,
    all_loan_percents_b numeric,
    all_loan_percents_e numeric
)
WITH (oids = false);
--
-- Structure for table loan_transact (OID = 466265) : 
--
CREATE TABLE buh.loan_transact (
    loan_transact_key serial NOT NULL,
    loan_key integer,
    separate_subdivision_key varchar,
    summ numeric,
    fact_key integer,
    account_transaction_key integer,
    transact_date date,
    create_date timestamp without time zone DEFAULT now(),
    active integer DEFAULT 1
)
WITH (oids = false);
--
-- Structure for table transact_calc_log (OID = 466280) : 
--
CREATE TABLE buh.transact_calc_log (
    transact_calc_log_key serial NOT NULL,
    loan_key integer,
    date date,
    procedure varchar,
    message varchar,
    create_date timestamp without time zone DEFAULT now()
)
WITH (oids = false);
--
-- Structure for table fact_names (OID = 466291) : 
--
SET search_path = facts, pg_catalog;
CREATE TABLE facts.fact_names (
    fact_name_key integer NOT NULL,
    fact_name varchar,
    func_register_fact varchar
)
WITH (oids = false);
--
-- Structure for table facts (OID = 466299) : 
--
CREATE TABLE facts.facts (
    fact_key serial NOT NULL,
    fact_date timestamp(6) without time zone,
    fact_name_key integer,
    obj_type integer,
    obj_key integer,
    loan_key integer,
    value numeric,
    user_key integer,
    subdivision_key integer,
    kod_podr varchar,
    additional_value integer,
    additional_value2 integer,
    creation_date timestamp without time zone DEFAULT (clock_timestamp())::timestamp without time zone NOT NULL
)
WITH (oids = false);
--
-- Structure for table separate_subdivision (OID = 466331) : 
--
SET search_path = public, pg_catalog;
CREATE TABLE public.separate_subdivision (
    saparate_subdivision_key serial NOT NULL,
    separate_subdivision varchar,
    subdivision_key integer,
    date_b date
)
WITH (oids = false);
--
-- Structure for table app_data (OID = 471162) : 
--
SET search_path = repair_data, pg_catalog;
CREATE TABLE repair_data.app_data (
    app_data_key serial NOT NULL,
    application_key integer,
    app_creation_date date,
    process_key integer,
    process_status integer,
    was_101 boolean,
    last_101 timestamp without time zone,
    was_113 boolean,
    last_113 timestamp without time zone,
    issued_by_app boolean,
    loan_key integer,
    issued_by_loan boolean,
    possible_loan_key integer,
    has_insurance boolean,
    insurance_status integer,
    subdivision_key integer,
    user_113 integer,
    verification_result integer,
    proceed integer DEFAULT 0,
    new_loan_key integer
)
WITH (oids = false);
--
-- Structure for table app_data_online (OID = 471171) : 
--
CREATE TABLE repair_data.app_data_online (
    app_data_online_key serial NOT NULL,
    application_key integer,
    process_key integer,
    creation_date date,
    account varchar,
    online_possibility integer,
    withdrawal_method integer,
    possible_loan_key integer,
    subdivision_key integer,
    proceed integer DEFAULT 0,
    last_113 timestamp without time zone,
    loan_key integer,
    issued_by_app boolean,
    issued_by_loan boolean
)
WITH (oids = false);
--
-- Structure for table list_recovery_type_2_3 (OID = 471183) : 
--
CREATE TABLE repair_data.list_recovery_type_2_3 (
    list_recovery_key serial NOT NULL,
    loan_key integer,
    repair_type integer,
    status integer
)
WITH (oids = false);
--
-- Structure for table log_errors (OID = 471239) : 
--
SET search_path = logging, pg_catalog;
CREATE TABLE logging.log_errors (
    error_key serial NOT NULL,
    error_text text,
    error_time timestamp without time zone DEFAULT now(),
    error_site varchar
)
WITH (oids = false);
--
-- Structure for table loan_version (OID = 471599) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE TABLE loan_calc_history.loan_version (
    loan_version_key serial NOT NULL,
    loan_key integer,
    creation_date timestamp without time zone DEFAULT (clock_timestamp())::timestamp without time zone,
    description varchar,
    nopeni integer DEFAULT 0,
    first_payment_schedule integer DEFAULT 0
)
WITH (oids = false);
--
-- Structure for table balance_value_history (OID = 471611) : 
--
CREATE TABLE loan_calc_history.balance_value_history (
    balance_value_key serial NOT NULL,
    loan_key integer,
    od_loan_b numeric,
    od_loan_e numeric,
    od_overpayment_planned_b numeric,
    od_overpayment_planned_e numeric,
    od_overpayment_for_delay_b numeric,
    od_overpayment_for_delay_e numeric,
    od_fine_b numeric,
    od_fine_e numeric,
    transact_date date,
    create_date timestamp without time zone DEFAULT now(),
    n_loan_b numeric,
    n_loan_e numeric,
    n_overpayment_planned_b numeric,
    n_overpayment_planned_e numeric,
    loan_b numeric,
    loan_e numeric,
    client_balance_b numeric,
    client_balance_e numeric,
    active integer DEFAULT 1,
    opened boolean DEFAULT true,
    n_overpayment_conditionally_b numeric,
    n_overpayment_conditionally_e numeric,
    all_loan_percents_b numeric,
    all_loan_percents_e numeric,
    loan_version_key integer
)
WITH (oids = false);
--
-- Structure for table loan_transact_history (OID = 471626) : 
--
CREATE TABLE loan_calc_history.loan_transact_history (
    loan_transact_key serial NOT NULL,
    loan_key integer,
    separate_subdivision_key varchar,
    summ numeric,
    fact_key integer,
    account_transaction_key integer,
    transact_date date,
    create_date timestamp without time zone DEFAULT now(),
    active integer DEFAULT 1,
    loan_version_key integer
)
WITH (oids = false);
--
-- Structure for table t_borrowers (OID = 472352) : 
--
SET search_path = asuz_db_local, pg_catalog;
CREATE TABLE asuz_db_local.t_borrowers (
    borrower_key integer NOT NULL,
    settlement_key integer NOT NULL,
    regular_client boolean,
    do_not_notify boolean,
    last_name varchar NOT NULL,
    name varchar NOT NULL,
    patronimic varchar,
    birthday date NOT NULL,
    pass_series varchar NOT NULL,
    pass_number varchar NOT NULL,
    pass_issue varchar NOT NULL,
    pass_date date NOT NULL,
    pass_code varchar NOT NULL,
    email varchar,
    reg_city varchar,
    reg_postcode varchar,
    reg_street varchar,
    reg_house varchar,
    reg_housing varchar,
    reg_flat varchar,
    actual_address boolean,
    actual_city varchar,
    actual_postcode varchar,
    actual_street varchar,
    actual_house varchar,
    actual_housing varchar,
    actual_flat varchar,
    martial_status integer,
    spouse_last_name varchar,
    spouse_name varchar,
    spouse_patronimiic varchar,
    spouse_birthday date,
    have_children boolean,
    nmb_of_children integer,
    date_of_divorce date,
    regular_profit numeric NOT NULL,
    pensioner boolean,
    standing integer,
    nmb_of_work integer,
    work varchar,
    work_address varchar,
    work_post varchar,
    work_comment varchar,
    work2 varchar,
    work2_address varchar,
    work2_post varchar,
    work2_comment varchar,
    spouse_regular_profit numeric,
    spouse_works integer,
    spouse_work varchar,
    spouse_work_address varchar,
    guarantor_last_name varchar,
    guarantor_name varchar,
    guarantor_patronimic varchar,
    guarantor_who varchar,
    guarantor2_last_name varchar,
    guarantor2_name varchar,
    guarantor2_patronimic varchar,
    guarantor2_who varchar,
    blacklist boolean,
    blacklist_date date,
    blacklist_comment varchar,
    individual_entrepreneur boolean,
    ie_ogrn varchar,
    ie_inn varchar,
    ie_comment text,
    discount_enabled boolean,
    discount_card_type integer,
    discount_card_number varchar,
    collector_comment text,
    user_name varchar DEFAULT "session_user"(),
    creation_date timestamp without time zone DEFAULT now(),
    ie_business_state boolean,
    employment_history boolean,
    discount_recommended_borrower_card_number varchar,
    gender varchar(1),
    regular_profit_unofficial numeric,
    source_information integer,
    blacklist_external boolean DEFAULT false,
    in_blacklist_before_date date,
    subdivision_key integer,
    reg_city_key bigint,
    actual_city_key bigint,
    collector_address integer,
    snils varchar,
    snils_check_mode integer DEFAULT 0,
    birthplace varchar
)
WITH (oids = false);
--
-- Structure for table t_subdivisions (OID = 472362) : 
--
CREATE TABLE asuz_db_local.t_subdivisions (
    subdivision_key integer NOT NULL,
    subdivision_name varchar,
    subdivision_type_key integer,
    subdivision_owner_key integer,
    dependent_subdivisions integer[],
    minioffice boolean DEFAULT false,
    included_in_service_office integer
)
WITH (oids = false);
--
-- Structure for table credit_cash_orders (OID = 472369) : 
--
CREATE TABLE asuz_db_local.credit_cash_orders (
    order_key integer NOT NULL,
    order_number varchar,
    order_type integer,
    loan_key integer NOT NULL,
    payment_sum numeric NOT NULL,
    borrower_name varchar NOT NULL,
    payment_date timestamp without time zone DEFAULT now() NOT NULL,
    user_name varchar DEFAULT "session_user"() NOT NULL,
    fixed boolean DEFAULT false,
    separate_subdivision_key varchar,
    subdivision_key integer,
    face3d varchar,
    discount numeric
)
WITH (oids = false);
--
-- Structure for table timelog (OID = 472402) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE TABLE loan_calc_history.timelog (
    key serial NOT NULL,
    description varchar,
    startcalc timestamp without time zone,
    endcalc timestamp without time zone,
    timecalc interval
)
WITH (oids = false);
--
-- Structure for table loan_percent (OID = 472556) : 
--
SET search_path = buh, pg_catalog;
CREATE TABLE buh.loan_percent (
    loan_percent_key serial NOT NULL,
    month integer,
    value numeric,
    days_min integer,
    days_max integer,
    year integer
)
WITH (oids = false);
--
-- Structure for table progress (OID = 472916) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE TABLE loan_calc_history.progress (
    progress_key serial NOT NULL,
    loan_version_key integer,
    total_days integer,
    current_day integer,
    creation_date timestamp without time zone DEFAULT clock_timestamp()
)
WITH (oids = false);
--
-- Structure for table list_participants_register (OID = 474556) : 
--
SET search_path = public, pg_catalog;
CREATE TABLE public.list_participants_register (
    list_key integer DEFAULT nextval('register_list_participants_list_key_seq'::regclass) NOT NULL,
    description varchar,
    closed_for_processing boolean,
    type_of_action integer,
    period_b date,
    period_e date
)
WITH (oids = false);
--
-- Structure for table special_offers_types (OID = 474564) : 
--
CREATE TABLE public.special_offers_types (
    special_offer_type_key integer,
    special_offer_name varchar,
    special_offer_description varchar,
    start_date date,
    end_date date,
    procedure_name varchar,
    id integer,
    only_new_client boolean,
    min_term_of_loan integer,
    max_term_of_loan integer,
    direct_discount boolean DEFAULT false,
    offer_category integer
)
WITH (oids = false);
--
-- Structure for table loan_restructuring_history (OID = 474587) : 
--
CREATE TABLE public.loan_restructuring_history (
    agreement_key integer,
    loan_key integer,
    agreement_date date,
    agreement_number integer,
    loan_sum numeric,
    loan_overpayment_original numeric,
    loan_overpayment numeric,
    loan_rate numeric,
    loan_term numeric,
    loan_returns numeric,
    nmb_of_payments integer,
    tariff_key integer,
    credit_product_key integer,
    discount_unused numeric,
    discount_compensation numeric,
    discount_special_offers numeric,
    discount_amount_overdue_fine numeric,
    discount_amount_overdue_overpayment numeric,
    discount_amount numeric,
    discount_amount_rs numeric,
    discount_amount_payment numeric,
    overdue_loand numeric,
    overdue_overpayment numeric,
    overdue_penalty numeric,
    overdue_overpayment_original numeric,
    overdue_penalty_original numeric,
    current_loan numeric,
    current_overpayment numeric,
    current_overpayment_original numeric,
    total_payment numeric,
    total_payment_original numeric,
    taken_money numeric,
    order_key integer,
    b_process_key integer,
    spec_offer_key integer,
    user_name varchar,
    permissible_loan_key integer,
    prepaid_loan numeric,
    prepaid_overpayment numeric,
    discount_loalty_perc numeric,
    new_sum_payment numeric,
    type_special_restructuring integer,
    fixed_payment_day integer,
    new_permissible_loan_key integer,
    payment_period interval,
    overdue_overpayment_for_delay_original numeric,
    overdue_overpayment_for_delay numeric,
    discount_amount_overdue_overpayment_for_delay numeric,
    rate_rs numeric,
    type_repayment_schedule_key integer,
    psk numeric,
    spec_offer_sum_paym numeric
)
WITH (oids = false);
--
-- Definition for foreign table special_offers (OID = 482618) : 
--
SET search_path = asuz_db, pg_catalog;
CREATE FOREIGN TABLE asuz_db.special_offers (
    spec_offer_key integer NOT NULL,
    regist_restr_key integer,
    latest_date_for_payment date,
    contract varchar,
    overpayment_to_pay numeric,
    fine_to_pay numeric,
    used boolean,
    loan_key integer,
    redmine_task varchar,
    process_name_key integer,
    start_date date
)
SERVER asuz01
OPTIONS (
  schema_name 'public',
  table_name 'special_offers');
--
-- Structure for table lock_calculation (OID = 586998) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE TABLE loan_calc_history.lock_calculation (
    lock_calculation_key serial NOT NULL,
    loan_key integer,
    create_date timestamp without time zone DEFAULT clock_timestamp()
)
WITH (oids = false);
--
-- Structure for table tmp_payment_schedules (OID = 588921) : 
--
SET search_path = asuz_db, pg_catalog;
CREATE TABLE asuz_db.tmp_payment_schedules (
    key serial NOT NULL,
    application_key integer,
    payment_date date,
    loan_sum integer,
    overpayment_sum integer
)
WITH (oids = false);
--
-- Structure for table current_calc (OID = 588974) : 
--
SET search_path = buh, pg_catalog;
CREATE TABLE buh.current_calc (
    current_calc_key serial NOT NULL,
    loan_key integer,
    loan_version_key integer,
    create_date timestamp without time zone DEFAULT clock_timestamp()
)
WITH (oids = false);
--
-- Structure for table t_xmin_usage (OID = 591871) : 
--
SET search_path = dwh, pg_catalog;
CREATE TABLE dwh.t_xmin_usage (
    name_table varchar(255),
    max_xmin bigint,
    mdate timestamp without time zone,
    type_id varchar(10)
)
WITH (oids = false);
--
-- Structure for table repayment_schedules_payment_date_ori (OID = 609142) : 
--
SET search_path = asuz_db, pg_catalog;
CREATE TABLE asuz_db.repayment_schedules_payment_date_ori (
    repayment_schedule_key integer,
    payment_date_ori date
)
WITH (oids = false);
--
-- Structure for table t_borrowers (OID = 792425) : 
--
CREATE TABLE asuz_db.t_borrowers (
    borrower_key integer DEFAULT nextval('borrowers_borrower_key_seq'::regclass) NOT NULL,
    settlement_key integer NOT NULL,
    regular_client boolean,
    do_not_notify boolean,
    last_name varchar NOT NULL,
    name varchar NOT NULL,
    patronimic varchar,
    birthday date NOT NULL,
    pass_series varchar NOT NULL,
    pass_number varchar NOT NULL,
    pass_issue varchar NOT NULL,
    pass_date date NOT NULL,
    pass_code varchar NOT NULL,
    email varchar,
    reg_city varchar,
    reg_postcode varchar,
    reg_street varchar,
    reg_house varchar,
    reg_housing varchar,
    reg_flat varchar,
    actual_address boolean,
    actual_city varchar,
    actual_postcode varchar,
    actual_street varchar,
    actual_house varchar,
    actual_housing varchar,
    actual_flat varchar,
    martial_status integer,
    spouse_last_name varchar,
    spouse_name varchar,
    spouse_patronimiic varchar,
    spouse_birthday date,
    have_children boolean,
    nmb_of_children integer,
    date_of_divorce date,
    regular_profit numeric NOT NULL,
    pensioner boolean,
    standing integer,
    nmb_of_work integer,
    work varchar,
    work_address varchar,
    work_post varchar,
    work_comment varchar,
    work2 varchar,
    work2_address varchar,
    work2_post varchar,
    work2_comment varchar,
    spouse_regular_profit numeric,
    spouse_works integer,
    spouse_work varchar,
    spouse_work_address varchar,
    guarantor_last_name varchar,
    guarantor_name varchar,
    guarantor_patronimic varchar,
    guarantor_who varchar,
    guarantor2_last_name varchar,
    guarantor2_name varchar,
    guarantor2_patronimic varchar,
    guarantor2_who varchar,
    blacklist boolean,
    blacklist_date date,
    blacklist_comment varchar,
    individual_entrepreneur boolean,
    ie_ogrn varchar,
    ie_inn varchar,
    ie_comment text,
    discount_enabled boolean,
    discount_card_type integer,
    discount_card_number varchar,
    collector_comment text,
    user_name varchar DEFAULT "session_user"(),
    creation_date timestamp without time zone DEFAULT now(),
    ie_business_state boolean,
    employment_history boolean,
    discount_recommended_borrower_card_number varchar,
    gender varchar(1),
    regular_profit_unofficial numeric,
    source_information integer,
    blacklist_external boolean DEFAULT false,
    in_blacklist_before_date date,
    subdivision_key integer,
    reg_city_key bigint,
    actual_city_key bigint,
    collector_address integer,
    snils varchar,
    snils_check_mode integer DEFAULT 0,
    birthplace varchar
)
WITH (oids = false);
--
-- Structure for table repayment_schedules (OID = 792436) : 
--
CREATE TABLE asuz_db.repayment_schedules (
    repayment_schedule_key integer DEFAULT nextval('repayment_schedules_repayment_schedule_key_seq'::regclass) NOT NULL,
    loan_key integer,
    payment_date date,
    loan_sum numeric,
    overpayment_sum numeric,
    delay boolean,
    debtor boolean,
    returned boolean,
    permissible_loan_key integer,
    returned_date date
)
WITH (oids = false);
--
-- Structure for table loans (OID = 792443) : 
--
CREATE TABLE asuz_db.loans (
    loan_key integer DEFAULT nextval('loans_loan_key_seq'::regclass) NOT NULL,
    borrower_key integer NOT NULL,
    for_what_purpose varchar,
    cause_of_failure varchar,
    priority_1 varchar,
    priority_2 varchar,
    priority_3 varchar,
    requested_amount numeric,
    requested_term integer,
    second_loan boolean,
    type_credit_product_key integer,
    test_pass_age boolean,
    test_pass_citizenship boolean,
    test_pass_registration boolean,
    test_pass_martial_status boolean,
    test_pass_nmb_of_children boolean,
    test_state_business integer,
    test_phone_mobile_phone boolean,
    test_phone_mobile_phone_spouse boolean,
    test_phone_third_person boolean,
    test_adds_call_home_phone boolean,
    test_adds_call_spouse boolean,
    test_adds_call_third_person boolean,
    test_fixed_income_call_work boolean,
    test_fixed_income_call_spouse boolean,
    test_fixed_income_call_third_person boolean,
    test_fixed_income_proof_of_income boolean,
    test_fixed_income_business_card boolean,
    test_fixed_income_pass boolean,
    test_fixed_income_civil_servant boolean,
    test_fixed_income_pension_certificate boolean,
    test_fixed_income_savings_book boolean,
    test_revenue_proof_of_income boolean,
    test_revenue_civil_servant boolean,
    test_revenue_call_spouse boolean,
    test_revenue_cash_book boolean,
    test_revenue_registration_vehicle boolean,
    test_revenue_pension_certificate boolean,
    test_revenue_savings_book boolean,
    test_revenue_declaration boolean,
    face_control integer,
    health integer,
    resolution integer,
    resolution_description text,
    loan_term integer,
    loan_sum numeric,
    loan_rate numeric,
    loan_plan_overpayment numeric,
    loan_plan_to_returns numeric,
    loan_fine_rate numeric,
    loan_penalty numeric,
    loan_maximum_delay integer,
    approved_user boolean,
    additional_confirmation boolean,
    additional_user varchar,
    approved_system integer,
    contract varchar,
    principial_contract integer,
    creation_date date DEFAULT ('now'::text)::date,
    printing_documents integer,
    issued boolean,
    delay boolean,
    debtor boolean,
    returned boolean,
    canceled boolean,
    canceled_decription varchar,
    user_name varchar DEFAULT "session_user"(),
    date_work timestamp without time zone DEFAULT now(),
    permissible_loan_key integer,
    agreed boolean,
    additional_agreement integer,
    requested_period integer,
    non_cash integer,
    calc_date timestamp without time zone,
    calc_user_name varchar,
    pre_agreed integer,
    test_utility_bills boolean,
    test_no_debt_another_organization boolean,
    loyalty_programm boolean,
    subdivision_key integer,
    test_adds_paid_receipt_terminal boolean DEFAULT false,
    reject_reason integer,
    test_revenue_osago boolean,
    cft_card_key integer,
    test_birth_certificate boolean DEFAULT false,
    test_valid_foreign_passport boolean DEFAULT false,
    test_house_book boolean DEFAULT false,
    test_statement_of_salary_account boolean DEFAULT false,
    cancel_reason varchar,
    restructuring integer,
    early_repayment boolean,
    application_key integer,
    closed_date date,
    debt_relieft_type integer DEFAULT 0,
    current_dpd integer DEFAULT 0,
    calc_stop_date date,
    calc_stop_reason varchar,
    claim_right integer,
    refinance integer
)
WITH (oids = false);
--
-- Structure for table loans_manual_correction (OID = 792460) : 
--
CREATE TABLE asuz_db.loans_manual_correction (
    lmc_key integer DEFAULT nextval('loans_manual_correction_lmc_key_seq'::regclass) NOT NULL,
    sum_cor_loan numeric,
    sum_cor_fine numeric,
    sum_cor_penalty numeric,
    loan_key integer,
    sum_cor_overpayment numeric,
    creation_date timestamp without time zone,
    comment varchar,
    user_name varchar,
    cco_key integer,
    backdated integer,
    reason integer,
    correction_type integer,
    sum_cor_overpayment_for_delay numeric
)
WITH (oids = false);
--
-- Structure for table register_records (OID = 792467) : 
--
CREATE TABLE asuz_db.register_records (
    register_records_key serial NOT NULL,
    register_key integer,
    payment_sum numeric,
    payment_date date,
    payer varchar,
    contract_number varchar,
    loan_key integer,
    date_work timestamp without time zone DEFAULT now(),
    user_name varchar DEFAULT "session_user"(),
    processed integer,
    fee numeric
)
WITH (oids = false);
--
-- Structure for table credit_cash_orders (OID = 792475) : 
--
CREATE TABLE asuz_db.credit_cash_orders (
    order_key integer DEFAULT nextval('credit_cash_orders_order_key_seq'::regclass) NOT NULL,
    order_number varchar,
    order_type integer,
    loan_key integer NOT NULL,
    payment_sum numeric NOT NULL,
    borrower_name varchar NOT NULL,
    payment_date timestamp without time zone DEFAULT now() NOT NULL,
    user_name varchar DEFAULT "session_user"() NOT NULL,
    fixed boolean DEFAULT false,
    separate_subdivision_key varchar,
    subdivision_key integer,
    face3d varchar,
    discount numeric
)
WITH (oids = false);
--
-- Structure for table payment (OID = 792485) : 
--
CREATE TABLE asuz_db.payment (
    payment_key integer DEFAULT nextval('payment_payment_key_seq'::regclass) NOT NULL,
    payment_date date,
    nmb_parts integer,
    payment_sum numeric,
    fixed boolean,
    loan_key integer,
    discount_reserved numeric,
    required_date date,
    date_changes date
)
WITH (oids = false);
--
-- Structure for table payment_part (OID = 792492) : 
--
CREATE TABLE asuz_db.payment_part (
    payment_part_key integer DEFAULT nextval('payment_part_payment_part_key_seq'::regclass) NOT NULL,
    payment_key integer,
    document_key integer
)
WITH (oids = false);
--
-- Structure for table facts (OID = 792496) : 
--
CREATE TABLE asuz_db.facts (
    fact_key serial NOT NULL,
    fact_date timestamp(6) without time zone,
    fact_name_key integer,
    obj_type integer,
    obj_key integer,
    loan_key integer,
    value numeric,
    user_key integer,
    subdivision_key integer,
    kod_podr varchar,
    additional_value integer,
    additional_value2 integer,
    creation_date timestamp without time zone DEFAULT (clock_timestamp())::timestamp without time zone NOT NULL
)
WITH (oids = false);
--
-- Structure for table loan_restructuring_history (OID = 792503) : 
--
CREATE TABLE asuz_db.loan_restructuring_history (
    agreement_key integer,
    loan_key integer,
    agreement_date date,
    agreement_number integer,
    loan_sum numeric,
    loan_overpayment_original numeric,
    loan_overpayment numeric,
    loan_rate numeric,
    loan_term numeric,
    loan_returns numeric,
    nmb_of_payments integer,
    tariff_key integer,
    credit_product_key integer,
    discount_unused numeric,
    discount_compensation numeric,
    discount_special_offers numeric,
    discount_amount_overdue_fine numeric,
    discount_amount_overdue_overpayment numeric,
    discount_amount numeric,
    discount_amount_rs numeric,
    discount_amount_payment numeric,
    overdue_loand numeric,
    overdue_overpayment numeric,
    overdue_penalty numeric,
    overdue_overpayment_original numeric,
    overdue_penalty_original numeric,
    current_loan numeric,
    current_overpayment numeric,
    current_overpayment_original numeric,
    total_payment numeric,
    total_payment_original numeric,
    taken_money numeric,
    order_key integer,
    b_process_key integer,
    spec_offer_key integer,
    user_name varchar,
    permissible_loan_key integer,
    prepaid_loan numeric,
    prepaid_overpayment numeric,
    discount_loalty_perc numeric,
    new_sum_payment numeric,
    type_special_restructuring integer,
    fixed_payment_day integer,
    new_permissible_loan_key integer,
    payment_period interval,
    overdue_overpayment_for_delay_original numeric,
    overdue_overpayment_for_delay numeric,
    discount_amount_overdue_overpayment_for_delay numeric,
    rate_rs numeric,
    type_repayment_schedule_key integer,
    psk numeric,
    spec_offer_sum_paym numeric
)
WITH (oids = false);
--
-- Structure for table loans_manual_correction_rs (OID = 792509) : 
--
CREATE TABLE asuz_db.loans_manual_correction_rs (
    lmc_rs_key integer DEFAULT nextval('loans_manual_correction_rs_lmc_rs_key_seq'::regclass) NOT NULL,
    lmc_key integer,
    repayment_schedule_key integer,
    sum_cor_loan numeric,
    sum_cor_overpayment numeric,
    sum_cor_fine numeric,
    sum_cor_penalty numeric,
    date_distribution date,
    date_accounting date,
    loan_key integer,
    reverse_object_key integer DEFAULT 0,
    sum_cor_overpayment_for_delay numeric
)
WITH (oids = false);
--
-- Structure for table permissible_loans (OID = 792517) : 
--
CREATE TABLE asuz_db.permissible_loans (
    key integer DEFAULT nextval('permissible_loans_key_seq'::regclass) NOT NULL,
    loan_key integer,
    tariff_key integer,
    period_key integer,
    period integer,
    max_amount numeric,
    rate numeric,
    refunds numeric,
    overpayment numeric,
    nmb_of_payments integer,
    sum_payment numeric,
    bonus numeric(8,2),
    tuning integer,
    discount_amount numeric,
    discount_loalty_perc numeric,
    fixed_payment_day integer,
    rate_rs numeric,
    type_repayment_schedule_key integer,
    version_number integer,
    version_date date,
    version_type integer,
    psk_percent numeric(8,3)
)
WITH (oids = false);
--
-- Structure for table setting_name (OID = 792524) : 
--
CREATE TABLE asuz_db.setting_name (
    setting_key integer DEFAULT nextval('setting_name_setting_key_seq'::regclass) NOT NULL,
    setting_name varchar,
    setting_data_type varchar,
    setting_comment varchar
)
WITH (oids = false);
--
-- Structure for table setting_values (OID = 792531) : 
--
CREATE TABLE asuz_db.setting_values (
    setting_value_key integer DEFAULT nextval('setting_values_setting_value_key_seq'::regclass) NOT NULL,
    setting_name_key integer,
    subdivision_key integer,
    value varchar,
    date_b date DEFAULT ('now'::text)::date,
    user_name varchar DEFAULT "session_user"(),
    date_work timestamp without time zone DEFAULT now()
)
WITH (oids = false);
--
-- Structure for table t_subdivisions (OID = 792541) : 
--
CREATE TABLE asuz_db.t_subdivisions (
    subdivision_key integer DEFAULT nextval('subdivisions_subdivision_key_seq'::regclass) NOT NULL,
    subdivision_name varchar,
    subdivision_type_key integer,
    subdivision_owner_key integer,
    dependent_subdivisions integer[],
    minioffice boolean DEFAULT false,
    included_in_service_office integer
)
WITH (oids = false);
--
-- Structure for table partial_early_agreement (OID = 792549) : 
--
CREATE TABLE asuz_db.partial_early_agreement (
    partial_early_agreement_key integer DEFAULT nextval('partial_early_agreement_partial_early_agreement_key_seq'::regclass) NOT NULL,
    loan_key integer,
    partial_early_date date,
    old_permissible_loan_key integer,
    start_date_new_repayment_schedule date,
    agreed_payment_sum numeric,
    payment_date date[],
    payment_sum integer[],
    payment_key integer[],
    status integer,
    fixed_date date,
    cancelled boolean DEFAULT false
)
WITH (oids = false);
--
-- Structure for table list_participants (OID = 792557) : 
--
CREATE TABLE asuz_db.list_participants (
    key serial NOT NULL,
    list_key integer,
    special_offer_type_key integer,
    loan_key integer,
    to_pay_overpayment numeric,
    to_pat_total numeric,
    to_pay_overpayment_actual numeric,
    to_pay_total_actual numeric,
    discount numeric,
    period_b date,
    period_e date,
    paid boolean,
    created_special boolean,
    to_pay_loan numeric,
    to_pay_fine numeric,
    to_pay_loan_actual numeric,
    to_pay_fine_actual numeric
)
WITH (oids = false);
--
-- Structure for table list_participants_register (OID = 792563) : 
--
CREATE TABLE asuz_db.list_participants_register (
    list_key integer DEFAULT nextval('register_list_participants_list_key_seq'::regclass) NOT NULL,
    description varchar,
    closed_for_processing boolean,
    type_of_action integer,
    period_b date,
    period_e date
)
WITH (oids = false);
--
-- Structure for table special_offers_types (OID = 792569) : 
--
CREATE TABLE asuz_db.special_offers_types (
    special_offer_type_key integer,
    special_offer_name varchar,
    special_offer_description varchar,
    start_date date,
    end_date date,
    procedure_name varchar,
    id integer,
    only_new_client boolean,
    min_term_of_loan integer,
    max_term_of_loan integer,
    direct_discount boolean DEFAULT false,
    offer_category integer
)
WITH (oids = false);
--
-- Definition for index idx__buh_account_transaction__account_key_credit__1 (OID = 466224) : 
--
SET search_path = buh, pg_catalog;
CREATE INDEX idx__buh_account_transaction__account_key_credit__1 ON account_transaction USING btree (account_key_credit);
--
-- Definition for index idx__buh_account_transaction__account_key_debet__1 (OID = 466225) : 
--
CREATE INDEX idx__buh_account_transaction__account_key_debet__1 ON account_transaction USING btree (account_key_debet);
--
-- Definition for index idx__buh_balance_value__loan_key__transact_date__opened__1 (OID = 466262) : 
--
CREATE INDEX idx__buh_balance_value__loan_key__transact_date__opened__1 ON balance_value USING btree (loan_key, transact_date, opened);
--
-- Definition for index idx__buh_loan_transact__account_transaction_key__1 (OID = 466276) : 
--
CREATE INDEX idx__buh_loan_transact__account_transaction_key__1 ON loan_transact USING btree (account_transaction_key);
--
-- Definition for index idx__buh_loan_transact__loan_key__transact_datee__fact_key__1 (OID = 466277) : 
--
CREATE INDEX idx__buh_loan_transact__loan_key__transact_datee__fact_key__1 ON loan_transact USING btree (loan_key, transact_date, fact_key);
--
-- Definition for index documents_idx (OID = 466307) : 
--
SET search_path = facts, pg_catalog;
CREATE INDEX documents_idx ON facts USING btree (fact_name_key, obj_type, obj_key);
--
-- Definition for index documents_idx2 (OID = 466308) : 
--
CREATE INDEX documents_idx2 ON facts USING btree (loan_key, fact_date, fact_name_key);
--
-- Definition for index idx__public_separate_subdivision__date_b__1 (OID = 466340) : 
--
SET search_path = public, pg_catalog;
CREATE INDEX idx__public_separate_subdivision__date_b__1 ON separate_subdivision USING btree (date_b);
--
-- Definition for index idx__public_separate_subdivision__separate_subdivision__1 (OID = 466341) : 
--
CREATE INDEX idx__public_separate_subdivision__separate_subdivision__1 ON separate_subdivision USING btree (separate_subdivision);
--
-- Definition for index idx__public_separate_subdivision__subdivision_key__1 (OID = 466342) : 
--
CREATE INDEX idx__public_separate_subdivision__subdivision_key__1 ON separate_subdivision USING btree (subdivision_key);
--
-- Definition for index balance_value_history_idx__buh_balance_value__loan_key__transac (OID = 471623) : 
--
SET search_path = loan_calc_history, pg_catalog;
CREATE INDEX balance_value_history_idx__buh_balance_value__loan_key__transac ON balance_value_history USING btree (loan_key, transact_date, opened);
--
-- Definition for index loan_transact_history_idx__buh_loan_transact__account_transacti (OID = 471637) : 
--
CREATE INDEX loan_transact_history_idx__buh_loan_transact__account_transacti ON loan_transact_history USING btree (account_transaction_key);
--
-- Definition for index loan_transact_history_idx__buh_loan_transact__loan_key__transac (OID = 471638) : 
--
CREATE INDEX loan_transact_history_idx__buh_loan_transact__loan_key__transac ON loan_transact_history USING btree (loan_key, transact_date, fact_key);
--
-- Definition for index balance_value_history_idx (OID = 578594) : 
--
CREATE INDEX balance_value_history_idx ON balance_value_history USING btree (loan_version_key);
--
-- Definition for index loan_transact_history_idx (OID = 579032) : 
--
CREATE INDEX loan_transact_history_idx ON loan_transact_history USING btree (loan_version_key);
--
-- Definition for index loan_transact_history_idx5 (OID = 579033) : 
--
CREATE INDEX loan_transact_history_idx5 ON loan_transact_history USING btree (loan_version_key, transact_date, active, account_transaction_key);
--
-- Definition for index asuz_db_tmp_payment_schedules_idx1 (OID = 588927) : 
--
SET search_path = asuz_db, pg_catalog;
CREATE INDEX asuz_db_tmp_payment_schedules_idx1 ON tmp_payment_schedules USING btree (application_key);
--
-- Definition for index repayment_schedules_payment_date_ori_idx (OID = 609145) : 
--
CREATE INDEX repayment_schedules_payment_date_ori_idx ON repayment_schedules_payment_date_ori USING btree (repayment_schedule_key);
--
-- Definition for index repayment_schedules_payment_date_ori_idx1 (OID = 609603) : 
--
CREATE INDEX repayment_schedules_payment_date_ori_idx1 ON repayment_schedules_payment_date_ori USING btree (payment_date_ori);
--
-- Definition for index repayment_schedules_payment_date_ori_idx2 (OID = 609605) : 
--
CREATE INDEX repayment_schedules_payment_date_ori_idx2 ON repayment_schedules_payment_date_ori USING btree (repayment_schedule_key, payment_date_ori);
--
-- Definition for index documents_idx (OID = 792702) : 
--
CREATE INDEX documents_idx ON facts USING btree (fact_name_key, obj_type, obj_key);
--
-- Definition for index documents_idx2 (OID = 792704) : 
--
CREATE INDEX documents_idx2 ON facts USING btree (loan_key, fact_date, fact_name_key);
--
-- Definition for index documents_idx3 (OID = 792705) : 
--
CREATE INDEX documents_idx3 ON facts USING btree (fact_key);
--
-- Definition for index payment_idx (OID = 792706) : 
--
CREATE INDEX payment_idx ON payment USING btree (loan_key, payment_date);
--
-- Definition for index payment_idx1 (OID = 792707) : 
--
CREATE INDEX payment_idx1 ON payment USING btree (payment_key);
--
-- Definition for index payment_part_idx (OID = 792708) : 
--
CREATE INDEX payment_part_idx ON payment_part USING btree (document_key);
--
-- Definition for index payment_part_idx1 (OID = 792709) : 
--
CREATE INDEX payment_part_idx1 ON payment_part USING btree (payment_key);
--
-- Definition for index credit_cash_orders_idx (OID = 792710) : 
--
CREATE INDEX credit_cash_orders_idx ON credit_cash_orders USING btree (order_type, subdivision_key);
--
-- Definition for index credit_cash_orders_idx1 (OID = 792711) : 
--
CREATE INDEX credit_cash_orders_idx1 ON credit_cash_orders USING btree (loan_key);
--
-- Definition for index credit_cash_orders_sep_sd_key_idx (OID = 792712) : 
--
CREATE INDEX credit_cash_orders_sep_sd_key_idx ON credit_cash_orders USING btree (separate_subdivision_key);
--
-- Definition for index loan_restructuring_history_idx (OID = 792719) : 
--
CREATE INDEX loan_restructuring_history_idx ON loan_restructuring_history USING btree (loan_key);
--
-- Definition for index loan_restructuring_history_idx2 (OID = 792720) : 
--
CREATE INDEX loan_restructuring_history_idx2 ON loan_restructuring_history USING btree (agreement_key);
--
-- Definition for index loans_idx (OID = 792721) : 
--
CREATE INDEX loans_idx ON loans USING btree (borrower_key);
--
-- Definition for index loans_idx1 (OID = 792722) : 
--
CREATE INDEX loans_idx1 ON loans USING btree (creation_date);
--
-- Definition for index loans_idx2 (OID = 792723) : 
--
CREATE INDEX loans_idx2 ON loans USING btree (contract varchar_pattern_ops);
--
-- Definition for index loans_idx_3 (OID = 792724) : 
--
CREATE INDEX loans_idx_3 ON loans USING btree (loan_key, subdivision_key, current_dpd);
--
-- Definition for index loans_idx_4 (OID = 792725) : 
--
CREATE INDEX loans_idx_4 ON loans USING btree (subdivision_key, issued, (COALESCE(returned, false)), current_dpd);
--
-- Definition for index loans_idx_5 (OID = 792727) : 
--
CREATE INDEX loans_idx_5 ON loans USING btree (permissible_loan_key);
--
-- Definition for index loans_idx_application (OID = 792728) : 
--
CREATE INDEX loans_idx_application ON loans USING btree (application_key);
--
-- Definition for index loans_manual_correction_idx (OID = 792729) : 
--
CREATE INDEX loans_manual_correction_idx ON loans_manual_correction USING btree (lmc_key);
--
-- Definition for index loans_manual_correction_idx1 (OID = 792730) : 
--
CREATE INDEX loans_manual_correction_idx1 ON loans_manual_correction USING btree (cco_key, reason);
--
-- Definition for index loans_manual_correction_idx2 (OID = 792731) : 
--
CREATE INDEX loans_manual_correction_idx2 ON loans_manual_correction USING btree (loan_key);
--
-- Definition for index loans_manual_correction_rs_idx (OID = 792732) : 
--
CREATE INDEX loans_manual_correction_rs_idx ON loans_manual_correction_rs USING btree (lmc_key);
--
-- Definition for index loans_manual_correction_rs_idx1 (OID = 792733) : 
--
CREATE INDEX loans_manual_correction_rs_idx1 ON loans_manual_correction_rs USING btree (repayment_schedule_key);
--
-- Definition for index loans_manual_correction_rs_idx2 (OID = 792734) : 
--
CREATE INDEX loans_manual_correction_rs_idx2 ON loans_manual_correction_rs USING btree (loan_key);
--
-- Definition for index loans_manual_correction_rs_idx3 (OID = 792735) : 
--
CREATE INDEX loans_manual_correction_rs_idx3 ON loans_manual_correction_rs USING btree (reverse_object_key);
--
-- Definition for index loans_manual_correction_rs_idx4 (OID = 792736) : 
--
CREATE INDEX loans_manual_correction_rs_idx4 ON loans_manual_correction_rs USING btree (lmc_rs_key);
--
-- Definition for index permissible_loans_loan_key_idx (OID = 792737) : 
--
CREATE INDEX permissible_loans_loan_key_idx ON permissible_loans USING btree (loan_key);
--
-- Definition for index permissible_loans_type_repayment_schedule_key_idx (OID = 792738) : 
--
CREATE INDEX permissible_loans_type_repayment_schedule_key_idx ON permissible_loans USING btree (type_repayment_schedule_key);
--
-- Definition for index public_credit_cash_orders_idx2 (OID = 792739) : 
--
CREATE INDEX public_credit_cash_orders_idx2 ON credit_cash_orders USING btree (date_trunc('day'::text, payment_date), user_name);
--
-- Definition for index repayment_schedules_idx (OID = 792740) : 
--
CREATE INDEX repayment_schedules_idx ON repayment_schedules USING btree (repayment_schedule_key);
--
-- Definition for index repayment_schedules_idx1 (OID = 792741) : 
--
CREATE INDEX repayment_schedules_idx1 ON repayment_schedules USING btree (loan_key);
--
-- Definition for index repayment_schedules_idx2 (OID = 792742) : 
--
CREATE INDEX repayment_schedules_idx2 ON repayment_schedules USING btree (permissible_loan_key);
--
-- Definition for index repayment_schedules_idx3 (OID = 792747) : 
--
CREATE INDEX repayment_schedules_idx3 ON repayment_schedules USING btree (permissible_loan_key, repayment_schedule_key);
--
-- Definition for index repayment_schedules_idx4 (OID = 792748) : 
--
CREATE INDEX repayment_schedules_idx4 ON repayment_schedules USING btree (permissible_loan_key, payment_date);
--
-- Definition for index repayment_schedules_returned_idx (OID = 792749) : 
--
CREATE INDEX repayment_schedules_returned_idx ON repayment_schedules USING btree (returned);
--
-- Definition for index setting_name_idx (OID = 792752) : 
--
CREATE INDEX setting_name_idx ON setting_name USING btree (setting_key);
--
-- Definition for index setting_name_idx1 (OID = 792753) : 
--
CREATE INDEX setting_name_idx1 ON setting_name USING btree (setting_name);
--
-- Definition for index setting_values_idx (OID = 792754) : 
--
CREATE INDEX setting_values_idx ON setting_values USING btree (setting_value_key);
--
-- Definition for index setting_values_idx1 (OID = 792755) : 
--
CREATE INDEX setting_values_idx1 ON setting_values USING btree (subdivision_key, setting_name_key);
--
-- Definition for index t_borrowers_idx (OID = 792756) : 
--
CREATE INDEX t_borrowers_idx ON t_borrowers USING btree (last_name varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx1 (OID = 792757) : 
--
CREATE INDEX t_borrowers_idx1 ON t_borrowers USING btree (name varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx10 (OID = 792758) : 
--
CREATE INDEX t_borrowers_idx10 ON t_borrowers USING btree (upper((guarantor_patronimic)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx11 (OID = 792759) : 
--
CREATE INDEX t_borrowers_idx11 ON t_borrowers USING btree (upper((guarantor2_last_name)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx12 (OID = 792760) : 
--
CREATE INDEX t_borrowers_idx12 ON t_borrowers USING btree (upper((guarantor2_name)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx13 (OID = 792761) : 
--
CREATE INDEX t_borrowers_idx13 ON t_borrowers USING btree (upper((guarantor2_patronimic)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx14 (OID = 792762) : 
--
CREATE INDEX t_borrowers_idx14 ON t_borrowers USING btree (borrower_key, settlement_key);
--
-- Definition for index t_borrowers_idx15 (OID = 792763) : 
--
CREATE INDEX t_borrowers_idx15 ON t_borrowers USING btree (pass_series, pass_number);
--
-- Definition for index t_borrowers_idx16 (OID = 792764) : 
--
CREATE INDEX t_borrowers_idx16 ON t_borrowers USING btree (subdivision_key);
--
-- Definition for index t_borrowers_idx17 (OID = 792765) : 
--
CREATE INDEX t_borrowers_idx17 ON t_borrowers USING btree (subdivision_key, borrower_key);
--
-- Definition for index t_borrowers_idx18 (OID = 792766) : 
--
CREATE INDEX t_borrowers_idx18 ON t_borrowers USING btree (birthday);
--
-- Definition for index t_borrowers_idx2 (OID = 792767) : 
--
CREATE INDEX t_borrowers_idx2 ON t_borrowers USING btree (patronimic varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx3 (OID = 792768) : 
--
CREATE INDEX t_borrowers_idx3 ON t_borrowers USING btree (settlement_key);
--
-- Definition for index t_borrowers_idx4 (OID = 792769) : 
--
CREATE INDEX t_borrowers_idx4 ON t_borrowers USING btree (upper((last_name)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx5 (OID = 792770) : 
--
CREATE INDEX t_borrowers_idx5 ON t_borrowers USING btree (settlement_key, borrower_key);
--
-- Definition for index t_borrowers_idx6 (OID = 792771) : 
--
CREATE INDEX t_borrowers_idx6 ON t_borrowers USING btree (upper((name)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx7 (OID = 792772) : 
--
CREATE INDEX t_borrowers_idx7 ON t_borrowers USING btree (upper((patronimic)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx8 (OID = 792773) : 
--
CREATE INDEX t_borrowers_idx8 ON t_borrowers USING btree (upper((guarantor_last_name)::text) varchar_pattern_ops);
--
-- Definition for index t_borrowers_idx9 (OID = 792774) : 
--
CREATE INDEX t_borrowers_idx9 ON t_borrowers USING btree (upper((guarantor_name)::text) varchar_pattern_ops);
--
-- Definition for index t_subdivisions_idx (OID = 792775) : 
--
CREATE INDEX t_subdivisions_idx ON t_subdivisions USING btree (subdivision_key);
--
-- Definition for index t_subdivisions_idx1 (OID = 792776) : 
--
CREATE INDEX t_subdivisions_idx1 ON t_subdivisions USING btree (subdivision_owner_key);
--
-- Definition for index register_records_idx (OID = 792777) : 
--
CREATE INDEX register_records_idx ON register_records USING btree (register_records_key);
--
-- Definition for index register_records_idx1 (OID = 792778) : 
--
CREATE INDEX register_records_idx1 ON register_records USING btree (register_key);
--
-- Definition for index register_records_idx2 (OID = 792779) : 
--
CREATE INDEX register_records_idx2 ON register_records USING btree (loan_key);
--
-- Definition for index register_records_idx3 (OID = 792780) : 
--
CREATE INDEX register_records_idx3 ON register_records USING btree ((((-1) * register_records_key)));
--
-- Definition for index register_records_idx4 (OID = 792781) : 
--
CREATE INDEX register_records_idx4 ON register_records USING btree (contract_number);
--
-- Definition for index special_offers_list_participants_idx1 (OID = 792782) : 
--
CREATE INDEX special_offers_list_participants_idx1 ON list_participants USING btree (key);
--
-- Definition for index special_offers_list_participants_idx2 (OID = 792783) : 
--
CREATE INDEX special_offers_list_participants_idx2 ON list_participants USING btree (loan_key);
--
-- Definition for index account_pkey (OID = 466199) : 
--
SET search_path = buh, pg_catalog;
ALTER TABLE ONLY account
    ADD CONSTRAINT account_pkey
    PRIMARY KEY ("aссount_key");
--
-- Definition for index account_code_pkey (OID = 466210) : 
--
ALTER TABLE ONLY account_code
    ADD CONSTRAINT account_code_pkey
    PRIMARY KEY (account_code_key);
--
-- Definition for index account_transaction_pkey (OID = 466222) : 
--
ALTER TABLE ONLY account_transaction
    ADD CONSTRAINT account_transaction_pkey
    PRIMARY KEY (account_transaction_key);
--
-- Definition for index account_transaction_1c_pkey (OID = 466236) : 
--
ALTER TABLE ONLY account_transaction_1c
    ADD CONSTRAINT account_transaction_1c_pkey
    PRIMARY KEY (account_transaction_key);
--
-- Definition for index stat_tables_count_pkey (OID = 466246) : 
--
ALTER TABLE ONLY stat_tables_count
    ADD CONSTRAINT stat_tables_count_pkey
    PRIMARY KEY (stat_tables_count_key);
--
-- Definition for index balance_value_pkey (OID = 466260) : 
--
ALTER TABLE ONLY balance_value
    ADD CONSTRAINT balance_value_pkey
    PRIMARY KEY (balance_value_key);
--
-- Definition for index loan_transact_pkey (OID = 466274) : 
--
ALTER TABLE ONLY loan_transact
    ADD CONSTRAINT loan_transact_pkey
    PRIMARY KEY (loan_transact_key);
--
-- Definition for index transact_calc_log_pkey (OID = 466288) : 
--
ALTER TABLE ONLY transact_calc_log
    ADD CONSTRAINT transact_calc_log_pkey
    PRIMARY KEY (transact_calc_log_key);
--
-- Definition for index saparate_subdivision_pkey (OID = 466338) : 
--
SET search_path = public, pg_catalog;
ALTER TABLE ONLY separate_subdivision
    ADD CONSTRAINT saparate_subdivision_pkey
    PRIMARY KEY (saparate_subdivision_key);
--
-- Definition for index app_data_pkey (OID = 471167) : 
--
SET search_path = repair_data, pg_catalog;
ALTER TABLE ONLY app_data
    ADD CONSTRAINT app_data_pkey
    PRIMARY KEY (app_data_key);
--
-- Definition for index app_data_online_pkey (OID = 471179) : 
--
ALTER TABLE ONLY app_data_online
    ADD CONSTRAINT app_data_online_pkey
    PRIMARY KEY (app_data_online_key);
--
-- Definition for index loan_version_pkey (OID = 471607) : 
--
SET search_path = loan_calc_history, pg_catalog;
ALTER TABLE ONLY loan_version
    ADD CONSTRAINT loan_version_pkey
    PRIMARY KEY (loan_version_key);
--
-- Definition for index balance_value_history_pkey (OID = 471621) : 
--
ALTER TABLE ONLY balance_value_history
    ADD CONSTRAINT balance_value_history_pkey
    PRIMARY KEY (balance_value_key);
--
-- Definition for index loan_transact_history_pkey (OID = 471635) : 
--
ALTER TABLE ONLY loan_transact_history
    ADD CONSTRAINT loan_transact_history_pkey
    PRIMARY KEY (loan_transact_key);
--
-- Definition for index timelog_pkey (OID = 472409) : 
--
ALTER TABLE ONLY timelog
    ADD CONSTRAINT timelog_pkey
    PRIMARY KEY (key);
--
-- Definition for index loan_percent_pkey (OID = 472563) : 
--
SET search_path = buh, pg_catalog;
ALTER TABLE ONLY loan_percent
    ADD CONSTRAINT loan_percent_pkey
    PRIMARY KEY (loan_percent_key);
--
-- Definition for index progress_pkey (OID = 472921) : 
--
SET search_path = loan_calc_history, pg_catalog;
ALTER TABLE ONLY progress
    ADD CONSTRAINT progress_pkey
    PRIMARY KEY (progress_key);
--
-- Definition for index lock_calculation_pkey (OID = 587003) : 
--
ALTER TABLE ONLY lock_calculation
    ADD CONSTRAINT lock_calculation_pkey
    PRIMARY KEY (lock_calculation_key);
--
-- Definition for index tmp_payment_schedules_pkey (OID = 588925) : 
--
SET search_path = asuz_db, pg_catalog;
ALTER TABLE ONLY tmp_payment_schedules
    ADD CONSTRAINT tmp_payment_schedules_pkey
    PRIMARY KEY (key);
--
-- Definition for index current_calc_pkey (OID = 588979) : 
--
SET search_path = buh, pg_catalog;
ALTER TABLE ONLY current_calc
    ADD CONSTRAINT current_calc_pkey
    PRIMARY KEY (current_calc_key);
--
-- Definition for index borrowers_pkey (OID = 792694) : 
--
SET search_path = asuz_db, pg_catalog;
ALTER TABLE ONLY t_borrowers
    ADD CONSTRAINT borrowers_pkey
    PRIMARY KEY (borrower_key);
--
-- Definition for index credit_cash_orders_pkey (OID = 792696) : 
--
ALTER TABLE ONLY credit_cash_orders
    ADD CONSTRAINT credit_cash_orders_pkey
    PRIMARY KEY (order_key);
--
-- Definition for index loans_pkey (OID = 792698) : 
--
ALTER TABLE ONLY loans
    ADD CONSTRAINT loans_pkey
    PRIMARY KEY (loan_key);
--
-- Definition for index permissible_loans_pkey (OID = 792700) : 
--
ALTER TABLE ONLY permissible_loans
    ADD CONSTRAINT permissible_loans_pkey
    PRIMARY KEY (key);
--
-- Comments
--
SET search_path = buh, pg_catalog;
COMMENT ON COLUMN buh.account."aссount_key" IS 'Первичный ключ';
COMMENT ON COLUMN buh.account.name IS 'Название счета';
COMMENT ON COLUMN buh.account_code.account_code_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.account_code.account_key IS 'Ссылка на buh.account (счет)';
COMMENT ON COLUMN buh.account_code.code IS 'Код счета';
COMMENT ON COLUMN buh.account_code.date_start IS 'Дата начала действия кода';
COMMENT ON COLUMN buh.account_code.date_end IS 'Дата окончания действия кода';
COMMENT ON COLUMN buh.account_transaction.account_transaction_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.account_transaction.account_transaction_1c_key IS 'Ссылка на таблицу account_transaction_1c';
COMMENT ON COLUMN buh.account_transaction.name IS 'Название проводки';
COMMENT ON COLUMN buh.account_transaction.account_key_debet IS 'Дебет';
COMMENT ON COLUMN buh.account_transaction.account_key_credit IS 'Кредит';
COMMENT ON COLUMN buh.account_transaction.description IS 'Описание';
COMMENT ON COLUMN buh.account_transaction."order" IS 'Ключ сортировки';
COMMENT ON COLUMN buh.account_transaction.storno IS 'Сторно';
COMMENT ON COLUMN buh.account_transaction_1c.account_transaction_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.account_transaction_1c.name IS 'Название проводки';
COMMENT ON COLUMN buh.account_transaction_1c.account_key_debet IS 'Дебет';
COMMENT ON COLUMN buh.account_transaction_1c.account_key_credit IS 'Кредит';
COMMENT ON COLUMN buh.account_transaction_1c.description IS 'Описание';
COMMENT ON COLUMN buh.account_transaction_1c."order" IS 'Ключ сортировки';
COMMENT ON COLUMN buh.account_transaction_1c.storno IS 'Сторно';
COMMENT ON COLUMN buh.stat_tables_count.stat_tables_count_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.stat_tables_count.create_date IS 'Дата создания записи';
COMMENT ON COLUMN buh.stat_tables_count.user_key IS 'Ключ пользователя';
COMMENT ON COLUMN buh.stat_tables_count.balance_value_count_b IS 'Количество балансовых величин на начало рассчета';
COMMENT ON COLUMN buh.stat_tables_count.balance_value_count_e IS 'Количество балансовых величин на конец рассчета';
COMMENT ON COLUMN buh.stat_tables_count.loans_count IS 'Количество записей в таблице public.loans';
COMMENT ON COLUMN buh.stat_tables_count.repayment_schedules_count IS 'Количество записей в таблице public.repayment_schedules на момент рассчета';
COMMENT ON COLUMN buh.stat_tables_count.permissible_loans_count IS 'Количество записей в таблице public.permissible_loans';
COMMENT ON COLUMN buh.stat_tables_count.separate_subdivision_count IS 'Количество записей в таблице public.separate_subdivision';
COMMENT ON COLUMN buh.stat_tables_count.loan_transact_count_b IS 'Количество записей в таблице loan_transact на начело рассчета';
COMMENT ON COLUMN buh.stat_tables_count.loan_transact_count_e IS 'Количество записей в таблице loan_transact на конец рассчета';
COMMENT ON COLUMN buh.stat_tables_count.transact_calc_log_count_b IS 'Количество записей в таблице transact_calc_log на начало рассчета';
COMMENT ON COLUMN buh.stat_tables_count.transact_calc_log_count_e IS 'Количество записей в таблице transact_calc_log на конец рассчета';
COMMENT ON COLUMN buh.balance_value.balance_value_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.balance_value.loan_key IS 'Займ. Ссылка на public.loans';
COMMENT ON COLUMN buh.balance_value.od_loan_b IS 'Проср. тело займа на начало (58.03.02)';
COMMENT ON COLUMN buh.balance_value.od_loan_e IS 'Проср. тело займа на конец (58.03.02)';
COMMENT ON COLUMN buh.balance_value.od_overpayment_planned_b IS 'Проср. % плановые на начало (76.03.02)';
COMMENT ON COLUMN buh.balance_value.od_overpayment_planned_e IS 'Проср. % плановые на конец (76.03.02)';
COMMENT ON COLUMN buh.balance_value.od_overpayment_for_delay_b IS 'Проср. % доначисленные на начало (76.03.03)';
COMMENT ON COLUMN buh.balance_value.od_overpayment_for_delay_e IS 'Проср. % доначисленные на конец (76.03.03)';
COMMENT ON COLUMN buh.balance_value.od_fine_b IS 'Проср. % пени на начало (76.05.02)';
COMMENT ON COLUMN buh.balance_value.od_fine_e IS 'Проср. % пени на конец (76.05.02)';
COMMENT ON COLUMN buh.balance_value.transact_date IS 'Дата рассчета';
COMMENT ON COLUMN buh.balance_value.create_date IS 'Дата создания записи';
COMMENT ON COLUMN buh.balance_value.n_loan_b IS 'Непросроченое тело займа на начало (58.03.01)';
COMMENT ON COLUMN buh.balance_value.n_loan_e IS 'Непросроченое тело займа на конец (58.03.01)';
COMMENT ON COLUMN buh.balance_value.n_overpayment_planned_b IS 'Непросроченные % на начало (76.03.01)';
COMMENT ON COLUMN buh.balance_value.n_overpayment_planned_e IS 'Непросроченные % на конец (76.03.01)';
COMMENT ON COLUMN buh.balance_value.loan_b IS 'Тело займа на начало 58.03';
COMMENT ON COLUMN buh.balance_value.loan_e IS 'Тело займа на начало 58.03';
COMMENT ON COLUMN buh.balance_value.client_balance_b IS 'Баланс клиента на начало (76.09)';
COMMENT ON COLUMN buh.balance_value.client_balance_e IS 'Баланс клиента на конец (76.09)';
COMMENT ON COLUMN buh.balance_value.active IS 'Активная запись';
COMMENT ON COLUMN buh.balance_value.opened IS 'Открыт ли займ';
COMMENT ON COLUMN buh.balance_value.n_overpayment_conditionally_b IS 'Условно непросроченные проценты на начало (76.03.04)';
COMMENT ON COLUMN buh.balance_value.n_overpayment_conditionally_e IS 'Условно непросроченные проценты на конец(76.03.04)';
COMMENT ON COLUMN buh.balance_value.all_loan_percents_b IS 'Cумма всех начисленных процентов по займу на начало';
COMMENT ON COLUMN buh.balance_value.all_loan_percents_e IS 'Cумма всех начисленных процентов по займу на конец';
COMMENT ON COLUMN buh.loan_transact.loan_transact_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.loan_transact.loan_key IS 'Займ. Ссылка на public.loans';
COMMENT ON COLUMN buh.loan_transact.separate_subdivision_key IS 'Подразделение';
COMMENT ON COLUMN buh.loan_transact.summ IS 'Сумма проводки';
COMMENT ON COLUMN buh.loan_transact.fact_key IS 'Ссылка на facts.facts';
COMMENT ON COLUMN buh.loan_transact.account_transaction_key IS 'Ключ транзакции. Ссылка на buh.account_transaction';
COMMENT ON COLUMN buh.loan_transact.transact_date IS 'Дата рассчета';
COMMENT ON COLUMN buh.loan_transact.create_date IS 'Дата создания записи';
COMMENT ON COLUMN buh.loan_transact.active IS 'Активная запись';
COMMENT ON COLUMN buh.transact_calc_log.transact_calc_log_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.transact_calc_log.loan_key IS 'Ключ займа';
COMMENT ON COLUMN buh.transact_calc_log.date IS 'Дата рассчета';
COMMENT ON COLUMN buh.transact_calc_log.procedure IS 'Хранимка, в которой произошла ошибка';
COMMENT ON COLUMN buh.transact_calc_log.message IS 'Сообщение об ошибке';
COMMENT ON COLUMN buh.transact_calc_log.create_date IS 'Дата создания записи';
SET search_path = facts, pg_catalog;
COMMENT ON COLUMN facts.fact_names.fact_name_key IS 'ключ записи';
COMMENT ON COLUMN facts.fact_names.fact_name IS 'Наименование события';
COMMENT ON COLUMN facts.fact_names.func_register_fact IS 'Наименование функции для регистрации события';
COMMENT ON COLUMN facts.facts.subdivision_key IS 'Ключ подразделения';
SET search_path = public, pg_catalog;
COMMENT ON COLUMN public.separate_subdivision.saparate_subdivision_key IS 'Первичный ключ';
COMMENT ON COLUMN public.separate_subdivision.separate_subdivision IS 'Название подразделения';
COMMENT ON COLUMN public.separate_subdivision.subdivision_key IS 'Номер подразделения';
COMMENT ON COLUMN public.separate_subdivision.date_b IS 'Дата начала работы подразделения';
SET search_path = loan_calc_history, pg_catalog;
COMMENT ON COLUMN loan_calc_history.loan_version.loan_version_key IS 'Первичный ключ';
COMMENT ON COLUMN loan_calc_history.loan_version.creation_date IS 'Время создания записи';
COMMENT ON COLUMN loan_calc_history.loan_version.description IS 'Описание';
COMMENT ON COLUMN loan_calc_history.loan_version.nopeni IS 'Расчет без пени';
COMMENT ON COLUMN loan_calc_history.loan_version.first_payment_schedule IS 'Рассчитать по первому графику платежей на момент выдачи (из таблицы loan_issue.tmp_payment_schedules)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.balance_value_key IS 'Первичный ключ';
COMMENT ON COLUMN loan_calc_history.balance_value_history.loan_key IS 'Займ. Ссылка на public.loans';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_loan_b IS 'Проср. тело займа на начало (58.03.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_loan_e IS 'Проср. тело займа на конец (58.03.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_overpayment_planned_b IS 'Проср. % плановые на начало (76.03.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_overpayment_planned_e IS 'Проср. % плановые на конец (76.03.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_overpayment_for_delay_b IS 'Проср. % доначисленные на начало (76.03.03)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_overpayment_for_delay_e IS 'Проср. % доначисленные на конец (76.03.03)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_fine_b IS 'Проср. % пени на начало (76.05.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.od_fine_e IS 'Проср. % пени на конец (76.05.02)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.transact_date IS 'Дата рассчета';
COMMENT ON COLUMN loan_calc_history.balance_value_history.create_date IS 'Дата создания записи';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_loan_b IS 'Непросроченое тело займа на начало (58.03.01)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_loan_e IS 'Непросроченое тело займа на конец (58.03.01)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_overpayment_planned_b IS 'Непросроченные % на начало (76.03.01)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_overpayment_planned_e IS 'Непросроченные % на конец (76.03.01)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.loan_b IS 'Тело займа на начало 58.03';
COMMENT ON COLUMN loan_calc_history.balance_value_history.loan_e IS 'Тело займа на начало 58.03';
COMMENT ON COLUMN loan_calc_history.balance_value_history.client_balance_b IS 'Баланс клиента на начало (76.09)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.client_balance_e IS 'Баланс клиента на конец (76.09)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.active IS 'Активная запись';
COMMENT ON COLUMN loan_calc_history.balance_value_history.opened IS 'Открыт ли займ';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_overpayment_conditionally_b IS 'Условно непросроченные проценты на начало (76.03.04)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.n_overpayment_conditionally_e IS 'Условно непросроченные проценты на конец(76.03.04)';
COMMENT ON COLUMN loan_calc_history.balance_value_history.all_loan_percents_b IS 'Cумма всех начисленных процентов по займу на начало';
COMMENT ON COLUMN loan_calc_history.balance_value_history.all_loan_percents_e IS 'Cумма всех начисленных процентов по займу на конец';
COMMENT ON COLUMN loan_calc_history.balance_value_history.loan_version_key IS 'Ссылка на версию рассчета';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.loan_transact_key IS 'Первичный ключ';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.loan_key IS 'Займ. Ссылка на public.loans';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.separate_subdivision_key IS 'Подразделение';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.summ IS 'Сумма проводки';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.fact_key IS 'Ссылка на facts.facts';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.account_transaction_key IS 'Ключ транзакции. Ссылка на buh.account_transaction';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.transact_date IS 'Дата рассчета';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.create_date IS 'Дата создания записи';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.active IS 'Активная запись';
COMMENT ON COLUMN loan_calc_history.loan_transact_history.loan_version_key IS 'Ссылка на версию рассчета';
SET search_path = asuz_db_local, pg_catalog;
COMMENT ON COLUMN asuz_db_local.t_borrowers.regular_client IS 'Постоянный клиент';
COMMENT ON COLUMN asuz_db_local.t_borrowers.do_not_notify IS 'Не уведомлять о приближении даты оплаты';
COMMENT ON COLUMN asuz_db_local.t_borrowers.snils IS 'СНИЛС';
COMMENT ON COLUMN asuz_db_local.t_borrowers.snils_check_mode IS 'Каким образом был проверен СНИЛС (табл. loan_issue.snils_verification_result)';
COMMENT ON COLUMN asuz_db_local.t_borrowers.birthplace IS 'Место рождения';
SET search_path = buh, pg_catalog;
COMMENT ON COLUMN buh.loan_percent.year IS 'Год';
SET search_path = loan_calc_history, pg_catalog;
COMMENT ON COLUMN loan_calc_history.progress.progress_key IS 'Первичный ключ';
COMMENT ON COLUMN loan_calc_history.progress.loan_version_key IS 'Ключ версии расчета';
COMMENT ON COLUMN loan_calc_history.progress.total_days IS 'За сколько дней идет расчет';
COMMENT ON COLUMN loan_calc_history.progress.current_day IS 'Текущий день';
SET search_path = public, pg_catalog;
COMMENT ON COLUMN public.special_offers_types.special_offer_type_key IS 'Первый ключ таблицы';
COMMENT ON COLUMN public.special_offers_types.special_offer_name IS 'Название акции';
COMMENT ON COLUMN public.special_offers_types.special_offer_description IS 'Описание акции';
COMMENT ON COLUMN public.special_offers_types.start_date IS 'Дата начала действия акции';
COMMENT ON COLUMN public.special_offers_types.end_date IS 'Дата окончания действия акции';
COMMENT ON COLUMN public.special_offers_types.procedure_name IS 'Хранимка создания акции для клиента';
COMMENT ON COLUMN public.special_offers_types.id IS 'Идентификатор процесса';
COMMENT ON COLUMN public.special_offers_types.only_new_client IS 'Флаг действия акции только для новых клиентов';
COMMENT ON COLUMN public.special_offers_types.min_term_of_loan IS 'Минимальный срок займа';
COMMENT ON COLUMN public.special_offers_types.max_term_of_loan IS 'Максимальный срок займа';
COMMENT ON COLUMN public.special_offers_types.direct_discount IS 'Скидка на сумму';
COMMENT ON COLUMN public.special_offers_types.offer_category IS 'Категория предложения';
SET search_path = loan_calc_history, pg_catalog;
COMMENT ON TABLE loan_calc_history.lock_calculation IS '/*
<description>
           Таблица для хранения блокировок при параллельных расчетах
</description>
<history_list>
    <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/';
COMMENT ON COLUMN loan_calc_history.lock_calculation.lock_calculation_key IS 'Первичный ключ';
COMMENT ON COLUMN loan_calc_history.lock_calculation.loan_key IS 'Ключ займа, который расчитывается в текущий момент';
COMMENT ON COLUMN loan_calc_history.lock_calculation.create_date IS 'Дта создания записи';
SET search_path = buh, pg_catalog;
COMMENT ON TABLE buh.current_calc IS '/*
<description>
	В таблице зранятся метки о выполняемых сейчас расчетах займ  + версия расчета
    для того, чтобы во время расчета можно было поднять настройки текущей версии расчета</description>
<history_list>
  <item><date_m>07.02.2019</date_m><task_n>466273</task_n><author>Тихоненко В.В.</author></item>
</history_list>
*/';
COMMENT ON COLUMN buh.current_calc.current_calc_key IS 'Первичный ключ';
COMMENT ON COLUMN buh.current_calc.loan_key IS 'Ключ займа';
COMMENT ON COLUMN buh.current_calc.loan_version_key IS 'Ссылка на версию расчета';
COMMENT ON COLUMN buh.current_calc.create_date IS 'Дата создания записи';
SET search_path = asuz_db, pg_catalog;
COMMENT ON COLUMN asuz_db.t_borrowers.regular_client IS 'Постоянный клиент';
COMMENT ON COLUMN asuz_db.t_borrowers.do_not_notify IS 'Не уведомлять о приближении даты оплаты';
COMMENT ON COLUMN asuz_db.t_borrowers.snils IS 'СНИЛС';
COMMENT ON COLUMN asuz_db.t_borrowers.snils_check_mode IS 'Каким образом был проверен СНИЛС (табл. loan_issue.snils_verification_result)';
COMMENT ON COLUMN asuz_db.t_borrowers.birthplace IS 'Место рождения';
COMMENT ON COLUMN asuz_db.repayment_schedules.repayment_schedule_key IS 'Ключ таблицы';
COMMENT ON COLUMN asuz_db.repayment_schedules.loan_key IS 'Ключ займа';
COMMENT ON COLUMN asuz_db.repayment_schedules.payment_date IS 'Плановая дата платежа';
COMMENT ON COLUMN asuz_db.repayment_schedules.loan_sum IS 'Сумма тела займа к выплате';
COMMENT ON COLUMN asuz_db.repayment_schedules.overpayment_sum IS 'Сумма плановых процентов к выплате';
COMMENT ON COLUMN asuz_db.repayment_schedules.delay IS 'Флаг просрочки';
COMMENT ON COLUMN asuz_db.repayment_schedules.returned IS 'Флаг, что платёж полностью погашен';
COMMENT ON COLUMN asuz_db.repayment_schedules.permissible_loan_key IS 'Ключ графика платежей';
COMMENT ON COLUMN asuz_db.repayment_schedules.returned_date IS 'Дата полного закрытия платежа';
COMMENT ON COLUMN asuz_db.loans.debt_relieft_type IS 'признак списания долга
цессия, смерть клиента, банкротство и т.п.
выставляется в процедурах списания долга,
юзается для отчетности и рассчета проводок';
COMMENT ON COLUMN asuz_db.loans.claim_right IS 'Переуступка прав требований по договору займа третьим лицам (нулл - до появления признака,-1 - не согласен,1 - согласен)';
COMMENT ON COLUMN asuz_db.loans.refinance IS 'Рефинанс';
COMMENT ON COLUMN asuz_db.facts.subdivision_key IS 'Ключ подразделения';
COMMENT ON TABLE asuz_db.loan_restructuring_history IS 'Реестр доп соглашений на реструктуризацию';
COMMENT ON COLUMN asuz_db.permissible_loans.type_repayment_schedule_key IS 'Тип графика платежей';
COMMENT ON COLUMN asuz_db.special_offers_types.special_offer_type_key IS 'Первый ключ таблицы';
COMMENT ON COLUMN asuz_db.special_offers_types.special_offer_name IS 'Название акции';
COMMENT ON COLUMN asuz_db.special_offers_types.special_offer_description IS 'Описание акции';
COMMENT ON COLUMN asuz_db.special_offers_types.start_date IS 'Дата начала действия акции';
COMMENT ON COLUMN asuz_db.special_offers_types.end_date IS 'Дата окончания действия акции';
COMMENT ON COLUMN asuz_db.special_offers_types.procedure_name IS 'Хранимка создания акции для клиента';
COMMENT ON COLUMN asuz_db.special_offers_types.id IS 'Идентификатор процесса';
COMMENT ON COLUMN asuz_db.special_offers_types.only_new_client IS 'Флаг действия акции только для новых клиентов';
COMMENT ON COLUMN asuz_db.special_offers_types.min_term_of_loan IS 'Минимальный срок займа';
COMMENT ON COLUMN asuz_db.special_offers_types.max_term_of_loan IS 'Максимальный срок займа';
COMMENT ON COLUMN asuz_db.special_offers_types.direct_discount IS 'Скидка на сумму';
COMMENT ON COLUMN asuz_db.special_offers_types.offer_category IS 'Категория предложения';
