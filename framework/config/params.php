<?php

$params['adminEmail'] = 'admin@example.com';

if ($_SERVER['HTTP_HOST'] == 'collection-calc-dev.vivadengi.ru')
{
    $params['PDFGEN_URL'] = 'https://pdfgen-prerelease.vivadengi.ru/';
}
else
{
    $params['PDFGEN_URL'] = 'https://pdfgen.vivadengi.ru/';
}
return $params;
