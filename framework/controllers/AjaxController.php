<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use app\models\SudModel;
use app\models\CalcModel;

class AjaxController extends Controller
{

    /**
     * Выполняем действия перед выполнением событий
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }
    /**
     * Основная страница
     *
     * @return string
     */
    public function actionIndex()
    {
        echo 456;
    }

    /**
     * Проверяет номер контракта
     *
     * @return string
     */
    public function actionValidatecontract()
    {
        $response               = [];
        $response['result']     = -1;
        $response['result_str'] = 'Не указан номер займа';

        $loan_contract = Yii::$app->request->post('loan_contract', '');

        if (strlen($loan_contract))
        {
            $validated = SudModel::checkCorrectContractNumber($loan_contract);
            //print_r($validated); exit;

            if ($validated['result'] > 0)
            {
                $response['result']     = intval($validated['result']);
                $response['result_str'] = 'OK';
            }
            else
            {
                $response['result_str'] = $validated['result_str'];
            }

        }

        // Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data   = $response;
    }

    /**
     * Создаем последовательность для прогрессбара
     *
     * @return json
     */
    public function actionCreatesequence()
    {

        $response               = [];
        $response['result']     = -1;
        $response['result_str'] = 'Не указан номер займа';

        $loan_key = intval(Yii::$app->request->post('loan_key', 0));

        if ($loan_key)
        {
            $response = SudModel::createSequence($loan_key);
        }

        // Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data   = $response;
    }

    /**
     * Получем значение последовательности
     *
     * @return json
     */
    public function actionGetsequencestatus()
    {

        $response               = [];
        $response['result']     = -1;
        $response['result_str'] = 'Не указана последовательность для прогрессбара';

        $sequence = Yii::$app->request->post('sequence', '');

        if (strlen($sequence))
        {
            $sequenceStatus         = SudModel::getSequenceValue($sequence);
            $response['result']     = $sequenceStatus['last_value'];
            $response['result_str'] = 'OK';
        }

        // Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data   = $response;
    }

    /**
     * Удаляем последовательность
     *
     * @return json
     */
    public function actionDropsequence()
    {

        $response               = [];
        $response['result']     = -1;
        $response['result_str'] = 'Не указана последовательность для прогрессбара';

        $sequence = Yii::$app->request->post('sequence', '');

        if (strlen($sequence))
        {
            $response = SudModel::dropSequence($sequence);
        }

        // Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data   = $response;
    }

    /**
     * Запускаем расчет
     *
     * @return json
     */
    public function actionCalcsud()
    {

        $response = [];

        $response['errors'] = [];
        $response['result'] = 0;

        $name             = Yii::$app->request->post('name', '');
        $bankcard         = intval(Yii::$app->request->post('bankcard', 0));
        $contract         = Yii::$app->request->post('contract', '');
        $involve_restruct = intval(Yii::$app->request->post('involve_restruct', 0)) ? 1 : 0;

        $modelCalcModel                   = new CalcModel();
        $modelCalcModel->name             = $name;
        $modelCalcModel->contract         = $contract;
        $modelCalcModel->involve_restruct = $involve_restruct;

        $modelCalcModel->bankcard = $bankcard;

        if ($modelCalcModel->validate())
        {
            $response['result']  = 1;
            $addTask             = $modelCalcModel->addTask();
            $response['addTask'] = $addTask;
            $response['return_url'] = "/sud/history/".$modelCalcModel->contract."/".$addTask['result'];
            
            if ($bankcard)
            {
                $response['return_url'] = "/bankcard/history/".$modelCalcModel->contract."/".$addTask['result'];
            }
        }
        else
        {
            // validation failed: $errors is an array containing error messages
            $response['errors'] = $modelCalcModel->errors;
        }
        // Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data   = $response;
    }

    /**
     * Запускаем расчет
     *
     * @return json
     */
    public function actionGet_pdf_json()
    {

        $calc_version_key = intval(Yii::$app->request->post('calc_version_key', '0'));
        $response         = CalcModel::getPdfJson($calc_version_key);

        // Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data   = $response;
    }

    /**
     * Фуункция возвращает JSON для генерации отчета
     * Выгрузка карточки договора микрозайма
     *
     * @return json
     */
    public function actionGet_card_pdf_json()
    {

        $calc_version_key = intval(Yii::$app->request->post('calc_version_key', '0'));
        $return_as_html = intval(Yii::$app->request->post('return_as_html', '0'));
        if (isset($_GET['tt']))
        {
            $calc_version_key = 17240;
        }

        $response         = CalcModel::getCardPdfJson($calc_version_key);

        header('Content-type: application/json');
        //echo $response['result_str']; exit;
        // Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data   = $response;
    }

    /**
     * Получаем JSON для генерации пояснительной записки
     *
     * @return json
     */
    public function actionGet_pdf_json_zap()
    {

        $calc_version_key = intval(Yii::$app->request->post('calc_version_key', '0'));
        $response         = CalcModel::getPdfJsonZap($calc_version_key);

        // Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data   = $response;
    }

    /**
     * Запускаем расчет
     *
     * @return json
     */
    public function actionCalc()
    {

        $response = [];

        $response['result']     = -1;
        $response['result_str'] = 'Не указан ключ займа';

        $name                   = Yii::$app->request->post('name', '');
        $contract               = Yii::$app->request->post('contract', '');
        $sequence               = Yii::$app->request->post('sequence', '');
        $nopeni                 = intval(Yii::$app->request->post('nopeni', 0));
        $nopeni                 = $nopeni ? 1 : 0;
        $first_payment_schedule = intval(Yii::$app->request->post('first_payment_schedule', 0));
        $first_payment_schedule = $first_payment_schedule ? 1 : 0;
        $loan_key               = intval(Yii::$app->request->post('loan_key', 0));

        if ($loan_key)
        {
            $response = SudModel::startCalc(
                $loan_key,
                $name,
                $nopeni,
                $sequence,
                $first_payment_schedule
            );
        }

        // Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data   = $response;
    }


    public function actionTests()
    {
        foreach (Yii::$app->log->targets as $target) {
            $target->setEnabled(false);
        }

        $response         = CalcModel::getCardPdfJson(17240);
        print_r($response);
        exit;
    }

    /**
     * Получаем JSON для генерации пояснительной записки
     *
     * @return json
     */
    public function actionPgfgencard($lvkey = 0)
    {
;
        $lvkey = intval($lvkey);
        $return_as_html = intval(Yii::$app->request->get('return_as_html', '0'));
        echo $return_as_html ;
        
        $response         = CalcModel::getCalcVersionJson($lvkey);
        if ($response['result']<0)
        {
            die($response['result_str']);
        }

        if ($return_as_html)
        {
            $json_array = json_decode($response['result_str'],true);
            if (isset($json_array[0]))
            {
                $json_array = $json_array[0];
            }
            if (isset($json_array['params']['template_out']))
            {
                $json_array['params']['template_out'] = 'html';
            }
            $response['result_str'] = json_encode($json_array);
            
            
        }

        $pdfgen_url = \Yii::$app->params['PDFGEN_URL'];

        echo '<form method="post" id="pdf_gen" action="'.$pdfgen_url.'json/">';
        echo '<textarea name="json" style="display:none;" id="" cols="30" rows="10">'.$response['result_str'].'</textarea>';
        echo '<input type="submit" style="display:none;" value="submit">';
        echo '</form>';
        
        echo "<script>window.onload = function(){
            document.forms['pdf_gen'].submit();
          }</script>";
        
        exit;
        /*
        $response         = CalcModel::getPdfJsonZap($calc_version_key);

        // Возвращаем данные в формате JSON
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data   = $response;
        */
    }

}
