<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\SudModel;
use app\models\SudAsuzModel;




class SiteController extends Controller
{


	public function actionIndex()
	{
		$this->layout = "empty_site"; 

		$contract = Yii::$app->request->get('contract', '');


		$contract_error = '';

		$validated = SudModel::checkCorrectContractNumber($contract);

		if ($validated['result']>0)
		{
			$LoanCalcHistory = SudModel::getLoanCalcHistory($contract);
		}
		else
		{
			$contract_error = $validated['result_str'];
			die($contract_error);
		}
		

		// Получаем данные о займе из АСУЗа
		$LoanInfo                   = SudAsuzModel::getLoanInfo($contract);

		// Получаем из АСУЗа общие данные по графику платежей
		$RepaymentSchedulesTotal    = SudAsuzModel::getRepaymentSchedulesTotal($contract);

		// Получаем из АСУЗа подробные данные по графику платежей
		$RepaymentSchedulesDetail              = SudAsuzModel::getRepaymentSchedulesDetail($contract);
		

		return $this->render(
			'index',
			[
				'contract'                  => $contract,
				'LoanInfo'                  => $LoanInfo,
				'RepaymentSchedulesTotal'   => $RepaymentSchedulesTotal,
				'RepaymentSchedulesDetail'  => $RepaymentSchedulesDetail,
			]
		);
		
		return $this->render('index');
	}
}
