<?php

namespace app\controllers;

use Mpdf\Mpdf;
use yii\web\Controller;
use app\models\SudModel;
use app\models\CalcModel;

class SudController extends Controller
{

    /**
     * Выполняем действия перед выполнением событий
     */
    public function beforeAction($action)
    {
        if ('calc' == $this->action->id)
        {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Основная страница
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * История расчетов для суда для указанного займа
     *
     * @return string
     */
    public function actionHistory(
        $contract,
        $version_key = 0
    )
    {

        $contract_error = '';

        $validated = SudModel::checkCorrectContractNumber($contract);

        if ($validated['result'] > 0)
        {
            $modelCalcModel                   = new CalcModel();
            $LoanCalcHistory = $modelCalcModel->getCalcHistory($validated['result']);
        }
        else
        {
            $contract_error = $validated['result_str'];
            die($contract_error);
        }

        return $this->render(
            'history',
            [
                'LoanCalcHistory' => $LoanCalcHistory,
                'contract_error'  => $contract_error,
                'version_key'     => $version_key,
                'contract'        => $contract,
                'pdfgen_url'      => \Yii::$app->params['PDFGEN_URL'],
            ]
        );

    }

    /**
     * Отображение расчета
     */
    public function actionPrint(
        $type,
        $template,
        $lvkey
    )
    {
        ini_set('pcre.backtrack_limit', '5000000');
        $type     = (in_array($type, ['html', 'pdf'])) ? $type : 'html';
        $template = (in_array($template, ['sud', 'bank', 'balance'])) ? $template : 'sud';
        $lvkey    = intval($lvkey) ? intval($lvkey) : 0;

        if (!$lvkey)
        {
            die('Не указана версия расчета');
        }

        if ('bank' == $template)
        {
            // Получаем информацию по займу для рассчета по суду
            $LoanInfo = SudModel::getLoanInfoBank($lvkey);

            // Получаем табличные даанные по балансам по займу
            $CalcHistoryData = SudModel::getCalcHistoryDataBank($lvkey);

            // Получаем задолженности по займу для рассчета по суду
            $CalcStateDuty = SudModel::getCalcStateDutyBank($lvkey);

            // Получаем список акций, в которых участвует займ
            $LoanSpecialOffers = SudModel::getLoanSpecialOffers($lvkey);
        }
        elseif ('balance' == $template)
        {
            $LoanInfo            = SudModel::getLoanInfo($lvkey);
            $BalanceValueHistory = SudModel::getBalanceValueHistory($lvkey);
            $LoanTransactHistory = SudModel::getLoanTransactHistory($lvkey);
            $LoanFacts           = SudModel::getLoanFacts($LoanInfo['loan_key']);

            // Получаем список фактов для указанного займа, те что из боевой таблицы АСУЗА
            $AsuzLoanFacts = SudModel::getAsuzLoanFacts($LoanInfo['loan_key']);

            // Получаем список акций, в которых участвует займ
            $LoanSpecialOffers = SudModel::getLoanSpecialOffers($lvkey);

            // Получение первоначального графика платежей
            $TmpPaymentSchedules = SudModel::getTmpPaymentSchedules($LoanInfo['loan_key']);

            // Получение все данные по всем графикам платежей для указанного займа
            $RepaymentSchedules = SudModel::getRepaymentSchedules($LoanInfo['loan_key']);

            // Получение список графиков платежей по указанному займу
            $PermissibleLoans = SudModel::getPermissibleLoans($LoanInfo['loan_key']);

            // Получаем список оплат из таблиц, что  АСУЗе (те которые перенесены)
            $PaymentsAsuz = SudModel::getPaymentsAsuz($LoanInfo['loan_key']);
        }
        else
        {
            // Получаем информацию по займу для рассчета по суду
            $LoanInfo = SudModel::getLoanInfo($lvkey);

            // Получаем задолженности по займу для рассчета по суду
            $CalcStateDuty = SudModel::getCalcStateDuty($lvkey);

            // Получаем табличные даанные по балансам по займу
            $CalcHistoryData = SudModel::getCalcHistoryData($lvkey);

            // Получаем список акций, в которых участвует займ
            $LoanSpecialOffers = SudModel::getLoanSpecialOffers($lvkey);
        }

        $this->layout = 'empty_' . $template;

        // Если это расчет для банка - тогда отображаем стандартно
        if (in_array($template, ['bank', 'sud']))
        {
            $render = $this->render(
                'report_' . $template,
                [
                    'type'              => $type,
                    'LoanInfo'          => $LoanInfo,
                    'CalcHistoryData'   => $CalcHistoryData,
                    'CalcStateDuty'     => $CalcStateDuty,
                    'LoanSpecialOffers' => $LoanSpecialOffers,
                ]
            );
        }
        // Если это расчет для проверки банансов, тогда отдельный view
        else
        {
            $render = $this->render(
                'report_' . $template,
                [
                    'type'                => $type,
                    'BalanceValueHistory' => $BalanceValueHistory,
                    'LoanTransactHistory' => $LoanTransactHistory,
                    'LoanFacts'           => $LoanFacts,
                    'AsuzLoanFacts'       => $AsuzLoanFacts,
                    'LoanSpecialOffers'   => $LoanSpecialOffers,
                    'TmpPaymentSchedules' => $TmpPaymentSchedules,
                    'RepaymentSchedules'  => $RepaymentSchedules,
                    'PermissibleLoans'    => $PermissibleLoans,
                    'LoanInfo'            => $LoanInfo,
                    'PaymentsAsuz'        => $PaymentsAsuz,
                ]
            );
        }

        if ('html' == $type)
        {
            // Отображаем HTML
            return $render;
        }
        else
        {
            // Генерируем PDF
            set_time_limit(0);

            $mpdf = new Mpdf(['', // mode - default ''
                '',                   // format - A4, for example, default ''
                8,                    // font size - default 0
                '',                   // default font family
                9,                    // margin_left
                9,                    // margin right
                9,                    // margin top
                9,                    // margin bottom
                9,                    // margin header
                9,                    // margin footer
                'L']);

            $mpdf->simpleTables  = true;
            $mpdf->packTableData = true;

            $mpdf->AddPage('L');

            $mpdf->SetHTMLFooter('
			<table width="100%">
				<tr>
					<td width="33%" style=" font-size:13px;">{DATE j-m-Y}</td>
					<td width="33%" align="center" style=" font-size:13px;">{PAGENO}/{nbpg}</td>
					<td width="33%" style="text-align: right; font-size:13px;">' . $LoanInfo['contract_number'] . '</td>
				</tr>
			</table>');
            $mpdf->shrink_tables_to_fit = 1;

            $stylesheet = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/theme/css/print_' . $type . '.css');
            $mpdf->WriteHTML($stylesheet, 1);

            $mpdf->WriteHTML($render);
            $mpdf->Output();

        }

    }

}
