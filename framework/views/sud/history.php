<?php


use yii\helpers\Html;



$this->title = 'Документы для суда';
?>
<input type="hidden" id="PDFGEN_URL" value="<?=$pdfgen_url?>">


<main role="main" class="container">
	<h1 class="mt-5 text-center">История расчетов займов</h1>
	<p class="lead">
		Для расчета истории операций по займу или просмотра уже сгенерированных данных по займу - укажите
		номер займа
	</p>
    <?= Yii::$app->view->renderFile('@app/views/blocks/sud_form.php');?>


	<br><br>
	<table class="table table-bordered table-condensed" id="rsd-info">
		<thead>
			<tr>
				<th>#</th>
				<th>Ключ займа</th>
				<th>Контракт</th>
				<th>Дата рассчета</th>
				<th>Описание</th>
				<th>Реструкт.</th>
				<th>Отчет</th>
			</tr>
		</thead>
		<tbody>
            <?php 
                $i = 0;
                if (count($LoanCalcHistory)):?>
                <?php foreach ($LoanCalcHistory as $r): ?>
                    <tr <?=($version_key==$r['calc_version_key'])?' class="bg-warning" ':'' ?>>
                        <td><?= ++$i; ?></td>
                        <td><?= $r['loan_key'] ?></td>
                        <td><?= $contract ?></td>
                        <td><?= date("d.m.Y H:i:s", strtotime($r['creation_date'])) ?></td>
                        <td><?= $r['name'] ?></td>
                        <td>
                            <?= intval($r['involve_restruct'])?'Да':'Нет' ?>
                        </td>

                        
                        
                        <td>



                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img style="max-width: 25px;" src="/theme/img/icons8-report-card-50.png">
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">


                                        <?= Html::a(
                                            'Отчет', 
                                            'javascript:void(0);',
                                            [
                                                'class' => 'btn_pdf_print dropdown-item',
                                                'idx' => $r['calc_version_key']
                                            ]
                                            );
                                        ?>

                                        <?= Html::a(
                                            'Пояснительная записка', 
                                            'javascript:void(0);',
                                            [
                                                'class' => 'btn_pdf_print_zap dropdown-item',
                                                'idx' => $r['calc_version_key']
                                            ]
                                            );
                                        ?>
                                    
                                </div>
                            </div>



                        </td>
                    </tr>
                <?php endforeach; ?>
			<?php  else: ?>
                <tr>
                    <td colspan="4" style="text-align: center;">Записей не найдено.</td>
                </tr>
            <?php endif;?>
		</tbody>
	</table>

</main>