<style>

.balance_table{
	
}

.balance_table thead tr th{
	font-size: 12px;
    font-weight: normal;
}

.balance_table tbody tr td{
	font-size: 12px;
}




</style>









<div class="container-fluid">

	<h2>
		Информация о займе
	</h2>

	<table class="table table-hover table-sm table-bordered">
		<tr>
			<th>
				Заемщик
			</th>
			<td>
				<?= $LoanInfo['fio']; ?>
			</td>
		</tr>
		<tr>
			<th>
				Номер контракта
			</th>
			<td>
				<?= $LoanInfo['contract_number']; ?>
			</td>
		</tr>
		<tr>
			<th>
				Дата выдачи
			</th>
			<td>
				<?= $LoanInfo['loan_date']; ?>
			</td>
		</tr>
		<tr>
			<th>
				Сумма займа
			</th>
			<td>
				<?= number_format ($LoanInfo['loan_summ'], 2 , "." , " " ); ?>
			</td>
		</tr>
		<tr>
			<th>
				Ключ займа
			</th>
			<td>
				<?= $LoanInfo['loan_key']; ?>
			</td>
		</tr>
	</table>




	<h2>
		Балансовые величины
	</h2>

	<div class="alert alert-warning" role="alert">
		Для просмотра списка транзакций за указанный день - кликните на ячейку с 
		балансовыми величинами на необходимый день.
	</div>


	<table class="table table-hover table-sm table-bordered balance_table">
		<thead class="thead-light">
			<tr>
				<th scope="col">
					Дата рассчета
					<br>
					<small class="text-muted">transact_date</small>
				</th>
				<th scope="col">
					Проср. тело займа на начало (58.03.02)
					<br>
					<small class="text-muted">od_loan_b</small>
				</th>

				<th scope="col">
					Проср. тело займа на конец (58.03.02)</small>
					<br>
					<small class="text-muted">od_loan_e</small>
				</th>

				<th scope="col">
				<small class="text-muted">od_overpayment_planned_b</small>
				<br>
				Проср. % плановые на начало (76.03.02)
				</th>

				<th scope="col">
				<small class="text-muted">od_overpayment_planned_e</small>
				<br>
				Проср. % плановые на конец (76.03.02)
				</th>

				<th scope="col">
				<small class="text-muted">od_overpayment_for_delay_b</small>
				<br>
				Проср. % доначисленные на начало (76.03.03)
				</th>

				<th scope="col">
				<small class="text-muted">od_overpayment_for_delay_e</small>
				<br>
				Проср. % доначисленные на конец (76.03.03)
				</th>

				<th scope="col">
				<small class="text-muted">od_fine_b</small>
				<br>
				Проср. % пени на начало (76.05.02)
				</th>

				<th scope="col">
				<small class="text-muted">od_fine_e</small>
				<br>
				Проср. % пени на конец (76.05.02)
				</th>
				<th scope="col">
				<small class="text-muted">n_loan_b</small>
				<br>
				Непросроченое тело займа на начало (58.03.01)
				</th>

				<th scope="col">
				<small class="text-muted">n_loan_e</small>
				<br>
				Непросроченое тело займа на конец (58.03.01)
				</th>

				<th scope="col">
				<small class="text-muted">n_overpayment_planned_b</small>
				<br>
				Непросроченные % на начало (76.03.01)
				</th>

				<th scope="col">
				<small class="text-muted">n_overpayment_planned_e</small>
				<br>
				Непросроченные % на конец (76.03.01)
				</th>

				<th scope="col">
				<small class="text-muted">loan_b</small>
				<br>
				Тело займа на начало 58.03
				</th>

				<th scope="col">
				<small class="text-muted">loan_e</small>
				<br>
				Тело займа на начало 58.03
				</th>

				<th scope="col">
				<small class="text-muted">client_balance_b</small>
				<br>
				Баланс клиента на начало (76.09)
				</th>

				<th scope="col">
				<small class="text-muted">client_balance_e</small>
				<br>
				Баланс клиента на конец (76.09)
				</th>

				<th scope="col">
				<small class="text-muted">active</small>
				<br>
				Активная запись
				</th>

				<th scope="col">
				<small class="text-muted">opened</small>
				<br>
				Открыт ли займ
				</th>

				<th scope="col">
				<small class="text-muted">n_overpayment_conditionally_b</small>
				<br>
				Условно непросроченные проценты на начало (76.03.04)
				</th>

				<th scope="col">
				<small class="text-muted">n_overpayment_conditionally_e</small>
				<br>
				Условно непросроченные проценты на конец(76.03.04)
				</th>

				<th scope="col">
				<small class="text-muted">all_loan_percents_b</small>
				<br>
				Cумма всех начисленных процентов по займу на начало
				</th>

				<th scope="col">
				<small class="text-muted">all_loan_percents_e</small>
				<br>
				Cумма всех начисленных процентов по займу на конец
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($BalanceValueHistory)): ?>
				<?php foreach ($BalanceValueHistory as $r): ?>
					<tr onclick="$('#date_<?=md5($r['transact_date'])?>').toggle();">
						<td>
							<?= $r['transact_date'] ? date("d.m.Y",strtotime($r['transact_date'])) : "" ?>
						</td>
						<td>
							<?= $r['od_loan_b']; ?>
						</td>
						<td>
							<?= $r['od_loan_e']; ?>
						</td>
						<td>
							<?= $r['od_overpayment_planned_b']; ?>
						</td>
						<td>
							<?= $r['od_overpayment_planned_e']; ?>
						</td>
						<td>
							<?= $r['od_overpayment_for_delay_b']; ?>
						</td>
						<td>
							<?= $r['od_overpayment_for_delay_e']; ?>
						</td>
						<td>
							<?= $r['od_fine_b']; ?>
						</td>
						<td>
							<?= $r['od_fine_e']; ?>
						</td>

						<td>
							<?= $r['n_loan_b']; ?>
						</td>
						<td>
							<?= $r['n_loan_e']; ?>
						</td>
						<td>
							<?= $r['n_overpayment_planned_b']; ?>
						</td>
						<td>
							<?= $r['n_overpayment_planned_e']; ?>
						</td>
						<td>
							<?= $r['loan_b']; ?>
						</td>
						<td>
							<?= $r['loan_e']; ?>
						</td>
						<td>
							<?= $r['client_balance_b']; ?>
						</td>
						<td>
							<?= $r['client_balance_e']; ?>
						</td>
						<td>
							<?= $r['active']; ?>
						</td>
						<td>
							<?= $r['opened']; ?>
						</td>
						<td>
							<?= $r['n_overpayment_conditionally_b']; ?>
						</td>
						<td>
							<?= $r['n_overpayment_conditionally_e']; ?>
						</td>
						<td>
							<?= $r['all_loan_percents_b']; ?>
						</td>
						<td>
							<?= $r['all_loan_percents_e']; ?>
						</td>
					</tr>
					<tr id="date_<?=md5($r['transact_date'])?>" style="display:none;">
						<td colspan="27">
							<?php 
								$count_tr_current_day = 0;
								if (count($LoanTransactHistory))
								{
									foreach ($LoanTransactHistory as $t)
									{
										if ($t['transact_date'] == $r['transact_date'])
										{
											$count_tr_current_day = 1;
											break;
										}
									}
								}
								if ($count_tr_current_day): 
							?>
								<b>Транзакции</b>
								<table class="table table-hover table-sm table-striped">
									<thead>
										<tr class="table-info">
											<th>
												#
												<br>
												<small class="text-muted">loan_transact_key</small>
											</th>
											<th>
												Имя транзакции
											</th>
											<th>Сумма проводки</th>
											<th>loan_key<br>Займ. Ссылка на public.loans</th>
											<th>separate_subdivision_key<br>Подразделение</th>
											<th>fact_key<br>Ссылка на facts.facts</th>
											<th>account_transaction_key<br>Ключ транзакции. Ссылка на buh.account_transaction</th>
											<th>transact_date<br>Дата рассчета</th>
											<th>create_date<br>Дата создания записи</th>
											<th>active<br>Активная запись</th>
											<th>loan_version_key<br>Ссылка на версию рассчета</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($LoanTransactHistory as $t): ?>
											<?php if ($t['transact_date'] == $r['transact_date']): ?>
												<tr>
													<td>
														<?= $t['loan_transact_key']; ?>
													</td>		
													<td>
														<?= $t['transactname']; ?>
													</td>
													<td>
														<?= $t['summ']; ?>
													</td>
													<td>
														<?= $t['loan_key']; ?>
													</td>
													<td>
														<?= $t['separate_subdivision_key']; ?>
													</td>
													<td>
														<?= $t['fact_key']; ?>
													</td>
													<td>
														<?= $t['account_transaction_key']; ?>
													</td>
													<td>
														<?= $t['transact_date'] ? date("d.m.Y",strtotime($t['transact_date'])) : "" ?>
													</td>
													<td>
														<?= $t['create_date']; ?>
													</td>
													<td>
														<?= $t['active']; ?>
													</td>
													<td>
														<?= $t['loan_version_key']; ?>
													</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									</tbody>
								</table>
							<?php else: ?>
								<p>Нет транзакций за текущий день</p>
							<?php endif; ?>


							<?php 
								$count_facts_current_day = 0;
								if (count($LoanFacts))
								{
									foreach ($LoanFacts as $t)
									{
										if ($t['transact_date'] == $r['transact_date'])
										{
											$count_facts_current_day = 1;
											break;
										}
									}
								}
								if ($count_facts_current_day): 
							?>
								<b>Факты</b>
								<table class="table table-hover table-sm table-striped">
									<thead>
										<tr class="table-info">
											<th>
												#
												<br>
												<small class="text-muted">fact_key</small>
											</th>
											<th>
												Дата факта
												<br>
												<small class="text-muted">fact_date</small>
											</th>
											<th>
												Имя факта
												<br>
												<small class="text-muted">fact_name</small>
											</th>
											<th>
												<small class="text-muted">obj_type</small>
											</th>
											<th>
												<small class="text-muted">obj_key</small>
											</th>
											<th>
												Значение
												<br>
												<small class="text-muted">value</small>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($LoanFacts as $t): ?>
											<?php if ($t['transact_date'] == $r['transact_date']): ?>
												<tr>
													<td>
														<?= $t['fact_key']; ?>
													</td>		
													<td>
														<?= $t['fact_date']; ?>
													</td>
													<td>
														<?= $t['fact_name']; ?>
													</td>
													<td>
														<?= $t['obj_type']; ?>
													</td>
													<td>
														<?= $t['obj_key']; ?>
													</td>
													<td>
														<?= $t['value']; ?>
													</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									</tbody>
								</table>
							<?php else: ?>
								<p>Нет фактов за текущий день</p>
							<?php endif; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>



	<br><br>

	<h2>
		Сгенерированные/скопированные факты
	</h2>
	<p>
		Получаем список фактов для указанного займа, те что из сгенерированной таблицы
		Используется для займов до 01_09_2017. В случае если дата создания займа позже чем
		01_09_2017 - факты переносятся из таблицы, что в АСУЗе
	</p>

	<table class="table table-hover table-sm table-striped">
		<thead>
			<tr class="table-info">
				<th>
					#
					<br>
					<small class="text-muted">fact_key</small>
				</th>
				<th>
					Дата факта
					<br>
					<small class="text-muted">fact_date</small>
				</th>
				<th>
					Имя факта
					<br>
					<small class="text-muted">fact_name</small>
				</th>
				<th>
					<small class="text-muted">obj_type</small>
				</th>
				<th>
					<small class="text-muted">obj_key</small>
				</th>
				<th>
					Значение
					<br>
					<small class="text-muted">value</small>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($LoanFacts as $t): ?>
				<tr>
					<td>
						<?= $t['fact_key']; ?>
					</td>		
					<td>
						<?= $t['fact_date']; ?>
					</td>
					<td>
						<?= $t['fact_name']; ?>
					</td>
					<td>
						<?= $t['obj_type']; ?>
					</td>
					<td>
						<?= $t['obj_key']; ?>
					</td>
					<td>
						<?= $t['value']; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>








	<br><br>

	<h2>
		Факты из АСУЗа
	</h2>
	<p>
		Cписок фактов для указанного займа, те что из боевой таблицы АСУЗА
	</p>

	<table class="table table-hover table-sm table-striped">
		<thead>
			<tr class="table-info">
				<th>
					#
					<br>
					<small class="text-muted">fact_key</small>
				</th>
				<th>
					Дата факта
					<br>
					<small class="text-muted">fact_date</small>
				</th>
				<th>
					Имя факта
					<br>
					<small class="text-muted">fact_name</small>
				</th>
				<th>
					<small class="text-muted">obj_type</small>
				</th>
				<th>
					<small class="text-muted">obj_key</small>
				</th>
				<th>
					Значение
					<br>
					<small class="text-muted">value</small>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($AsuzLoanFacts as $t): ?>
				<tr>
					<td>
						<?= $t['fact_key']; ?>
					</td>		
					<td>
						<?= $t['fact_date']; ?>
					</td>
					<td>
						<?= $t['fact_name']; ?>
					</td>
					<td>
						<?= $t['obj_type']; ?>
					</td>
					<td>
						<?= $t['obj_key']; ?>
					</td>
					<td>
						<?= $t['value']; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>








	<br><br>

	<h2>
		Акции
	</h2>
	<p>
		Cписок акций, в которых участвует займ
	</p>

	<table class="table table-hover table-sm table-striped">
		<thead>
			<tr class="table-info">
				<th>
					Акция
				</th>
				<th>
					Дата
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($LoanSpecialOffers)): ?>
				<?php foreach ($LoanSpecialOffers as $t): ?>
					<tr>
						<td>
							<?= $t['special_offer_name']; ?>
						</td>		
						<td>
							<?= $t['start_date']; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="2">
						Займ не участвует в акциях
					</td>
				</tr>
			<?php endif; ?>
		</tbody>
	</table>








	<br><br>

	<h2>
		Оплаты
	</h2>
	<p>
		Cписок оплат из АСУЗ (в паре случаев отсутствовали факты о поступлении пратежей, 
		при этом данные в payment были)
	</p>

	<table class="table table-hover table-sm table-striped">
		<thead>
			<tr class="table-info">
				<th>
					Дата 
				</th>
				<th>
					Сумма
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($PaymentsAsuz)): ?>
				<?php foreach ($PaymentsAsuz as $t): ?>
					<tr>
						<td>
							<?= $t['payment_date']; ?>
						</td>		
						<td>
							<?= $t['payment_sum']; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="2">
						Записи о платежах в АСУЗе отсутствуют
					</td>
				</tr>
			<?php endif; ?>
		</tbody>
	</table>





	<br><br>

	<h2>
		Первоначальный график платежей
	</h2>
	<p>
		Тот что из таблицы loan_issue.tmp_payment_schedules
	</p>

	<table class="table table-hover table-sm table-striped">
		<thead>
			<tr class="table-info">
				<th>
					#
				</th>
				<th>
					application_key
				</th>
				<th>
					Дата платежа
				</th>
				<th>
					Сумма к уплате тело
				</th>
				<th>
					Сумма к уплате по процентам
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($TmpPaymentSchedules)): ?>
				<?php foreach ($TmpPaymentSchedules as $t): ?>
					<tr>
						<td>
							<?= $t['key']; ?>
						</td>		
						<td>
							<?= $t['application_key']; ?>
						</td>
						<td>
							<?= $t['payment_date']; ?>
						</td>		
						<td>
							<?= $t['loan_sum']; ?>
						</td>		
						<td>
							<?= $t['overpayment_sum']; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="2">
						У займа нет первоначального графика платежей
					</td>
				</tr>
			<?php endif; ?>
		</tbody>
	</table>
















	<br><br>

	<h2>
		Графики платежей
	</h2>
	<p>
		Список всех графиков платежей из таблицы repayment_schedules
	</p>

	<table class="table table-hover table-sm table-striped">
		<thead>
			<tr class="table-info">
				<th>
					#
				</th>
				<th>
					Дата графика
				</th>
				<th>
					Процентная ставка в день
				</th>
				<th>
					Аннуитетная процентная ставка в день
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if (count($PermissibleLoans)): ?>
				<?php foreach ($PermissibleLoans as $r): ?>
					<tr>
						<td>
							<?= $r['key']; ?>
						</td>
						<td>
							<?= $r['version_date']; ?>
						</td>
						<td>
							<?= $r['rate']; ?>
						</td>
						<td>
							<?= $r['rate_rs']; ?>
						</td>
					</tr>
					<tr>
						<td colspan="5">
							<table class="table table-hover table-sm table-striped">
								<thead>
									<tr class="table-success">
										<th>
											#
										</th>
										<th>
											Плановая дата платежа
										</th>
										<th>
											Сумма тела займа к выплате
										</th>
										<th>
											Сумма плановых процентов к выплате
										</th>
										<th>
											Флаг просрочки
										</th>
										<th>
											debtor
										</th>
										<th>
											Флаг, что платёж полностью погашен
										</th>
										<th>
											Ключ графика платежей
										</th>
										<th>
											Дата полного закрытия платежа
										</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($RepaymentSchedules as $t): ?>
										<?php if ($t['permissible_loan_key'] == $r['key']): ?>
											<tr>
												<td>
													<?= $t['repayment_schedule_key']; ?>
												</td>
												<td>
													<?= $t['payment_date']; ?>
												</td>
												<td>
													<?= $t['loan_sum']; ?>
												</td>
												<td>
													<?= $t['overpayment_sum']; ?>
												</td>
												<td>
													<?= $t['delay']; ?>
												</td>
												<td>
													<?= $t['debtor']; ?>
												</td>
												<td>
													<?= $t['returned']; ?>
												</td>
												<td>
													<?= $t['permissible_loan_key']; ?>
												</td>
												<td>
													<?= $t['returned_date']; ?>
												</td>
											</tr>
										<?php endif; ?>
									<?php endforeach; ?>

								</tbody>
							</table>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="2">
						У займа нет графиков платежей
					</td>
				</tr>
			<?php endif; ?>
		</tbody>
	</table>
</div>