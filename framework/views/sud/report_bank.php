<link rel="stylesheet" href="/theme/css/print_<?php echo $type; ?>.css" />
<link rel="stylesheet" href="/theme/css/print_cb.css" />




<div class="page landscape">



	<h4 style="text-align:center; " class="bold">
		Расчет
		<br>
		начислений и поступивших платежей по договору
		№ <?= $LoanInfo['contract_number'] ?>
		от
		<?= date("d.m.Y", strtotime($LoanInfo['loan_date'])) ?>
	</h4>

	<table class="table_header_cb" style="width:100%;">
		<tr>
			<td style="width:40%">
				<table class="" style="width:100%;">
					<tr>
						<td class="bold">
							Заемщик (Ф.И.О.):
						</td>
						<td class="bold align-right">
							<?= $LoanInfo['fio'] ?>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bold">
							Первоначальная сумма займа:
						</td>
						<td class="align-right">
							<?=number_format($LoanInfo['loan_summ'], 2, ",", " ") ?> руб.
						</td>
					</tr>
					<tr>
						<td class="bold">
							Дата выдачи займа:
						</td>
						<td class="align-right">
							<?= date("d.m.Y", strtotime($LoanInfo['loan_date'])) ?>
						</td>
					</tr>
					<tr>
						<td class="bold">
							Срок займа (в днях):
						</td>
						<td class="align-right">
							<?= $LoanInfo['term_days'] ?>
						</td>
					</tr>
					<tr>
						<td class="bold">
							Процентная ставка (% в день):
						</td>
						<td class="align-right">
							<?=number_format($LoanInfo['loan_rate'], 3, ",", " ") ?>
						</td>
					</tr>
					<tr>
						<td class="bold">
							Процентная ставка (% годовых):
						</td>
						<td class="align-right">
							<?=number_format($LoanInfo['year_rate'], 3, ",", " ") ?>
						</td>
					</tr>
					<tr>
						<td class="bold">
							Дата погашения займа:
						</td>
						<td class="align-right">
							<?= $LoanInfo['close_date'] ?>
						</td>
					</tr>
				</table>
			</td>
			<td>
				&nbsp;
				</th>
			<td style="width:40%">
				<table class="" style="width:100%;">
					<tr>
						<td class="bold">
							Дата составления:
						</td>
						<td class="bold align-right">
							<?= date("d.m.Y",strtotime($CalcStateDuty['creation_date'])) ?>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class="bold">
							Оплачено всего:
						</td>
						<td class="bold align-right">
							<?= number_format($CalcStateDuty['total_income_payments'], 2, ",", " ");?>
							руб.
						</td>
					</tr>
					<tr>
						<td>
							<span class="bold">Из них</span> в погашение процентов:
						</td>
						<td class="align-right">
							<?= number_format($CalcStateDuty['total_pays_percents'], 2, ",", " ");?>
							руб.
						</td>
					</tr>
					<tr>
						<td>
							в погашение основного долга:
						</td>
						<td class="align-right">
							<?= number_format($CalcStateDuty['total_pays_body'], 2, ",", " ");?>
							руб.
						</td>
					</tr>
					<tr>
						<td>
							в погашение штрафов:
						</td>
						<td class="align-right">
							<?= number_format($CalcStateDuty['total_pays_peni'], 2, ",", " ");?>
							руб.
						</td>
					</tr>
					<tr>
						<td>
							в погашение иных платежей:
						</td>
						<td class="align-right">
							<?= number_format(0, 2, ",", " ");?>
							руб.
						</td>
					</tr>
					<tr>
						<td class="bold">
							Общая сумма задолженности:
						</td>
						<td class="align-right">
							<?php echo  number_format($CalcStateDuty['state_duty_penalty'], 2, ",", " ");?>
							руб.
						</td>
					</tr>
					<tr>
						<td>
							<span class="bold">В том числе</span> по основному долгу:
						</td>
						<td class="align-right">
							<?php echo  number_format($CalcStateDuty['od_loan_e'], 2, ",", " ");?>
							руб.
						</td>
					</tr>
					<tr>
						<td>
							по процентам:
						</td>
						<td class="align-right">
							<?php echo number_format($CalcStateDuty['od_overpayment_total'], 2, ",", " ");?>
							руб.
						</td>
					</tr>
					<tr>
						<td>
							по штрафам:
						</td>
						<td class="align-right">
							<?php echo number_format($CalcStateDuty['od_fine_e'], 2, ",", " ");?>
							руб.
						</td>
					</tr>
					<tr>
						<td>
							по иным платежам:
						</td>
						<td class="align-right">
							<?= number_format(0, 2, ",", " ");?>
							руб.
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>


	<br>


	<table class="table table-bordered table-condensed table_main_calc" id="rsd-info">
		<thead>
			<tr>
				<th></th>
				<th colspan="7">Начислено</th>
				<th colspan="5">Оплачено</th>
				<th colspan="5">Остаток задолженности</th>
			</tr>
			<tr class="innertop">
				<th class="rotate">Дата расчета</th>
				<th class="rotate">Количество дней с даты получения займа</th>
				<th class="rotate">Остаток основного долга, руб.</th>
				<th class="rotate">Процентная ставка в день, %</th>
				<th class="rotate">Сумма начисленных процентов в день, руб.</th>
				<th class="rotate">Сумма процентов накопительным итогом, руб.</th>
				<th class="rotate">Сумма штрафов в день, руб.</th>
				<th class="rotate">Сумма иных платежей в день, руб. (с указанием назначения платежа)</th>


				<th class="rotate">Всего</th>
				<th class="rotate">В счет основного долга</th>
				<th class="rotate">В счет процентов</th>
				<th class="rotate">В счет штрафов</th>
				<th class="rotate">В счет иных платежей (с указанием назначения платежа)</th>

				<th class="rotate">Всего</th>
				<th class="rotate">Основной долг</th>
				<th class="rotate">Проценты</th>
				<th class="rotate">Штрафы</th>
				<th class="rotate">Иные платежи (с указанием назначения платежа)</th>

			</tr>
			<tr class="innertop">
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>5</th>
				<th>6</th>
				<th>7</th>
				<th>8</th>


				<th>9</th>
				<th>10</th>
				<th>11</th>
				<th>12</th>
				<th>13</th>

				<th>14</th>
				<th>15</th>
				<th>16</th>
				<th>17</th>
				<th>18</th>
			</tr>


		</thead>
		<tbody>
			<?php
if (count($CalcHistoryData)) {

foreach ($CalcHistoryData as $r) {
?>
			<tr>
				<td>
					<?= $r['f1_reporting_date'] ? date("d.m.Y",strtotime($r['f1_reporting_date'])) : "" ?>
				</td>
				<td>

					<?=$r['f2_date_diff_from_start'] ?>
				</td>
				<td>
					<!-- Поле #3 -->
					<?= $r['f2_1_loan'] ? number_format($r['f2_1_loan'], 2, ",", " ") : "0" ?>
				</td>
				<td>
					<?=number_format($r['f2_2__loan_rate__balance_date'], 3, ",", " ") ?>
				</td>
				<td>
					<?= number_format($r['f3_od_n_overpayment_all'], 2, ",", " ") ?>
				</td>
				<td>
					<!-- Поле #6 -->
					<?= number_format($r['f4_overpayment_all_summ'], 2, ",", " ") ?>
				</td>
				<td>
					<?= number_format($r['f5_fine_per_day'], 2, ",", " ") ?>
				</td>
				<td>
				</td>
				<td>
					<?= number_format($r['f7_pays_all'], 2, ",", " ") ?>
				</td>
				<td>
					<!-- Поле #10 -->
					<?= number_format($r['f8_pays_body'], 2, ",", " ") ?>
				</td>
				<td>
					<?= number_format($r['f9_pays_percent'], 2, ",", " ") ?>
				</td>
				<td>
					<?= number_format($r['f10_pays_peni'], 2, ",", " ") ?>
				</td>
				<td>
				</td>


				<td>
					<?= number_format($r['f12_balance_all'], 2, ",", " ") ?>
				</td>
				<td>
					<?= number_format($r['f13_balance_body'], 2, ",", " ") ?>
				</td>
				<td>
					<?= number_format($r['f14_balance_percents_all'], 2, ",", " ") ?>
				</td>
				<td>
					<?= number_format($r['f15_balance_fine'], 2, ",", " ") ?>
				</td>
				<td></td>
			</tr>
			<?php
		}
	} 
	else 
	{
?>
			<tr>
				<td colspan="17" style="text-align: center;">Записей не найдено.</td>
			</tr>
			<?php
	}
?>
		</tbody>
	</table>


	<!--pagebreak orientation="L" page-break-type="slice"></pagebreak>
	<br>
	<div>
		<ul>
			<li>
				"Процентная ставка (% в день)", отображаемая в "шапке" отчета может отличаться от процентной ставки, 
				которая используется при расчете доначисленных процентов, согласно задаче
				<a href="https://redmine.vivadengi.ru/issues/473770">https://redmine.vivadengi.ru/issues/473770</a>
				<br>
				При расчете  - только при типе графика платежей 2 (asuz_db.permissible_loans.type_repayment_schedule_key)
				используется комиссия за день (asuz_db.permissible_loans.rate). 
				В иных случаях используется ануитетная ставка (asuz_db.permissible_loans.rate_rs) и только если 
				она не указана явно - то используется комиссия за день (asuz_db.permissible_loans.rate)
			</li>
		</ul>
	</div-->



</div>