<link rel="stylesheet" href="/theme/css/print_<?= $type; ?>.css" />

<div class="page landscape">
	<h4 style="text-align:center; ">
		<?= 'Расчет "'.$CalcStateDuty['description'].'" от '.
		date("d.m.Y G:i:s",strtotime($CalcStateDuty['creation_date'])) ?>
	</h4>
	<div class="row">
		<div class="col-xs-4">
			<table class="table table-bordered table-condensed" id="loan-info">
				<tbody>

					<tr>
						<td>ФИО</td>
						<td><?= $LoanInfo['fio'] ?></td>
					</tr>
					<tr>
						<td>Номер договора</td>
						<td><?= $LoanInfo['contract_number'] ?></td>
					</tr>
					<tr>
						<td>Дата договора</td>
						<td><?= date("d.m.Y", strtotime($LoanInfo['loan_date'])) ?></td>
					</tr>
					<tr>
						<td>Сумма займа</td>
						<td><?=number_format($LoanInfo['loan_summ'], 2, ",", " ") ?></td>
					</tr>
					<tr>
						<td>% ставка по договору (в день)</td>
						<td><?=number_format($LoanInfo['loan_rate'], 4, ",", " ") ?></td>
					</tr>
					<tr>
						<td>% ставка на просроченный основной долг</td>
						<td><?=number_format($LoanInfo['rate'], 4, ",", " ") ?></td>
					</tr>
					<tr>
						<td>Дата расчета</td>
						<td><?= date("d.m.Y",strtotime($CalcStateDuty['creation_date'])) ?></td>
					</tr>
					<tr>
						<td>Всего реструктуризаций</td>
						<td><?= $LoanInfo['loan_restructuring_count'] ?></td>
					</tr>
					<?php if ($LoanInfo['loan_restructuring_count']): ?>
					<tr>
						<td>Даты реструктуризаций</td>
						<td><?= implode('<br>',explode(';',$LoanInfo['loan_restructuring_dates'])) ?></td>
					</tr>
					<?php endif; ?>

					<?php if (count($LoanSpecialOffers)): ?>
					<tr>
						<td colspan="2" style="text-align:center;"><b>Акции</b></td>
					</tr>
					<?php foreach ($LoanSpecialOffers as $r): ?>
					<tr>
						<td style="font-weight:normal;"><?= $r['special_offer_name'] ?></td>
						<td><?= $r['start_date'] ?></td>
					</tr>
					<?php endforeach; ?>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
		<div class="col-xs-2">
		</div>
		<div class="col-xs-6">
			<table class="table table-bordered table-condensed" id="loan-info">
				<tbody>
					<tr>
						<td>Просроченный основной долг </td>
						<td colspan="2"><?php echo  number_format($CalcStateDuty['od_loan_e'], 2, ",", " ");?> руб.</td>
					</tr>
					<tr>
						<td>Просроченные проценты</td>
						<td colspan="2">
							<?php echo number_format($CalcStateDuty['od_overpayment_planned_e'], 2, ",", " ");?> руб.
						</td>
					</tr>
					<tr>
						<td>Проценты на просроченный основной долг</td>
						<td colspan="2">
							<?php echo  number_format($CalcStateDuty['od_overpayment_for_delay_e'], 2, ",", " ");?> руб.
						</td>
					</tr>
					<tr>
						<td>Неустойка</td>
						<td colspan="2"><?php echo number_format($CalcStateDuty['od_fine_e'], 2, ",", " ");?> руб. </td>
					</tr>

					<tr>
						<td></td>
						<td><b style="font-size: 10px;">Без пени</b></td>
						<td><b style="font-size: 10px;">С пени</b></td>
					</tr>
					<tr>
						<td>Сумма иска</td>
						<td><?= $CalcStateDuty['state_duty'] ? number_format($CalcStateDuty['state_duty'], 2, ",", " ") : "" ?>
							руб.</td>
						<td><?= $CalcStateDuty['state_duty_penalty'] ? number_format($CalcStateDuty['state_duty_penalty'], 2, ",", " ") : "" ?>
							руб.</td>
					</tr>
					<tr>
						<td>Госпошлина для Искового производства</td>
						<td><?= $CalcStateDuty['gosp'] ? number_format($CalcStateDuty['gosp'], 2, ",", " ") : "" ?> руб.
						</td>
						<td><?= $CalcStateDuty['gosp_penalty'] ? number_format($CalcStateDuty['gosp_penalty'], 2, ",", " ") : "" ?>
							руб. </td>
					</tr>
					<tr>
						<td>Госпошлина для Приказного производства</td>
						<td>
							<?= $CalcStateDuty['gosp'] ? number_format(($CalcStateDuty['gosp']/2), 2, ",", " ") : "" ?>
							руб.
						</td>
						<td>
							<?= $CalcStateDuty['gosp_penalty'] ? number_format(($CalcStateDuty['gosp_penalty']/2), 2, ",", " ") : "" ?>
							руб.
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>



	<table class="table table-bordered table-condensed table_main_calc" id="rsd-info">
		<thead>
			<tr>
				<th rowspan="3">№</th>
				<th rowspan="2">Отчетная дата</th>
				<th rowspan="2">Плановый платеж по графику (% и основной долг)</th>
				<th rowspan="2">Сумма поступивших платежей (руб.)</th>
				<th colspan="3">Основной долг (руб.)</th>
				<th colspan="3">Проценты (руб.)</th>
				<th colspan="6">Проценты на просроченный основной долг (руб.)</th>

				<th rowspan="2">Итого задолжен- ность (руб.)</th>
			</tr>
			<tr>
				<th>Задолжен- ность по графику</th>
				<th>Погашен- ный основной долг</th>
				<th>Просроченный основной долг (нарастающим итогом)</th>

				<th>Задолжен- ность по графику</th>
				<th>Погашенные проценты</th>
				<th>Просроченные проценты (нарастающим итогом)</th>


				<th>Начало периода начисления</th>
				<th>Конец периода начисления</th>
				<th>База для начисления (Просрочен- ный основной долг)</th>
				<th>Начислен- ные проценты</th>
				<th>Погашен- ные проценты</th>
				<th>Проценты на просроченный основной долг руб. (нарастающим итогом)</th>

			</tr>

			<tr>
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>5</th>
				<th>6</th>
				<th>7</th>
				<th>8</th>
				<th>9</th>
				<th>10</th>
				<th>11</th>
				<th>12</th>
				<th>13</th>
				<th>14</th>
				<th>15</th>
				<th>16</th>
			</tr>
		</thead>
		<tbody>
			<?php
if (count($CalcHistoryData)) {

foreach ($CalcHistoryData as $r) {
?>


			<tr <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>

				<?php
					if ($r['npp']==-1)
					{
						?> <td colspan="2" style="background:#cacaca;">Итого</td>
				<?php
					}
					else
					{
						?>
				<td><?php echo $r['npp']; ?></td>
				<td><?= $r['f1_reporting_date'] ? date("d.m.Y",strtotime($r['f1_reporting_date'])) : "" ?></td>
				<?php
					}
				?>


				<td style="white-space:nowrap;  <?php if ($r['npp']==-1):?>background:#cacaca;<?php endif;?>">
					<?= $r['f2_scheduled_payment_on_schedule'] ? number_format($r['f2_scheduled_payment_on_schedule'], 2, ",", " ") : "" ?>
				</td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f3_amount_of_payments_received'] ? number_format($r['f3_amount_of_payments_received'], 2, ",", " ") : "" ?>
				</td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f4_debt_on_schedule'] ? number_format($r['f4_debt_on_schedule'], 2, ",", " ") : "" ?></td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f5_deducted_principal_debt'] ? number_format($r['f5_deducted_principal_debt'], 2, ",", " ") : "" ?>
				</td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?> title="f6_overdue_principal">
					<?= $r['f6_overdue_principal'] ? number_format($r['f6_overdue_principal'], 2, ",", " ") : "" ?></td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f7_debt_on_schedule'] ? number_format($r['f7_debt_on_schedule'], 2, ",", " ") : "" ?></td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f8_interest_exempted'] ? number_format($r['f8_interest_exempted'], 2, ",", " ") : "" ?></td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f9_overdue_interest'] ? number_format($r['f9_overdue_interest'], 2, ",", " ") : "" ?></td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f10_start_of_accrual_period'] ? date("d.m.Y",strtotime($r['f10_start_of_accrual_period'])) : "" ?>
				</td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f11_end_of_accrual_period'] ? date("d.m.Y",strtotime($r['f11_end_of_accrual_period'])) : "" ?>
				</td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f12_base_for_charging'] ? number_format($r['f12_base_for_charging'], 2, ",", " ") : "" ?>
				</td>
				<td style="white-space:nowrap; <?php if ($r['npp']==-1):?>background:#cacaca;<?php endif;?>">
					<?= $r['f13_interest_charges'] ? number_format($r['f13_interest_charges'], 2, ",", " ") : "" ?></td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f14_interest_exempted'] ? number_format($r['f14_interest_exempted'], 2, ",", " ") : "" ?>
				</td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f15_interest_on_overdue_principal_debt'] ? number_format($r['f15_interest_on_overdue_principal_debt'], 2, ",", " ") : "" ?>
				</td>
				<td style="white-space:nowrap; <?php if ($r['npp']==-1):?>background:#cacaca;<?php endif;?>">
					<?= $r['f16_total_debt'] ? number_format($r['f16_total_debt'], 2, ",", " ") : "" ?></td>
			</tr>
			<?php
}
} else {
?>
			<tr>
				<td colspan="26" style="text-align: center;">Записей не найдено.</td>
			</tr>
			<?php
}
?>
		</tbody>
	</table>



	<pagebreak orientation="L" page-break-type="slice"></pagebreak>



	<table class="table table-bordered table-condensed table_peni_calc" style="page-break-inside:avoid" id="rsd-info">
		<thead>
			<tr>
				<th rowspan="3">№</th>
				<th rowspan="2">Отчетная дата</th>
				<th colspan="6">Неустойка (руб.)</th>
			</tr>
			<tr>
				<th>Начало периода начисления</th>
				<th>Конец периода начисления</th>
				<th>База для начисления (нарастающим итогом)</th>

				<th>Начисленныая неустойка</th>
				<th>Погашенная неустойка</th>
				<th>Неустойка на просроченный основной долг руб.</th>
			</tr>

			<tr>
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>5</th>
				<th>6</th>
				<th>7</th>
			</tr>
		</thead>
		<tbody>
			<?php
if (count($CalcHistoryData)) {

foreach ($CalcHistoryData as $r) {
?>


			<tr <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
				<?php
					if ($r['npp']==-1)
					{
						?> <td colspan="2" style="background:#cacaca;">Итого</td>
				<?php
					}
					else
					{
						?>
				<td><?php echo $r['npp']; ?></td>
				<td><?= $r['f1_reporting_date'] ? date("d.m.Y",strtotime($r['f1_reporting_date'])) : "" ?></td>
				<?php
					}
				?>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f17_penalty_start_of_accrual_period'] ? date("d.m.Y",strtotime($r['f17_penalty_start_of_accrual_period'])) : "" ?>
				</td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f18_penalty_end_of_accrual_period'] ? date("d.m.Y",strtotime($r['f18_penalty_end_of_accrual_period'])) : "" ?>
				</td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f19_penalty_base_for_charging'] ? number_format($r['f19_penalty_base_for_charging'], 2, ",", " ") : "" ?>
				</td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f20_penalty_charges'] ? number_format($r['f20_penalty_charges'], 2, ",", " ") : "" ?></td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f21_penalty_exempted'] ? number_format($r['f21_penalty_exempted'], 2, ",", " ") : "" ?></td>
				<td <?php if ($r['npp']==-1):?> style="background:#cacaca;" <?php endif;?>>
					<?= $r['f22_penalty_on_overdue_principal_debt'] ? number_format($r['f22_penalty_on_overdue_principal_debt'], 2, ",", " ") : "" ?>
				</td>
			</tr>
			<?php
}
} else {
?>
			<tr>
				<td colspan="26" style="text-align: center;">Записей не найдено.</td>
			</tr>
			<?php
}
?>
		</tbody>
	</table>
</div>