<?php
    $this->title = 'Документы для суда';
?>


<main role="main" class="container">
    <h1 class="mt-5 text-center">Расчет займа для суда</h1>
    <p class="lead">Для расчета истории операций по займу или просмотра уже сгенерированных данных по займу - укажите номер займа</p>

    <?= Yii::$app->view->renderFile('@app/views/blocks/sud_form.php');?>
</main>