<?php
    $this->title = 'Документы для суда';
?>
<div class="row">
        <div class="col"></div>
        <div class="col">
            <br>
            <form method="GET">
                <button type="submit" name="action" value="history" id="btn__submit_history" onclick="return false;" class="btn btn-primary">История расчетов</button>
                <button type="submit" name="action" value="calc" id="btn__submit_calc" onclick="return false;" class="btn btn-primary">Рассчитать</button>
            </form>
        </div>
        <div class="col"></div>
</div>







<!-- Modal -->
<div class="modal fade" id="modal__calc_confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<form method="GET">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Расчет займа</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Название расчета</label>
						<input type="text" class="form-control" id="modal_input_name">
						<small id="error__name" class="sudform_error form-text text-danger"></small>
					</div>
					<div class="form-group">
						<label for="contract">Номер займа</label>
						<input type="text" class="form-control contract" id="modal_input_contract" placeholder="ZXXXXXXXXXXXX" value="">
						<small id="error__contract" class="sudform_error form-text text-danger"></small>
					</div>
					<div class="form-check">
						<label class="form-check-label" for="modal_input_involve_restruct"
							data-toggle="tooltip" data-placement="top" 
							title="Использовать в случае, если необходимо при расчете учитывать реструктуризацию">
							<input type="checkbox" class="form-check-input" id="modal_input_involve_restruct" checked="checked"> -
							Учитывать реструктуризацию
						</label>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
					<button type="button" id="btn__submit_calc_modal_sud" onclick="return false;" class="btn btn-primary">Рассчитать</button>
				</div>
			</div>
		</div>
	</form>			
</div>










<!-- Modal -->
<div class="modal fade" id="modal__calc_history" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<form method="GET">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">История расчетов</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="contract">Номер займа</label>
						<input type="text" class="form-control contract" id="modal_input_history_contract" placeholder="ZXXXXXXXXXXXX" value="">
						<small id="modal_input_history_contract__error" class="sudform_error form-text text-danger"></small>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
					<button type="button" id="btn__submit_calc_history" onclick="return false;" class="btn btn-primary">Просмотреть историю</button>
				</div>
			</div>
		</div>
	</form>			
</div>
