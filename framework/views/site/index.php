<?php

$this->title = '';

?>
<div class="page landscape">
	<div class="row">
		<div class="col-xs-4">
			<table class="table table-bordered table-condensed" id="loan-info">
				<tbody>
					<tr>
						<td>Номер договора</td>
						<td><?= $contract ?></td>
					</tr>
					<tr>
						<td>ФИО</td>
						<td><?= $LoanInfo['borrower_fio'] ?></td>
					</tr>
					<tr>
						<td>Дата выдачи</td>
						<td><?= date("d.m.Y", strtotime($LoanInfo['creation_date'])) ?></td>
					</tr>
					<tr>
						<td>Дата окончания (плановая)</td>
						<td><?= date("d.m.Y", strtotime($LoanInfo['planned_close_date'])) ?></td>
					</tr>
					<tr>
						<td>Дата окончания</td>
						<td><?= date("d.m.Y", strtotime($LoanInfo['loan_end_date'])) ?></td>
					</tr>
					<tr>
						<td>Сумма займа</td>
						<td><?= $LoanInfo['loan_sum'] ?></td>
					</tr>
					<tr>
						<td>Ставка (дневная)</td>
						<td><?= $LoanInfo['loan_rate'] ?></td>
					</tr>
					<tr>
						<td>Ставка (аннуитет)</td>
						<td><?= $LoanInfo['loan_rate_annuitet'] ?></td>
					</tr>
					<tr>
						<td>Кол-во реструктуризаций</td>
						<td><?= $LoanInfo['restructuring'] ?></td>
					</tr>
					<tr>
						<td>Заявление на досрочку</td>
						<td><?= $LoanInfo['active_pdp'] ?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-xs-8">
			<table class="table table-bordered table-condensed" id="rst-info">
				<thead>
					<tr>
						<th>&nbsp;</th>
						<th>Начисления</th>
						<th>Оплата</th>
						<th>Остаток</th>
						<th>Просрочка</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Остаток</td>
						<td><?= number_format($RepaymentSchedulesTotal['loan'], 0, ",", " ") ?></td>
						<td><?= number_format($RepaymentSchedulesTotal['paid_loan'], 0, ",", " ") ?></td>
						<td><?= number_format($RepaymentSchedulesTotal['debt_loan'], 0, ",", " ") ?></td>
						<td><?= number_format($RepaymentSchedulesTotal['delay_loan'], 0, ",", " ") ?></td>
					</tr>
					<tr>
						<td>Проценты начисленные</td>
						<td><?= number_format($RepaymentSchedulesTotal['interest'], 0, ",", " ") ?></td>
						<td><?= number_format($RepaymentSchedulesTotal['paid_interest'], 0, ",", " ") ?></td>
						<td><?= number_format($RepaymentSchedulesTotal['debt_interest'], 0, ",", " ") ?></td>
						<td><?= number_format($RepaymentSchedulesTotal['delay_interest'], 0, ",", " ") ?></td>
					</tr>
					<tr>
						<td>Проценты на просрочку</td>
						<td><?= number_format($RepaymentSchedulesTotal['overpayment_for_delay'], 0, ",", " ") ?></td>
						<td><?= number_format($RepaymentSchedulesTotal['paid_overpayment_for_delay'], 0, ",", " ") ?>
						</td>
						<td><?= number_format($RepaymentSchedulesTotal['debt_overpayment_for_delay'], 0, ",", " ") ?>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>Пени</td>
						<td><?= number_format($RepaymentSchedulesTotal['fine'], 0, ",", " ") ?></td>
						<td><?= number_format($RepaymentSchedulesTotal['paid_fine'], 0, ",", " ") ?></td>
						<td><?= number_format($RepaymentSchedulesTotal['debt_fine'], 0, ",", " ") ?></td>
						<td></td>
					</tr>
					<tr>
						<td>Дни просрочки</td>
						<td></td>
						<td></td>
						<td></td>
						<td><?= $LoanInfo['loan_dpd'] ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>



	<table class="table table-bordered table-condensed" id="rsd-info">
		<thead>
			<tr>
				<th rowspan="2"></th>
				<th colspan="2">График платежей</th>
				<th colspan="3">Начисление</th>
				<th colspan="2">Признак</th>
				<th colspan="4">Оплата</th>
				<th rowspan="2">Остаток</th>
			</tr>
			<tr>
				<th>Основной долг</th>
				<th>Проценты</th>
				<th>Проценты</th>
				<th>Проценты на просрочку</th>
				<th>Пени</th>
				<th>Просрочки</th>
				<th>Оплаты</th>
				<th>Основной долг</th>
				<th>Проценты</th>
				<th>Проценты на просрочку</th>
				<th>Пени</th>
			</tr>
		</thead>
		<tbody>
			<?php
if (count($RepaymentSchedulesDetail)) {

foreach ($RepaymentSchedulesDetail as $r) {
?>
			<tr>
				<td><?= date("d.m.Y", strtotime($r['date'])) ?></td>

				<td><?= $r['loan_sum'] ? number_format($r['loan_sum'], 2, ",", " ") : "" ?></td>
				<td><?= $r['loan_overpayment'] ? number_format($r['loan_overpayment'], 2, ",", " ") : "" ?></td>
				<td><?= $r['calc_overpayment'] ? number_format($r['calc_overpayment'], 2, ",", " ") : "" ?></td>
				<td><?= $r['calc_overpayment_for_delay'] ? number_format($r['calc_overpayment_for_delay'], 2, ",", " ") : "" ?>
				</td>
				<td><?= $r['calc_fine'] ? number_format($r['calc_fine'], 2, ",", " ") : "" ?></td>
				<td><?= $r['delay'] ? number_format($r['delay'], 0, ",", " ") : "" ?></td>
				<td><?= $r['payment_sum'] ? number_format($r['payment_sum'], 0, ",", " ") : "" ?></td>
				<td><?= $r['payment_loan'] ? number_format($r['payment_loan'], 2, ",", " ") : "" ?></td>
				<td><?= $r['payment_overpayment'] ? number_format($r['payment_overpayment'], 2, ",", " ") : "" ?></td>
				<td><?= $r['payment_overpayment_for_delay'] ? number_format($r['payment_overpayment_for_delay'], 2, ",", " ") : "" ?>
				</td>
				<td><?= $r['payment_fine'] ? number_format($r['payment_fine'], 2, ",", " ") : "" ?></td>
				<td><?= $r['debt_total'] ? number_format($r['debt_total'], 2, ",", " ") : "" ?></td>
			</tr>
			<?php
        }
    } else {        
?>
			<tr>
				<td colspan="13" style="text-align: center;">Вхождений не найдено.</td>
			</tr>
			<?php
    }
?>
		</tbody>
	</table>
</div>