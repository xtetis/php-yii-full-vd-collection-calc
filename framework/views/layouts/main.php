<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible"
              content="IE=edge">
        <meta name="viewport"
              content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link href="/theme/css/common.css"
              rel="stylesheet">
    </head>

    <body>


        <nav class="navbar navbar-expand-lg  navbar-dark bg-dark">
            <a class="navbar-brand"
               href="#">Collection</a>
            <button class="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarNav"
                    aria-controls="navbarNav"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse"
                 id="navbarNav">
                <ul class="navbar-nav">
                <!--
                    <li class="nav-item active">
                        <a class="nav-link"
                           href="/sud/">
                           Суд <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"
                           href="/bank">Банк</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link"
                           href="/bankcard">Выгрузка карточки договора микрозайма</a>
                    </li>
                    -->
                    <li class="nav-item">
                        <a class="nav-link"
                           href="/sudold">Суд (старая версия)</a>
                    </li>
                </ul>
            </div>
        </nav>


        <?php $this->beginBody() ?>

        <?= $content ?>

        <footer class="footer">
            <div class="container">
                <span class="text-muted">ООО МФК "ЦФП", 117218, г. Москва, ул. Кржижановского, д. 29, корп. 5, пом. I,
                    ком. 10. </span>
            </div>
        </footer>




        <?php $this->endBody() ?>
        <script src="/theme/js/jquery.redirect.js"></script>
        <script src="/theme/js/functions.js"></script>
        <script src="/theme/js/jquery.maskedinput.js"></script>
        <?php if ((strpos($_SERVER['REQUEST_URI'],'/bankold')!==false) || (strpos($_SERVER['REQUEST_URI'],'/sudold')!==false)): ?>
            <script src="/theme/js/common.js?v=<?=uniqid()?>"></script>
        <?php elseif((strpos($_SERVER['REQUEST_URI'],'/bankcard')!==false)): ?>
            <script src="/theme/js/bankcard.js?v=<?=uniqid()?>"></script>
        <?php else: ?>
            <script src="/theme/js/sud.js?v=<?=uniqid()?>"></script>
        <?php endif; ?>
        

        <div id="container_top_show_message"  class="alert alert-danger" role="alert"></div>
        
        <div id="progress_container">
            <div id="progress_center">
                <div class="loader1"></div>
            </div>
        </div>

    </body>

</html>
<?php $this->endPage() ?>
