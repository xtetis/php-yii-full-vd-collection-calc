<?php
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);

$this->beginPage() 
?>
<!DOCTYPE html>
<html lang="ru">
	<head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/theme/css/bootstrap.min.css" />
		
        <title><?= Html::encode($this->title) ?></title>
	</head>
	<body>
		<?php $this->beginBody() ?>
			<?= $content ?>
		<?php $this->endBody() ?>
	</body>
</html>
<?php $this->endPage() ?>