<?php
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);

$this->beginPage() 
?>
<!DOCTYPE html>
<html lang="ru">
	<head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/theme/css/bootstrap.min.css" />
        <title><?= Html::encode($this->title) ?></title>
		<style type="text/css">
            body {
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0;
                background-color: #FAFAFA;
                font: 12pt "Tahoma";
            }
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .page {
                width: 210mm;
                min-height: 297mm;
                padding: 20mm;
                margin: 10mm auto;
                border: 1px #D3D3D3 solid;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }

            .page.landscape {
                width: 297mm;
                min-height: 210mm;
            }

            @media print {
                html, body {
                    width: 210mm;
                    height: 297mm;
                }
                .page {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                }
            }

            #loan-info tbody tr td:first-child {
                width: 1px;
                white-space: nowrap;
                font-weight: bold;
            }

            #rst-info thead tr th,
            #rst-info tbody tr td {
                width: 15%;
                text-align: right;
            }

            #rst-info tbody tr td:first-child {
                text-align: left;
            }

            #rst-info thead tr th:first-child {
                width: 40%;
            }

            #rst-info tbody tr td:first-child {
                font-weight: bold;
            }

            #rsd-info tbody tr td:first-child {
                white-space: nowrap;
            }

            table * {
                font-size: smaller;
            }

            #rsd-info thead tr:first-child th {
                text-align: center;
            }

            #rsd-info thead tr:nth-child(2) th,
            #rsd-info tbody tr td {
                text-align: right;
            }
        </style>
	</head>
	<body>
		<?php $this->beginBody() ?>
			<?= $content ?>
		<?php $this->endBody() ?>
	</body>
</html>
<?php $this->endPage() ?>