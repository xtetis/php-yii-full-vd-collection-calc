<?php

namespace app\models;

use Yii;
use yii\base\Model;


/**
 * Модель для получения данныз из АСУЗа
 */
class SudAsuzModel extends Model
{
    
    /**
     * Получаем данные о займе из АСУЗа
     */
    public static function getLoanInfo($contract)
    {

        $connection = Yii::$app->db_asuz;
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    collection.get_loan_info(:contract)  
            ", 
            [
                ':contract' => $contract
            ]
            );
        
        return $command->queryOne();
    }



    /**
     * Получаем из АСУЗа общие данные по графику платежей
     */
    public static function getRepaymentSchedulesTotal($contract)
    {

        $connection = Yii::$app->db_asuz;
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    collection.get_repayment_schedules_total(:contract)  
            ", 
            [
                ':contract' => $contract
            ]
            );
        
        return $command->queryOne();
    }



    /**
     * Получаем из АСУЗа подробные данные по графику платежей
     */
    public static function getRepaymentSchedulesDetail($contract)
    {

        $connection = Yii::$app->db_asuz;
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    collection.get_repayment_schedules_detail(:contract)  
            ", 
            [
                ':contract' => $contract
            ]
            );
        
        return $command->queryAll();
    }
    
}
