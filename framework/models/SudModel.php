<?php

namespace app\models;

use Yii;
use yii\base\Model;


/**
 * Модель для методов в базе
 */
class SudModel extends Model
{
    
    /**
     * Проверяем в базе калидность номера контракта займа
     */
    public static function checkCorrectContractNumber($contract)
    {

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    loan_calc_history.check_correct_contract_number(:contract)  
            ", 
            [
                ':contract' => $contract
            ]
            );
        
        return $command->queryOne();
    }
    

    /**
     * Получаем из базы историю расчетов по займу
     */
    public static function getLoanCalcHistory($contract)
    {

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    loan_calc_history.get_loan_calc_history(:contract)  
            ", 
            [
                ':contract' => $contract
            ]
            );
        
        return $command->queryAll();
    }
    
    
    /**
     * Генерируем последовательность
     */
    public static function createSequence($loan_key)
    {
        $sequence_id = uniqid('sequence');

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    loan_calc_history.create_calc_sequence(:loan_key, :sequence_id)  
            ", 
            [
                ':loan_key'     => $loan_key,
                ':sequence_id'  => $sequence_id,
            ]
            );
        
        return $command->queryOne();
    }
    
    
    /**
     * Генерируем последовательность
     */
    public static function getSequenceValue($sequence)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    last_value 
                FROM 
                    ".preg_replace("/[^A-Za-z0-9?!]/",'',$sequence)."
            ", 
            []
            );
        
        return $command->queryOne();
    }
    
    
    /**
     * Удаляем последовательность
     */
    public static function dropSequence($sequence)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                loan_calc_history.drop_calc_sequence(:sequence)  
            ", 
            [
                ':sequence'     => $sequence,
            ]
            );
        
        return $command->queryOne();
    }
    
    
    /**
     * Запускаем расчет
     */
    public static function startCalc(
            $loan_key, 
            $name, 
            $nopeni, 
            $sequence, 
            $first_payment_schedule
        )
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                loan_calc_history.start_us_calc(
                        :loan_key,
                        :name,
                        :nopeni,
                        :sequence,
                        :first_payment_schedule
                    ) 
            ", 
            [
                ':loan_key'                 => $loan_key,
                ':name'                     => $name,
                ':nopeni'                   => $nopeni,
                ':sequence'                 => $sequence,
                ':first_payment_schedule'   => $first_payment_schedule,
            ]
            );
        
        return $command->queryOne();
    }


    
    
    /**
     * Получаем информацию по займу для рассчета по суду
     */
    public static function getLoanInfo($lvkey)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    facts.get_loan_info(
                        :lvkey
                    ) 
            ", 
            [
                ':lvkey'     => $lvkey,
            ]
            );
        
        return $command->queryOne();
    }    
    

    /**
     * Получаем табличные даанные по балансам по займу
     */
    public static function getCalcHistoryData($lvkey)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    loan_calc_history.get_calc_history_data(
                        :lvkey
                    ) 
            ", 
            [
                ':lvkey'     => $lvkey,
            ]
            );
        
        return $command->queryAll();
    } 
    

    /**
     * Получаем задолженности по займу для рассчета по суду
     */
    public static function getCalcStateDuty($lvkey)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    loan_calc_history.get_calc_state_duty(
                        :lvkey
                    ) 
            ", 
            [
                ':lvkey'     => $lvkey,
            ]
            );
        
        return $command->queryOne();
    }

   
    /**
     * Получаем список акций, в которых участвует займ
     */
    public static function getLoanSpecialOffers($lvkey)
    {
        $connection = Yii::$app->getDb();
        
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    loan_calc_history.get_loan_special_offers(
                        :lvkey
                    ) 
            ", 
            [
                ':lvkey'     => $lvkey,
            ]
            );
        
        return $command->queryAll();
    } 
    

    



    
    
    /**
     * Получаем информацию по займу для рассчета БАНК
     */
    public static function getLoanInfoBank($lvkey)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    facts.get_loan_info_br(
                        :lvkey
                    ) 
            ", 
            [
                ':lvkey'     => $lvkey,
            ]
            );
        
        return $command->queryOne();
    }    
    

    /**
     * Получаем табличные даанные по балансам по займу БАНК
     */
    public static function getCalcHistoryDataBank($lvkey)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    loan_calc_history.get_calc_history_data_bank(
                        :lvkey
                    ) 
            ", 
            [
                ':lvkey'     => $lvkey,
            ]
            );
        
        return $command->queryAll();
    } 
    

    /**
     * Получаем задолженности по займу для рассчета БАНК
     */
    public static function getCalcStateDutyBank($lvkey)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    loan_calc_history.get_calc_state_duty_br(
                        :lvkey
                    ) 
            ", 
            [
                ':lvkey'     => $lvkey,
            ]
            );
        
        return $command->queryOne();
    }
    

    /**
     * Получаем список балансовых величин для указанного расчета
     */
    public static function getBalanceValueHistory($lvkey)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    loan_calc_history.get_balance_value_history(
                        :lvkey
                    ) 
            ", 
            [
                ':lvkey'     => $lvkey,
            ]
            );
        
        return $command->queryAll();
    }
    

    /**
     * Получаем список транзакций для указанного расчета
     */
    public static function getLoanTransactHistory($lvkey)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                    loan_calc_history.get_loan_transact_history(
                        :lvkey
                    ) 
            ", 
            [
                ':lvkey'     => $lvkey,
            ]
            );
        
        return $command->queryAll();
    }
    

    /**
     * Получаем список фактов для указанного займа, те что из боевой таблицы АСУЗА
     */
    public static function getAsuzLoanFacts($loan_key)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                facts.get_asuz_loan_facts(
                        :loan_key
                    ) 
            ", 
            [
                ':loan_key'     => $loan_key,
            ]
            );
        
        return $command->queryAll();
    }
    

    /**
     * Получаем список фактов для указанного займа, те что из сгенерированной таблицы
     * Используется для займов до 01_09_2017. В случае если дата создания займа позже чем
     * 01_09_2017 - факты переносятся из таблицы, что в АСУЗе
     */
    public static function getLoanFacts($loan_key)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                facts.get_loan_facts(
                        :loan_key
                    ) 
            ", 
            [
                ':loan_key'     => $loan_key,
            ]
            );
        
        return $command->queryAll();
    }
    

    /**
     * Получение первоначального графика платежей
     */
    public static function getTmpPaymentSchedules($loan_key)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                loan_calc_history.get_tmp_payment_schedules(
                        :loan_key
                    ) 
            ", 
            [
                ':loan_key'     => $loan_key,
            ]
            );
        return $command->queryAll();
    }

    

    /**
     * Получение все данные по всем графикам платежей для указанного займа
     */
    public static function getRepaymentSchedules($loan_key)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                loan_calc_history.get_repayment_schedules(
                        :loan_key
                    ) 
            ", 
            [
                ':loan_key'     => $loan_key,
            ]
            );
        
        return $command->queryAll();
    }



    

    /**
     * Получение список графиков платежей по указанному займу
     */
    public static function getPermissibleLoans($loan_key)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    * 
                FROM 
                loan_calc_history.get_permissible_loans(
                        :loan_key
                    ) 
            ", 
            [
                ':loan_key'     => $loan_key,
            ]
            );
        
        return $command->queryAll();
    }






    /**
     * Получаем список оплат из таблиц, что  АСУЗе (те которые перенесены)
     */
    public static function getPaymentsAsuz($loan_key)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
            "
                SELECT 
                    *
                FROM 
                    asuz_db.payment p
                WHERE 
                    p.loan_key = :loan_key
            ", 
            [
                ':loan_key'     => $loan_key,
            ]
            );
        
        return $command->queryAll();
    }
    
}
